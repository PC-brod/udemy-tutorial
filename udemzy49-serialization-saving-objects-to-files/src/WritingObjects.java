import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class WritingObjects {
    public static void main(String[] args) {
        System.out.println("Writing objects ... ");

        Person person = new Person(65, "Ron");
        Person person1 = new Person(654, "John");

        System.out.println(person);
        System.out.println(person1);

        // will automatically call close method
        try(FileOutputStream fs = new FileOutputStream("people.bin")) {

            ObjectOutputStream os = new ObjectOutputStream(fs);

            os.writeObject(person);
            os.writeObject(person1);

            os.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
