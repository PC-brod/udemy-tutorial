
public class App {

	public static void main(String[] args) {
		
		// Inefficient example
		String info = "";
		
		info += "My name is Bob.";
		info += " ";
		info += "I am a builder.";
		
		System.out.println(info);
		
		// Example with string builder
		StringBuilder sb = new StringBuilder("");
		
		sb.append("My name is Sue.");
		sb.append(" ");
		sb.append("I am a lion tamer.");
		
		System.out.println(sb.toString());
		
		StringBuilder sw = new StringBuilder();
		
		sw.append("My name is Luke.")
			.append(" ")
			.append("I am a sky walker.");
		
		System.out.println(sw.toString());
		
		// string Buffer for multi-threading
		System.out.print("Some text.\tThat makes a tab.\nAnd this was for new line.");
		System.out.println(" More text.");
		System.out.printf("Total cost %-10d; quantity is %d\n", 5, 120); // %d for distance %d-10 for distance 10 from behind
		
		for(int i=0; i<20; i++) {
			System.out.printf("%2d: some text here\n", i); // %2d for two dimensional number
		}

		for(int i=0; i<20; i++) {
			System.out.printf("%-2d: %s\n", i, "some text"); // %-2d will shift them to left
		}
		
		System.out.printf("Total number: %.3f\n", 5.6); // %.3f for floating point with 3 decimal places
		System.out.printf("Total number: %.3f\n", 2.336955258455); // system will automatically round it
		System.out.printf("Total number: %10.3f\n", 1256.336955258455); // 10 for 10 characters of number
		System.out.printf("Total number: %10.3f\n", 123456.299999);
	}

}
