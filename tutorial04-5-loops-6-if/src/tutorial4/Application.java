package tutorial4;

// formating Control + Shift + f
public class Application {
	public static void main(String[] args) {
		boolean loop = true;

		System.out.println(loop); // true

		loop = 4 < 3;

		System.out.println(loop); // false

		int iterator = 0;

		while (iterator < 10) {
			System.out.println("Iterator is: " + iterator);

			iterator = iterator + 1;

			iterator++;

			iterator += 2;
		}

		for (int i = 0; i < 5; i++) {
			System.out.printf("The value of i is: %d ", i);
		}

		for (int i = 0; i < 5; i++) {
			System.out.printf("The value of i is: %d\n", i);
		}

		boolean cond = 4 >= 3;
		System.out.println(cond);

		cond = 4 >= 4;
		System.out.println(cond);

		cond = 4 == 4;
		System.out.println(cond);

		cond = 4 != 5;
		System.out.println(cond);

		if (1 == 1) {
			System.out.println("True!");
		}

		if (5 != 5) {
			System.out.println("False!");
		}

		int myInt = 25;

		if (myInt < 10) {
			System.out.println("My int is < 10");
		} else if (myInt < 20) {
			System.out.println("My int is > 20");
		} else {
			System.out.println("Non of them");
		}

		int ia = 0;

		while (true) {
			if (ia == 10) {
				break;
			}
			ia++;
			System.out.println("While with if break " + ia);
		}

		int howMany = 10;

		int multiplicator = 5;

		int[] values;
		values = new int[howMany];

		for (int i = 0; i < howMany; i++) {
			values[i] = (i + 1) * multiplicator;
			System.out.println(values[i]);
		}

		System.out.println(values[2]);

	}
}
