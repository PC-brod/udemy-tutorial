public interface StringExecutable {
    String execute(String s);
}
