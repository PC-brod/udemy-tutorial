public interface IntExecutable {
    int execute(int i);
}
