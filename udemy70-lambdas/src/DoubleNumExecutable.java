public interface DoubleNumExecutable {
    float executeAB(float a, float b);
}
