
public class App {
    public static void main(String[] args) {
        Runner runner = new Runner();
        runner.run(new Executable() {
            @Override
            public int execute() {
                System.out.println("execute() method !!!");
                return 21;
            }
        });

        runner.run(() -> 55);

        runner.run(() -> {
            System.out.println("First command");
            System.out.println("some more stuff");
            return 10;
        });

        System.out.println("/////////////////////////////////////////");

        DefaultRunner defaultRunner = new DefaultRunner();
        defaultRunner.iRun(new IntExecutable() {
            @Override
            public int execute(int i) {
                System.out.println("Gonna add 5");
                return 5 + i;
            }

        });

        defaultRunner.iRun((int i) -> 25 + i);

        defaultRunner.iRun((int i) -> {
            System.out.println("Some more stuff...");
            System.out.println("Gonna add 15");
            return 15 + i;
        });

        System.out.println("/////////////////////////////////////////");

        defaultRunner.sRun(new StringExecutable() {
            @Override
            public String execute(String s) {
                System.out.println("Lets say something");
                return "Something new " + s;
            }
        });

        defaultRunner.sRun((String s) -> "Hoja hoj! " + s);

        defaultRunner.sRun((String s) -> {
            System.out.println("Extra hoj hoj.");
            return "Be happy " + s;
        });

        System.out.println("/////////////////////////////////////////");

        defaultRunner.doubleIntRun(new DoubleNumExecutable() {
            @Override
            public float executeAB(float a, float b) {
                System.out.println("Lets multiple a * b");
                return a * b;
            }
        });

        defaultRunner.doubleIntRun((a, b) ->  a + b + 10);

        defaultRunner.doubleIntRun((a, b) -> {
            System.out.println("Lets count (a * b) / 10");
            return (a * b) / 10;
        });
    }
}
// 11:26
