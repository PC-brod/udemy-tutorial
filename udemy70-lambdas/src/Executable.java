public interface Executable {
    int execute();
}
/*
This is functional interface because it has only one method

Others are
comparable interface
(https://www.javatpoint.com/Comparable-interface-in-collection-framework),
runnable interface
(https://www.javatpoint.com/runnable-interface-in-java)
 */
