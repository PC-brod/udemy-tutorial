public class DefaultRunner {
    public void iRun(IntExecutable e) {
        System.out.println("===================");
        System.out.println("Executing with default value 55 ...");
        int value = e.execute(55);
        System.out.println("The value is: " + value);
    }

    public void sRun(StringExecutable e) {
        System.out.println("===================");
        System.out.println("Executing with default string ...");
        String value = e.execute("Default string");
        System.out.println("The value is: " + value);
    }

    public void doubleIntRun(DoubleNumExecutable e) {
        System.out.println("===================");
        System.out.println("Executing with 2 default values ...");
        float value = e.executeAB(55, 5);
        System.out.println("The value is: " + value);
    }
}
