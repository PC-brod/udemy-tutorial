public class Runner {
    public void run(Executable e) {
        System.out.println("===================");
        System.out.println("Executing ...");
        int value = e.execute();
        System.out.println("The value is: " + value);
    }
}
