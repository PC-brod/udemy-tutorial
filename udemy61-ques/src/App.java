import java.util.NoSuchElementException;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

public class App {
    public static void main(String[] args) {
        // (head) <- ooooooooooooooooooooooooo <- (tail) FIFO (first in, first out)

        Integer queueSize = 65; // capacity

        Queue<Integer> queue = new ArrayBlockingQueue<Integer>(queueSize);

        for (Integer i = 0; i < queueSize; i++) {
            Integer num = (int) (Math.random() * 10);
            try {
                queue.add(num);
            } catch (IllegalStateException e) {
                System.out.println("Tried to add too many times to queue.");
            }
        }

        System.out.println("head: " + queue.element());

        System.out.println(queue);
/*
        for (Integer value: queue) {
            System.out.println(value);
        }
 */
        Integer value;
        value = queue.remove();
        System.out.println(value);

        System.out.println(queue);

        for (Integer i = 0; i < queueSize; i++) {
            try {
                Integer v;
                v = queue.remove();
                System.out.println(v);
                System.out.println(queue);
            } catch (NoSuchElementException e) {
                System.out.println("Tried to remove too many times from queue.\n\n");
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////

        queueSize = 55; // capacity

        Queue<Integer> queue2 = new ArrayBlockingQueue<Integer>(queueSize);

        System.out.println("Peek: " + queue2.peek());

        for (Integer i = 0; i < queueSize + 2; i++) {
            Integer num = (int) (Math.random() * 10);
            if (queue2.offer(num) == false) {
                System.out.println("Offered: " + num + " Offer failed. Capacity of queue: " + queueSize + "Offer number: " + i);
            }
        }

        System.out.println("Peek: " + queue2.peek());

        System.out.println("Head: " + queue2.element());

        System.out.println(queue2);

        System.out.println("Queue2 poll: " + queue2.poll());

        System.out.println(queue2);

        for (Integer i = 0; i < queueSize; i++) {
            System.out.println("Queue2 poll: " + queue2.poll());
            System.out.println(queue2);

        }
    }
}
