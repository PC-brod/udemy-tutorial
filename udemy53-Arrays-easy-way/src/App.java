import java.util.ArrayList;
import java.util.List;

public class App {
    public static void main(String[] args) {

        int arraySize = 5;

        ArrayList<Integer> numbers = new ArrayList<Integer>(arraySize);

        int i = 0;

        // Adding
        while (i < arraySize){
            numbers.add((int) (Math.random() * 10));
            i++;
        }

        System.out.println(numbers);

        // Retrieving
        System.out.println(numbers.get(0));

        // Indexed for loop iteration
        System.out.println("\nIteration #1");
        for (int index=0; index < numbers.size(); index++) {
            System.out.println(numbers.get(index));
        }

        // Removing items (careful! - system is making copy of current array)
        numbers.remove(numbers.size()-1);

        // This gonna be slow
        numbers.remove(0);

        System.out.println("\nIteration #2");
        for (Integer value: numbers) {
            System.out.println(value);
        }

        // List interface ...
        List<String> values = new ArrayList<String>();
        System.out.println(values);

    }
}
