import java.util.Objects;

class Person {
   private  int id;
   private  String name;

    public Person(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return id == person.id &&
                Objects.equals(name, person.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}


public class App {
    public static void main(String[] args) {
        // Person person1 = new Person(5, "Bob");
        // Person person2 = new Person(7, "Ron");

        // person2 = person1;

        Person person1 = new Person(5, "Bob");
        Person person2 = new Person(5, "Bob");

        System.out.println(person1 == person2);
        System.out.println(person1.equals(person2));
        person2.setName("John");
        System.out.println(person1.equals(person2));

        Double value1 = 7.2;
        Double value2 = 7.2;
        System.out.println(value1 == value2); //false
        System.out.println(value1.equals(value2)); //true

        Integer num1 = 6;
        Integer num2 = 6;
        System.out.println(num1 == num2); //true

        String text1 = "Hello";
        String text2 = "Hello";
        System.out.println(text1 == text2); //true

        String text3 = "Hello";
        String text4 = "Hellos;dgjkd;lgj".substring(0, 5);
        System.out.println(text3 == text4); //false
        System.out.println(text3.equals(text4)); //true

    }
}
