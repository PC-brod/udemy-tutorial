class Temp implements AutoCloseable {

    @Override
    public void close() throws Exception {
        System.out.println("Closing...");
        throw new Exception("Some exception.");
    }
}

public class App {
    public static void main(String[] args) {

        Temp temp = new Temp();
        try {
            temp.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Try with resources
        try (Temp temp1 = new Temp()) {
            // Automatically implements close() method
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
