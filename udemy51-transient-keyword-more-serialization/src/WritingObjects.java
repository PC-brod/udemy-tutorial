import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;

public class WritingObjects {
    public static void main(String[] args) {
        System.out.println("Writing objects ... ");

        // will automatically call close method
        try (
                FileOutputStream fs = new FileOutputStream("test.ser")
                ; ObjectOutputStream os = new ObjectOutputStream(fs)
        ) {

            Person person = new Person(7, "Bob");
            Person.setRank(5);
            os.writeObject(person);

            Person person1 = new Person(7, "Bob");
            Person.setRank(25);
            os.writeObject(person1);

            Person person2 = new Person();
            Person.setRank(15);
            os.writeObject(person2);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
