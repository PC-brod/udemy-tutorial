import java.io.Serializable;

public class Person implements Serializable {

    private static final long serialVersionUID = 6654386543216513135L;

    private transient int id; // it will get default value
    private String name;
    private static int rank;

    public static int getRank() {
        return rank;
    }

    public static void setRank(int rank) {
        Person.rank = rank;
    }

    public Person() {
        System.out.println("default constructor");
    }

    public Person(int id, String name) {
        this.id = id;
        this.name = name;

        System.out.println("2-args constructor");
    }

    @Override
    public String toString() {
        return "Person [" +
                "id=" + id +
                ", name=" + name +
                ", rank=" + rank +
                ']';
    }
}
