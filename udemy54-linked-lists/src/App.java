import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class App {
    public static void main(String[] args) {
        /**
         * ArrayLists manage arrays internally.
         * [0][1][2][3] ...
         * if we want to get value on specific position, it is easy for Java to calculate the position in memory
         * and return value.
         *
         * If size of array is not specified, than default size will be 10.
         * And if you want to add 11th element in to it, Java will create new array with size 20,
         * than Java copy old items in to new array and after that it put your new item in to it.
         *
         * Adding in to middle or beginning:
         * Java moves all following items up (right) one unit and that add`s new item.
         */
        List<Integer> arrayList = new ArrayList<Integer>(); // If we want to add/remove items at the end

        /**
         * LinkedLists consists of elements of where each element
         * has a reference to the previous and next element.
         * [0]<->[1]<->[2]<->[3] ...
         * So to get value of specific position means to get first item, than link to next element, than the text element...
         * -it is a big number of small steps.
         * So adding to beginning or in to middle is relative fast.
         */
        List<Integer> linkedList = new LinkedList<Integer>(); // If we want to add/remove items anywhere in the list

        doTimings("ArrayList", arrayList);
        doTimings("LinkedList", linkedList);
    }

    private static void doTimings(String type, List<Integer> list) {

        // 1E4   1 times 10 exponential 4 makes 10 000 times
        for (int i = 0; i < 1E4; i++) {
            list.add(i);
        }

        System.out.println("\n\nDo timings method.\nType: " + type + "\nList: " + list + "\n");

        // Add items at the and and show time
        long start = System.currentTimeMillis();

        for (int i = 0; i < 5E5; i++) {
            list.add(i);
        }

        long end = System.currentTimeMillis();

        System.out.println("Time taken to add at the end: " + (end - start) + " ms for " + type);

        // Add items at the beginning of list and show time
        long start2 = System.currentTimeMillis();

        for (int i = 0; i < 5E5; i++) {
            list.add(0,i);
        }

        long end2 = System.currentTimeMillis();

        System.out.println("Time taken to add at beginning: " + (end2 - start2) + " ms for " + type);

        // Add items elsewhere in list and show time
        long start3 = System.currentTimeMillis();

        for (int i = 0; i < 5E5; i++) {
            list.add(list.size() - 1000,i);
        }

        long end3 = System.currentTimeMillis();

        System.out.println("Time taken to add close to end: " + (end3 - start3) + " ms for " + type);
    }
}
