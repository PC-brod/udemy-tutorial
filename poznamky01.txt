wifi Berescak iterator

Java SE
- definuje jazyk, sitaxi, JVN, jak má JVN nakládat s třídami

Compilovaný program - nejjednodušší
- pro jakou architekturu
- s jakými parametry
- pro infrastrukturu systému

1) do bit code
2) JVN dostane absolutně univerzální bit code
-zprocesuje ho

JDK - javac - comiler, obsahuje decompilery a další nástroje, knihovny Math, String

Compile time - kontroluje program

Poslat tutorál - vrací hash example

OpenJDK - komunitní, zaštiťuje JCP -> zpracovává JSR - Java Specification Request
OracleJava - největší standart
OpenJava - bez licence, linuxáci, nemusí toho obsahovat tolik, jako OracleJava
JavaMobileEdition - neaktuální
čipové karty mohu obsahovat javu

Java EE
reakce na ASP a PHP
nerozsiruje jazyk
specifikace API -> přenositelnost
JPA - Java persistance API - přenositelnost

Implementace na AS-Aplikační Server
skladba url adresy http://www.bcos.cz:8080/xyz.php?a=hjkl

http head

telnet

GET / HTTP/1.1

HTTP methods

TCP / IP spojení

keep alive na 30 sec

how APACHE server works

SNI protocol - zapisuje do nešifrované části hlavičky

vývojový cyklus
-spring framework - projekt jedné firmy, zlepšuje vývojový cyklus

Delta Spike


AS - aplikační server
J Boss
WebLigic
GlassFish

StormTale

Apache Tomcat - není EE, jen ty nejvyužívanější

jednotlivé knihovy jsou specifikace JavaEE technologí,
při běhu do něj vkládám svoji app, nebo její část, nebo více app

Java EE technologies
Serverlet - ne
JSP - základní šab. sys - ne
JSF 2 - prime faces... možná

CDI - context dependecy injection
EJB 3.1 - bussines logika
JTA - budeme používat, bez zásahu, znát princip
JPA 2 - java persistance API - hodně
Bean Valiadation - ano - kontroluje API komunikaci

JAX-WS - web services soap services, budeme volat ext. služby jako klient - ano
JAX-RS - rest API - ano
JavaMail - možná
JNDI- Java Name and Directory Interface - správa vzájemnného prostoru - minimálně

KONTEJNER
spustím app > spustí se kontejner - sdílené prostředí, sdílená paměť, sdílené třídy

Tier - schema

Servlets - nebudeme je psát, budeme je používat
app vrstva

servlet - na něm stojí:
-JSF - 1 servlet, tak zaregistruje na všechno a volá svoje metody
-JSP - vytvoří se x šablom a příslušná šablona se vezme a zkompiluje se a dynamicky se zaregistruje jako servlet
-REST JAX WS

-je vytvářený webovým kontejnerem, základ webového kontejneru
může dynamicky vytvářet obsah
reakce na cdi scripts
request - spojení - proces odpovědi

LOMBOK !!!! projekt lombok

ServiceLokator - antipatern, neměl by se používat
-rozhoduje přidělování služeb na serveru v dat. vrstvě
-v běžící aplikaci může být jenom jeden
singlton návrhový vzor, app scope

public static ServiceLokator nazevLokatoru()

WebConext, ServletContext
-správa http protocolu - sesions, cookies

http - bezvztahový protokol

aspektově orientované programování - aspekt přihlášení

viditelnost tříd - private, public, static/nonstatic, inheritance



EJB 3 - Enterprise Java Beans

objekt - nejmenší jednotka aplikace
komponenta - žije v kontejneru, něco se o ni stará
		-dá se konfigurovat
		-obsahuje bussiness pravidla
nemíchat app-logiku a bussines logiku

konvence:
-vešechny proměnné maji getr setter
-nikdy ne publick
-měla by bý seralizable

hlavní tech. Java EE, nepovinná ale doporučená

-komponenty poskytují jako blackboxy služby a infrastrukturu applikační vrstvě
poskytují:
-bussines logiku
-přístup do DB
-integraci s ostatními systémy

EJB vs Spring
ejb - configuration by exception
spring - configurate all


EJB kontajner
-potřebuje běhové prostředí, nikdy nevytvářet přes new
-knihovna v app serveru a vytvoří se při spuštění serveru
-stará se o komponentu - postconstruct, predestroy
	-bezpečnost
-transakcnost a persistance

messaging - ne
clustering - ne

vláknová bezpečnost - důležitá


PROXY
-návrhový vzor
-interface -> implementace
-v app...


Vývojový model EJB
deklarativní kofigurace
-xml, nebo moderně Java anotace

-config by exception
	-def. konfig. programator to překrývá
-skalovatelnost - pooling zdroji
-persistance a transakčnost - přímé czužívání JPA a JTA
-zvonupoužitelnost

-dependency injection
-interceptors
-POJO model - clasa ze které se stane Servlet


Vývojářské Role
...


OBJEKTY
session beans
-singleton bean
-stateless bean
-statefull bean

message driven - ne
...

session beans
-implementují bussines logiku
-implementace metod živ. cyklu

dependeci injection
-kontejner validne vytvoří a injectne třídu


Singleton session bean
-jedna jediná v app
-hlídá bezpečnou vláknovitost
	-dá se vypnout
@Singleton
defaultne lazy(@Startup)
závislost @DependsOn

@ConcurrencyManagement(BEAN)
do 2 metod můžou 2 vlákna

Statelss session beans
-bezstavová
-pooluje se - vytvoří xy tříd do poolu, připraví je a pak jsou jen volány
-dělá to automaticky zkrze to že chci danou servisu
-nedrží žádný stav, žádnou informaci
-při novém requestu dostanu novou instanci

arrays, set, list, map