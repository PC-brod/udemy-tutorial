class Robot {
	public void speak(String greet) {
		if (greet != null) {
			System.out.println(greet);
		} else {
			System.out.println("Hello");	
		}
	}
	
	public void jump(int height) {
		System.out.println("Jumping " + height);
	}
	
	public void move(String direction, double distance) {
		System.out.println("Mooving " + distance + " meters in direction " + direction);
	}
}

public class App {

	public static void main(String[] args) {
		Robot joe = new Robot();
		
		joe.speak("Hi");
		joe.speak(null);
		
		joe.jump(7);
		
		joe.move("top-left", 15.5);
		
		String warmGreeting = "Hello everybody! It's amazing to see you!";
		joe.speak(warmGreeting);
		
		int alt = 550;
		joe.jump(alt);
	}

}
