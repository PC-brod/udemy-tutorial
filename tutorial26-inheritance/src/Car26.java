// car is child of machine, car will inherit from machine
public class Car26 extends Machine26 {
	// created with eclipse functionality by following:
	// right click, source > Override/Implement method, click to select method and
	// click ok

	@Override
	public void start() {
		System.out.println("Car started.");
	}

	public void rollDownDriversWindow() {
		System.out.println("Rolling down window on driver side.");
	}
	/*
	public void showInfo() {
		System.out.println("Info " + intro); //The field Machine26.intro is not visible
	}
	*/
	
	// but with protected name it is possible
	public void showName() {
		System.out.println("Name is: " + name);
	}
}
