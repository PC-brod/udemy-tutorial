
public class App26inheritance {

	public static void main(String[] args) {
		Machine26 mach1 = new Machine26();
		mach1.start();
		mach1.stop();
		
		Car26 car1 = new Car26();
		car1.start();
		car1.getIntro();
		car1.showName();
		car1.rollDownDriversWindow();
		car1.stop();
	}

}
