
public class Machine26 {

	protected String name = "Machine26";	// Protected means, that it is accessible from within the package, and also
											// from any child class
	private String intro = "Machine type 1.";

	public void start() {
		System.out.println("Machine started.");
	}

	public void getIntro() {
		System.out.println(intro);
	}

	public void stop() {
		System.out.println("Machine stoped.");
	}
}
