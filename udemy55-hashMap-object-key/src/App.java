import java.util.HashMap;
import java.util.Map;

public class App {
    public static void main(String[] args) {
        HashMap<Integer, String> map = new HashMap<Integer, String>();
        map.put(5, "five");
        map.put(1, "one");
        map.put(4, "four");
        map.put(8, "eight");
        map.put(3, "three");
        map.put(6, "six");
        map.put(6, "seven ha ha"); // by making a duplication we overwrite old key

        String text = map.get(6);
        System.out.println(text);

        for (Map.Entry<Integer, String> entry : map.entrySet()) {
            int key = entry.getKey();
            String value = entry.getValue();

            System.out.println("Key: " + key + "\nValue: " + value + "\n");
        }
    }
}
