public class App {
    public static void main(String[] args) {
        System.out.println("Hello");

        int val = 4;
        calculate(val);

        // Exception in thread "main" java.lang.StackOverflowError
        // Stack is memory used for local variables and for
        // remembering which method called which method (function calls)
        // Heap - objects are allocated in there
        // met(0);

        // factorial of number 4 is 4! = 4*3*2*1
        System.out.println(factorial(4));

        // Tower of Hanoi
    }

    private static int factorial(int val) {
        System.out.println(val);

        if (val == 1) {
            return 1;
        }

        return factorial(val - 1) * val;
    }

    private static void calculate(int val) {
        System.out.println("Calculation ... ("+val+" - 3) * 2");

        val = (val - 3) * 2;

        System.out.println("gives ... " + val);

        if (val > - 10){
            calculate(val);
        }
    }

    private static void met(int v) {
        v--;
        System.out.println(v);
        met(v);
    }
}
