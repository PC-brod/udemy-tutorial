import java.util.*;

public class App {
    public static void main(String[] args) {

        Person p1 = new Person(0, "Bob");
        Person p2 = new Person(1, "Ron");
        Person p3 = new Person(2, "Joe");
        Person p4 = new Person(3, "Pat");
        Person p5 = new Person(0, "Bob");

        Map<String, Integer> map = new HashMap<String, Integer>();

        map.put("a", 1);
        map.put("b", 2);
        map.put("c", 3);
        map.put("d", 1);

        for (String key : map.keySet()) {
            System.out.println(key + ": " + map.get(key));
        }

        Set<String> set = new LinkedHashSet<String>();

        set.add("lorem");
        set.add("ipsum");

        System.out.println(set);

        System.out.println("+++++++++++++++++++++++++++++++++++");

        Map<Person, Integer> personIntegerMap = new LinkedHashMap<Person, Integer>();
        personIntegerMap.put(p1, 6541);
        personIntegerMap.put(p2, 2654);
        personIntegerMap.put(p3, 3514);
        personIntegerMap.put(p4, 4465);
        personIntegerMap.put(p5, 5846);

        for (Person person: personIntegerMap.keySet()){
            System.out.println(person + ": " + personIntegerMap.get(person));
        }


        Set<Person> personSet = new LinkedHashSet<Person>();
        personSet.add(p1);
        personSet.add(p2);
        personSet.add(p3);
        personSet.add(p4);
        personSet.add(p5);

        System.out.println(personSet);
    }
}
