import java.util.*;

public class App {

    public static String[] vehicles = {
            "ambulance", "helicopter", "lifeboat"
    };

    public static String[][] drivers = {
            {"Fred", "Sue", "Pete"},
            {"Sue", "Richard", "Bob", "Fred"},
            {"Pete", "Mary", "Bob"}
    };

    public static void main(String[] args) {

        Map<String, Set<String>> personnel = new HashMap<>();

        for (int i = 0; i < vehicles.length; i++) {
            String vehicle = vehicles[i];
            String[] driversList = drivers[i];

            Set<String> driverSet = new LinkedHashSet<>(Arrays.asList(driversList));
            // System.out.println(driverSet);

            /*
            Set<String> driverSet = new LinkedHashSet<>();
            System.out.println(driverSet);
            for (String driver: driversList) {
                driverSet.add(driver);
            }
            System.out.println(driverSet);
*/
            personnel.put(vehicle, driverSet);
        }
        System.out.println(personnel);

        Set<String> pilotsList = personnel.get("helicopter");

        for (String pilot: pilotsList) {
            System.out.println(pilot);
        }

        // iterate through whole thing
        for (String vehicle: personnel.keySet()) {
            System.out.print(vehicle);
            System.out.print(": ");
            Set<String> properDrivers = personnel.get(vehicle);

            for (String driver: properDrivers) {
                System.out.print(driver);
                System.out.print(", ");
            }
            System.out.println();
        }
    }
}
