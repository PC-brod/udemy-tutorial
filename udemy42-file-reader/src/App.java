import java.io.*;

public class App {
    public static void main(String[] args) {
        File file = new File("data.txt----");
        BufferedReader br = null;

        try {
            FileReader fr = new FileReader(file);
            br = new BufferedReader(fr);
            String line;

            while( (line = br.readLine()) != null ) {
                System.out.println(line);
            }
        } catch (FileNotFoundException e) {
            System.out.println("File: " + file.toString() + " not found.");
            // e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Unable to read file: " + file.toString());
            e.printStackTrace();
        }
        finally {
            try {
                br.close();
            } catch (IOException e) {
                System.out.println("Unable to close file: " + file.toString());
                e.printStackTrace();
            }
            catch (NullPointerException ex) {
                // File was probably never opened!
                System.out.println("File: " + file + "wasn`t opened.");
            }
        }
    }
}
