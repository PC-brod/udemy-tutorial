// class is a template for creating objects
class Person {
	// this is general idea of person

	// instance variables (data or state)
	String name;
	int age;

	// classes can contain

	// 1. data - position, heart rate
	// 2. subroutines (methods) methods starts with small letter
	void speak() {
		System.out.println("Method speak - person name: " + name + ", " + "person age: " + age + ".");
	}

	void sayHello() {
		System.out.println("Hi there!");
	}

	void ageCalc() {
		int ageBonification = 0;
		if (age < 18) {
			ageBonification = 0;
		} else if (age >= 18 && age < 25) {
			ageBonification = 2;
		} else if (age >= 25 && age < 35) {
			ageBonification = 3;
		} else if (age >= 35 && age < 45) {
			ageBonification = 1;
		} else if (age >= 45) {
			ageBonification = 0;
		}
		
		System.out.println("Age bonification is: " + ageBonification);
	}
	
	int ageBon() {							// get method
		int result = (65 - age) / 2;	
		return result;
	}
	
	String getName() {						// get method
		return name;
	}
}

public class App {
// public class names must match file name (App.java, public class App)
	public static void main(String[] args) { // main method
		// TODO Auto-generated method stub
		Person person1 = new Person();
		person1.name = "Joe Black";
		person1.age = 37;
		person1.speak();
		person1.sayHello();

		Person person2 = new Person();
		person2.name = "Sarah Smith";
		person2.age = 20;
		person2.speak();
		person1.sayHello();

		System.out.println(person1.name);
		System.out.println(person2.age);

		System.out.println(person1.name);
		System.out.println(person1.age);
		person1.ageCalc();
		
		int boni = person1.ageBon();		
		System.out.println("Another way: Bonification is: " + boni);
		
		String someName = person1.getName();
		System.out.println("The name is: " + someName);
	}

}
