import java.util.Scanner;

public class tut13switch {
	public static void main(String[] args) {
		
		Scanner userInput = new Scanner(System.in);
		System.out.println("Please enter a command ('help' for help): ");
		String inputText = userInput.nextLine();
		
		switch(inputText) {
		case "help":
			System.out.println("Command 'start' will start the Machine.");
			System.out.println("Command 'stop' will stop the Machine.");
			break;
	
		case "start":
			System.out.println("Machine started.");
			break;
			
		case "stop":
			System.out.println("Machine stoped.");
			break;
			
		default:
			System.out.println("Command not recognized, you can try command 'help'.");
		}
		
		int value = 7;
		
		int[] someValues;
		someValues = new int[3];
		
		System.out.println(someValues[0]);
		System.out.println(someValues[1]);
		System.out.println(someValues[2]);
		System.out.println(someValues.length);
		
		someValues[0] = 10;
		someValues[1] = 20;
		someValues[2] = 30;
		
		System.out.println(someValues[0]);
		System.out.println(someValues[1]);
		System.out.println(someValues[2]);
		
		for(int i=0; i<someValues.length; i++) {
			System.out.println(someValues[i]);
		}
		
		int[] numbers = {5, 6, 7};
		
		for(int i=0; i<numbers.length; i++) {
			System.out.println(numbers[i]);
		}
		
		String[] words = new String[3];
		
		words[0] = "Hello";
		words[1] = "to";
		words[2] = "you";
		
		System.out.println(words[2]);
		
		String[] fruits = {"apple", "banana", "pear", "kiwi"};
		
		for(String oneItem: fruits) {
			System.out.println(oneItem);
		}
		
		int anotherValue = 0;			// allocates space in memory and pointing on that
		System.out.println(anotherValue);
		
		String someText = null;			// no allocation, not pointing to anything
		System.out.println("someText" + someText);
		
		/* allocates only memory for two references to strings,
		 * not allocating memory for the strings.
		*/
		String[] someLabels = new String[2];
		System.out.println("someLabels: " + someLabels[0]);		// null
		
		// two dimensional arrays is array of arrays
		
		int[][] grid = {
				{3, 5, 2563},
				{2, 4},
				{8, 16, 24}
		};
		
		System.out.println("grid[1][1]: " + grid[1][1]);		// 4
		System.out.println("grid[0][2]: " + grid[0][2]);		// 2563
		
		String[][] titles = new String[2][3];
		titles[0][1] = "Brekeke";
		System.out.println(titles[0][1]);
		
		for(int row=0; row<grid.length; row++) {
			for(int col=0; col < grid[row].length; col++) {
				System.out.print(grid[row][col] + "\t");
			}
			System.out.println();
		}
		
		for(int row=0; row<grid.length; row++) {
			for(int col=0; col < grid[row].length; col++) {
				System.out.println("Grid " + row + "," + col + " is: " + grid[row][col]);
			}
		}
		
		for(int row=0; row<titles.length; row++) {
			for(int col=0; col < titles[row].length; col++) {
				System.out.println("titles: " + titles[row][col]);
			}
		}
		
		String[][] niceWords = new String[2][];
		System.out.println("niceWords " + niceWords[0]);
		
		niceWords[0] = new String[4];		// here [4] means give me 4 position for that array so you will have 0,1,2,3 
		niceWords[0][3] = "please";
		System.out.println("niceWords " + niceWords[0][3]);
		
		for(int row=0; row<niceWords.length; row++) {
			for(int col=0; col < niceWords[row].length; col++) {
				System.out.print(niceWords[row][col] + "\t");
			}
			System.out.println();
		}
	}
}
