class Machine {
	public void start() {
		System.out.println("Machine started.");
	}
}

interface Plant {
	public void grow();
}

public class App {
	public static void main(String[] args) {
		Machine machine1 = new Machine() {
			// here I can rewrite - override methods of Machine class
			@Override
			public void start() {
				System.out.println("machine 1");
			}
		};
		machine1.start();
		
		Plant plant1 = new Plant() {

			public void grow() {
				System.out.println("Plant growing ...");
				
			}
		};
		
		plant1.grow();
	}
}
