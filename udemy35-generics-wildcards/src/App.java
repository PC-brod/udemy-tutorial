import java.util.ArrayList;

class Machine {
    @Override
    public String toString() {
        return "Machine{}";
    }

    public void start() {
        System.out.println("Machine started");
    }
}

class Camera extends Machine {
    @Override
    public String toString() {
        return "Camera{}";
    }

    public void snap() {
        System.out.println("Photo taken");
    }
}

public class App {
    public static void main(String[] args) {
        ArrayList<String> mainList = new ArrayList<String>();
        mainList.add("asdf");
        mainList.add("plk;l");
        showList(mainList);
        System.out.println("::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
        machineList();
        System.out.println("::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
        cameraList();
        System.out.println("::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
        anyList();
    }

    public static void showList(ArrayList<String> list) {
        for (String value: list) {
            System.out.println(value);
        }
    }

    public static void machineList() {
        ArrayList<Machine> machineList = new ArrayList<Machine>();
        machineList.add( new Machine() );
        machineList.add( new Machine() );
        showMachineList(machineList);
    }

    private static void showMachineList(ArrayList<Machine> listByClass) {
        for (Machine value: listByClass) {
            System.out.println(value);
        }
    }

    private static void cameraList() {
        ArrayList<Camera> cameraList = new ArrayList<Camera>();
        cameraList.add( new Camera() );
        cameraList.add( new Camera() );
        showCameraList(cameraList);
    }

    private static void showCameraList(ArrayList<Camera> cameraList) {
        for (Camera value: cameraList) {
            System.out.println(value);
        }
    }

    private static void anyList() {
        ArrayList<Machine> machineList = new ArrayList<Machine>();
        machineList.add( new Machine() );
        machineList.add( new Machine() );

        ArrayList<Camera> cameraList = new ArrayList<Camera>();
        cameraList.add( new Camera() );
        cameraList.add( new Camera() );

        showAnyList(machineList);
        showAnyList(cameraList);
        System.out.println("==============================================================");
        extendedAnyList(machineList);
        extendedAnyList(cameraList);
        System.out.println("==============================================================");
        superAnyList(machineList);
        // superdedAnyList(cameraList); // not gonna work
        System.out.println("==============================================================");
        anyList(machineList);
        anyList(cameraList);
    }

    private static void showAnyList(ArrayList<?> list) { // ? - wildcard
        for (Object value: list) {
            System.out.println(value);
            // value.start(); // error
            // value.snap(); // error
        }
    }

    private static void extendedAnyList(ArrayList<? extends Machine> list) {
        for (Machine value: list) {
            System.out.println(value);
            value.start();
            // value.snap(); // Can`t call Camera`s methods. All it knows is, that it is some Object extending Machine, but don`t know which Object
        }
    }

    private static void superAnyList(ArrayList<? super Camera> list) {
        for (Object value: list) {
            System.out.println(value);
            // value.start(); // error
            // value.snap(); // error
        }
    }

    private static void anyList(ArrayList<?> list) {
        for (Object value: list) {
            System.out.println(value);
            // value.start(); // error
            // value.snap(); // error
        }
    }
}
