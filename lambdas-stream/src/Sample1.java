import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;

public class Sample1 {
    public static void main(String[] args) {
        Thread th = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("In runnable\n");
            }
        });
        th.start();

        System.out.println("In main");

        anotherWay();

        th.stop();

        System.out.println();
        runArrays();

        System.out.println();
        duplicatesArray();

        System.out.println();
        performanceExample();

        System.out.println();
        characteristics();

        System.out.println("\n");
        infiniteStream();

        System.out.println("\n");
        infiniteStreamExample();
    }

    private static void anotherWay() {
        /*
        function has 4 things:
        1. name                 -can be anonymous
            2. parameter list
            3. body
        4. return type          -can be inferred
         */

        // lambdas invoke dynamic
        Thread th = new Thread(() -> System.out.println("In runnable of anotherWay"));
        th.start();

        System.out.println("In main of anotherWay");
    }

    private static void runArrays() {
        Integer length = 5;
        List<Integer> list = new ArrayList<Integer>(Arrays.asList());

        // external iterators
        for (Integer i = 0; i < length; i++) {
            list.add(i);
        }
        System.out.println(list);

        for (Integer i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        for (int el : list) {
            System.out.println(el);
        }

        // internal iterators
        list.forEach(new Consumer<Integer>() {
            @Override
            public void accept(Integer element) {
                System.out.println(element);
            }
        });

        // list.forEach((Integer element) -> System.out.println(element));

        // just for lambda expressions
        // list.forEach((element) -> System.out.println(element));

        // just for one parameter
        list.forEach(element -> System.out.println(element * element));
        // passing a argument to another instance method. Target is System.out
        list.forEach(System.out::println); // method reference to instance method
        /*
        note: System.out is an object, and pritnln() is instance method(not static method) of this obj
         */

        System.out.println("==================");

        list.stream()
                .map(e -> Integer.toString(e))
                .forEach(System.out::println);

        list.stream()
                .map(e -> String.valueOf(e)) // valueOf() is static method, parameter e  becomes a argument
                .forEach(System.out::println);

        list.stream()
                .map(String::valueOf) // reference to static method value.Of()
                .forEach(System.out::println);

        list.stream()
                .map(e -> e.toString(e)) // parameter e becomes a target, calling function on that obj
                .map(String::toString)
                .forEach(System.out::println);

        list.stream()
                .map(e -> String.valueOf(e)) // e is used as a target
                .map(String::toString)      // toString is a instance method
                .forEach(System.out::println);


        System.out.println();
        twoParameters(list);

        // function composition
        // given the values, double the even numbers and total of them
        System.out.println();
        doubleThem(list);

        System.out.println();
        // with Timeit
        afterBreak(list);

    }


    private static void duplicatesArray() {
        System.out.println("duplicatesArray duplicatesArray duplicatesArray");
        Integer length = 5;
        List<Integer> numbers = new ArrayList<Integer>(Arrays.asList());

        // external iterators
        for (Integer i = 1; i < length + 1; i++) {
            numbers.add(i);
        }
        for (Integer i = 1; i < length + 1; i++) {
            numbers.add(i);
        }
        System.out.println(numbers);

        // double the values and put them in to a list
        System.out.println();
        shearedMutability(numbers);

        System.out.println();
        List<Person> people = createPeople();
        System.out.println(people);

        // create a Map with name and age as key, and the person as value
        System.out.println();
        mapPeople(people);

        // given a list of people, create a map where
        // their name is the key and value is all people with that name
        System.out.println();
        groupingPeople(people);
    }

    private static void twoParameters(List<Integer> list) {
        System.out.println("++++++++++++++++++++++++++++++");

        List<Integer> numbers = list;

        System.out.println(
                numbers.stream() // function reduce() with 2 parameters. 1. parameter is '0'(initiate value) , second param. is lambda expresion
                        .reduce(0, (total, e) -> Integer.sum(total, e))     // sum = total + e sum function do not care about order of parameters
                // so it can be Integer.sum(e, total)
                // others functions can care abut order of parameters
                // method references can be used only when order is same
        );

        System.out.println();
        System.out.println(
                numbers.stream().reduce(0, Integer::sum) // method reference
        );

        System.out.println();
        System.out.println(
                numbers.stream()
                        .map(String::valueOf)
                        .reduce("", (carry, str) -> carry.concat(str))); // 1. parameter is target, 2. parameter is argument, order can not be changed

        System.out.println();
        System.out.println(
                numbers.stream()
                        .map(String::valueOf)
                        .reduce("", String::concat));
    }


/*
Limitation

You can not use them for DATA manipulation,
or if there is conflict between instance method and static method
 */

    private static void doubleThem(List<Integer> list) {
        System.out.println("**************************");
        // imperative code style (spaghetti code)
        int result = 0;

        for (int e : list) {
            if (e % 2 == 0) {
                result += e * 2;
            }
        }

        System.out.println(result);

        System.out.println();
        // single pass through code, declarative style
        System.out.println(
                list.stream()
                        .filter(e -> e % 2 == 0)
                        .map(e -> e * 2)
                        .reduce(0, Integer::sum));

        System.out.println();
        System.out.println(
                list.stream() // collection
                        .filter(e -> e % 2 == 0) // give me even numbers
                        .mapToInt(e -> e * 2) // multiply each number by 2
                        .sum()); // give me sum
        /* function composition, series of operations or pipeline
        .stream()
        .filter(e -> e % 2 == 0)
        .mapToInt(e -> e * 2)
         .sum()); // give me sum

         filter - filters values, som of them block, some of them pass
         input: Stream<T> filter takes Predicate<T>

         map transform values
         number of output == number of input
         no guarantee on type of the output with respect to the type of the input
         input-parameter: Stream<T> map takes Function<T, R> to return Stream<R>
         transform input Stream to output Stream ex:
         .map(e -> e * 2.5) input is Stream<Integer>, output is Stream<Double>

         both filter and map stay within their swimlanes
         reduce cuts across the swimlanes ex:
         .reduce(0.0, (carry, e) -> carry + e));

                  filter        map     reduce
                                         0.0
         x1         |                     \
         --------------------------        \
         x2         ->          x2`   ->    +   // takes value, performs the operation on it, produce next result from it and continue
         --------------------------          \
         x3         |                         \
         --------------------------            \
         x4         ->          x4`     ->      +    // takes value, performs the operation on it, produce next result from it and continue
         --------------------------              \

                1         2         3         4
               /         /         /         /
         1 -> * -> 1 -> * -> 2 -> * -> 6 -> * -> 24

         reduce on Stream<T> takes two parameters:
         first parameter is of type T
         second parameter is of type (bite function)
         BiFunction<R, T, R> to produce a result of R
                    /  /   \
         input type-  /     -output type
         element type-

         output and input type will be same, always?

         what is reduce operation:
         1. cuts across swimlanes, brings values together
         2. makes transform a collection in to a single value (potentially) or
         it may transform in to a non stream and to concrete type dates

1:45:13
         */
    }
    private static void afterBreak(List<Integer> numbers) {
        System.out.println("BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB");
        Timeit.code(() ->
                System.out.println(
                        numbers.stream()
                                .filter(e -> e % 2 == 0)
                                .mapToInt(e -> compute(e)) // or .mapToInt(Sample1::compute)
                                .sum())
        );

        System.out.println("same with parallelStream");
        /*
        Be extremely care full with parallelStream. It is like a bulldozer.
        I do not mind using a lot of thread a lot of resources, so I can get the result faster.
        He calls them all in same time.
        Use it:
        a) when it makes sense to use it - when problem is parallelisable
        b) when you want to use a lot more resources to get the answer faster
        c) when the data size is big enough you will get a benefit on performance
        d) when the task computation tim si big enough you will benefit on performance
         */
        Timeit.code(() ->
                System.out.println(
                        numbers.parallelStream()
                                .filter(e -> e % 2 == 0)
                                .mapToInt(e -> compute(e))
                                .sum())
        );
    }


    // time consuming to write,
    // program is calling this function
    // as many times as is number of elements in array
    private static int compute(Integer e) {
        // lets pretend this is something time intensive
        try {
            Thread.sleep(100);
        } catch (Exception exception) {
        }
        return e * 2;
    }

    /*
    Streams are an abstraction
    it is not a physical object of data
    stream is a bunch of function you will evaluate eventually
    there is no data sitting in the stream
    you can not point to data in a stream it does not exists
    it is a pure abstraction

    Not mutating pipeline
    you have a function composition, a pipeline of transformation,
    data flows over pipeline composition,
    you are asking for transforming, but not mutating any of values or collection
    ! avoid sheared mutability ! - not change things in sheared context
     */

    private static void shearedMutability(List<Integer> numbers) {
        // double the values and put them in to a list
        System.out.println("shearedMutability");
        // wrong way
        List<Integer> doubleOfEven = new ArrayList<Integer>();

        numbers.stream()
                .filter(e -> e % 2 == 0)
                .map(e -> e * 2)
                .forEach(e -> doubleOfEven.add(e));
        // mutability is OK, sharing is nice, shared mutability is devils work
        System.out.println(doubleOfEven); // wrong
        // friends do not let friends do shared mutation
        // potentionally code with sheared mutability can run in parallelStream() and
        // multiple threads can change same variable and some data can be lost

        // better way with Collector

        List<Integer> doubleOfEven2 =
                numbers.stream()
                        .filter(e -> e % 2 == 0)
                        .map(e -> e * 2)
                        .collect(Collectors.toList());
        System.out.println(doubleOfEven2);
        // .collect() function keeps threads safe automatically

        Set<Integer> doubleOfEven3 =
                numbers.stream()
                        .filter(e -> e % 2 == 0)
                        .map(e -> e * 2)
                        .collect(Collectors.toSet()); // set do not perform duplication
        System.out.println(doubleOfEven3);
    }
    private static List<Person> createPeople() {
        System.out.println("createPeople");
        return Arrays.asList(
                //new Person("Sara", Gender.FEMALE, 20)
                new Person("Sara", "FEMALE", 20),
                new Person("Sara", "FEMALE", 22),
                new Person("Bob", "MALE", 20),
                new Person("Paula", "FEMALE", 32),
                new Person("Paul", "MALE", 32),
                new Person("Jack", "MALE", 2),
                new Person("Jack", "MALE", 72),
                new Person("Jill", "FEMALE", 12)
        );
    }

    private static void mapPeople(List<Person> people) {
        System.out.println("mapPeople");
        // create a Map with name and age as key, and the person as value
        System.out.println(
                people.stream()
                        .collect(toMap( // tady bych to potreboval vysvetlit krok po kroku
                                person -> person.getName() + "-" + person.getAge(),
                                person -> person
                        ))
        );
    }

    private static void groupingPeople(List<Person> people) {
        System.out.println("groupingPeople");
        // given a list of people, create a map where
        // their name is the key and value is all people with that name

        System.out.println(
                people.stream()
                        .collect(groupingBy(Person::getName)) // Name is a name of person,
                // value is a list that contains people with same name
        );

        // given a list of people, create a map where
        // their name is the key and value is all the ages of people with that name
        System.out.println(
                people.stream()
                        .collect(groupingBy(Person::getName,
                                mapping(Person::getAge, toList())))
        );
    }


    private static void performanceExample() {
        System.out.println("performanceExample");
        List<Integer> integerList = Arrays.asList(1, 2, 3, 5, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20);
        // given an ordered list find the double of the first even number greater than 3

        // imperative style
        int result = 0;
        for (int e : integerList) {
            if (e > 3 && e % 2 == 0) {
                result = e * 2;
                break;
            }
        }
        System.out.println(result);
        // how much work? 8 units of work

        // 20 + 17 + 9 + 1
        System.out.println(
                integerList.stream() // all the numbers in the collection
                        .filter(e -> e > 3) // 20 evaluations (systems asks for each element)
                        // all the numbers greater than 3
                        .filter(e -> e % 2 == 0) // 17 evaluations
                        // all even numbers greater than 3 (4,6,8...)
                        .map(e -> e * 2) // 9 evaluations
                        .findFirst() // 1 operation
        );


        // streams are absolutely lazy, they will not do anything until last command
        System.out.println(
                integerList.stream()
                .filter(Sample1::isGT3) // hey do filter - I have function isGT3 (but nothing happens)
                .filter(Sample1::isEven) // hey do filter - I have function isGT3 from previous line and I will chain it to isEven function (like isGT3->isEven) (but nothing happens)
                .map(Sample1::doubleIt) // hey do map - I have isGT3->isEven and now I`ll add doubleIT so it is isGT3->isEven->doubleIT (but nothing happens)
                .findFirst() // this point turns on everything (calls daddy)
        );
    }



    // functions will not be applied for all data separately
    // but all chained function will by applied to first element, than to second ...

    // !!! so in fact it is still 8 units of work !!!
    // so it is not actually lazy, it is efficient evaluation
    // lazy evaluation is possible only if the functions don`t have side effect
    // do not print output like in this example!

    private static boolean isGT3(Integer integer) {
        System.out.println("isGT3" + integer);
        return integer > 3;
    }
    private static boolean isEven(Integer integer) {
        System.out.println("isEven" + integer);
        return integer % 2 == 0;
    }

    private static <R> int doubleIt(Integer integer) {
        System.out.println("doubleIt" + integer);
        return integer * 2;
    }


    // characteristics of stream:
    // sized, ordered, distinct, sorted

    private static void characteristics() {
        System.out.println("characteristics characteristics characteristics");
        Integer length = 5;
        List<Integer> numbers = new ArrayList<Integer>(Arrays.asList());

        // external iterators
        for (Integer i = 1; i < length + 1; i++) {
            numbers.add(i);
        }
        for (Integer i = 1; i < length + 1; i++) {
            numbers.add(i);
        }
        System.out.println(numbers);

        numbers.stream()
                .filter(e -> e % 2 ==0)
                .forEach(System.out::print);
        // sized - list is bounded so it is sized
        // ordered - first element, second element ...
        // non-distinct - list do not prevent duplicates (nerozlisuje hodnoty elementu)
        // non-sorted - values are not sorted (by natural ordering)

        System.out.println();
        numbers.stream()
                .filter(e -> e % 2 ==0)
                .sorted()
                .forEach(System.out::print);
        // sized, ordered, non-distinct, sorted

        System.out.println();
        numbers.stream()
                .filter(e -> e % 2 ==0)
                .distinct() // no duplicates
                .forEach(System.out::print);
        // sized, ordered, distinct, non-sorted
    }
    private static void infiniteStream() {
        System.out.println("infiniteStream");
        System.out.print(Stream.iterate(100, e -> e + 1)); // returns Head reference

        // start with 100, creates a series
        // 100, 101, 102, 103, ...
        // Im potentially infinite stream, but I`m not gonna create a single value until it is demanded
    }

    private static void infiniteStreamExample() {
        System.out.println("Example of using infinite stream");
        /*
        Given number k, given count n.
        Find the total of double of n even numbers starting with k,
        where sqrt of each number is > 20
         */

        int k = 121;
        int n = 51;
        System.out.println(goCount(k ,n));

        System.out.println(streamCount(k, n));
    }

    private static int goCount(int k, int n) {
        System.out.println("goCount");
        int result = 0;

        int index = k;
        int counter = 0;

        while (counter < n) {
            if(index % 2 == 0 && Math.sqrt(index) > 20) {
                result += index * 2;
                counter++;
            }
            index++;
        }
        return result;
    }

    private static int streamCount(int k, int n) {
        System.out.println("streamCount");
        return Stream.iterate(k, e -> e + 1)        // unbounded, lazy
                .filter(e -> e % 2 == 0)            // unbounded, lazy
                .filter(e -> Math.sqrt(e) > 20)     // unbounded, lazy
                .mapToInt(e -> e * 2)               // unbounded, lazy
                .limit(n)                           // sized, lazy
                .sum();
    }
}

// 2:08:00 https://www.youtube.com/watch?v=1OpAgZvYXLQ&fbclid=IwAR31XzqxwmHE2eSaBUabXQBcSzkwNcoP56SIUag-4KwpGQNKjYo8JnlIl_8

