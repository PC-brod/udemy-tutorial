import java.util.Iterator;
import java.util.LinkedList;

public class App {
    public static void main(String[] args) {
        LinkedList<String> animals = new LinkedList<String>();

        animals.add("cat");
        animals.add("dog");
        animals.add("mouse");

        Iterator<String> it = animals.iterator();

        while (it.hasNext()) {
            String value = it.next();
            System.out.println(value);

            if (value.equals("dog")) {
                it.remove();
                System.out.println("'dog' removed");
            }
        }

        System.out.println();

        // Modern Java 5 and later
        for (String animal: animals) {
            System.out.println(animal);
        }

    }
}
