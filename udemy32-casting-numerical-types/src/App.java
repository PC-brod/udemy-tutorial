public class App {
    public static void main(String[] args) {
        byte somethingSmall = 20;
        short something = 55;
        int intName = 5585;
        long longValue = 1354321;

        float floatValue = 654.4654f;
        float floatValue2 = (float) 22.555;
        double doubleVal = 25.6;

        System.out.println(Byte.MAX_VALUE);
        System.out.println(Byte.MIN_VALUE);

        System.out.println(Short.MAX_VALUE);
        System.out.println(Short.MIN_VALUE);

        System.out.println(Integer.MAX_VALUE);
        System.out.println(Integer.MIN_VALUE);

        System.out.println(Long.MAX_VALUE);
        System.out.println(Long.MIN_VALUE);

        System.out.println(Float.MAX_VALUE);
        System.out.println(Float.MIN_VALUE);

        System.out.println(Double.MAX_VALUE);
        System.out.println(Double.MIN_VALUE);

        intName = (int)longValue;
        System.out.println(intName);

        doubleVal = intName;
        System.out.println(doubleVal);

        intName = (int)floatValue;      // will cut everything behind .
        System.out.println(intName);    // Math.round(); is for rounding

        somethingSmall = (byte)128;
        System.out.println(somethingSmall); // -128 because it goes around a circle

        somethingSmall = (byte)130;
        System.out.println(somethingSmall); // -126 because it goes around a circle

    }
}
