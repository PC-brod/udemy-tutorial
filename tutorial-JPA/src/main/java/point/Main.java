package point;

import javax.persistence.*;
import javax.security.auth.login.AppConfigurationEntry;
import javax.security.auth.login.Configuration;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

enum ConfigurationType {
    INT,
    LONG,
    STRING,
    DOUBLE,
    BOOL
}

// https://www.objectdb.com/tutorial/jpa/intellij/maven
public class Main {
    public static void main(String[] args) {
        // Open a database connection
        // (create a new database if it doesn't exist yet):
        EntityManagerFactory emf =
            Persistence.createEntityManagerFactory("$objectdb/db/points.odb");
        EntityManager em = emf.createEntityManager();

        // Store 1000 Point objects in the database:
        em.getTransaction().begin();
        for (int i = 0; i < 1000; i++) {
            Point p = new Point(i, i);
            em.persist(p);
        }
        em.getTransaction().commit();

        // Find the number of Point objects in the database:
        Query q1 = em.createQuery("SELECT COUNT(p) FROM Point p");
        System.out.println("Total Points: " + q1.getSingleResult());

        // Find the average X value:
        Query q2 = em.createQuery("SELECT AVG(p.x) FROM Point p");
        System.out.println("Average X: " + q2.getSingleResult());

        // Retrieve all the Point objects from the database:
        TypedQuery<Point> query =
            em.createQuery("SELECT p FROM Point p", Point.class);
        List<Point> results = query.getResultList();
        for (Point p : results) {
            System.out.println(p);
        }

        // Close the database connection:
        em.close();
        emf.close();


        //////////////////////////////////////////////////////////////////////////////////////


        // https://www.javatpoint.com/example-to-connect-to-the-mysql-database

        // https://stackoverflow.com/questions/26515700/mysql-jdbc-driver-5-1-33-time-zone-issue

        // jdbc:mysql://localhost:3306/kp2?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=CET
        // jdbc:mysql://localhost:3306/kp2?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            System.out.println("I have the Class");
            Connection connection = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/kp2?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=CET","root","rootroot"
              //"jdbc:mysql://localhost:3306/localmysql80","root","rootroot"
            );
            Statement stmt = connection.createStatement();
            System.out.println(stmt);
            ResultSet rs = stmt.executeQuery("SELECT * FROM kp2.configurations;");
            while (rs.next())
                System.out.println(rs.getInt(1) + " "
                        + rs.getInt(2) + " "
                        + rs.getString(3)+ " "
                        + rs.getString(4)+ " "
                        + rs.getString(5)+ " "
                        + rs.getString(6)+ " "
                        + rs.getString(7));
            connection.close();
        } catch (Exception e) {
            System.out.println("Error");
            System.out.println(e);
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////

        // https://www.javatpoint.com/jpa-entity-manager

        class Configuration {

            @Enumerated(EnumType.STRING)
            private ConfigurationType type;

            private String prefix;
            private String name;
            private String value;
            private boolean isFrontend;
            private String note;

            public Configuration(int id, boolean isFrontend, String name, String note, String prefix, ConfigurationType type, String value) {
            }

            public AppConfigurationEntry[] getAppConfigurationEntry(Integer id, boolean isFrontend, String name, String note, String prefix, String type, String value) {
                return new AppConfigurationEntry[0];
            }
        }

        Configuration configuration = new Configuration(3,true,"3-name","3-note","3-prefix",ConfigurationType.INT,"55");

        EntityManagerFactory emf2 = Persistence.createEntityManagerFactory(String.valueOf(configuration));
        EntityManager em2 = emf2.createEntityManager();
        em2.getTransaction().begin();
        em2.persist(configuration);
        em2.getTransaction().commit();
        emf2.close();
    }
}
