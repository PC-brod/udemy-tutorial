package cz.bcas.counters.application.rest.filter.authorized;

import cz.bcas.counters.bussiness.errors.UnauthorizedException;
import cz.bcas.counters.bussiness.services.AuthService;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

@Authorizable
@Priority(1)
@Provider
public class AuthorizableImpl implements ContainerRequestFilter {

    @Inject
    private AuthService service;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        String token = requestContext.getHeaderString("Token");

        if (!isSession(token)) {
            System.out.println("401 - UNAUTHORIZED - bad token: " + token);
            throw new UnauthorizedException();
        }
    }

    private boolean isSession(String token) {
        if (service.isValidToken(token)){
            return true;
        } else {
            return false;
        }
    }
}
