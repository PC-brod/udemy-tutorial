package cz.bcas.counters.application.rest.endpoint;

import cz.bcas.counters.application.rest.endpoint.objects.FacebookLoginRequest;
import cz.bcas.counters.application.rest.endpoint.objects.GoogleLoginRequest;
import cz.bcas.counters.application.rest.endpoint.objects.LoginRequest;
import cz.bcas.counters.application.rest.endpoint.objects.Credentials;
import cz.bcas.counters.application.rest.filter.logged.Loggable;
import cz.bcas.counters.bussiness.login.LoginResponse;
import cz.bcas.counters.bussiness.services.AuthService;
import cz.bcas.counters.domain.object.Session;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.wildfly.swarm.spi.runtime.annotations.Post;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@Loggable
@ApplicationScoped
@Path("auth")
public class AuthEndpoint {

    @Inject
    private AuthService service;

    /**
     * Provides a login functionality for user. Successful login depends on users name and password
     * @param loginRequest contains UserName and Password
     * @return data in json format containing result and token (if user name and password matches)
     * @throws FileNotFoundException in case that users.json file is not found
     * @throws NoSuchAlgorithmException in case that algorithm for creating hash for user token is missing
     */

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("login")
    @Operation(
            summary = "Login user by name and password",
            tags = {"login"},
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Result OK / BAD_USER_NAME / BAD_PASSWORD and token if user name and password matches",
                            content = @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = LoginResponse.class))
                    ),
            })
    @Parameter(in = ParameterIn.HEADER, name = "userName", description = "The user name for login", required = true)
    @Parameter(in = ParameterIn.HEADER, name = "password", description = "The user`s password for login", required = true)
    public LoginResponse login( LoginRequest loginRequest ) throws FileNotFoundException, NoSuchAlgorithmException {
        return service.login(loginRequest.getUserName(), loginRequest.getPassword());
    }

    /**
     * Alternative logging with Credentials
     * @param credentials contains UserName and Password
     * @return ok response and data in json format containing result and token (if user name and password matches)
     * @throws FileNotFoundException in case that users.json file is not found
     * @throws NoSuchAlgorithmException in case that algorithm for creating hash for user token is missing
     */

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("loginUser")
    public Response loginUser(Credentials credentials) throws FileNotFoundException, NoSuchAlgorithmException {
        return Response.ok(service.login(credentials.getUserName(), credentials.getPassword())).build();
    }

    /**
     * showSessions provides functionality for development period that displays list of all sessions
     * @return a list of all sessions in json format
     */

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/list")
    @Operation(
            summary = "Get sessions",
            description = "Get list of all session including expired but not yet deleted sessions",
            tags = {"list"},
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "sessions in json format",
                            content = @Content(mediaType = "application/json")
                    )
            }
    )
    public List<Session> showSessions() {
        return service.showSessions();
    }

    @GET
    @Produces(MediaType.TEXT_HTML)
    @Path("login/google")
    public String loginPage() { return service.googlePage(); }

    @GET
    @Produces(MediaType.TEXT_HTML)
    @Path("login/google2")
    public String loginGoogle() throws FileNotFoundException { return service.loginGoogle2(); }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("login/get-google")
    public void getGoogle() throws IOException { service.getGoogle(); }

    @GET
    @Produces(MediaType.TEXT_HTML)
    @Path("login/google/redirect")
    public String redirectPage() { return "Success!"; }

    @POST
    @Consumes("application/json")
    @Path("loginGoogle")
    public void userData(GoogleLoginRequest googleLoginRequest) {
        service.googleLogin(googleLoginRequest.getUserGmail(), googleLoginRequest.getState(), googleLoginRequest.getIdToken());
    }

    @GET
    @Produces(MediaType.TEXT_HTML)
    @Path("google")
    public String google() throws FileNotFoundException {
        return service.loginGoogle3();
    }

    @POST
    @Consumes("application/json")
    @Path("google/validation")
    public void localValidation(GoogleLoginRequest googleLoginRequest) throws IOException, NoSuchAlgorithmException {
        service.googleLocalValidation(googleLoginRequest);
    }

    @GET
    @Consumes("application/json")
    @Produces("application/json")
    @Path("loginGoogleTest")
    public GoogleLoginRequest userData() {
        GoogleLoginRequest r = new GoogleLoginRequest();
        r.setIdToken("xxx");
        r.setState(" ssss");
        r.setUserGmail(" adsa@dada.zc");
        return r;

    }

    @GET
    @Produces(MediaType.TEXT_HTML)
    @Path("facebook")
    public String fbLoginBtn() throws FileNotFoundException {
        return service.loginFb();
    }

    @POST
    @Consumes("application/json")
    @Path("facebook/validation")
    public void facebookValidation(FacebookLoginRequest facebookLoginRequest) throws NoSuchAlgorithmException {
        service.facebookValidation(facebookLoginRequest);
    }
}
