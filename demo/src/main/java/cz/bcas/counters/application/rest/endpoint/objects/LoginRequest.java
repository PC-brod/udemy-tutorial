package cz.bcas.counters.application.rest.endpoint.objects;

import io.swagger.v3.oas.models.media.XML;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class LoginRequest {
    private String userName;
    private String password;
}
