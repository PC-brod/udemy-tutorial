package cz.bcas.counters.application.rest.filter.logged;

import javax.annotation.Priority;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

@Loggable
@Priority(2)
@Provider
public class LoggableImpl implements ContainerRequestFilter {

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        System.out.println("\nLoggableImpl.java\n" + requestContext.getUriInfo().getPath() );
    }
}