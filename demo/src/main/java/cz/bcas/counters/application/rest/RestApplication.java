package cz.bcas.counters.application.rest;

import cz.bcas.counters.application.rest.endpoint.AuthEndpoint;
import cz.bcas.counters.application.rest.endpoint.CounterEndpoint;
import cz.bcas.counters.application.rest.endpoint.UserEndpoint;
import cz.bcas.counters.application.rest.endpoint.mappers.GenericExceptionMapper;
import cz.bcas.counters.application.rest.endpoint.mappers.NoCounterFoundExceptionMapper;
import cz.bcas.counters.application.rest.endpoint.mappers.UnauthorizedExceptionMapper;
import cz.bcas.counters.application.rest.filter.authorized.AuthorizableImpl;
import cz.bcas.counters.application.rest.filter.logged.LoggableImpl;
import cz.bcas.counters.application.rest.filter.measured.Measurable;
import cz.bcas.counters.application.rest.filter.measured.MeasurableImpl;
import io.swagger.v3.jaxrs2.integration.resources.AcceptHeaderOpenApiResource;
import io.swagger.v3.jaxrs2.integration.resources.OpenApiResource;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ApplicationPath("/")
public class RestApplication extends Application {
    @Override
    public Set<Class<?>> getClasses() {
        return Stream.of(
                OpenApiResource.class,
                AcceptHeaderOpenApiResource.class,
                AuthEndpoint.class,
                CounterEndpoint.class,
                UserEndpoint.class,
                AuthorizableImpl.class,
                LoggableImpl.class,
                MeasurableImpl.class,
                NoCounterFoundExceptionMapper.class,
                UnauthorizedExceptionMapper.class,
                GenericExceptionMapper.class
        ).collect(Collectors.toSet()
        );
    }

}
