package cz.bcas.counters.application.rest.endpoint.objects;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class FacebookLoginRequest {
    private String userID;
    private String name;
}
