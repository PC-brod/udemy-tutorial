package cz.bcas.counters.application.rest.endpoint;

import cz.bcas.counters.application.rest.filter.authorized.Authorizable;
import cz.bcas.counters.application.rest.filter.measured.Measurable;
import cz.bcas.counters.bussiness.services.CounterService;
import cz.bcas.counters.domain.object.Counter;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import java.util.List;

@Authorizable
@Measurable
@ApplicationScoped
@Path("counter")
public class CounterEndpoint {

    @Inject
    private CounterService service;
    @Inject
    private CounterService init;

    /**
     * Displays list of all counters in json format
     * @return list of all counters in json format
     */

    @GET
    @Produces("application/json")
    @Operation(
            summary = "All counters",
            description = "Displays data of all counters in json format",
            tags = {"counter"},
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "List of all counters in json format",
                            content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Counter.class))
                    ),
                    @ApiResponse(
                            responseCode = "400",
                            description = "UNAUTHORIZED + exception message",
                            content = @Content(mediaType = "application/json")
                    )

            }
    )
    public List<Counter> getAllCounters() {
        return service.getAllCounters();
    }

    /**
     * Creates new counter
     * @return just created counter with initial value
     */
    @GET
    @Produces("application/json")
    @Path("init")
    @Operation(
            summary = "Creates new counter",
            description = "Creates new counter and displays its data with initial value",
            tags = {"counter"},
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Data of newly created counter in json format with initial value",
                            content = @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = Counter.class))
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "UNAUTHORIZED + exception message",
                            content = @Content(mediaType = "application/json")
                    )
            }
    )
    public Counter initCounter() {
        return init.createCounter();
    }

    /**
     *Get data of counter specified by id
     * @param id specifies counter
     * @return data of demanded counter in json format
     */
    @GET
    @Produces("application/json")
    @Path("{id}")
    @Operation(
            summary = "Get counter",
            description = "Get data of counter specified by id",
            tags = {"counter"},
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Data of demanded counter in json format",
                            content = @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = Counter.class))
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "UNAUTHORIZED + exception message",
                            content = @Content(mediaType = "application/json")
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "message: Counter with id: (counter id) not found.",
                            content = @Content(mediaType = "application/json")
                    )
            }
    )
    public Counter getCounter(
            @Parameter(in = ParameterIn.PATH, name = "id", description = "id Specifies demanded counter", required = true)
            @PathParam("id") int id
    ) {
        return service.getCounter(id);
    }

    /**
     * Rises value of counter specified with it`s id by one
     * @param id Specifies counter that should be incremented
     * @return data of incremented counter in json format
     */
    @GET
    @Produces("application/json")
    @Path("/{id}/inc")
    @Operation(
            summary = "Increments counter",
            description = "Rises value of counter specified with it`s id by one",
            tags = {"counter"},
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Data of incremented counter in json format",
                            content = @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = Counter.class))
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "UNAUTHORIZED + exception message",
                            content = @Content(mediaType = "application/json")
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "message: Counter with id: (counter id) not found.",
                            content = @Content(mediaType = "application/json")
                    )
            }
    )
    public Counter addToCounter(
            @Parameter(in = ParameterIn.PATH, name = "id", description = "id Specifies counter that should be incremented", required = true)
            @PathParam("id") int id
    ) {
        return service.addToCounter(id, 1);
    }

    /**
     *  Add @param value to counter actual value
     * @param id Specifies counter
     * @param value Value of how much should be add to counter value
     * @return Data of incremented counter in json format
     */
    @GET
    @Produces("application/json")
    @Path("/{id}/inc/{value}")
    @Operation(
            summary = "Increments counter",
            description = "Rises value of counter specified with it`s id by specified value",
            tags = {"counter"},
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Data of incremented counter in json format",
                            content = @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = Counter.class))
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "UNAUTHORIZED + exception message",
                            content = @Content(mediaType = "application/json")
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "message: Counter with id: (counter id) not found.",
                            content = @Content(mediaType = "application/json")
                    )
            }
    )
    public Counter addToCounter(
            @Parameter(in = ParameterIn.PATH, name = "id", description = "Id of targeted counter", required = true)
            @PathParam("id") int id,
            @Parameter(in = ParameterIn.PATH, name = "value", description = "Value of how much should be add to counter`s value", required = true)
            @PathParam("value") int value
    ) {
        return service.addToCounter(id, value);
    }

    /**
     * Decrement counter
     * @param id Id of targeted counter
     * @return Data of decremented counter in json format
     */
    @GET
    @Produces("application/json")
    @Path("/{id}/dec")
    @Operation(
            summary = "Decrement counter",
            description = "Reduces value of counter specified with it`s id by one",
            tags = {"counter"},
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Data of reduced counter in json format",
                            content = @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = Counter.class))
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "UNAUTHORIZED + exception message",
                            content = @Content(mediaType = "application/json")
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "message: Counter with id: (counter id) not found.",
                            content = @Content(mediaType = "application/json")
                    )
            }
    )
    public Counter subToCounter(
            @Parameter(in = ParameterIn.PATH, name = "id", description = "Id of targeted counter", required = true)
            @PathParam("id") int id
    ) {
        return service.subToCounter(id, 1);
    }

    /**
     *
     * @param id Id of targeted counter
     * @param value Specifies of how much should be deduct from counters value
     * @return Data of decremented counter in json format
     */
    @GET
    @Produces("application/json")
    @Path("/{id}/dec/{value}")
    @Operation(
            summary = "Decrement counter",
            description = "Reduces value of counter specified with it`s id by specified value",
            tags = {"counter"},
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Data of reduced counter in json format",
                            content = @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = Counter.class))
                    ),
                    @ApiResponse(
                            responseCode = "400",
                            description = "UNAUTHORIZED + exception message",
                            content = @Content(mediaType = "application/json")
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "message: Counter with id: (counter id) not found.",
                            content = @Content(mediaType = "application/json")
                    )
            }
    )
    public Counter subToCounter(
            @Parameter(in = ParameterIn.PATH, name = "id", description = "Id of targeted counter", required = true)
            @PathParam("id") int id,
            @Parameter(in = ParameterIn.PATH, name = "value", description = "Specifies of how much should be deduct from counter`s value", required = true)
            @PathParam("value") int value
    ) {
        return service.subToCounter(id, value);
    }

    /**
     * Set up value of targeted counter to specified value
     * @param id Id of targeted counter
     * @param value New value of targeted counter
     * @return
     */
    @GET
    @Produces("application/json")
    @Path("/{id}/set/{value}")
    @Operation(
            summary = "Set up counter",
            description = "Sets new value of targeted counter",
            tags = {"counter"},
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Data of newly setup ed counter in json format",
                            content = @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = Counter.class))
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "UNAUTHORIZED + exception message",
                            content = @Content(mediaType = "application/json")
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "message: Counter with id: (counter id) not found.",
                            content = @Content(mediaType = "application/json")
                    )
            }
    )
    public Counter setCounter(
            @Parameter(in = ParameterIn.PATH, name = "id", description = "Id of targeted counter", required = true)
            @PathParam("id") int id,
            @Parameter(in = ParameterIn.PATH, name = "value", description = "New value of targeted counter", required = true)
            @PathParam("value") int value
    ) {
        return service.setCounter(id, value);
    }

    /**
     * Reset value of counter specified with it`s id to zero
     * @param id Id of targeted counter
     * @return Data of counter after reset
     */
    @GET
    @Produces("application/json")
    @Path("/{id}/reset")
    @Operation(
            summary = "Reset counter",
            description = "Reset value of counter specified with it`s id to zero",
            tags = {"counter"},
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Data of counter after reset",
                            content = @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = Counter.class))
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "UNAUTHORIZED + exception message",
                            content = @Content(mediaType = "application/json")
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "message: Counter with id: (counter id) not found.",
                            content = @Content(mediaType = "application/json")
                    )

            }
    )
    public Counter resetCounter(
            @Parameter(in = ParameterIn.PATH, name = "id", description = "Id of targeted counter", required = true)
            @PathParam("id") int id
    ) {
        return service.setCounter(id, 0);
    }

    /**
     * Remove counter from list of counters
     * @param id Id of targeted counter
     */
    @GET
    @Path("/{id}/clear")
    @Operation(
            summary = "Clear counter",
            description = "Remove counter from list of counters",
            tags = {"counter"},
            responses = {
                    @ApiResponse(
                            responseCode = "401",
                            description = "UNAUTHORIZED + exception message",
                            content = @Content(mediaType = "application/json")
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "message: Counter with id: (counter id) not found.",
                            content = @Content(mediaType = "application/json")
                    )
            }
    )
    public void clearCounter(
            @Parameter(in = ParameterIn.PATH, name = "id", description = "Id of targeted counter", required = true)
            @PathParam("id") int id
    ) {
        service.clearCounter(id);
    }
}
