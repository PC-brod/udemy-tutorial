package cz.bcas.counters.application.rest.filter.measured;

import com.codahale.metrics.*;
import cz.bcas.counters.bussiness.services.metrics.CountersMetrics;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

@Measurable
@Priority(3)
@Provider
public class MeasurableImpl implements ContainerRequestFilter {
    @Inject
    private CountersMetrics metricsService;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {

        Meter counter = metricsService.getRegistry().meter("Request counter (" + requestContext.getUriInfo().getRequestUri() + ")");
        counter.mark();

    }
}
