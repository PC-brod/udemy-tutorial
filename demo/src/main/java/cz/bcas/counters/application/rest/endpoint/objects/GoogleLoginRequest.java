package cz.bcas.counters.application.rest.endpoint.objects;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class GoogleLoginRequest {
    private String userGmail;
    private String state;
    private String idToken;

}