package cz.bcas.counters.application.rest.endpoint.mappers;

import cz.bcas.counters.bussiness.errors.UnauthorizedException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;


@Provider
public class GenericExceptionMapper implements ExceptionMapper<Exception> {


    @Override
    public Response toResponse(Exception exception) {
        exception.printStackTrace();
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("NEZNAMA CHYBA " + exception.getMessage()).build();
    }
}
