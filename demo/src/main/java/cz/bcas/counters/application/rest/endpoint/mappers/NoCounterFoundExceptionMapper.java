package cz.bcas.counters.application.rest.endpoint.mappers;

import cz.bcas.counters.bussiness.errors.NoCounterFoundException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * This mapper catch`s <NoCounterFoundException> request for not existing Counter
 * @return response status NOT_FOUND and brief information for user in it`s entity with params:
 * @param e is current exception, it is important for getting Counter id
 */

@Provider
public class NoCounterFoundExceptionMapper implements ExceptionMapper<NoCounterFoundException> {

    @Override
    public Response toResponse(NoCounterFoundException e) {
        return Response.status(Response.Status.NOT_FOUND).entity("Counter with id: " + e.getCounterId() + " not found.").build();
    }
}
