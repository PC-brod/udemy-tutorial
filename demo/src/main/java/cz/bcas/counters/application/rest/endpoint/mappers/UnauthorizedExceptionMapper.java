package cz.bcas.counters.application.rest.endpoint.mappers;

import cz.bcas.counters.bussiness.errors.UnauthorizedException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * This mapper catch`s <UnauthorizedException> unauthorized requests
 * @return response status UNAUTHORIZED and brief information for user in it`s entity with params:
 * @param e is current exception, we are getting it`s message
 */
@Provider
public class UnauthorizedExceptionMapper implements ExceptionMapper<UnauthorizedException> {

    @Override
    public Response toResponse(UnauthorizedException e) {
        return Response.status(Response.Status.UNAUTHORIZED).entity("401 - UNAUTHORIZED " + e.getMessage()).build();
    }
}
