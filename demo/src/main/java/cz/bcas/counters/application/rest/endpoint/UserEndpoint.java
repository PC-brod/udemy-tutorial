package cz.bcas.counters.application.rest.endpoint;

import cz.bcas.counters.application.rest.filter.authorized.Authorizable;
import cz.bcas.counters.bussiness.services.UserService;
import cz.bcas.counters.domain.object.User;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.*;

import java.io.FileNotFoundException;
import java.util.List;

@ApplicationScoped
@Path("/user")
public class UserEndpoint {

    @Inject
    private UserService service;

    @Inject
    private UserService init;

    /**
     * Displays list of all users saved in proper file
     * @return list of all users in json format
     */

    @Authorizable
    @GET
    @Produces("application/json")
    @Operation(
            summary = "Show all users",
            tags={"user"},
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "OK - list of all users in json format",
                            content = @Content(mediaType = "application/json",
                                schema = @Schema(implementation = User.class))
                    ),
                    @ApiResponse(
                            responseCode = "401",
                            description = "UNAUTHORIZED + exception message",
                            content = @Content(mediaType = "application/json")
                    )
            }
    )
    public List<User> getAllUsers() { return service.getAllUsers(); }

    /**
     * Loads users data from file, and creates a list automatically when server start`s
     * @throws FileNotFoundException in case that users.json file is not found
     */

    @GET
    @Produces("application/json")
    @Path("/reload")
    @Operation(
            summary = "Loads users",
            description = "Loads users data from file, and creates a list. Runs automatically when server start`s.",
            tags = {"user"}
    )
    public void reloadUsers() throws Exception { service.reloadUsers(); }
}

