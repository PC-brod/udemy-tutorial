package cz.bcas.counters.bussiness.services;

import com.google.api.client.util.Base64;
import com.google.api.client.util.DateTime;
import cz.bcas.counters.application.rest.endpoint.objects.FacebookLoginRequest;
import cz.bcas.counters.application.rest.endpoint.objects.GoogleLoginRequest;
import cz.bcas.counters.bussiness.login.LoginResponse;
import cz.bcas.counters.bussiness.login.LoginResult;
import cz.bcas.counters.domain.object.Session;
import cz.bcas.counters.domain.object.User;
import io.fusionauth.jwt.Verifier;
import io.fusionauth.jwt.domain.JWT;
import io.fusionauth.jwt.ec.ECVerifier;
import io.fusionauth.jwt.hmac.HMACVerifier;
import org.json.JSONArray;
import org.json.JSONObject;
import org.wildfly.swarm.spi.runtime.annotations.ConfigurationValue;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Scanner;

@Stateless
public class AuthServiceImpl implements AuthService {

    @Inject
    private UserService userService;

    @Inject
    private SessionService sessionService;

    @Override
    public LoginResponse login(String userName, String password) throws NoSuchAlgorithmException {

        List<User> users = userService.getAllUsers();

        for (User user : users) {
            if (user.getName().equals(userName)) {
                if (user.getPassword().equals(password)) {
                    LocalDateTime loginDT = LocalDateTime.now();
                    String token = createHash(password, loginDT);
                    sessionService.createSession(user, token);
                    return new LoginResponse(LoginResult.OK, token);
                } else {
                    return new LoginResponse(LoginResult.BAD_PASSWORD);
                }
            }
        }
        return new LoginResponse(LoginResult.BAD_USER_NAME);
    }

    @Override
    public List<Session> showSessions() {
        return sessionService.allSessions();
    }

    @Override
    public Session getSessionByToken(String token) {
        return null;
    }

    @Override
    public boolean isValidToken(String token) {
        if (sessionService.getValidSession(token) != null) {
            return true;
        }
        return false;
    }

    @Override
    public String googlePage() {
        StringBuilder contentBuilder = new StringBuilder();
        try {
            BufferedReader in = new BufferedReader(new FileReader("src/main/html/google-login.html"));
            String str;
            while ((str = in.readLine()) != null) {
                contentBuilder.append(str);
            }
            in.close();
        } catch (IOException e) {
        }
        String page = contentBuilder.toString();

        return page;
    }

    @Override
    public void googleLogin(String userGmail, String state, String idToken) {
        // Check the id token
        BufferedReader reader;
        String line;
        StringBuffer responseConnect = new StringBuffer();

        String urlString = "https://oauth2.googleapis.com/tokeninfo?id_token=" + idToken;

        try {
            URL url = new URL(urlString);
            connection = (HttpURLConnection) url.openConnection();

            //Request setup
            connection.setRequestMethod("GET");
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);

            int status = connection.getResponseCode();
            System.out.println("Status: " + status);

            if (status > 299) {
                reader = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
            } else {
                reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                // Is it our user?
                List<User> users = userService.getAllUsers();
                for (User user : users) {
                    if (user.getName().equals(userGmail)) {
                        // Yes it is our user
                        // So let`s destroy old session with state and create new session with our token
                        sessionService.destroySession(state);
                        sessionService.createSession(user, createHash(user.getPassword(), LocalDateTime.now()));
                        return;
                    }
                }
                // Unknown user
                System.out.println("Unknown user: " + userGmail);

            }
            while ((line = reader.readLine()) != null) {
                responseConnect.append(line);
            }
            reader.close();

            System.out.println(responseConnect.toString());

        } catch (IOException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        } finally {
            connection.disconnect();
        }
// return new LoginResponse(LoginResult.OK, token);
        // return new GoogleLoginResponse(GoogleLoginResult.OK, userGmail);
    }

    @Inject
    @ConfigurationValue("google.clientid")
    private String clientId;

    @Inject
    @ConfigurationValue("google.applicationname")
    private String applicationName;

    @Inject
    @ConfigurationValue("swarm.port.offset")
    private String portOffset;

    @Inject
    @ConfigurationValue("facebook.appid")
    private String appId;

    @Inject
    @ConfigurationValue("facebook.apiversion")
    private String apiVersion;


    @Override
    public String loginGoogle2() throws FileNotFoundException {
        // Create a state token to prevent request forgery.
        // Store it in the session for later validation.
        String state = "security_token" + new BigInteger(130, new SecureRandom()).toString(32);

        sessionService.createGoogleSession(state);

        // Read index.html into memory, and set the client ID,
        // token state, and application name in the HTML before serving it.
        return new Scanner(new File("src/main/html/google-login2.html"), "UTF-8")
                .useDelimiter("\\A").next()
                .replaceAll("[{]{2}\\s*CLIENT_ID\\s*[}]{2}", clientId)
                .replaceAll("[{]{2}\\s*STATE\\s*[}]{2}", state)
                .replaceAll("[{]{2}\\s*APPLICATION_NAME\\s*[}]{2}", applicationName)
                .replaceAll("[{]{2}\\s*PORT\\s*[}]{2}", 8080 + portOffset);
    }

    private static HttpURLConnection connection;

    @Override
    public void getGoogle() throws IOException {
        System.out.println("Get google");

        BufferedReader reader;
        String line;
        StringBuffer responseConnect = new StringBuffer();

        /*

        https://accounts.google.com/o/oauth2/v2/auth?
        response_type=code&
        client_id=424911365001.apps.googleusercontent.com&
        scope=openid%20email&
        redirect_uri=https%3A//oauth2.example.com/code&
        state=security_token%3D138r5719ru3e1%26url%3Dhttps%3A%2F%2Foauth2-login-demo.example.com%2FmyHome&
        login_hint=jsmith@example.com&
        nonce=0394852-3190485-2490358&
        hd=example.com

http://localhost:8090/auth/login/google/redirect
http://localhost:8090/auth/login/google
         */

        String urlString =
                "https://accounts.google.com/o/oauth2/v2/auth?" +
                        "response_type=" + "code" + "&" + // https://developers.google.com/identity/protocols/oauth2/openid-connect#response-type
                        "client_id=" + "893738920547-ei5v4gk0efua4o0gn3pou5era50irn84.apps.googleusercontent.com" + "&" +
                        "scope=" + "openid%20email" + "&" +
                        "redirect_url=" + "http://localhost:8090/auth/login/redirect" + "&" +
                        "login_hint=" + "petrbcas@gmail.com" + "&" +
                        "state=" + "security_token76F72D556EB7FEBC5DC4A6319FA618D2";

        try {
            URL url = new URL(urlString);
            connection = (HttpURLConnection) url.openConnection();

            //Request setup
            connection.setRequestMethod("GET");
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);

            int status = connection.getResponseCode();
            System.out.println("Status: " + status);

            /*
                        if (status > 299) {
                reader = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
                while ((line = reader.readLine()) != null) {
                    responseConnect.append(line);
                } reader.close();
            } else {
                reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                while ((line = reader.readLine()) != null ){
                    responseConnect.append(line);
                } reader.close();
            }
             */

            if (status > 299) {
                reader = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
            } else {
                reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            }
            while ((line = reader.readLine()) != null) {
                responseConnect.append(line);
            }
            reader.close();

            System.out.println(responseConnect.toString());

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } finally {
            connection.disconnect();
        }

        /////////////////////// java 11 method with java.net.http.HttpClient //////////////////////////////
        /*
        System.out.println("java.net.http.HttpClient");
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder().uri(URI.create("https://jsonplaceholder.typicode.com/todos/5")).build();
        client.sendAsync(request, HttpResponse.BodyHandlers.ofString())
        .thenApply(HttpResponse::body)
        .thenAccept(System.out::println)
        .join();
        */
    }


    @Override
    public String loginGoogle3() throws FileNotFoundException {
        // Create a state token to prevent request forgery.
        // Store it in the session for later validation.
        String state = "security_token" + new BigInteger(130, new SecureRandom()).toString(32);

        sessionService.createGoogleSession(state);

        // Read index.html into memory, and set the client ID,
        // token state, and application name in the HTML before serving it.
        return new Scanner(new File("src/main/html/google-login3.html"), "UTF-8")
                .useDelimiter("\\A").next()
                .replaceAll("[{]{2}\\s*CLIENT_ID\\s*[}]{2}", clientId)
                .replaceAll("[{]{2}\\s*STATE\\s*[}]{2}", state)
                .replaceAll("[{]{2}\\s*APPLICATION_NAME\\s*[}]{2}", applicationName)
                .replaceAll("[{]{2}\\s*PORT\\s*[}]{2}", 8080 + portOffset);
    }

    @Override
    public void googleLocalValidation(GoogleLoginRequest googleLoginRequest) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        String state = googleLoginRequest.getState();
        String idToken = googleLoginRequest.getIdToken();
        String userGmail = googleLoginRequest.getUserGmail();

        // Is state valid?
        Session session = sessionService.getValidSession(state);
        if (session != null){

            // Decode id Token from Google
            String[] parts = idToken.split("\\.");
            String b64data = parts[1];
            String jsonData = new String(Base64.decodeBase64(b64data), "UTF-8");
            System.out.println(jsonData);
            JSONObject jsonObject = new JSONObject(jsonData);

            // Verify the token
            String iss = (String) jsonObject.get("iss");
            if ( iss.equals("https://accounts.google.com") || iss.equals("accounts.google.com") ){
                String aud = (String) jsonObject.get("aud");
                if (aud.equals(clientId)) {
                    Integer iat = (Integer) jsonObject.get("iat");
                    Integer exp = (Integer) jsonObject.get("exp");

                    // Expiration check
                    if (iat < exp) {
                        // Is email verified?
                        if (jsonObject.get("email_verified").equals(true)) {
                            String sub = (String) jsonObject.get("sub");

                            // Is it our user?
                            List<User> users = userService.getAllUsers();
                            for (User user : users) {
                                if( !user.getGoogleClientInfo().getGsub().equals(null) || !user.getGoogleClientInfo().getGsub().equals("") ) {

                                    if (user.getGoogleClientInfo().getGsub().equals(sub) ) {
                                        // Yes it is our user

                                        // Now compare our google data with his actual google data and set new data
                                        if (!user.getGoogleClientInfo().getGemail().equals(userGmail)) {
                                            user.getGoogleClientInfo().setGemail(userGmail);
                                        }
                                        if (!user.getGoogleClientInfo().getGemailVerified().equals(jsonObject.get("email_verified"))) {
                                            if (jsonObject.get("email_verified").equals(true)) {
                                                user.getGoogleClientInfo().setGemailVerified("true");
                                            } else {
                                                user.getGoogleClientInfo().setGemailVerified("false");
                                            }
                                        }
                                        if (!user.getGoogleClientInfo().getGname().equals(jsonObject.get("name"))) {
                                            user.getGoogleClientInfo().setGname((String) jsonObject.get("name"));
                                        }

                                        // Let`s destroy old session with state and create new session with our token
                                        sessionService.destroySession(state);
                                        sessionService.createSession(user, createHash(user.getPassword(), LocalDateTime.now()));
                                        return;
                                    }
                                }
                            }
                            // Unknown user
                            System.out.println("Unknown user: " + userGmail);
                        } else {
                            return;
                        }
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            } else {
                return;
            }
        } else {
            return;
        }
    }

    @Override
    public String loginFb() throws FileNotFoundException {
        return new Scanner(new File("src/main/html/fb-login.html"), "UTF-8")
                .useDelimiter("\\A").next()
                .replaceAll("[{]{2}\\s*APP_ID\\s*[}]{2}", appId)
                .replaceAll("[{]{2}\\s*API_VERSION\\s*[}]{2}", apiVersion);
    }

    @Override
    public void facebookValidation(FacebookLoginRequest facebookLoginRequest) throws NoSuchAlgorithmException {
        String fbUserId = facebookLoginRequest.getUserID();
        String fbName = facebookLoginRequest.getName();

        System.out.println("facebookValidation");

        // Is it our user?
        List<User> users = userService.getAllUsers();
        for (User user : users) {
            if (!user.getFacebookClientInfo().getFbid().equals(null) || !user.getFacebookClientInfo().getFbid().equals("")) {
                if (user.getFacebookClientInfo().getFbid().equals(fbUserId)) {
                    // Yes it is our user
                    if (!user.getFacebookClientInfo().getFbname().equals(fbName)) {
                        user.getFacebookClientInfo().setFbname(fbName);
                    }
                    sessionService.createSession(user, createHash(user.getPassword(), LocalDateTime.now()));
                    return;
                }
            }
        }
        // Unknown user
        System.out.println("Unknown user: " + fbName + " " + fbUserId);
    }

    public String getResponse(String urlString) {
        BufferedReader reader;
        String line;
        StringBuffer responseConnect = new StringBuffer();

        try {
            URL url = new URL(urlString);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);

            int status = connection.getResponseCode();

            if (status > 299) {
                reader = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
            } else {
                reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            }
            while ((line = reader.readLine()) != null) {
                responseConnect.append(line);
            }
            reader.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            connection.disconnect();
        }

        return responseConnect.toString();
    }

    private String createHash(String password, LocalDateTime loginDT) throws NoSuchAlgorithmException {
        String timePass = loginDT.toString() + password;
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(timePass.getBytes());
        byte[] digest = md.digest();
        String loginToken = DatatypeConverter.printHexBinary(digest).toUpperCase();

        return loginToken;
    }
}
