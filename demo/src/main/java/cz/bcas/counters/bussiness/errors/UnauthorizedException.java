package cz.bcas.counters.bussiness.errors;

import javax.ejb.ApplicationException;

@ApplicationException
public class UnauthorizedException extends RuntimeException {
}
