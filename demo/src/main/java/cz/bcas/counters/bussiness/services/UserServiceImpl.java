package cz.bcas.counters.bussiness.services;

import cz.bcas.counters.domain.daos.UserDAO;
import cz.bcas.counters.domain.object.User;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.FileNotFoundException;
import java.util.List;

@Stateless
public class UserServiceImpl implements UserService {

    @Inject
    private UserDAO dao;

    @Override
    public List<User> getAllUsers() {
        return dao.getAllUsers();
    }

    @Override
    public User getUser(int id) {
        return dao.getUser(id);
    }

    @Override
    public void reloadUsers() throws Exception {
        dao.reloadUsers();
    }


}
