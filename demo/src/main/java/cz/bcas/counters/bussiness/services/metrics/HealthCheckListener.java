package cz.bcas.counters.bussiness.services.metrics;

import com.codahale.metrics.health.HealthCheckRegistry;
import com.codahale.metrics.servlets.HealthCheckServlet;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;

@Dependent
// public class HealthCheck extends HealthCheckServlet.ContextListener {
public class HealthCheckListener extends HealthCheckServlet.ContextListener {

    /*
    @Inject
    private FileHealthCheck healthCheck;
*/
    final HealthCheckRegistry healthChecks = new HealthCheckRegistry();

    @Override
    protected HealthCheckRegistry getHealthCheckRegistry() {
        return healthChecks;
    }
}
