package cz.bcas.counters.bussiness.services.metrics;

import com.codahale.metrics.health.HealthCheckRegistry;
import com.codahale.metrics.health.HealthCheck.Result;
import cz.bcas.counters.domain.daos.UserDAO;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.File;

@Singleton
public class FileHealthCheckImpl implements FileHealthCheck {

    @Inject
    private UserDAO dao;

/*
    @Override
    protected Result check() throws Exception {
        if (dao.isFile("users.json")) {
            return Result.healthy();
        } else {
            return Result.unhealthy("File not exists");
        }
    }
*/
    @Inject
    private HealthCheckListener healthCheckListener;

    @Override
    public HealthCheckRegistry getRegistry() {
        return healthCheckListener.getHealthCheckRegistry();
    }

    @Override
    public Result check() throws Exception {
        if (dao.isFile("users.json")) {
            return Result.healthy();
        } else {
            return Result.unhealthy("File not exists");
        }
    }


/*
    private String fileName = "users.json";

    final HealthCheckRegistry healthChecks = new HealthCheckRegistry();

    @Override
    public HealthCheck.Result check() throws Exception {
        if (dao.isFile("users.json")) {
            return HealthCheck.Result.healthy();
        } else {
            return HealthCheck.Result.unhealthy("File not exists");
        }
    }

    healthChecks.register("users", new FileHealthCheckImpl(fileName));

    @Override
    public HealthCheckRegistry getRegistry() {
        return healthChecks.getHealthCheck("users");
    }

 */
}
