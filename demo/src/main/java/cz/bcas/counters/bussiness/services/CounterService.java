package cz.bcas.counters.bussiness.services;

import cz.bcas.counters.domain.object.Counter;

import java.util.List;

public interface CounterService {
    List<Counter> getAllCounters();

    Counter createCounter();
    Counter getCounter(int id);
    Counter addToCounter(int id, int value);
    Counter subToCounter(int id, int value);
    Counter setCounter(int id, int value);
    void clearCounter(int id);
}
