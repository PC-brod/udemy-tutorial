package cz.bcas.counters.bussiness.services.control;

import cz.bcas.counters.bussiness.services.metrics.CountersMetrics;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

@Interceptor
@CounterTracking
public class CounterTrackingInterceptor {

    // inject metrics --- timer

    @PostConstruct
    private void init() {
        System.out.println("Interceptor initialized");
    }


    @AroundInvoke
    public Object log(InvocationContext context) throws Exception {
        System.out.println("Intercepting " + context.getParameters().toString() + " " + context.getMethod().getName() + " " + context.getTimer());
        // processTracker.track();
        return context.proceed();
    }
}
