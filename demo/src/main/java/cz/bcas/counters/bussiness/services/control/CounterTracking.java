package cz.bcas.counters.bussiness.services.control;

import javax.interceptor.InterceptorBinding;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@InterceptorBinding
@Retention(RUNTIME)
@Target({METHOD,TYPE})      // defines the program element to which this interceptor can be applied
                            // annotation @CounterServe can be applied to a method or a type (class, interface, or enum)
public @interface CounterTracking {
}
