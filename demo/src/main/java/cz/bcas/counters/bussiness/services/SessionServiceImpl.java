package cz.bcas.counters.bussiness.services;

import cz.bcas.counters.domain.object.Session;
import cz.bcas.counters.domain.object.User;
import org.wildfly.swarm.spi.runtime.annotations.ConfigurationValue;

import javax.ejb.Singleton;
import javax.inject.Inject;
import java.time.LocalDateTime;
import java.util.*;

@Singleton
public class SessionServiceImpl implements SessionService {

    @Inject
    @ConfigurationValue("auth.sessionlen")

    private int sessionLen;

    Map<String, Session> sessionMap = new HashMap<>();

    @Override
    public void createSession(User user, String token) {
        Session session = new Session(user, token);
        sessionMap.put(token, session);
        refreshSession(session);
    }

    @Override
    public List<Session> allSessions() {
        return new ArrayList<>(sessionMap.values());
    }

    public Session getValidSession(String token) {
        Session session = (Session) sessionMap.get(token);
        if (session != null) {
            if (session.getLastAction().plusMinutes(sessionLen).isBefore(LocalDateTime.now())) {
                destroySession(token);
            } else {
                refreshSession(session);
                return session;
            }
        }
        return null;
    }

    public void destroySession(String token) {
        sessionMap.remove(token);
    }

    @Override
    public void createGoogleSession(String state) {
        Session session = new Session(null, state);
        sessionMap.put(state, session);
        refreshSession(session);
    }

    public void refreshSession(Session session) {
        session.setLastAction(LocalDateTime.now());
    }
}
