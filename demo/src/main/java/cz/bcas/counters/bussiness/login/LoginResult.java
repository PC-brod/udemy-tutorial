package cz.bcas.counters.bussiness.login;

public enum LoginResult {
    OK,
    BAD_USER_NAME,
    BAD_PASSWORD
}
