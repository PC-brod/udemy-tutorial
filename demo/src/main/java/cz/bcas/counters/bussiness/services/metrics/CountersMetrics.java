package cz.bcas.counters.bussiness.services.metrics;

import com.codahale.metrics.MetricRegistry;

import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;

public interface CountersMetrics {
    MetricRegistry getRegistry();
}
