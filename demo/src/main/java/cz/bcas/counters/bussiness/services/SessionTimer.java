package cz.bcas.counters.bussiness.services;

import cz.bcas.counters.domain.object.Session;
import lombok.Getter;
import lombok.Setter;
import org.wildfly.swarm.spi.runtime.annotations.ConfigurationValue;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.*;
import javax.inject.Inject;
import java.time.LocalDateTime;
import java.util.List;

@Singleton
@Getter
@Setter
@Startup
public class SessionTimer implements TimedObject {


    @Inject
    @ConfigurationValue("auth.sessionlen")
    private int sessionLen;

    @Inject
    @ConfigurationValue("sessioncheck.freqhour")
    private String freqhour;

    @Inject
    @ConfigurationValue("sessioncheck.freqmin")
    private String freqmin;

    @Inject
    @ConfigurationValue("sessioncheck.freqsec")
    private String freqsec;

    @Inject
    SessionService sessionService;

    @Resource
    TimerService timerService;

    @PostConstruct
    public void initTimer() {
        if (timerService.getTimers() != null) {
            for (Timer timer : timerService.getTimers()) {
                timer.cancel();
            }
        }
        timerService.createCalendarTimer(
                new ScheduleExpression().hour(freqhour).minute(freqmin).second(freqsec), new TimerConfig("sessionCheck", true)
        );
    }

    @Override
    public void ejbTimeout(Timer timer) {
        List<Session> allSessions = sessionService.allSessions();

        for (Session session : allSessions) {
            if (session.getLastAction().plusMinutes(sessionLen).isBefore(LocalDateTime.now())) {
                sessionService.destroySession(session.getToken());
            }
        }
    }
}
