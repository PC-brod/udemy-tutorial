package cz.bcas.counters.bussiness.services;

import cz.bcas.counters.domain.object.User;

import java.io.FileNotFoundException;
import java.util.List;

public interface UserService {
    List<User> getAllUsers();
    User getUser(int id);

    void  reloadUsers() throws Exception;
}
