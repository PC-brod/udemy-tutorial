package cz.bcas.counters.bussiness.services.metrics;

import com.codahale.metrics.health.HealthCheck;
import com.codahale.metrics.health.HealthCheckRegistry;

public interface FileHealthCheck {
    HealthCheckRegistry getRegistry();
    HealthCheck.Result check() throws Exception;
}
