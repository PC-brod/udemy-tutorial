package cz.bcas.counters.bussiness.services.metrics;

import com.codahale.metrics.*;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.util.concurrent.TimeUnit;

@Singleton
@Startup
public class CountersMetricsImpl implements CountersMetrics {

    static final MetricRegistry metrics = new MetricRegistry();

    @PostConstruct
    private void init() {
        startReport();

    }

    private void startReport() {
        ConsoleReporter reporter = ConsoleReporter.forRegistry(metrics)
                .convertRatesTo(TimeUnit.SECONDS)
                .convertDurationsTo(TimeUnit.MILLISECONDS)
                .build();
        reporter.start(10, TimeUnit.SECONDS);
    }

    @Override
    public MetricRegistry getRegistry() {
        return metrics;
    }
}
