package cz.bcas.counters.bussiness.services;

import cz.bcas.counters.bussiness.services.control.CounterTracking;
import cz.bcas.counters.bussiness.services.control.CounterTrackingInterceptor;
import cz.bcas.counters.domain.daos.CounterDAO;
import cz.bcas.counters.domain.object.Counter;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import java.util.List;

@Interceptors({CounterTrackingInterceptor.class})
@Stateless
public class CounterServiceImpl implements CounterService {

    @Inject
    private CounterDAO dao;

    @CounterTracking
    public List<Counter> getAllCounters() {
        return dao.getAllCounters();
    }

    @Override
    public Counter createCounter() {
        return dao.createCounter();
    }

    @Override
    public Counter getCounter(int id) {
        return dao.getCounter(id);
    }

    @Override
    public Counter addToCounter(int id, int valChange) {
        System.out.println("addToCounter");
        Counter counter = getCounter(id);
        counter.setValue(counter.getValue() + valChange);
        return dao.saveCounter(counter);
    }

    @Override
    public Counter subToCounter(int id, int valChange) {
        Counter counter = getCounter(id);
        counter.setValue(counter.getValue() - valChange);
        return dao.saveCounter(counter);
    }

    @Override
    public Counter setCounter(int id, int valTo) {
        Counter counter = getCounter(id);
        counter.setValue(valTo);
        return dao.saveCounter(counter);
    }

    @Override
    public void clearCounter(int id) {
        dao.clearCounter(id);
    }

}
