package cz.bcas.counters.bussiness.services.metrics;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.servlets.MetricsServlet;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;

@Dependent
public class ServletMetrics extends MetricsServlet.ContextListener {

    // public static final MetricRegistry METRIC_REGISTRY = new MetricRegistry();

    @Inject
    private CountersMetrics metricsService;

    @Override
    protected MetricRegistry getMetricRegistry() {
        return metricsService.getRegistry();
    }
}
