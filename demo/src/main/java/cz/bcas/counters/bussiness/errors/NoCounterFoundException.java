package cz.bcas.counters.bussiness.errors;

import lombok.Getter;
import lombok.Setter;

import javax.ejb.ApplicationException;

@ApplicationException
public class NoCounterFoundException extends RuntimeException {

    @Getter
    @Setter
    private int counterId;

    public NoCounterFoundException(int id) {
        this.counterId = id;
    }
}
