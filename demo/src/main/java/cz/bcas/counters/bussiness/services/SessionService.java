package cz.bcas.counters.bussiness.services;

import cz.bcas.counters.domain.object.Session;
import cz.bcas.counters.domain.object.User;

import javax.ejb.Singleton;
import java.util.List;

@Singleton
public interface SessionService {
    void createSession(User user, String token);
    List<Session> allSessions();

    Session getValidSession(String token);
    void refreshSession(Session session);
    void destroySession(String token);

    void createGoogleSession(String state);
}
