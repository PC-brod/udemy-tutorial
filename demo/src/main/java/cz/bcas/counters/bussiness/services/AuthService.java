package cz.bcas.counters.bussiness.services;

import cz.bcas.counters.application.rest.endpoint.objects.FacebookLoginRequest;
import cz.bcas.counters.application.rest.endpoint.objects.GoogleLoginRequest;
import cz.bcas.counters.bussiness.login.LoginResponse;
import cz.bcas.counters.domain.object.Session;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public interface AuthService {
    LoginResponse login(String userName, String password) throws FileNotFoundException, NoSuchAlgorithmException;

    List<Session> showSessions();

    Session getSessionByToken(String token);

    boolean isValidToken(String token);

    void getGoogle() throws IOException;

    String loginGoogle2() throws FileNotFoundException;

    String googlePage();

    void googleLogin(String userGmail, String state, String idToken);

    void googleLocalValidation(GoogleLoginRequest googleLoginRequest) throws UnsupportedEncodingException, NoSuchAlgorithmException;

    String loginGoogle3() throws FileNotFoundException;

    String loginFb() throws FileNotFoundException;

    void facebookValidation(FacebookLoginRequest facebookLoginRequest) throws NoSuchAlgorithmException;
}
