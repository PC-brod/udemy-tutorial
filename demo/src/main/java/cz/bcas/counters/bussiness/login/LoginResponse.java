package cz.bcas.counters.bussiness.login;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonSerialize
public class LoginResponse {

    private LoginResult result;
    private String token;

    public LoginResponse(LoginResult result, String token) {
        this.token = token;
        this.result = result;
    }

    public LoginResponse(LoginResult result) {
         this(result, null);
    }
}
