package cz.bcas.counters.domain.daos;

import cz.bcas.counters.bussiness.errors.NoCounterFoundException;
import cz.bcas.counters.domain.object.Counter;
import lombok.Getter;
import lombok.Setter;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import java.util.*;

@Singleton
@Getter
@Setter
public class CounterDAOImpl implements CounterDAO {

    private int idCounter = 0;
    private Map<Integer, Counter> data;

    @PostConstruct
    private void init() {
        data = new HashMap<>();
    }

    @Override
    public List<Counter> getAllCounters() {
        return new ArrayList<>(data.values());
    }

    @Override
    public Counter createCounter() {
        Counter c = new Counter(idCounter);
        data.put(idCounter, c);
        idCounter++;

        return c;
    }

    @Override
    public Counter getCounter(int id) {
        Counter counter = data.get(id);
        if (counter == null) throw new NoCounterFoundException(id);
        else return data.get(id).clone();
    }

    @Override
    public Counter saveCounter(Counter c) {
        data.put(c.getId(), c);
        return c;
    }

    @Override
    public void clearCounter(int id) {
        Counter counter = data.get(id);
        if (counter == null) throw new NoCounterFoundException(id);
        else data.remove(id);
    }
}
