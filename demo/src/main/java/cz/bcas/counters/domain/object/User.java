package cz.bcas.counters.domain.object;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class User {

    public User() {
    }

    public User(int id) {
        this.id = id;
        name = "";
        password = "";
    }

    private int id;
    private String name;
    private String password;

    private GoogleClientInfo googleClientInfo;

    private FacebookClientInfo facebookClientInfo;

    /*
    private String gsub;
    private String gemail;
    private String gemailVerified;
    private String gname;
    private String fbname;
    private String fbid;

     */

    public User clone() {
        User newUser = new User();
        newUser.setId(id);
        newUser.setName(name);
        newUser.setPassword(password);

        newUser.setGoogleClientInfo(new GoogleClientInfo());
        newUser.setFacebookClientInfo(new FacebookClientInfo());


/*
        newUser.setGsub(gsub);
        newUser.setGemail(gemail);
        newUser.setGemailVerified(gemailVerified);
        newUser.setGname(gname);

        newUser.setFbname(fbname);
        newUser.setFbid(fbid);

 */

        return newUser;
    }
}
