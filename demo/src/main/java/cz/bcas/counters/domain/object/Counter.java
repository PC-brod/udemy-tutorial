package cz.bcas.counters.domain.object;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
// @JsonSerialize
public class Counter {

    public Counter() {}

    public Counter(int id) {
        this.id = id;
        value = 0;
    }

    private int id;
    private int value;

    public Counter clone() {

        Counter newCounter = new Counter();
        newCounter.setId(id);
        newCounter.setValue(value);
        return newCounter;
    }

    // public int getId(){return id;};
}
