package cz.bcas.counters.domain.daos;

import cz.bcas.counters.domain.object.User;

import java.io.FileNotFoundException;
import java.util.List;

public interface UserDAO {
    List<User> getAllUsers();
    User getUser(int id);
    void saveToFile();

    void reloadUsers() throws Exception;
    boolean isFile(String fileName);
}
