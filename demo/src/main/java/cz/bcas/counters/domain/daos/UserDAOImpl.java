package cz.bcas.counters.domain.daos;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import cz.bcas.counters.bussiness.services.metrics.FileHealthCheck;
import cz.bcas.counters.domain.object.User;

import lombok.Getter;
import lombok.Setter;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import java.io.*;
import java.util.*;

@Singleton
@Getter
@Setter
@Startup
public class UserDAOImpl implements UserDAO {

    private int idUser = 0;
    private Map<Integer, User> userData;
/*
    @Inject
    private FileHealthCheck fileHealthCheck;
*/
    @PostConstruct
    private void init() throws Exception {
        // fileHealthCheck.check();
        // System.out.println(fileHealthCheck.getRegistry());
        loadUsersFromFile();
    }

    private void loadUsersFromFile() throws Exception {
        ObjectMapper mapper = new ObjectMapper();

        try {
            TypeFactory typeFactory = mapper.getTypeFactory();

            CollectionType collectionType = typeFactory.constructCollectionType(
                    List.class, User.class);

            List<User> loadedUsers = mapper.readValue(new File("users-g-fb.json"), collectionType);

            System.out.println(loadedUsers);

            userData = new HashMap<>();

            for (User loadedUser : loadedUsers) {
                userData.put(idUser, loadedUser);
                idUser++;
            }
            idUser = 0;

            System.out.println(userData.values());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<User> getAllUsers() {
        return new ArrayList<>(userData.values());
    }


    @Override
    public User getUser(int id) {
        return userData.get(id).clone();
    }

    @Override
    public void saveToFile() {
        ObjectMapper mapper = new ObjectMapper();

        try {
            String userListJson = mapper.writeValueAsString(getAllUsers());
            File file = new File("users-g-fb.json");
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            writer.write(userListJson);
            writer.close();

        } catch (JsonProcessingException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void reloadUsers() throws Exception {
        loadUsersFromFile();
    }

    @Override
    public boolean isFile(String fileName) {
        if (new File(fileName).exists()){
            return true;
        } else {
            return false;
        }
    }
}
