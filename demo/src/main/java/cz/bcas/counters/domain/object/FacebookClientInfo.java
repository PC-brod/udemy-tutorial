package cz.bcas.counters.domain.object;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FacebookClientInfo {
    private String fbname;
    private String fbid;

    public FacebookClientInfo(String fbname, String fbid) {
        this.fbname = fbname;
        this.fbid = fbid;
    }

    public FacebookClientInfo() {

    }
}
