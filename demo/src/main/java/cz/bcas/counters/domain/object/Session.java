package cz.bcas.counters.domain.object;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
// @JsonSerialize
public class Session {
    private String token;
    private User user;
    LocalDateTime loginDT = LocalDateTime.now();
    private LocalDateTime lastAction;

    public Session(User user, String token) {
        this.user = user;
        this.token = token;
    }
    /*
    public LocalDateTime getLastAction(){return lastAction;};
    public String getToken(){return token;};

     */
}
