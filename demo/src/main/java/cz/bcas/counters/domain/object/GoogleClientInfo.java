package cz.bcas.counters.domain.object;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GoogleClientInfo {
    private String gsub;
    private String gemail;
    private String gemailVerified;
    private String gname;

    public GoogleClientInfo(String gsub, String gemail, String gemailVerified, String gname) {
        this.gsub = gsub;
        this.gemail = gemail;
        this.gemailVerified = gemailVerified;
        this.gname = gname;
    }

    public GoogleClientInfo() {

    }
}
