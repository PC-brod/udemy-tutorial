package cz.bcas.counters.domain.daos;

import cz.bcas.counters.domain.object.Counter;

import java.util.List;

public interface CounterDAO {
    List<Counter> getAllCounters();
    Counter createCounter();
    Counter getCounter(int id);
    Counter saveCounter(Counter c);
    void clearCounter(int id);
}
