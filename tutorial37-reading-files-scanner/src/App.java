import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class App {
	public static void main(String[] args) throws FileNotFoundException {
		// String fileName =
		// "C:\\Users\\petrc\\Documents\\udemy-tutorial\\tutorial37-reading-files-scanner\\src\\text.txt";
		String fileName = "text2.txt";

		File textFile = new File(fileName);

		Scanner in = new Scanner(textFile);

		int value = in.nextInt();
		System.out.println("Int value is: " + value);
		in.nextLine();

		int count = 2;
		while (in.hasNextLine()) {
			String line = in.nextLine();
			System.out.println(count + ": " + line);
			count++;
		}

		in.close();
	}
}
