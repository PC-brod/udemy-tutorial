package tutorial3;

public class Application {
	public static void main(String[] args) { // main Ctrl+Spacebar -> main method
		int myInt = 7; // int is primitive type
		
		String text = "Hello"; 	// String is NOT a primitive type. It is a class!
								// It is a type of object that can hold/store a string
								// 'text' is variable capable of referring to a thing that has the type string
		String text2 = "Hello";
		
		System.out.println(text + " " + text2);
		
		System.out.println(text == text2); // true
		
		String blank = " ";
		
		String name = "Bob";
		
		String greeting = text + blank + name;
		
		System.out.println(greeting);
		
		System.out.println("My integer is: " + myInt);
		
		double myDouble = 7.8;
		
		System.out.println("My double is: " + myDouble + ".");
		
	}
}
