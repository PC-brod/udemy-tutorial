import { Environment } from './environment.model';

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment: Environment = {
  production: false,
  uriPrefix: 'http://127.0.0.1:8080/https://klientskyportal-dev.bcas.cz/api/v1',
  uriInbosPrefix: 'https://inbos-kalkulik.bcas.cz:449/inbos/oauth2/sso-klient/BC'
};
