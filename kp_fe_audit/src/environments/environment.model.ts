export interface Environment {
  production: boolean;
  uriPrefix: string;
  uriInbosPrefix: string;
}
