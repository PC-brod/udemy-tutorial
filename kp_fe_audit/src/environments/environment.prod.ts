import { Environment } from './environment.model';

export const environment: Environment = {
  production: true,
  // uriPrefix: 'http://client.bcas.cz/api/v1'
  uriPrefix: 'https://klientskyportal.bcas.cz/api/v1',
  uriInbosPrefix: 'https://inbos-kalkulik.bcas.cz:449/inbos/oauth2/sso-klient/BC'
};
