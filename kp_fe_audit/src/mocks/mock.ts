
import { ResCPPartner } from '../app/binder/shared/models/res-cp-partner.model';
import { ResCPProductGroup } from '../app/binder/shared/models/res-cp-product-group.model';
import { CPImage } from '../app/shared/models/cp-image.model';
import { ResConsultant } from '../app/shared/models/res-consultant.model';
import { ResGDPRConsentState } from '../app/shared/models/res-gdpr-consent-state.model';
import { ResContractDetail } from '../app/binder/contract/models/res-contract-detail.model';
import { ResContract } from '../app/binder/contract/models/res-contract.model';
import { ResNotificationPrescript } from '../app/binder/notification/models/res-notification-prescript.model';
import { ResClient } from '../app/profile/shared/models/res-client.model';
import { ResNotificationInstance } from '../app/dashboard/shared/models/notification-instance.model';
import { ResFinance } from '../app/finance/shared/models/finance.model';
import { ResAttachmentUpload } from '../app/binder/shared/models/res-attachment-upload.model';
import { NewsItem } from '../app/news/shared/models/news-item.model';


export const codelistpartners: Array<ResCPPartner> = [
  {
    name: 'ČSOB a.s.',
    isBC: false,
    partnerId: '?'
  }
];
export const codelistproductgroups: Array<ResCPProductGroup> = [
  {
    name: 'Hypoteční úvěr',
    productGroupId: '1'
  }
];
export const clients: Array<ResClient> = [
  {
    titlesBehind: 'Phd.',
    lastName: 'Testík',
    titlesInFront: 'M.A.',
    clientId: '100000000000001',
    correspondingAddress: {
      awaitingApproval: false,
      city: 'Praha',
      streetNumber: '22',
      countryCode: 'CZ',
      street: 'Lidická',
      postalCode: '15000'
    },
    gender: 'MALE',
    dateOfBirth: '22.10.1990',
    emailAwaitingApproval: false,
    hasPrimaryConsultant: true,
    firstName: 'Jan',
    mobilePhone: '+420666777888',
    mobilePhoneAwaitingApproval: false,
    permanentAddress:
    {
      awaitingApproval: false,
      city: 'Praha',
      streetNumber: '22',
      countryCode: 'CZ',
      street: 'Lidická',
      postalCode: '15000'
    },
    email: 'mtest@mtest.com'}
];
export const photo: CPImage = {
  extension: '?',
  data: 'cid:13700098201'
};
export const consultants: Array<ResConsultant> = [
  {
    firstName: 'Konzultantka',
    lastName: 'Alternatívna',
    emailAddress: 'konzultant2@bc.cz',
    gender: 'FEMALE',
    photo: this.photo,
    contactPhone: '73088777'
  }
];
export const GDPRConsents: Array<ResGDPRConsentState> = [
  {
  consentTypeId: 'GDPRConsentType_1',
  accepted: false,
  consentText: `VaÅ¡e osobnÃ­ Ãºdaje budeme zpracovÃ¡vat za ÃºÄelem poskytovÃ¡nÃ­ poradenskÃ½ch
   sluÅ¾eb nad rÃ¡mec uzavÅenÃ½ch smluv a vyjednÃ¡vÃ¡nÃ­ o nich, jde zejmÃ©na oÂ
    sestavovÃ¡nÃ­ finanÄnÃ­ho plÃ¡nu a souvisejÃ­cÃ­ analÃ½zu finanÄnÃ­ situace klientÅ¯ BC.`
},
  {
  consentTypeId: 'GDPRConsentType_2',
  accepted: false,
  consentText: `ÃÄelom spracÃºvania osobnÃ½ch Ãºdajov je poskytovanie realitnÃ½ch sluÅ¾ieb
   obsahujÃºce najmÃ¤ sluÅ¾by: vypracovanie zmluvnÃ½ch dokumentov k predaju, prenÃ¡jmu a kÃºpe
    nehnuteÄ¾nosti, inzercia, vyhotovenie fotografiÃ­ nehnuteÄ¾nosti, odbornÃ¡ pomoc pri prevode
     nehnuteÄ¾nostÃ­ a inÃ½ch ÄalÅ¡Ã­ch ÄinnostÃ­. So spracÃºvanÃ­m osobnÃ½ch Ãºdajov na ÃºÄely poskytovania realitnÃ½ch sluÅ¾ieb.`
},
  {
  consentTypeId: 'GDPRConsentType_3',
  accepted: false,
  consentText: `VaÅ¡e osobnÃ­ Ãºdaje budeme pÅedÃ¡vat ÄlenÅ¯m skupiny BC, jejichÅ¾ seznam tvoÅÃ
  ­ pÅÃ­lohu Ä. 3 Informace, a to za ÃºÄely, pro kterÃ© bude VaÅ¡e osobnÃ­ Ãºdaje oprÃ¡vnÄn zpracovÃ¡vat
   BC na zÃ¡kladÄ VaÅ¡eho souhlasu, a za stejnÃ½ch podmÃ­nek uvedenÃ½ch v tomto souhlasu a v Informaci. SloÅ¾enÃ
   ­ skupiny BC se mÅ¯Å¾e mÄnit, pÅiÄemÅ¾ souhlas se vztahuje vÅ¾dy na vÅ¡echny spoleÄnosti
    skupiny BC, kterÃ© budou jejÃ­mi Äleny v pÅÃ­sluÅ¡nÃ© dobÄ.`
},
  {
  consentTypeId: 'GDPRConsentType_4',
  accepted: false,
  consentText: `VaÅ¡e osobnÃ­ Ãºdaje budeme zpracovÃ¡vat pro ÃºÄely marketingu, kterÃ½ zahrnuje zejmÃ©na
   personalizovanou nabÃ­dku sluÅ¾eb a produktÅ¯ a poskytovÃ¡nÃ­ informacÃ­ o novÃ½ch produktech finanÄnÃ­ho
    zprostÅedkovÃ¡nÃ­, personalizovanou nabÃ­dku pracovnÃ­ch pÅÃ­leÅ¾itostÃ­, vyhodnocovÃ¡nÃ­ klientskÃ½ch dat,
     vÄetnÄ jejich profilovÃ¡nÃ­, za ÃºÄelem zvyÅ¡ovÃ¡nÃ­ kvality, komplexnosti a optimalizace naÅ¡ich sluÅ¾eb.`
},
  {
  consentTypeId: 'GDPRConsentType_5',
  accepted: false,
  consentText: `VÃ½slovnÄ tÃ©Å¾ souhlasÃ­m, aby BC zpracovÃ¡val rovnÄÅ¾ Ãºdaje o mÃ©m zdravotnÃ­m stavu, a to
   pouze pro ÃºÄely zprostÅedkovÃ¡nÃ­ uzavÅenÃ­ smlouvy o Å¾ivotnÃ­m pojiÅ¡tÄnÃ­ a jejÃ­ nÃ¡slednÃ© archivace.`
}
];
export const attachments: Array<ResAttachmentUpload> = [];
export const contractsDetails: Array<ResContractDetail> = [
  {
      note: 'poznámka',
      attachments: [
        {
        contractChangeId: '3',
          createdByClient: false,
          contractId: '300000000111111',
          name: 'jmeno prilohy3',
          contractAttachmentId: '300000002121111',
          relevantDate: '2018-02-03'
        }
      ],
      partnerName: 'Poišťovna X - BC je partner',
      changes: [
        {
          contractChangeId: '1',
          attachments: [
            {
              contractChangeId: '?',
              createdByClient: false,
              contractId: '?',
              name: 'priloha 1',
              contractAttachmentId: '?',
              relevantDate: '2018-05-31'
            }
          ],
          contractId: '300000000111112',
          newPayment: true,
          validFrom: '2017-12-13',
          signDate: '2017-12-12',
          type: 'DECREASE',
          validTo: '2019-12-13'
        }
      ],
      productGroupId: '6699',
      productGroupName: 'Životní pojištení - platná smlouva',
      accessBC: true,
      clientContractRole: 'CLIENT',
      validFrom: '24.04.2018',
      contractOwnerBirthDate: '22.10.1990',
      signDate: '22.04.2018',
      closedBC: true,
      contractOwner: 'Jan Testík',
      contractId: '300000000111112',
      contractNumber: '300000000111112',
      partnerId: '9966',
      state: 'ACTIVE',
      hasAttachments: false,
      validTo: '25.05.2030'
  },
  {
      note: 'poznámka - proposed',
      attachments: [
        {
          contractChangeId: '2',
          createdByClient: true,
          contractId: '100000000111111',
          name: 'jmeno prilohy',
          contractAttachmentId: '100000002121111',
          relevantDate: '2018-02-03'
        }
      ],
      partnerName: 'ČSOB a.s. proposed',
      changes: [
        {
          contractChangeId: '?',
          attachments: [
            {
              contractChangeId: '?',
              createdByClient: false,
              contractId: '?',
              name: '?',
              contractAttachmentId: '?',
              relevantDate: '?'
            }
          ],
          contractId: '?',
          newPayment: false,
          validFrom: '?',
          signDate: '?',
          type: 'DECREASE',
          validTo: '?'
        }
      ],
      productGroupId: '6699',
      productGroupName: 'Hypoteční úvěr - non bc',
      accessBC: false,
      clientContractRole: 'CLIENT',
      validFrom: '24.06.2018',
      contractOwnerBirthDate: '22.10.1990',
      closedBC: false,
      contractOwner: 'Jan Testík',
      contractId: '200000000111112',
      contractNumber: '200000000111112',
      partnerId: '9966',
      state: 'PROPOSED',
      hasAttachments: true,
      validTo: '25.05.2030'
  },
  {
      note: 'poznámka - teminated BC',
      attachments: [
        {
          contractChangeId: '2',
          createdByClient: false,
          contractId: '400000000111112',
          name: 'jmeno prilohy',
          contractAttachmentId: '400000002121111',
          relevantDate: '2018-02-03'
        }
      ],
      partnerName: 'Moneta - terminated',
      changes: [
        {
          contractChangeId: '?',
          attachments: [
            {
              contractChangeId: '?',
              createdByClient: true,
              contractId: '?',
              name: '?',
              contractAttachmentId: '?',
              relevantDate: '?'
            }
          ],
          contractId: '?',
          newPayment: true,
          validFrom: '?',
          signDate: '?',
          type: 'DECREASE',
          validTo: '?'
        }
      ],
      productGroupId: '6699',
      productGroupName: '2. kontrakt BC - terminated',
      accessBC: true,
      clientContractRole: 'CLIENT',
      validFrom: '24.04.2018',
      contractOwnerBirthDate: '22.10.1990',
      signDate: '22.04.2018',
      closedBC: true,
      contractOwner: 'Jan Testík',
      contractId: '400000000111112',
      contractNumber: '400000000111112',
      partnerId: '9966',
      state: 'TERMINATED',
      hasAttachments: false,
      validTo: '25.05.2018'
  }
];
export const contracts: Array<ResContract> = [
  {
      note: 'poznámka',
      attachments: [
        {
        contractChangeId: '3',
          createdByClient: false,
          contractId: '300000000111111',
          name: 'jmeno prilohy3',
          contractAttachmentId: '300000002121111',
          relevantDate: '2018-02-03'
        }
      ],
      partnerName: 'Poišťovna X - BC je partner',
      changes: [
        {
          contractChangeId: '1',
          attachments: [
            {
              contractChangeId: '?',
              createdByClient: false,
              contractId: '?',
              name: 'priloha 1',
              contractAttachmentId: '?',
              relevantDate: '2018-05-31'
            }
          ],
          contractId: '300000000111112',
          newPayment: true,
          validFrom: '2017-12-13',
          signDate: '2017-12-12',
          type: 'DECREASE',
          validTo: '2019-12-13'
        }
      ],
      productGroupId: '6699',
      productGroupName: 'Životní pojištení - platná smlouva',
      accessBC: true,
      clientContractRole: 'CLIENT',
      validFrom: '24.04.2018',
      contractOwnerBirthDate: '22.10.1990',
      signDate: '22.04.2018',
      closedBC: true,
      contractOwner: 'Jan Testík',
      contractId: '300000000111112',
      contractNumber: '300000000111112',
      partnerId: '9966',
      state: 'ACTIVE',
      hasAttachments: false,
      validTo: '25.05.2030'
  },
  {
      note: 'poznámka - proposed',
      attachments: [
        {
          contractChangeId: '2',
          createdByClient: true,
          contractId: '100000000111111',
          name: 'jmeno prilohy',
          contractAttachmentId: '100000002121111',
          relevantDate: '2018-02-03'
        }
      ],
      partnerName: 'ČSOB a.s. proposed',
      changes: [
        {
          contractChangeId: '?',
          attachments: [
            {
              contractChangeId: '?',
              createdByClient: false,
              contractId: '?',
              name: '?',
              contractAttachmentId: '?',
              relevantDate: '?'
            }
          ],
          contractId: '?',
          newPayment: false,
          validFrom: '?',
          signDate: '?',
          type: 'DECREASE',
          validTo: '?'
        }
      ],
      productGroupId: '6699',
      productGroupName: 'Hypoteční úvěr - non bc',
      accessBC: false,
      clientContractRole: 'CLIENT',
      validFrom: '24.06.2018',
      contractOwnerBirthDate: '22.10.1990',
      closedBC: false,
      contractOwner: 'Jan Testík',
      contractId: '200000000111112',
      contractNumber: '200000000111112',
      partnerId: '9966',
      state: 'PROPOSED',
      hasAttachments: true,
      validTo: '25.05.2030'
  },
  {
      note: 'poznámka - teminated BC',
      attachments: [
        {
          contractChangeId: '2',
          createdByClient: false,
          contractId: '400000000111112',
          name: 'jmeno prilohy',
          contractAttachmentId: '400000002121111',
          relevantDate: '2018-02-03'
        }
      ],
      partnerName: 'Moneta - terminated',
      changes: [
        {
          contractChangeId: '?',
          attachments: [
            {
              contractChangeId: '?',
              createdByClient: true,
              contractId: '?',
              name: '?',
              contractAttachmentId: '?',
              relevantDate: '?'
            }
          ],
          contractId: '?',
          newPayment: true,
          validFrom: '?',
          signDate: '?',
          type: 'DECREASE',
          validTo: '?'
        }
      ],
      productGroupId: '6699',
      productGroupName: '2. kontrakt BC - terminated',
      accessBC: true,
      clientContractRole: 'CLIENT',
      validFrom: '24.04.2018',
      contractOwnerBirthDate: '22.10.1990',
      signDate: '22.04.2018',
      closedBC: true,
      contractOwner: 'Jan Testík',
      contractId: '400000000111112',
      contractNumber: '400000000111112',
      partnerId: '9966',
      state: 'TERMINATED',
      hasAttachments: false,
      validTo: '25.05.2018'
  }
];
export const finances: ResFinance = {
  financialPlans: [{
    clientId: null,
    financialPlanId: 996688,
    dateOfRelevance: '22.01.2018',
    consultant: 'Jozef Konzultantovič',
  }],
  financialPlanAttachment: {
    financialPlanId: '996688',
    fpAttachmentId: '23321',
    fpAttachmentName: 'Finanční plán 1',
    fpAttachmentFile: 'cid:1484930711481',
    urlAddress: null
  },
  financialSituations: {
    financialSituation: {
      financialSituationId: 1,
      clientId: null,
      financialBalance: null,
      dateCreated: '2018-05-24T18:28:23.000+02:00',
      lastUpdated: '2018-05-25T11:49:24.000+02:00'
    }
  },
  regularExpenses: { regularExpense: [
    {
      regExpenseId: 1,
      regExpAmount: 1000,
      regExpDescription: 'string',
      regExpFrequency: 'string',
      regExpCategory: 'jiné',
      validFrom: 'string',
      validTo: 'string'
    },
    {
      regExpenseId: 2,
      regExpAmount: 2000,
      regExpDescription: 'string',
      regExpFrequency: 'string',
      regExpCategory: 'zábava',
      validFrom: 'string',
      validTo: 'string'
    },
    {
      regExpenseId: 3,
      regExpAmount: 5000,
      regExpDescription: 'string',
      regExpFrequency: 'string',
      regExpCategory: 'potraviny',
      validFrom: 'string',
      validTo: 'string'
    }
  ] },
  regularIncomes: { regularIncome: [
    {
      regIncomeId: 1,
      regIncAmount: 10000,
      regIncFrequency: 'string',
      regIncomeDescription: 'string',
      validFrom: 'string',
      validTo: 'string'
    }
  ] },
  oneTimeExpense: {
    oneTimeIncomeId: 'string',
    financialSituationId: 'string',
    dateOfOTIncome: 'string',
    oneTimeIncAmount: 1,
    oneTimeIncDescription: 'string'
  },
  oneTimeIncome: {
    regIncomeId: 'string',
    financialSituationId: 'string',
    dateOfOTIncome: 'string',
    regIncomeAmount: 1,
    regIncDescription: 'string'
  }
};
export const notificationPrescripts: Array<ResNotificationPrescript> = [
  // {
  //   DateOfStart: '2018-05-10T02:00:00.000+02:00',
  //   Description: 'desc',
  //   StandingOrder: true,
  //   TerminationDate: '2018-12-10T01:00:00.000+01:00',
  //   PrescriptId: 1,
  //   Frequency: 'Daily',
  //   Enabled: true,
  //   TypeOfNotification: 'Payment',
  //   PaymentCurrency: 'Kc',
  //   ContractId: '456',
  //   PaymentAmount: 2500
  // },
  // {
  //   DateOfStart: '2018-05-10T02:00:00.000+02:00',
  //   Description: 'desc',
  //   StandingOrder: true,
  //   TerminationDate: '2018-12-10T01:00:00.000+01:00',
  //   PrescriptId: 2,
  //   Frequency: 'Daily',
  //   Enabled: true,
  //   TypeOfNotification: 'Payment',
  //   PaymentCurrency: 'Kc',
  //   ContractId: '456',
  //   PaymentAmount: 1025
  // },
  // {
  //   DateOfStart: '2018-05-10T02:00:00.000+02:00',
  //   Description: 'desc',
  //   StandingOrder: true,
  //   TerminationDate: '2018-12-10T01:00:00.000+01:00',
  //   PrescriptId: 6,
  //   Frequency: 'Daily',
  //   Enabled: true,
  //   TypeOfNotification: 'Payment',
  //   PaymentCurrency: 'Kc',
  //   ContractId: '1456',
  //   PaymentAmount: 105
  // },
  // {
  //   DateOfStart: '2018-05-13T02:00:00.000+02:00',
  //   Description: 'desc',
  //   StandingOrder: true,
  //   TerminationDate: '2018-12-10T01:00:00.000+01:00',
  //   PrescriptId: 7,
  //   Frequency: 'Daily',
  //   Enabled: true,
  //   TypeOfNotification: 'Payment',
  //   PaymentCurrency: 'Kc',
  //   ContractId: '1456',
  //   PaymentAmount: 105
  // },
  // {
  //   DateOfStart: '2018-05-13T02:00:00.000+02:00',
  //   Description: 'desc',
  //   StandingOrder: true,
  //   TerminationDate: '2018-12-10T01:00:00.000+01:00',
  //   PrescriptId: 8,
  //   Frequency: 'Daily',
  //   Enabled: true,
  //   PaymentCurrency: 'Kc',
  //   ContractId: '1456',
  //   PaymentAmount: 105
  // },
  // {
  //   DateOfStart: '13.5. 2018', // TODO '2018-05-13T02:00:00.000+02:00'
  //   Description: 'not contract notif 1',
  //   StandingOrder: true,
  //   PrescriptId: 9,
  //   Frequency: 'Daily',
  //   Enabled: true
  // },
  // {
  //   DateOfStart: '15.5. 2018', // '2018-05-15T02:00:00.000+02:00'
  //   Description: 'not contract notif 2',
  //   StandingOrder: true,
  //   PrescriptId: 10,
  //   Frequency: 'Daily',
  //   Enabled: true
  // }
];
export const notificationInstances: Array<ResNotificationInstance> = [];
// export const users: Array<ExtendedUser> = [];
export const sessions: Array<Session> = [];

// export interface ExtendedUser extends ReqBodyUsernamePassword {
//   clientId: 'string';
// }

export interface Session {
  userName: 'string';
}




// export const clients: Array<ResClient> = [];
// export const clients: Array<ResClient> = [];
// export const clients: Array<ResClient> = [];
// export const clients: Array<ResClient> = [];
// export const clients: Array<ResClient> = [];
// export const clients: Array<ResClient> = [];
// export const clients: Array<ResClient> = [];

// export const cars: Cars = [
//   {
//     id: '-L6ubMOHdZJolhd0IQaT',
//     brand: 'Škoda',
//     model: 'Superb',
//     fuel: 'diesel',
//     power: 120,
//     description: 'Perfect czech car'
//   },
//   {
//     id: '-L6ucUBi9wWPjs_htuXJ',
//     brand: 'Volkswagen',
//     model: 'Golf',
//     fuel: 'petrol',
//     power: 100,
//     description: 'Basic small car with long history'
//   },
//   {
//     id: '-L6uctIP9LRh3qA1xncn',
//     brand: 'Porsche',
//     model: '911',
//     fuel: 'petrol',
//     power: 270
//   }
// ];

//export const news: Array<NewsItem> = [
//  {
//    newsId: '1',
//    date: '12.12.2017',
//    title: 'Nový portál',
//    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed in nulla id sem egestas convallis. Sed iaculis maximus elit eget finibus. Phasellus vehicula pharetra viverra. Sed convallis ac risus sit amet pulvinar. Donec convallis at neque non consectetur. Suspendisse sit amet fermentum nisl. Aliquam volutpat, enim quis rutrum cursus, neque enim porttitor lacus, et eleifend erat erat at mi. Etiam luctus quam at dictum convallis. Mauris nec massa lectus. Integer molestie, tellus vitae egestas pulvinar, nisl enim cursus nunc, at semper purus nulla nec velit. Vivamus finibus, mi sed porta laoreet, orci purus scelerisque felis, non bibendum elit diam ut mauris. Suspendisse ultrices, nulla sit amet fringilla malesuada, sem orci tempus est, eget iaculis risus ante nec orci. Mauris finibus quam orci, quis hendrerit nibh tincidunt ultrices. Pellentesque viverra volutpat lacinia. Proin bibendum sed purus venenatis varius. Phasellus at consequat tellus.'
//  },  
//  {
//    newsId: '2',
//    date: '03.03.2018',
//    title: 'Novinky na trhu',
//    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed in nulla id sem egestas convallis. Sed iaculis maximus elit eget finibus. Phasellus vehicula pharetra viverra. Sed convallis ac risus sit amet pulvinar. Donec convallis at neque non consectetur. Suspendisse sit amet fermentum nisl. Aliquam volutpat, enim quis rutrum cursus, neque enim porttitor lacus, et eleifend erat erat at mi. Etiam luctus quam at dictum convallis. Mauris nec massa lectus. Integer molestie, tellus vitae egestas pulvinar, nisl enim cursus nunc, at semper purus nulla nec velit. Vivamus finibus, mi sed porta laoreet, orci purus scelerisque felis, non bibendum elit diam ut mauris. Suspendisse ultrices, nulla sit amet fringilla malesuada, sem orci tempus est, eget iaculis risus ante nec orci. Mauris finibus quam orci, quis hendrerit nibh tincidunt ultrices. Pellentesque viverra volutpat lacinia. Proin bibendum sed purus venenatis varius. Phasellus at consequat tellus.'
//  },  
//  {
//    newsId: '3',
//    date: '20.04.2018',
//    title: 'Akce na poradenství',
//    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed in nulla id sem egestas convallis. Sed iaculis maximus elit eget finibus. Phasellus vehicula pharetra viverra. Sed convallis ac risus sit amet pulvinar. Donec convallis at neque non consectetur. Suspendisse sit amet fermentum nisl. Aliquam volutpat, enim quis rutrum cursus, neque enim porttitor lacus, et eleifend erat erat at mi. Etiam luctus quam at dictum convallis. Mauris nec massa lectus. Integer molestie, tellus vitae egestas pulvinar, nisl enim cursus nunc, at semper purus nulla nec velit. Vivamus finibus, mi sed porta laoreet, orci purus scelerisque felis, non bibendum elit diam ut mauris. Suspendisse ultrices, nulla sit amet fringilla malesuada, sem orci tempus est, eget iaculis risus ante nec orci. Mauris finibus quam orci, quis hendrerit nibh tincidunt ultrices. Pellentesque viverra volutpat lacinia. Proin bibendum sed purus venenatis varius. Phasellus at consequat tellus.'
//  },  
//  {
//    newsId: '4',
//    date: '21.06.2018',
//    title: 'Novinky z oblasti pojištění',
//    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed in nulla id sem egestas convallis. Sed iaculis maximus elit eget finibus. Phasellus vehicula pharetra viverra. Sed convallis ac risus sit amet pulvinar. Donec convallis at neque non consectetur. Suspendisse sit amet fermentum nisl. Aliquam volutpat, enim quis rutrum cursus, neque enim porttitor lacus, et eleifend erat erat at mi. Etiam luctus quam at dictum convallis. Mauris nec massa lectus. Integer molestie, tellus vitae egestas pulvinar, nisl enim cursus nunc, at semper purus nulla nec velit. Vivamus finibus, mi sed porta laoreet, orci purus scelerisque felis, non bibendum elit diam ut mauris. Suspendisse ultrices, nulla sit amet fringilla malesuada, sem orci tempus est, eget iaculis risus ante nec orci. Mauris finibus quam orci, quis hendrerit nibh tincidunt ultrices. Pellentesque viverra volutpat lacinia. Proin bibendum sed purus venenatis varius. Phasellus at consequat tellus.'
//  },  
//  {
//    newsId: '5',
//    date: '03.07.2018',
//    title: 'Novinky na trhu',
//    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed in nulla id sem egestas convallis. Sed iaculis maximus elit eget finibus. Phasellus vehicula pharetra viverra. Sed convallis ac risus sit amet pulvinar. Donec convallis at neque non consectetur. Suspendisse sit amet fermentum nisl. Aliquam volutpat, enim quis rutrum cursus, neque enim porttitor lacus, et eleifend erat erat at mi. Etiam luctus quam at dictum convallis. Mauris nec massa lectus. Integer molestie, tellus vitae egestas pulvinar, nisl enim cursus nunc, at semper purus nulla nec velit. Vivamus finibus, mi sed porta laoreet, orci purus scelerisque felis, non bibendum elit diam ut mauris. Suspendisse ultrices, nulla sit amet fringilla malesuada, sem orci tempus est, eget iaculis risus ante nec orci. Mauris finibus quam orci, quis hendrerit nibh tincidunt ultrices. Pellentesque viverra volutpat lacinia. Proin bibendum sed purus venenatis varius. Phasellus at consequat tellus.'
//  },  
//  {
//    newsId: '6',
//    date: '31.07.2018',
//    title: 'Novinky z oblasti hypoték',
//    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed in nulla id sem egestas convallis. Sed iaculis maximus elit eget finibus. Phasellus vehicula pharetra viverra. Sed convallis ac risus sit amet pulvinar. Donec convallis at neque non consectetur. Suspendisse sit amet fermentum nisl. Aliquam volutpat, enim quis rutrum cursus, neque enim porttitor lacus, et eleifend erat erat at mi. Etiam luctus quam at dictum convallis. Mauris nec massa lectus. Integer molestie, tellus vitae egestas pulvinar, nisl enim cursus nunc, at semper purus nulla nec velit. Vivamus finibus, mi sed porta laoreet, orci purus scelerisque felis, non bibendum elit diam ut mauris. Suspendisse ultrices, nulla sit amet fringilla malesuada, sem orci tempus est, eget iaculis risus ante nec orci. Mauris finibus quam orci, quis hendrerit nibh tincidunt ultrices. Pellentesque viverra volutpat lacinia. Proin bibendum sed purus venenatis varius. Phasellus at consequat tellus.'
//  }
//];
