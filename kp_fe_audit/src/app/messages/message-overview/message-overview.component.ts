import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ChangeDetectorRef, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CPMessage } from '../shared/models/cp-message.model';
import { CPMessageAttachment } from '../shared/models/cp-message-attachment.model';
import { MessagesService } from '../../shared/messages.service';
import { b64toBlob, createAndDownloadBlobFile } from '../../utils/common.utils';
import { LoggerService } from '../../shared/logger.service';
import { SharedService } from '../../shared/shared.service';
import { AuthService } from '../../auth/shared/auth.service';
import { DomSanitizer } from '@angular/platform-browser';
import { EnvironmentConfig } from '../../environment.config';

@Component({
  selector: 'app-message-overview',
  templateUrl: './message-overview.component.html',
  styleUrls: ['./message-overview.component.css'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MessageOverviewComponent implements OnInit {

  i18NPrefix = 'messages.overview.';

  constructor(
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private messagesService: MessagesService,
    private sharedService: SharedService,
    private log: LoggerService,
    private authService: AuthService,
    private sanitizer: DomSanitizer
  ) { }

  @Input() message: CPMessage;
  editShow = false;
  hasAttachments: boolean;
  @Output() showDetail = new EventEmitter<boolean>();
  @Output() detailId = new EventEmitter<string>();

  ngOnInit() {
    this.hasAttachments = this.message.attachments === undefined ? false : true;
  }

  getAttachmentUrl(attachment: CPMessageAttachment) {
    let attachmentName = attachment.name.replace(/č/g, 'c').replace(/Č/g, 'C').replace(/Ě/g, 'E').replace(/ě/g, 'e').replace(/\(/g, '').replace(/\)/g, '').replace(/\+/g, '').replace(/ď/g, 'd').replace(/Ď/g, 'D');
    return this.sanitizer.bypassSecurityTrustUrl( EnvironmentConfig.settings.env.uriPrefix +'/messages/emailmessages/' +
      attachment.messageAttachmentId + '/'
      + this.authService.clientId + '/'
      + encodeURIComponent(attachmentName)
      + '?accessToken=' + encodeURIComponent(this.authService.accessToken));

  }

  showEdit() {
    this.showDetail.emit(true);
    this.log.debug(this.showDetail);
    this.detailId.emit(this.message.messageId);
  }

  public downloadAttachment(event, attachment: CPMessageAttachment): void {
    event.stopPropagation();
    if (!attachment) {
      return;
    }
    this.messagesService.getMessageAttachmentContent$(attachment.messageAttachmentId).subscribe((resp) => {
      const blob = b64toBlob(resp.attachment, this.sharedService.getContentType(attachment.name.split('.')[1]));

      this.sharedService.openBlobInNewTab(blob);
      createAndDownloadBlobFile(blob, attachment.name);
    }, (e) => {
      this.log.error(e);
      this.cd.markForCheck();
    });
  }

  messageDetailShow(message: number) {
    //if (this.messageDetail || this.showDetail) {
    //  if (message === 0) {
    //    this.showDetail = false;
    //  } else {
    //    this.messageDetail1 = false;
    //  }

    //} else {
    //  if (message === 0) {
    //    this.messageDetail = true;
    //  } else {
    //    this.messageDetail1 = true;
    //  }
    //}
  }

}
