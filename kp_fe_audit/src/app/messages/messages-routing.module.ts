import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MessagesComponent } from './messages.component';
import { AuthGuard } from '../auth/shared/auth-guard.service';

@NgModule({
  imports: [
    RouterModule.forChild(
      [
        // {
        //     path: '',
        //     redirectTo: 'messages',
        //     pathMatch: 'full'
        // },
        {
          path: 'messages',
          component: MessagesComponent,
          canActivate: [AuthGuard]
        },
      ]// ,
      // {
      //   enableTracing: false
      // }
    )
  ],
  exports: [
    RouterModule
  ]
})
export class MessagesRoutingModule { }
