import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ChangeDetectorRef, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CPMessage } from './shared/models/cp-message.model';
import { MessagesService } from '../shared/messages.service';
import { LoggerService } from '../shared/logger.service';
import { LoaderIndicatorService } from '../shared/LoaderIndicator.service';
import { DateFormatPipe } from 'angular2-moment';
declare function setActiveNav(id: string): Function;
declare function setActivePage(id: string, pages: number[]): Function;

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MessagesComponent implements OnInit {

  i18NPrefix = 'messages.';

  constructor(
    //private messagesService: MessagesService,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private messagesService: MessagesService,
    private log: LoggerService,
    private loaderService: LoaderIndicatorService
  ) { }

  showedMessages: CPMessage[] = [];
  messages: CPMessage[] = [];
  loadError = false;
  showDetail = false;
  detailId = '';
  filterText = '';
  pages: number[] = [];
  activePage: number = 1;

  ngOnInit() {
    setActiveNav('messages');
    this.getMessages(this.activePage);
    this.cd.markForCheck();
  }

  onShowDetailChange(show) {
    this.showDetail = show;
    if (!show) {
      this.getMessages(this.activePage);
      this.cd.markForCheck();
    }
  }

  onDetailIdSelect(selectedMessageId) {
    this.detailId = selectedMessageId;
  }


  filterUnreaded() { }

  filterNewest() { }

  getMessages(page:number) {

    this.loaderService.showLoader();
    this.messagesService.getMessages$().subscribe((messages) => {
      if (messages != null) {
        this.messages = messages;
      }
      this.messages.forEach(e => { if (e.dateSent === undefined) { e.dateSent = this.currentDateString() } });
      this.messages.forEach(msg => {
        if (msg.attachments && !(msg.attachments instanceof Array)) {
          let att = msg.attachments;
          msg.attachments = new Array();
          msg.attachments.push(att);
        }
      });
      this.filterMessages(page);
      this.log.debug(this.showedMessages);
      this.log.debug(this.messages);
      this.loadError = false;
      this.cd.markForCheck();
      this.loaderService.disableLoader();
    }, (e) => {
      this.log.error(e);
      this.loadError = true;
      this.cd.markForCheck();
      this.loaderService.disableLoader();
    });
    this.showedMessages = [];
    this.cd.markForCheck();
  }

  dateFormatSwitch(dateToConvert) {
    return new Date(dateToConvert.split('.').reverse().join('-'));
  }

  filterMessages(page) {
    this.messages.sort((a, b) => {
      if (this.dateFormatSwitch(a.dateSent) < this.dateFormatSwitch(b.dateSent)) {
        return 1;
      } else if (this.dateFormatSwitch(a.dateSent) > this.dateFormatSwitch(b.dateSent)) {
        return -1;
      } else {
        return 0;
      }
    });
    this.setPagesCount();
    this.setShowedMessages(page - 1);
  }

  public onMessageSent(): void {
    this.getMessages(this.activePage);
  }

  setPagesCount() {
    this.pages = [];
    for (let i = 1; i <= Math.ceil(this.messages.length / 10); i++) {
      this.pages.push(i);
    }
  }

  setShowedMessages(section) {
    this.showedMessages = this.getSomeTen(section);
    this.cd.markForCheck();
    this.log.debug(this.showedMessages);
    window.scroll(0, 0);
    this.activePage = section+1;
    this.loaderService.showLoader();
    setTimeout(() => { this.loaderService.disableLoader(); setActivePage(section + 1, this.pages); },1000);
  }

  getSomeTen(section) {

    let someTen: CPMessage[] = [];
    if (this.messages.length < section * 10 - 10) {
      someTen = this.messages.slice(section * 10, this.messages.length);

    }
    else {
      someTen = this.messages.slice(section * 10, (section * 10 + 10));

    }
    return someTen;
  }

  private currentDateString(): string {
    const date: Date = new Date();

    const month: number = date.getMonth() + 1;
    const monthStr: string = (month < 10) ? '0' + String(month) : String(month);
    const day: number = date.getDate();
    const dayStr: string = (day < 10) ? '0' + String(day) : String(day);

    return dayStr + '.' + monthStr + '.' + String(date.getFullYear());
  }

}
