import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { CookieXSRFStrategy, XSRFStrategy, HttpModule } from '@angular/http';

import { ConsultantMessageBoxModule } from './consultant-message-box/consultant-message-box.module';
import { MessageDetailComponent } from './message-detail/message-detail.component';
import { MessageOverviewComponent } from './message-overview/message-overview.component';
import { MessagesRoutingModule } from './messages-routing.module';
import { MessagesComponent } from './messages.component';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { AuthGuard } from '../auth/shared/auth-guard.service';
import { MessagesIntegratedService } from '../shared/messages-integrated.service';
import { MessagesService } from '../shared/messages.service';
import { FilterModule } from '../utils/filter.module';
import { ValidationModule } from '../validation/validation.module';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    HttpClientModule,
    MessagesRoutingModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    FilterModule,
    ValidationModule,
    ConsultantMessageBoxModule,
    ScrollToModule.forRoot()
  ],
  declarations: [MessagesComponent, MessageDetailComponent, MessageOverviewComponent],
  providers: [
      AuthGuard,
      {
        provide: MessagesService,
          // useClass: MessagesMockedService
        useClass: MessagesIntegratedService
      },
  ]
})
export class MessagesModule { }
