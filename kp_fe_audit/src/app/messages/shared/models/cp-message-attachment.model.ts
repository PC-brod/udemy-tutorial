import { SafeUrl } from "@angular/platform-browser";

export interface CPMessageAttachment {
  messageAttachmentId?: string;
  name?: string;
  img?: string;
}
