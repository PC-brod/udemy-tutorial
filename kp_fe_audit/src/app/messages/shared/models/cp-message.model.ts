import { CPMessageAttachment } from './cp-message-attachment.model';

export interface CPMessage {
  messageId?: string;
  dateSent?: string;
  sender?: string;
  recipient?: string;
  subject?: string;
  bodyHTML?: string;
  body?: string;
  attachments?: CPMessageAttachment[];
}
