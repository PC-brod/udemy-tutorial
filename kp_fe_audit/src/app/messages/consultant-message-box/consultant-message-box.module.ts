import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ConsultantMessageBoxComponent } from './consultant-message-box.component';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { AuthGuard } from '../../auth/shared/auth-guard.service';
import { MessagesService } from '../../shared/messages.service';
import { MessagesIntegratedService } from '../../shared/messages-integrated.service';
import { FilterModule } from '../../utils/filter.module';
import { ValidationModule } from '../../validation/validation.module';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    FilterModule,
    ValidationModule,
    ScrollToModule.forRoot()
  ],
  exports: [ConsultantMessageBoxComponent],
  declarations: [ConsultantMessageBoxComponent],
  providers: [
	AuthGuard,
	{
	  provide: MessagesService,
	  useClass: MessagesIntegratedService
	},
]
})
export class ConsultantMessageBoxModule { }
