import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ChangeDetectorRef, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CPMessage } from '../shared/models/cp-message.model';
import { CPMessageAttachment } from '../shared/models/cp-message-attachment.model';
import { MessagesService } from '../../shared/messages.service';
import { b64toBlob, createAndDownloadBlobFile, downloadMobileFile } from '../../utils/common.utils';
import { LoggerService } from '../../shared/logger.service';
import { SharedService } from '../../shared/shared.service';
import { DomSanitizer } from '@angular/platform-browser';
import { AuthService } from '../../auth/shared/auth.service';
import { EnvironmentConfig } from '../../environment.config';
declare function isSafari(): Function;

@Component({
  selector: 'app-message-detail',
  templateUrl: './message-detail.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MessageDetailComponent implements OnInit {

  i18NPrefix = 'messages.detail.';

  @Input() messageId: string;
  message: CPMessage = {};
  @Output() showDetail = new EventEmitter<boolean>();
  loadError = false;
  @Input() showedMessages: CPMessage[];
  loaded: boolean = false;


  constructor(
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private messagesService: MessagesService ,
    private sharedService: SharedService,
    private log: LoggerService,
    private sanitizer: DomSanitizer,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.showedMessages.forEach((c) => { if (c.messageId == this.messageId) { this.message = c } if (this.message.attachments) { this.message.attachments.forEach((a) => { this.getImg(a); }); } });
    setTimeout(() => { this.loaded = true; this.cd.markForCheck(); }, 1500)
  }

  close() {
    this.showDetail.emit(false);
  }

  getIsSafari() {
    return isSafari();
  }

  public downloadAttachment(attachment: CPMessageAttachment): void {
    if (!attachment) {
        return;
    }
 
    this.messagesService.getMessageAttachmentContent$(attachment.messageAttachmentId).subscribe((resp) => {
      const blob = b64toBlob(resp.attachment, this.sharedService.getContentType(attachment.name.split('.')[1]));
      this.log.debug(blob);
      this.sharedService.openBlobInNewTab(blob);
      createAndDownloadBlobFile(resp.attachment, attachment.name);
    }, (e) => {
      this.log.error(e);
      this.cd.markForCheck();
  });
  }

  getAttachmentUrl(attachment: CPMessageAttachment) {

    let attachmentName = attachment.name.replace(/č/g, 'c').replace(/Č/g, 'C').replace(/Ě/g, 'E').replace(/ě/g, 'e').replace(/\(/g, '').replace(/\)/g, '').replace(/\+/g, '').replace(/ď/g, 'd').replace(/Ď/g, 'D');
    return this.sanitizer.bypassSecurityTrustUrl(EnvironmentConfig.settings.env.uriPrefix + '/messages/emailmessages/' +
      attachment.messageAttachmentId + '/'
      + this.authService.clientId + '/'
      + encodeURIComponent(attachmentName)
      + '?accessToken=' + encodeURIComponent(this.authService.accessToken));

  }

  getImg(attachment: CPMessageAttachment){
  
    this.messagesService.getMessageAttachmentContent$(attachment.messageAttachmentId).subscribe((resp) => {
      const blob = b64toBlob(resp.attachment, this.sharedService.getContentType(attachment.name.split('.')[1]));
      this.log.debug(blob);
      this.sharedService.openBlobInNewTab(blob);
      //console.log(resp.attachment + this.sharedService.getContentType(attachment.name.split('.')[1]));
      attachment.img = 'data:' + this.sharedService.getContentType(attachment.name.split('.')[1]) + ';' + 'base64,' + resp.attachment;
    }, (e) => {
      this.log.error(e);
      this.cd.markForCheck();
      });
  
  }

  downloadMobileAttachment(attachment: CPMessageAttachment) {
    this.getImg(attachment);
    setTimeout(() => { downloadMobileFile(attachment, this.getIsSafari()); }, 3000);
    
  }

}
