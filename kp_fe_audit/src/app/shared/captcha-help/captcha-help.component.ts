import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-captcha-help',
  templateUrl: './captcha-help.component.html',
  styleUrls: ['./captcha-help.component.css'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CaptchaHelpComponent implements OnInit {

  i18NPrefix = 'captcha.';

  constructor(private translateService: TranslateService) { }

  ngOnInit() {
    this.translateService.onLangChange.subscribe((e) => {
      this.showHelp();
    });
  }

  showHelp() {
    if (document.getElementById("showHelpText")) {
      setTimeout(() => {
        if (document.getElementById("showHelpText").className === "collapsed") {
          document.getElementById("showHelpText").innerHTML = this.getThatTranslate('captcha.showHint');
        }
        else {
          document.getElementById("showHelpText").innerHTML = this.getThatTranslate('captcha.hideHint');
        }
      }, 100);
    }
    
  }

  getThatTranslate(key :string): string {
    let returnString = "";
    this.translateService.get(key).subscribe((res: string) => {
      returnString = res;
    });
    return returnString;
  }

}
