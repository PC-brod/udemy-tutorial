import { Injectable, EventEmitter, isDevMode, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { HttpClient } from '@angular/common/http';

import { Router, ActivatedRoute, Params } from '@angular/router';
import { ChangeDetectorRef, Input, Output } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { AuthService } from '../auth/shared/auth.service';
import { Observable } from 'rxjs/Observable';
import { MessagesService } from './messages.service';
import { ApiConfig } from '../api.model';
import { apiConfigToken } from '../api.di';
import { CallbackRequest } from './models/callback-request.model';
import { assign, interpolate } from '../utils/common.utils';
import { ReqMessages } from './models/req-messages.model';
import { Message } from './models/message.model';
import { CPMessage } from './models/cp-message.model';
import { EnvironmentConfig } from '../environment.config';
import { LoggerService } from './logger.service';

@Injectable()
export class MessagesIntegratedService extends MessagesService {

  constructor(
    @Inject(apiConfigToken) private config: ApiConfig,
    private http: HttpClient,
    private authService: AuthService,
    private log: LoggerService
  ) {
    super();
  }

 sendCallbackRequest$(body: CallbackRequest): Observable<any> {
  body = assign(body);
  return this.http.post(EnvironmentConfig.settings.env.uriPrefix + this.config.sendCallbackRequestUrl, body)
  .map((res) => {
    // this.log.debug(res);
    // if (!res) {
    //   throw new Error(`sendCallbackRequest error.`);
    // }
    return res;
  });
 }

 sendNotificationMessages$(body: ReqMessages): Observable<any> {
  body = assign(body);
  return this.http.post(EnvironmentConfig.settings.env.uriPrefix + this.config.sendNotificationMessagesUrl, body)
  .map((res) => {
    if (!res) {
      throw new Error(`sendNotificationMessages error.`);
    }
    return res;
  });
 }

 getMessageAttachmentContent$(messageAttachmentId: string): Observable<any> {
  return this.authService.get$(interpolate(EnvironmentConfig.settings.env.uriPrefix + this.config.getMessageAttachmentContentUrl,
    { messageAttachmentId: messageAttachmentId, clientId: this.authService.clientId }))
  .map((res) => {
    if (!res) {
      throw new Error(`getMessageAttachmentContent error.`);
    }
    return res;
  });
 }

 sendMessage$(body: Message): Observable<any> {
 return this.authService.post$(interpolate(EnvironmentConfig.settings.env.uriPrefix + this.config.sendMessageUrl,
  { clientId: this.authService.clientId }), body)
  .map(() => { return null });
 }

 getMessages$(): Observable<Array<CPMessage>> {
  return this.authService.get$(interpolate(EnvironmentConfig.settings.env.uriPrefix + this.config.getMessagesUrl,
    { clientId: this.authService.clientId }))
  .map((res) => {
    if (!res) {
      this.log.debug("zadne zpravy u tohot klienta");
    }
    return res;
  });
 }
}
