import { Injectable, EventEmitter, isDevMode, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { HttpClient } from '@angular/common/http';

import { Router, ActivatedRoute, Params } from '@angular/router';
import { ChangeDetectorRef, Input, Output } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { AuthService } from '../auth/shared/auth.service';
import { assign, interpolate } from '../utils/common.utils';
import { Observable } from 'rxjs/Observable';
import { ReqBodyGDPRConsents } from './models/req-body-gdpr-consents.model';
import { ResGDPRConsentState } from './models/res-gdpr-consent-state.model';
import { ResConsultant } from './models/res-consultant.model';
import { Subject } from '../../../node_modules/rxjs/Subject';
import { OnboardingShown } from './models/onboardingShown.model';
import { TechnicalReportModel } from './models/technical-report.model';
declare function openGdprModal(): Function;
declare function openModal(): Function;

@Injectable()
export abstract class SharedService {

  GDPRConsents: Array<ResGDPRConsentState> = [];
  GDPRModalOpened: Subject<any>;
  GDPRModalClosed: Subject<any>;
  GDPRIsOnboarding = false;
  consultantLoaded: Subject<ResConsultant>;
  consultant: ResConsultant;
  nonEditableProperty: string;

  abstract setNonEditableProperty(value);

  abstract openGDPRModal();

  abstract openCarouselModal();

  abstract getGDPRConsents(): Array<ResGDPRConsentState>;

  abstract getGDPR$(): Observable<Array<ResGDPRConsentState>>;

  abstract updateGDPR$(body: ReqBodyGDPRConsents): Observable<any>;

  abstract getConsultant$(): Observable<ResConsultant>;

  abstract loadConsultant();

  abstract toggleInfoAlert(messageKey: string, params: any, customContent?: boolean);

  abstract toggleSuccessAlert(messageKey: string, params: any, customContent?: boolean);

  abstract toggleWarningAlert(messageKey: string, params: any, customContent?: boolean);

  abstract toggleDangerAlert(messageKey: string, params: any, customContent?: boolean);

  abstract toggleAlert(messageKey: string, params: any, alertType: string, autohide: boolean): void

  abstract toggleAlertCustom(content: string, params: any, alertType: string, autohide: boolean): void

  abstract postOnboardingShown$(body?: any): Observable<any>;

  abstract getOnboardingShown$(): Observable<OnboardingShown>;

  abstract scrollToTop();

  abstract getContentType(extensionWithoutDot: string): string;

  abstract openBlobInNewTab(blob): string;

  abstract getTechnicalReport$(): Observable<TechnicalReportModel>;
}
