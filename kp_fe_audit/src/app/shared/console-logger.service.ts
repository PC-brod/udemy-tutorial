import { Injectable } from '@angular/core';
import { EnvironmentConfig } from '../environment.config';

import { Logger } from './logger.service';


const noop = (): any => undefined;

@Injectable()
export class ConsoleLoggerService implements Logger {

 	debugValue: boolean = EnvironmentConfig.settings.env.debug;

  	get info() {
    	return console.info.bind(console);
  	}

  	get warn() {
		return console.warn.bind(console);
  	}

  	get error() {
    	return console.error.bind(console);
	}
	  
	get debug() {
		if (this.debugValue) {
			return console.debug.bind(console);
		} else {
			return noop;
		}
	}
}
