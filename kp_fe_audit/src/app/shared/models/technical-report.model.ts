export interface TechnicalReportModel {
  title: string,
  message: string,
  dateFrom: string,
  dateTo:string
}
