import { CPGender } from './cp-gender.model';
import { CPImage } from './cp-image.model';

export interface ResConsultant extends CPGender {
    extension?: string;
    photo?: CPImage;
    firstName?: string;
    lastName?: string;
    contactPhone?: string;
    emailAddress?: string;
}

export namespace ResConsultant {
}
