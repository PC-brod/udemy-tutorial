export interface ResGDPRConsentState {
  consentTypeId?: string;
  consentTypeName?: string;
  consentText?: string;
  accepted?: boolean;
  dateAccepted?: string;
}
