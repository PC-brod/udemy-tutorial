import { CPNotificationMessage } from './cp-notification-message.model';
import { NotLoged } from './not-loged.model';

export interface ReqMessages extends NotLoged {
  messages?: CPNotificationMessage[];
}
