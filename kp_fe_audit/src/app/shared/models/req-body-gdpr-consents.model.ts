import { NotLoged } from './not-loged.model';
import { ResGDPRConsentState } from './res-gdpr-consent-state.model';

export interface ReqBodyGDPRConsents extends NotLoged {
  gdpr?: Array<ResGDPRConsentState>;
  clientIP?: string; // musi si dotahovat WSO2
  userAgent?: string;
  consentTimestamp?: string;
}
