export interface Message {
  subject?: string;
  body?: string;
  attachmentName?: string;
  attachmentData?: string;
  bodyHTML?: string;
  dateSent?: string;
}
