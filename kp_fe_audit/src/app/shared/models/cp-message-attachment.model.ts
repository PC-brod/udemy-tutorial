export interface CPMessageAttachment {
  messageAttachmentId?: string;
  name?: string;
}
