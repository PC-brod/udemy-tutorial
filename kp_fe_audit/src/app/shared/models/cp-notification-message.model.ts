import { NotLoged } from './not-loged.model';
import { CPNotificationType } from './cp-notification-type.model';

export interface CPNotificationMessage extends NotLoged {
  clientId?: string;
  notificationId?: string;
  contractId?: string;
  dateOfEfficiency?: string;
  notificationDescription?: string;
  paymentAmount?: string;
  paymentCurrency?: string;
  type?: CPNotificationType;
}
