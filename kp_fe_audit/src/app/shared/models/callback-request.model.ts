import { NotLoged } from './not-loged.model';

export interface CallbackRequest extends NotLoged {
  body?: string;
  emailAddress?: string;
  nameSurname?: string;
  phoneNumber?: string;
}
