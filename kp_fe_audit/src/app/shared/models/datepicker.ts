import { Component } from '@angular/core';

@Component({
    selector: 'app-datepicker',
    template: `<div class="input-group date datepicker" data-provide="datepicker" >
                            <input type="text" class="form-control">
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                        </div>`,
    styleUrls: []
})
export class DatepickerComponent {
}
