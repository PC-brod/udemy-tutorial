import { NotLoged } from './not-loged.model';

export interface ReqBodyClientId extends NotLoged {
  clientId?: string;
}
