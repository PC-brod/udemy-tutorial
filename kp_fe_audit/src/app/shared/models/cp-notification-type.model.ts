export type CPNotificationType = 'PAYMENT' | 'OTHER';

export const CPNotificationType = {
  PAYMENT: 'PAYMENT' as CPNotificationType,
  OTHER: 'OTHER' as CPNotificationType
};
