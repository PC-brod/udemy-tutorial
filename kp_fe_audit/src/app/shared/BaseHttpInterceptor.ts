
import { Injectable, Inject, InjectionToken } from '@angular/core';
import 'rxjs/add/operator/timeout';
import { Observable } from 'rxjs/Observable';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent } from '@angular/common/http';
import { TimeoutError } from 'rxjs/util/TimeoutError';

export const DEFAULT_TIMEOUT = new InjectionToken<number>('defaultTimeout');

@Injectable()
export class BaseHttpInterceptor implements HttpInterceptor {
  constructor(@Inject(DEFAULT_TIMEOUT) protected defaultTimeout) {}
  
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
	const timeout = Number(req.headers.get('timeout')) || this.defaultTimeout;
	req.headers.delete('timeout');
    return next.handle(req).timeout(timeout);
  }
}