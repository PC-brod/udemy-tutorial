import { Injectable, EventEmitter, isDevMode, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { HttpClient } from '@angular/common/http';

import { Router, ActivatedRoute, Params } from '@angular/router';
import { ChangeDetectorRef, Input, Output } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { AuthService } from '../auth/shared/auth.service';
import { Observable } from 'rxjs/Observable';
import { CallbackRequest } from './models/callback-request.model';
import { ReqMessages } from './models/req-messages.model';
import { Message } from './models/message.model';
import { CPMessage } from './models/cp-message.model';

@Injectable()
export abstract class MessagesService {

 abstract sendCallbackRequest$(body: CallbackRequest): Observable<any>;

 abstract sendNotificationMessages$(body: ReqMessages): Observable<any>;

 abstract getMessageAttachmentContent$(messageAttachmentId: string): Observable<any>;

 abstract sendMessage$(body: Message): Observable<any>;

 abstract getMessages$(): Observable<Array<CPMessage>>;
}
