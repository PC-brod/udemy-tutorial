import { ErrorHandler, Injectable, Injector} from '@angular/core';
import { SharedService } from './shared.service';
import { LoaderIndicatorService } from './LoaderIndicator.service';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {
	constructor(private injector: Injector) {}
	  
	handleError(error: any) {		
		const loaderService = this.injector.get(LoaderIndicatorService);
		loaderService.disableLoader();

      const sharedService = this.injector.get(SharedService);
      if (error.message === "Uncaught (in promise): Timeout") {
        
        return;
      }
        else {
        sharedService.toggleDangerAlert("global.errors.generic", null);
        throw error;
      }
  	}
  
}
