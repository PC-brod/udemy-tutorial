import { Injectable, EventEmitter, isDevMode, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { HttpClient } from '@angular/common/http';

import { Router, ActivatedRoute, Params } from '@angular/router';
import { ChangeDetectorRef, Input, Output } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { AuthService } from '../auth/shared/auth.service';
import { assign, interpolate } from '../utils/common.utils';
import { Observable } from 'rxjs/Observable';
import { consultants, GDPRConsents } from '../../mocks/mock';
import { MessagesService } from './messages.service';
import { ApiConfig } from '../api.model';
import { apiConfigToken } from '../api.di';
import { CallbackRequest } from './models/callback-request.model';
import { ReqMessages } from './models/req-messages.model';
import { CPMessage } from './models/cp-message.model';
import { Message } from './models/message.model';
declare function openGdprModal(): Function;
declare function openModal(): Function;

const delay = 500;

@Injectable()
export class MessagesMockedService extends MessagesService {

  constructor(
    @Inject(apiConfigToken) private config: ApiConfig,
    private http: HttpClient,
    private authService: AuthService
  ) {
    super();
  }

 sendCallbackRequest$(body: CallbackRequest): Observable<any> {
  return Observable.timer(delay).map(() => {
    return null;
  });
 }

 sendNotificationMessages$(body: ReqMessages): Observable<any> {
  return Observable.timer(delay).map(() => {
    return null;
  });
 }

 getMessageAttachmentContent$(messageAttachmentId: string): Observable<any> {
  return Observable.timer(delay).map(() => {
    return null;
  });
 }

 sendMessage$(body: Message): Observable<any> {
  return Observable.timer(delay).map(() => {
    return null;
  });
 }

 getMessages$(): Observable<Array<CPMessage>> {
  return Observable.timer(delay).map(() => {
    return null;
  });
 }
}
