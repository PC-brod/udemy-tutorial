import { Injectable, EventEmitter, isDevMode, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { HttpClient } from '@angular/common/http';

import { Router, ActivatedRoute, Params } from '@angular/router';
import { ChangeDetectorRef, Input, Output } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { AuthService } from '../auth/shared/auth.service';
import { assign, interpolate } from '../utils/common.utils';
import { Observable } from 'rxjs/Observable';
import { consultants, GDPRConsents } from '../../mocks/mock';
import { ResConsultant } from './models/res-consultant.model';
import { SharedService } from './shared.service';
import { ResGDPRConsentState } from './models/res-gdpr-consent-state.model';
import { ReqBodyGDPRConsents } from './models/req-body-gdpr-consents.model';
import { ApiConfig } from '../api.model';
import { apiConfigToken } from '../api.di';
import { EnvironmentConfig } from '../environment.config';
import { TranslateService } from '@ngx-translate/core';
import { OnboardingShown } from './models/onboardingShown.model';
import { notImplemented } from '@angular/core/src/render3/util';

declare function openGdprModal(): Function;
declare function openModal(): Function;
declare function toggleGlobalAlert(alertClass, message, autohide): Function;
declare function toggleGlobalAlertCustomContent(alertClass, globalAlertClass, message, autohide): Function;

const delay = 500;

@Injectable()
export class SharedMockedService extends SharedService {
  private contentTypes = ["audio/aac","application/x-abiword","application/x-freearc","video/x-msvideo","application/vnd.amazon.ebook","application/octet-stream","image/bmp","application/x-bzip","application/x-bzip2","application/x-csh","text/css","text/csv","application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document","application/vnd.ms-fontobject","application/epub+zip","image/gif","text/html","text/html","image/vnd.microsoft.icon","text/calendar","application/java-archive","image/jpeg","image/jpeg","text/javascript","application/json","audio/midi audio/x-midi","audio/midi audio/x-midi","application/javascript","audio/mpeg","video/mpeg","application/vnd.apple.installer+xml","application/vnd.oasis.opendocument.presentation","application/vnd.oasis.opendocument.spreadsheet","application/vnd.oasis.opendocument.text","audio/ogg","video/ogg","application/ogg","font/otf","image/png","application/pdf","application/vnd.ms-powerpoint","application/vnd.openxmlformats-officedocument.presentationml.presentation","application/x-rar-compressed","application/rtf","application/x-sh","image/svg+xml","application/x-shockwave-flash","application/x-tar","image/tiff","image/tiff","font/ttf","text/plain","application/vnd.visio","audio/wav","audio/webm","video/webm","image/webp","font/woff","font/woff2","application/xhtml+xml","application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/xml if not readable from casual users (RFC 3023, section 3)","text/xml if readable from casual users (RFC 3023, section 3)","application/vnd.mozilla.xul+xml","application/zip","video/3gpp","audio/3gpp if it doesn't contain video","video/3gpp2","audio/3gpp2 if it doesn't contain video","application/x-7z-compressed"];
  private extensions = [ ".aac",".abw",".arc",".avi",".azw",".bin",".bmp",".bz",".bz2",".csh",".css",".csv",".doc",".docx",".eot",".epub",".gif",".htm",".html",".ico",".ics",".jar",".jpeg",".jpg",".js",".json",".mid",".midi",".mjs",".mp3",".mpeg",".mpkg",".odp",".ods",".odt",".oga",".ogv",".ogx",".otf",".png",".pdf",".ppt",".pptx",".rar",".rtf",".sh",".svg",".swf",".tar",".tif",".tiff",".ttf",".txt",".vsd",".wav",".weba",".webm",".webp",".woff",".woff2",".xhtml",".xls",".xlsx",".xml",".xul",".zip",".3gp",".3g2",".7z" ];
  nonEditableProperty = "";

  setNonEditableProperty(value) {
    this.nonEditableProperty = value;
  }

  postOnboardingShown$(body?: any): Observable<any> {
    throw new Error("Method not implemented.");
  }
  getOnboardingShown$(): Observable<OnboardingShown> {
    throw new Error("Method not implemented.");
  }

  constructor(
    @Inject(apiConfigToken) private config: ApiConfig,
    private http: HttpClient,
    private authService: AuthService,
    private translate: TranslateService
  ) {
    super();
  }

  consultant: ResConsultant;
  GDPRConsents: Array<ResGDPRConsentState> = [];

  getContentType(extensionWithoutDot: string): string
  {
    let extension: string = '.' + extensionWithoutDot;
    var index = this.extensions.findIndex(x => x === extension);
    return this.contentTypes[index];
  }

  openBlobInNewTab(blob)
  {
    var objectURL = URL.createObjectURL(blob);
    return objectURL;/* window.open(objectURL, "_blank");*/
  }

 openGDPRModal() {
   console.log('shared open GDPR');
   openGdprModal();
   console.log('after shared GDPR');
 }

 openCarouselModal() {
   console.log('shared openModal');
   if (this.authService.GDPRShowed) {
     console.log('shared inner openModal');
     openModal();
     console.log('after shared inner openModal');
   }
   console.log('after shared openModal');
 }

updateGDPR$(body: ReqBodyGDPRConsents): Observable<any> {
  body = assign(body);
  return this.authService.put$(interpolate(EnvironmentConfig.settings.env.uriPrefix + this.config.updateGDPRUrl,
    { clientId: this.authService.clientId }), body)
  .map(() => null);
}

getGDPR$(): Observable<Array<ResGDPRConsentState>> {
  return Observable.timer(delay).map(() => {
    return GDPRConsents;
  });
}

getGDPRConsents(): Array<ResGDPRConsentState> {
  return this.GDPRConsents;
}

getConsultant$(): Observable<ResConsultant> {
  return Observable.timer(delay).map(() => {
    // const client = clients.findIndex((c) => c.clientID === clientId);
    // const consultant = consultants.find((c) => c.id === client.consultantId);
    // if (!consultant) {
    //   throw new Error(`Car with id ${clientId} not exists.`);
    // }
    return consultants[0];
  });
}

loadConsultant() {  }

/*
global alerts
*/

toggleInfoAlert(messageKey: string, params: any, customContent?: boolean) {
	if (customContent && customContent === true) {
	   this.toggleAlertCustom(messageKey, params, "alert-info", true);
	} else {
		this.toggleAlert(messageKey, params, "alert-info", true);
	}
}

toggleSuccessAlert(messageKey: string, params: any, customContent?: boolean) {
   if (customContent && customContent === true) {
	  this.toggleAlertCustom(messageKey, params, "alert-success", true);
   } else {
	   this.toggleAlert(messageKey, params, "alert-success", true);
   }
}

toggleWarningAlert(messageKey: string, params: any, customContent?: boolean) {
   if (customContent && customContent === true) {
	  this.toggleAlertCustom(messageKey, params, "alert-warning", true);
   } else {
	   this.toggleAlert(messageKey, params, "alert-warning", true);
   }
}

toggleDangerAlert(messageKey: string, params: any, customContent?: boolean) {
   if (customContent && customContent === true) {
	  this.toggleAlertCustom(messageKey, params, "alert-danger", false);
   } else {
	   this.toggleAlert(messageKey, params, "alert-danger", false);
   }

}

public toggleAlert(messageKey: string, params: any, alertType: string, autohide: boolean): void {
   this.translate.get(messageKey, params).subscribe(message => {
	   toggleGlobalAlert(alertType, message, autohide);
   });
}

public toggleAlertCustom(content: string, params: any, alertType: string, autohide: boolean): void {
   this.translate.get(content, params).subscribe(message => {
	   toggleGlobalAlertCustomContent(alertType, "global-alert-custom", message, autohide);
   });
}

  scrollToTop() {
     window.scroll(0, 0);
  }

  getTechnicalReport$() {
    return null;
  }

}
