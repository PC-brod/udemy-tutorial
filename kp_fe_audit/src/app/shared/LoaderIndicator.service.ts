import { Injectable } from '@angular/core';
import { Subject } from '../../../node_modules/rxjs/Subject';

@Injectable()
export class LoaderIndicatorService {

	private loading: boolean = false;
	loadingChanged: Subject<boolean>;

	constructor() {
		this.loadingChanged = new Subject<boolean>();
	}
	
	public showLoader(): void {
		this.loading = true;
		this.fireLoadingChanged();
	}

	public disableLoader(): void {
		this.loading = false;
		this.fireLoadingChanged();
	}

	private fireLoadingChanged(): void {
		this.loadingChanged.next(this.loading);
	}
}