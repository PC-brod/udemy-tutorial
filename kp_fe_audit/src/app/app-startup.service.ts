import { AppConfig } from './app.model';
import { Injectable, Injector, Inject } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs/Observable';

import { appConfigToken } from './app.di';

@Injectable()
export class AppStartupService {

  constructor(
    private translate: TranslateService,
    @Inject(appConfigToken) private appConfig: AppConfig,
    private meta: Meta,
    private title: Title
  ) {}

  load(): Promise<any> {
    let version = '1.6.2.0';
    this.appConfig.version = version;
    this.meta.addTag({
      name: 'app:version',
      content: version
    });

    const lang = 'cz';
    this.translate.setDefaultLang(lang);
    return this.translate.use(lang).first().mergeMap(() => {
      return this.translate.get('app.title').do((title) => {
        this.title.setTitle(title);
      });
    }).toPromise();
  }

  getAppVersion(): string {
    return this.appConfig.version;
  }

}
