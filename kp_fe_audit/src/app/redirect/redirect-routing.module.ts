import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { RedirectComponent } from './redirect.component';

@NgModule({
  imports: [
    RouterModule.forChild(
      [
        {
          path: 'redirect',
          component: RedirectComponent
        },
      ]
    )
  ],
  exports: [
    RouterModule
  ]
})
export class RedirectRoutingModule { }
