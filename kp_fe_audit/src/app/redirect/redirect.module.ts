import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { RedirectComponent } from './redirect.component';
import { RedirectRoutingModule } from './redirect-routing.module';

import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';

@NgModule({
  imports: [
    CommonModule,
    RedirectRoutingModule,
    TranslateModule,
    ScrollToModule.forRoot()
  ],
  declarations: [RedirectComponent],
  providers: []
})
export class RedirectModule { }
