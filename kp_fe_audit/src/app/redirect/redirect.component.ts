import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { ChangeDetectorRef, EventEmitter, Input, Output } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AuthService } from '../auth/shared/auth.service';
import { interpolate } from '../utils/common.utils';
import { SharedService } from '../shared/shared.service';
import { ApiConfig } from '../api.model';
import { apiConfigToken } from '../api.di';
import { EnvironmentConfig } from '../environment.config';
import { LoggerService } from '../shared/logger.service';
import { LoaderIndicatorService } from '../shared/LoaderIndicator.service';
import { LoginResult } from '../auth/shared/models/login-result.model';

@Component({
  selector: 'app-redirecte',
  templateUrl: './redirect.component.html',
  styleUrls: ['../../../src/assets/content/styles/main-style.css'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RedirectComponent implements OnInit {
	i18NPrefix = 'redirect.';

	constructor(
		@Inject(apiConfigToken) private config: ApiConfig,
		private authService: AuthService,
		private router: Router,
		private log: LoggerService,
		private cd: ChangeDetectorRef,
		private sharedService: SharedService,
		private loaderService: LoaderIndicatorService
	) {}

 	ngOnInit() {
		this.log.debug("redirect init");
	
		const urlParams: URLSearchParams = new URLSearchParams(this.router.url.substring(this.router.url.indexOf("?") + 1));
		const oauthCodeArr = urlParams.paramsMap.get("oauthCode");
		let oauthCode = oauthCodeArr ? oauthCodeArr[0] : undefined;

		if (oauthCode === undefined || oauthCode === null) {
			this.log.error("No oauth code is recognized.");
			return;
		}
	
		this.log.debug('oauthCode=' + oauthCode);
		oauthCode = oauthCode.replace(/%2523/g, '#');
		oauthCode = oauthCode.replace(/%40/g, '@');
		this.log.debug('decoded oauthCode=' + oauthCode);
		this.authService.redirectLogin$(oauthCode).subscribe((res: any) => {
			this.log.debug("accepted response redirectLogin: " + JSON.stringify(res));
			if (res.resultCode === 'SUCCESS') {
				this.authService.setLoginInfo(res.accessTokenExpiresIn, res.accessTokenExpiration, res.clientId, res.accessToken,
												res.inAccessRecovery, res.activated, res.sessionId, null);
				this.sharedService.loadConsultant();
				if (res.requiresSMSVerification) {
					this.cd.markForCheck();
					this.log.debug("navigating to /verify");
					this.loaderService.disableLoader();
					this.router.navigate(['/verify']);
				} else {
					this.log.debug("navigating to /selfservice");
					this.authService.login = true;
					this.cd.markForCheck();
					this.loaderService.disableLoader();
					this.router.navigate(['/selfservice']);
				}
	 		} else if (res.resultCode === 'PASSWORD_EXPIRED') {
				this.authService.setLoginInfo(res.accessTokenExpiresIn, res.accessTokenExpiration, res.clientId, res.accessToken,
											res.inAccessRecovery, res.activated, res.sessionId, null);
				this.authService.login = true;
				this.cd.markForCheck();
				this.loaderService.disableLoader();
				this.router.navigate(['/changePassword']);
			} else {
				this.authService.login = false;
				this.cd.markForCheck();
				this.loaderService.disableLoader();
			}
		}, (e) => {
			this.log.error(e);
			this.authService.relogin = false;
			this.authService.login = false;
			this.router.navigate(['/portal'], { queryParams: { redirectLogin: 'failed' } });
			this.cd.markForCheck();
		});
  }
}
