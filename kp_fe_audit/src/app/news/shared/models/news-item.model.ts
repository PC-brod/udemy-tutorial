export interface NewsItem {
  id?: string,
  dateFrom?: string,
  dateTo?: string,
  title?: string,
  content?: string,
  status?: string,
  preview?: string,
  imageIds?: any[];
}
