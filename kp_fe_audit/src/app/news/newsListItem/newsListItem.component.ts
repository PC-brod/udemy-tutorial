import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ChangeDetectorRef, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NewsItem } from '../shared/models/news-item.model';
import { LoaderIndicatorService } from '../../shared/LoaderIndicator.service';

@Component({
    selector: 'app-news-list-item',
    templateUrl: './newsListItem.component.html',
    styleUrls: ['./newsListItem.component.css']
})
export class NewsListItemComponent implements OnInit {

  i18NPrefix = 'news.list-item.';
  constructor(
    private fb: FormBuilder, 
    private cd: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  @Input() showAll: boolean;
  @Input() item: NewsItem;
  preview: string;

  ngOnInit() {
    if (this.item.preview) {
      this.preview = this.item.preview.substring(0, this.showAll ? 500 : 250) + '...';

    } else
      this.preview = this.item.content.substring(0, this.showAll ? 500 : 250) + '...';
  }


}
