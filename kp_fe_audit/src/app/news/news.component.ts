import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { ChangeDetectorRef, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NewsItem } from './shared/models/news-item.model';
import { NewsService } from './news.service';
import { LoaderIndicatorService } from '../shared/LoaderIndicator.service';

@Component({
    selector: 'app-news',
    templateUrl: './news.component.html',
    styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {

  i18NPrefix = 'news.';
  constructor(
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private newsService: NewsService
  ) { }

  @Input() showAll: boolean = false;
  newsInstances: NewsItem[] = [];

  ngOnInit() {
    this.newsInstances = [];
    this.cd.markForCheck();
    this.newsService.getNews$().subscribe((res) => {
      console.log(res);
      this.newsInstances = res;
      this.newsFilterDate();
      this.cutNewsInstances();
      this.cd.markForCheck();
    },
      (e) => {
        this.newsInstances = this.newsService.getNewsMocked();
        this.newsFilterDate();
        this.cutNewsInstances();
        this.cd.markForCheck();
        console.error(e);
      });
  }

  cutNewsInstances() {
    this.activatedRoute.queryParams.subscribe(params => {
      let show = params['showAll'];
      if (show === "true") {
        this.showAll = true;
      } else {
        this.newsInstances = this.newsInstances.slice(0, 3);
      }
    });
  }

  newsFilterDate() {
    this.newsInstances.sort((a, b) => {
      // expected date format is dd.MM.yyyy
      let aDate = a.dateFrom.split('.').reverse().join('');
      let bDate = b.dateFrom.split('.').reverse().join('');
      return bDate.localeCompare(aDate);
    });
  }

}
