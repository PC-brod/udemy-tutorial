import { Injectable, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { NewsItem } from './shared/models/news-item.model';
import { EnvironmentConfig } from '../environment.config';
import { getNewsInstancesTemporary } from './shared/newsInstances';
import { interpolate } from '../utils/common.utils';

@Injectable()
export class NewsService {
  baseUrl: string;
  constructor(
    private http: HttpClient,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
  }

  getNews$(): Observable<NewsItem[]> {
    return this.http.get(EnvironmentConfig.settings.env.getNewsUrl)
      .map((res: NewsItem[]) => {
        if (!res) {
          throw new Error(`No news found.`);
        }
        else {
          console.log(res);
        }
        return res;
      });
  }

  getNewsMocked(): NewsItem[] {
    return getNewsInstancesTemporary();
  }

  getSpecificNewsItem$(getNewsId: number) {
    return this.http.get(interpolate(EnvironmentConfig.settings.env.getNewsById,
      { newsId: getNewsId }))
      .map((res: NewsItem) => {
        if (!res) {
          throw new Error(`No news found.`);
        }
        else {
          console.log(res);
        }
        return res;
      });
  }



}



