import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ChangeDetectorRef, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NewsItem } from '../shared/models/news-item.model';
import { notBlankValidator, phoneValidator, emailValidator } from '../../validation/validation.validators';
import { trimToNull } from '../../validation/validation.utils';
import { CallbackRequest } from '../../shared/models/callback-request.model';
import { MessagesService } from '../../shared/messages.service';
import { LoggerService } from '../../shared/logger.service';
import { LoaderIndicatorService } from '../../shared/LoaderIndicator.service';
import { NewsService } from '../news.service';
import { getNewsInstancesTemporary } from '../shared/newsInstances';

@Component({
  selector: 'app-news-detail',
  templateUrl: './newsDetail.component.html'
})
export class NewsDetailComponent implements OnInit {

  i18NPrefix = 'news.detail.';
  constructor(
    private messagesService: MessagesService,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private log: LoggerService,
    private loaderService: LoaderIndicatorService,
    private newsService: NewsService
  ) { }

  item: NewsItem = {};
  news: NewsItem[] = [];
  fromList: boolean = false;
  backlink: string = '/portal'
  form: FormGroup;

  alreadySubmitted = false;
  submitting = false;
  showMessage: boolean = false;
  showMessageError: boolean = false;
  showMessageErrorS: boolean = false;
  showMessageSuccess: boolean = false;

  phoneNumber: string;
  nameSurname: string;
  emailAddress: string;
  body: string;

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {

      let list = params['list'];
      if (list === "true") {
        this.fromList = list;
        this.backlink = '/news';
      }

      let newsId = params['newsId'];
      if (newsId) {
        this.newsService.getSpecificNewsItem$(newsId).subscribe((res: NewsItem) => {
          this.item = res;
          this.cd.markForCheck();
        },
          (e) =>
          {
            this.newsService.getNewsMocked().forEach((n) => { if (n.id === newsId) this.item = n } );
            console.log(this.item);
            this.cd.markForCheck();
          });
        //getNewsInstancesTemporary().forEach((n) => { if (n.id === newsId) this.item = n; });
        //this.cd.markForCheck();
        //this.newsService.getSpecificNewsItem$(parseInt(newsId)).subscribe((resp) => {
        //  this.item = resp;
        //  this.cd.markForCheck();
        //},
        //  (e) =>
        //  {
        //    this.item = newsInstances.filter((n) => { n.newsId === newsId })[0];
        //    this.cd.markForCheck();
        //    if (this.fromList === true) {
        //      this.router.navigate(['/news'], { queryParams: { showAll: true } });
        //    } else {
        //      this.router.navigate(['/portal']);
        //    }
        //  });

      } else {
        if (this.fromList === true) {
          this.router.navigate(['/news'], { queryParams: { showAll: true } });
        } else {
          this.router.navigate(['/portal']);
        }
      }
    });

    this.buildForm();
    this.loaderService.disableLoader();
  }

  private buildForm(): void {
    this.form = this.fb.group({
      phoneNumber: [this.phoneNumber, Validators.compose([notBlankValidator, phoneValidator])],
      nameSurname: [this.nameSurname, notBlankValidator],
      emailAddress: [this.emailAddress, Validators.compose([notBlankValidator, emailValidator])],
      body: [this.body, notBlankValidator]
    });
  }

  onSubmit(form: FormGroup): void {
    this.alreadySubmitted = true;
    if (!form.valid || this.submitting) {
      return;
    }

    this.loaderService.showLoader();

    this.submitting = true;
    const value = this.form.value;
    value.phoneNumber = trimToNull(value.phoneNumber);
    value.nameSurname = trimToNull(value.nameSurname);
    value.emailAddress = trimToNull(value.emailAddress);
    value.body = trimToNull(value.body);

    const body: CallbackRequest = {
      phoneNumber: value.phoneNumber,
      nameSurname: value.nameSurname,
      emailAddress: value.emailAddress,
      body: value.body
    };

    this.messagesService.sendCallbackRequest$(body).subscribe((res) => {
      this.submitting = false;
      this.alreadySubmitted = false;
      this.form.reset();
      this.cd.markForCheck();
      this.loaderService.disableLoader();
      this.animation(false);
    }, (e) => {
      this.loaderService.disableLoader();
      this.log.error(e);
      this.submitting = false;
      this.alreadySubmitted = false;
      this.cd.markForCheck();
      this.animation(true);
    });
  }

  animation(error: boolean) {
    this.showMessage = true;
    if (error) {
      this.showMessageError = true;
      this.showMessageErrorS = true;
    }
    else {
      this.showMessageSuccess = true;
    }
    this.cd.markForCheck();
    setTimeout(() => {
      if (error) {
        this.showMessageError = false;
        setTimeout(() => { this.showMessageErrorS = false; }, 1500);
      }
      else {
        this.showMessageSuccess = false;
      }
      this.showMessage = false;
      this.cd.markForCheck();
    }, 3000);
  }
}
