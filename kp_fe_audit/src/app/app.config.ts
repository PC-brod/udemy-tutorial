import { environment } from './../environments/environment';
import { AppConfig } from './app.model';
import { ApiConfig } from './api.model';

export const appConfigValue: AppConfig = {
  version: '0.0.1'
};

export const apiConfigValue: ApiConfig = {
  // GDPR
  getGDPRUrl: `/clients/#{clientId}/gdprConsents`,
  updateGDPRUrl: `/clients/#{clientId}/gdprConsents`,

  // consultant
  getConsultantUrl: `/clients/#{clientId}/consultant`,

  // session management
  loginUrl: `/users/login`,
  logoutUrl: `/users/#{clientId}/logout`,
  redirectLoginUrl: `/auth/redirectLogin`,
  reloginUrl: `/users/relogin`, // not tested
  refreshTokenUrl: `/users/#{clientId}/refreshToken`,

  // confirmation code
  verifyConfirmationCodeUrl: `/users/#{clientId}/verifyConfirmationCode`,
  sendConfirmationCodeUrl: `/users/#{clientId}/sendConfirmationCode`, // not used
  validateLoginConfirmationCodeUrl: `/users/#{clientId}/verifyLoginCode`,

  // password
  changeUsernameUrl: `/users/#{clientId}/changeUsername`, // not tested
  changePasswordUrl: `/users/#{clientId}/changePassword`,
  updatePasswordUrl: `/users/#{clientId}/updatePassword`,
  recoverPasswordUrl: `/users/recoverPassword`,

  // user
  identifyUserUrl: `/users/recoverUsername`,
  getClientByIdUrl: `/clients/#{clientId}`,
  updateClientUrl: `/clients/#{clientId}`,
  getAMLDataUrl: `/#{clientId}`,
  setAMLDataUrl: `/#{clientId}`,
  getAMLSensitiveActivitiesUrl:`/sensitiveActivities`,

  // contracts
  getContractsUrl: `/contracts?clientId=#{clientId}`,
  createContractUrl: `/contracts?clientId=#{clientId}`,
  getContractUrl: `/contracts/#{contractId}?clientId=#{clientId}`,
  updateContractUrl: `/contracts/#{contractId}?clientId=#{clientId}`,
  deleteContractUrl: `/contracts/#{contractId}?clientId=#{clientId}`,

  // attachments
  createAttachmentUrl: `/contracts/#{contractId}/attachments?clientId=#{clientId}`,
  getAttachmentContentUrl: `/contracts/attachments/#{attachmentId}?clientId=#{clientId}`,
  deleteAttachmentUrl: `/contracts/attachments/#{attachmentId}?clientId=#{clientId}`,

  // notifications
  getNotificationsListUrl: `/notifications/prescripts?clientId=#{clientId}`,
  getNotificationPrescriptUrl: `/notifications/prescripts/#{notPrescriptId}?clientId=#{clientId}`,
  createNotificationPrescriptUrl: `/notifications/prescripts?clientId=#{clientId}`,
  deleteNotificationUrl: `/notifications/prescripts/#{notPrescriptId}?clientId=#{clientId}`,
  updateNotificationPrescriptUrl: `/notifications/prescripts/#{notPrescriptId}?clientId=#{clientId}`,
  getNotificationInstancesUrl: `/notifications/prescripts/instances?clientId=#{clientId}`,
  updateNotificationInstanceUrl: `/notifications/prescripts/instances/#{notificationId}?clientId=#{clientId}`,

  // codelists
  getListOfCodesProductGroupsUrl: `/codelists/productGroups`,
  getListOfCodesPartnersUrl: `/codelists/partners`,

  // finance
  getFinancesUrl: `/finances?clientId=#{clientId}`,
  financesPostUrl: `/finances?clientId=#{clientId}`,
  financesRegularExpensesPostUrl: `/finances/#{financialSituationId}/regularExpenses?clientId=#{clientId}`,
  financesRegularExpensesDeleteUrl: `/finances/regularExpenses/#{regularExpenseId}?clientId=#{clientId}`,
  financesFinancialSituationIdPutUrl: `/finances/#{financialSituationId}?clientId=#{clientId}`,
  financesFinancialSituationIdDeleteUrl: `/finances/#{financialSituationId}?clientId=#{clientId}`,
  getFinancialPlanAttachmentUrl: `/finances/financialPlans/attachments/#{attachmentId}?clientId=#{clientId}`,
  postRequestFinancialPlanUrl: `/finances/requestFinancialPlan?clientId=#{clientId}`,

  // messages
  sendCallbackRequestUrl: `/messages/callback`,
  sendNotificationMessagesUrl: `/messages/notificationMessages`, // not used
  getMessageAttachmentContentUrl: `/messages/emailmessages/#{messageAttachmentId}?clientId=#{clientId}`,
  sendMessageUrl: `/messages/contactformmessages?clientId=#{clientId}`,
  getMessagesUrl: `/messages/emailmessages?clientId=#{clientId}`,

  // onboarding

  postOnboardingShownUrl: '/users/#{clientId}/onboarding_shown',
  getOnboardingShownUrl:'/users/#{clientId}/onboarding_shown',

  // OAuth2
  accessTokenUrl: `/auth/#{clientId}`

};
