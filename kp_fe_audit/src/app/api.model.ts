export interface ApiConfig {
  getListOfCodesPartnersUrl: string;
  getListOfCodesProductGroupsUrl: string;
  getClientByIdUrl: string;
  getConsultantUrl: string;
  getGDPRUrl: string;
  updateClientUrl: string;
  updateGDPRUrl: string;
  createAttachmentUrl: string;
  createContractUrl: string;
  deleteAttachmentUrl: string;
  deleteContractUrl: string;
  getAttachmentContentUrl: string;
  getContractUrl: string;
  getContractsUrl: string;
  updateContractUrl: string;
  financesFinancialSituationIdDeleteUrl: string;
  financesFinancialSituationIdPutUrl: string;
  financesPostUrl: string;
  financesRegularExpensesPostUrl: string;
  financesRegularExpensesDeleteUrl: string;
  getFinancesUrl: string;
  getFinancialPlanAttachmentUrl: string;
  postRequestFinancialPlanUrl: string;
  redirectLoginUrl: string;
  createNotificationPrescriptUrl: string;
  deleteNotificationUrl: string;
  getNotificationInstancesUrl: string;
  getNotificationPrescriptUrl: string;
  getNotificationsListUrl: string;
  updateNotificationInstanceUrl: string;
  updateNotificationPrescriptUrl: string;
  changePasswordUrl: string;
  updatePasswordUrl: string;
  changeUsernameUrl: string;
  identifyUserUrl: string;
  loginUrl: string;
  logoutUrl: string;
  recoverPasswordUrl: string;
  sendConfirmationCodeUrl: string;
  verifyConfirmationCodeUrl: string;
  validateLoginConfirmationCodeUrl: string;
  accessTokenUrl: string;
  reloginUrl: string;
  refreshTokenUrl: string;
  sendCallbackRequestUrl?: string;
  sendNotificationMessagesUrl?: string;
  getMessageAttachmentContentUrl?: string;
  sendMessageUrl?: string;
  getMessagesUrl?: string;
  postOnboardingShownUrl?: string;
  getOnboardingShownUrl?: string;
  getAMLDataUrl?: string,
  getAMLSensitiveActivitiesUrl?: string,
  setAMLDataUrl?: string
}
