import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpStatus404Component } from './http-status-404/http-status-404.component';

import { PasswordForgotComponent } from './auth/password-forgot/password-forgot.component';
import { PasswordVerifyComponent } from './auth/password-verify/password-verify.component';
import { InsuranceIframeComponent } from './portal/insurance/iframe/insuranceIframe.component';
import { InsuranceFormComponent } from './portal/insurance/form/insuranceForm.component';
import { NewsDetailComponent } from './news/newsDetail/newsDetail.component';
import { NewsComponent } from './news/news.component';
import { LoginComponent } from './auth/login/login.component';
import { GdprConsentsComponent } from './portal/gdpr-consents/gdpr-consents.component';
import { PasswordChangeComponent } from './auth/password-change/password-change.component';
import { AuthGuard } from './auth/shared/auth-guard.service';
import { PortalComponent } from './portal/portal.component';
import { UsernameRecoveryComponent } from './auth/username-recovery/username-recovery.component';
import { PasswordUpdateComponent } from './auth/password-update/password-update.component';
import { TermConditionsNewPageComponent } from './portal/term-conditions-new-page/term-conditions-new-page.component';
import { SelfserviceComponent } from './selfservice/selfservice.component';
import { OnboardingComponent } from './onboarding/onboarding.component';
import { MessagesComponent } from './messages/messages.component';
import { FinanceComponent } from './finance/finance.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BinderComponent } from './binder/binder.component';
import { ProfileComponent } from './profile/profile.component';
import { UpdateVerifyComponent } from './auth/update-verify/update-verify.component';
import { RedirectComponent } from './redirect/redirect.component';
import { AmlComponent } from './aml/aml.component';

@NgModule({
  imports: [
    RouterModule.forRoot(
      [
        {
            path: 'forgot',
            component: PasswordForgotComponent,
            canActivate: [AuthGuard]
        },
        {
            path: 'recover',
            component: UsernameRecoveryComponent,
            canActivate: [AuthGuard]
        },
        {
            path: 'verify',
            component: PasswordVerifyComponent,
            canActivate: [AuthGuard]
        },
        {
          path: 'selfservice',
          component: SelfserviceComponent,
          canActivate: [AuthGuard]
        },
        {
          path: 'onboarding',
          component: OnboardingComponent,
          canActivate: [AuthGuard]
        },
        {
          path: 'messages',
          component: MessagesComponent,
          canActivate: [AuthGuard]
        },
        {
          path: 'finance',
          component: FinanceComponent,
          canActivate: [AuthGuard]
        },
        {
          path: 'dashboard',
          component: DashboardComponent,
          canActivate: [AuthGuard]
        },
        {
          path: 'binder',
          component: BinderComponent,
          canActivate: [AuthGuard]
        },
        {
            path: 'login',
            component: LoginComponent,
            canActivate: [AuthGuard]
        },
        {
            path: 'login/:showlogin',
            component: LoginComponent,
            canActivate: [AuthGuard]
        },
        // {
        //     path: 'changepassword',
        //     component: ChangePasswordComponent,
        //     canActivate: [AuthGuard]
        // },
        {
            path: 'updatePassword',
            component: PasswordUpdateComponent,
            canActivate: [AuthGuard]
        },
        {
            path: 'changePassword',
            component: PasswordChangeComponent,
            canActivate: [AuthGuard]
        },
        {
            path: 'verify',
            component: PasswordVerifyComponent,
            canActivate: [AuthGuard]
        },
        {
            path: 'verifyUpdate',
            component: UpdateVerifyComponent,
            canActivate: [AuthGuard]
        },
        {
            path: 'forgot',
            component: PasswordForgotComponent,
            canActivate: [AuthGuard]
        },
        {
            path: 'recover',
            component: UsernameRecoveryComponent,
            canActivate: [AuthGuard]
        },
        {
          path: 'profile',
          component: ProfileComponent,
          canActivate: [AuthGuard]
        },
        {
            path: 'changePassword',
            component: PasswordChangeComponent,
            canActivate: [AuthGuard]
        },
        {
            path: 'updatePassword',
            component: PasswordUpdateComponent,
            canActivate: [AuthGuard]
        },
        {
            path: 'login',
            component: LoginComponent,
            canActivate: [AuthGuard]
        },
        {
            path: 'iframe',
            component: InsuranceIframeComponent,
            canActivate: [AuthGuard]
        },
        {
            path: 'form',
            component: InsuranceFormComponent,
            canActivate: [AuthGuard]
        },
        {
            path: 'news',
            component: NewsComponent,
            canActivate: [AuthGuard]
        },
        {
          path: 'termConditions',
          component: TermConditionsNewPageComponent,
        //   canActivate: [AuthGuard]
        },
        {
            path: 'newsDetail',
            component: NewsDetailComponent,
            canActivate: [AuthGuard]
        },
        {
            path: 'portal',
            component: PortalComponent,
            canActivate: [AuthGuard]
        },
        {
            path: 'portal/logout',
            component: PortalComponent,
            canActivate: [AuthGuard]
        },
        {
          path: 'binder/:element',
          component: BinderComponent
        },
        {
            path: 'redirect',
            component: RedirectComponent
        },
        {
          path: 'formAml',
          component: AmlComponent,
          canActivate: [AuthGuard]
        },
        {
            path: '',
            component: PortalComponent,
            canActivate: [AuthGuard]
        },
        {
          path: '**',
          component: HttpStatus404Component
      },
      ],
      {
        enableTracing: false
      }
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
