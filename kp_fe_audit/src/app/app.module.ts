import { HttpClient, HttpClientModule } from '@angular/common/http';
import { APP_INITIALIZER, NgModule, LOCALE_ID } from '@angular/core';
import { CookieXSRFStrategy, XSRFStrategy, HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ValidationModule } from './validation/validation.module';
import { DatepickerComponent } from '../app/shared/models/datepicker';
import { AppRoutingModule } from './app-routing.module';
import { AppStartupService } from './app-startup.service';
import { AppComponent } from './app.component';
import { appConfigValue, apiConfigValue } from './app.config';
import { appConfigToken } from './app.di';
import { apiConfigToken } from './api.di';
import { AppConfig } from './app.model';
import { BinderModule } from './binder/binder.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { FinanceModule } from './finance/finance.module';
import { HttpStatus404Component } from './http-status-404/http-status-404.component';
import { MessagesModule } from './messages/messages.module';
import { OnboardingModule } from './onboarding/onboarding.module';
import { CarouselCardsComponent } from './portal/carouselCards/carouselCards.component';
import { InsuranceFormComponent } from './portal/insurance/form/insuranceForm.component';
import { InsuranceIframeComponent } from './portal/insurance/iframe/insuranceIframe.component';
import { NewsComponent } from './news/news.component';
import { NewsDetailComponent } from './news/newsDetail/newsDetail.component';
import { NewsListItemComponent } from './news/newsListItem/newsListItem.component';
import { PasswordForgotComponent } from './auth/password-forgot/password-forgot.component';
import { PasswordVerifyComponent } from './auth/password-verify/password-verify.component';
import { PortalComponent } from './portal/portal.component';
import { ProfileModule } from './profile/profile.module';
import { SelfserviceModule } from './selfservice/selfservice.module';
import { RedirectModule } from './redirect/redirect.module';
import { AuthService } from './auth/shared/auth.service';
import { AuthMockedService } from './auth/shared/auth-mocked.service';
import { AuthIntegratedService } from './auth/shared/auth-integrated.service';
import { AuthModule } from './auth/auth.module';
import { LoginComponent } from './auth/login/login.component';
import { PasswordChangeComponent } from './auth/password-change/password-change.component';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { AuthGuard } from './auth/shared/auth-guard.service';
import { RegistrationComponent } from './auth/registration/registration.component';
import { InnerLoginComponent } from './auth/login/inner-login/inner-login.component';
import { UsernameRecoveryComponent } from './auth/username-recovery/username-recovery.component';
import { PasswordUpdateComponent } from './auth/password-update/password-update.component';
import { TermConditionsComponent } from './portal/term-conditions/term-conditions.component';
import { TermConditionsNewPageComponent } from './portal/term-conditions-new-page/term-conditions-new-page.component';
import { UpdateVerifyComponent } from './auth/update-verify/update-verify.component';
import { SharedService } from './shared/shared.service';
import { GdprConsentsComponent } from './portal/gdpr-consents/gdpr-consents.component';
import { SingleConsentComponent } from './portal/gdpr-consents/single-consent/single-consent.component';
import { SharedIntegratedService } from './shared/shared-integrated.service';
import { CookieService } from 'ngx-cookie-service';
import { MessagesService } from './shared/messages.service';
import { MessagesIntegratedService } from './shared/messages-integrated.service';
import { EnvironmentConfig } from './environment.config';
import { DropzoneModule } from 'ngx-dropzone-wrapper';
import { DROPZONE_CONFIG } from 'ngx-dropzone-wrapper';
import { DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
import { DatePipe, DecimalPipe } from '@angular/common';
import { LoggerService } from './shared/logger.service';
import { ConsoleLoggerService } from './shared/console-logger.service';
import { RecaptchaModule } from 'ng-recaptcha';
import { RecaptchaFormsModule } from 'ng-recaptcha/forms';
import { GlobalErrorHandler } from './shared/GlobalErrorHandler.service';
import { ErrorHandler } from '@angular/core';
import { NgxLoadingModule } from 'ngx-loading';
import { LoaderIndicatorService } from './shared/LoaderIndicator.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { BaseHttpInterceptor, DEFAULT_TIMEOUT } from './shared/BaseHttpInterceptor';
import { registerLocaleData } from '@angular/common'
import localeCs from '@angular/common/locales/cs';
import { RECAPTCHA_LANGUAGE } from 'ng-recaptcha';
import { CaptchaHelpComponent } from './shared/captcha-help/captcha-help.component';
import { NewsService } from './news/news.service';
import { AmlComponent } from './aml/aml.component';
import { FirstPageComponent } from './aml/first-page/first-page.component';
import { SecondPageComponent } from './aml/second-page/second-page.component';
import { ThirdPageComponent } from './aml/third-page/third-page.component';
import { AmlModule } from './aml/aml.module';

registerLocaleData(localeCs);

export function initializeApp(envConfig: EnvironmentConfig) {
  return () => envConfig.load();
}

export function translateLoaderFactory(http: HttpClient, appConfig: AppConfig) {
  return new TranslateHttpLoader(http as any, './assets/i18n/', `.json?version=${appConfig.version}`);
}

export function initializerFactory(startupService: AppStartupService) {
  return () => startupService.load();
}

// using https://github.com/zefoy/ngx-dropzone-wrapper
const DEFAULT_DROPZONE_CONFIG: DropzoneConfigInterface = {
  // Change this to your upload POST address:
   url: 'https://httpbin.org/post',
   maxFilesize: 5000,
   acceptedFiles: 'application/pdf'
 };

@NgModule({
  declarations: [
    AppComponent,
    HttpStatus404Component,
    PasswordForgotComponent,
    PasswordVerifyComponent,
    InsuranceIframeComponent,
    InsuranceFormComponent,
    NewsDetailComponent,
    NewsComponent,
    NewsListItemComponent,
    CarouselCardsComponent,
    PortalComponent,
    DatepickerComponent,
    LoginComponent,
    PasswordChangeComponent,
    InnerLoginComponent,
    RegistrationComponent,
    UsernameRecoveryComponent,
    PasswordUpdateComponent,
    TermConditionsComponent,
    TermConditionsNewPageComponent,
    UpdateVerifyComponent,
    GdprConsentsComponent,
    SingleConsentComponent,
    CaptchaHelpComponent
  ],
  imports: [
    AmlModule,
    BrowserModule,
    HttpModule,
    HttpClientModule,
    BinderModule,
    DashboardModule,
    FinanceModule,
    MessagesModule,
    OnboardingModule,
    ProfileModule,
    SelfserviceModule,
    RedirectModule,
    FormsModule,
    ReactiveFormsModule,
    ValidationModule,
    AuthModule,
    DropzoneModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: translateLoaderFactory,
          deps: [HttpClient, appConfigToken]
      }
    }),
    AppRoutingModule,
    ScrollToModule.forRoot(),
    RecaptchaModule.forRoot(),
    RecaptchaFormsModule,
	NgxLoadingModule.forRoot({})
  ],
  providers: [
	DatePipe,
	DecimalPipe,
    AuthGuard,
    {
      provide: appConfigToken,
      useValue: appConfigValue
    },
    {
      provide: apiConfigToken,
      useValue: apiConfigValue
    },
    {
      provide: SharedService,
      //  useClass: SharedMockedService
      useClass: SharedIntegratedService
    },
    {
      provide: MessagesService,
      //  useClass: MessagesMockedService
      useClass: MessagesIntegratedService
    },
    {
      provide: NewsService,
      useClass: NewsService
    },
    {
      provide: AuthService,
      //  useClass: AuthMockedService
      useClass: AuthIntegratedService
    },
    {
      provide: CookieService,
      useClass: CookieService
    },
    {
      provide: DROPZONE_CONFIG,
      useValue: DEFAULT_DROPZONE_CONFIG
    },
    {
      provide: LoggerService,
      useClass: ConsoleLoggerService
    },
    AppStartupService,
    {
      provide: APP_INITIALIZER,
      useFactory: initializerFactory,
      deps: [AppStartupService],
      multi: true
    },
    EnvironmentConfig,
    {
      provide: APP_INITIALIZER,
      useFactory: initializeApp,
      deps: [EnvironmentConfig],
      multi: true
    },
    { provide: XSRFStrategy,
      useValue: new CookieXSRFStrategy('cxt', 'hxt')
    },    
    {
      provide: ErrorHandler, 
      useClass: GlobalErrorHandler
	},
	{
		provide: LoaderIndicatorService,
		useClass: LoaderIndicatorService
	},
	{
		provide: HTTP_INTERCEPTORS,
		useClass: BaseHttpInterceptor,
		multi: true
	},
	{
		provide: DEFAULT_TIMEOUT,
		useValue: 180000
	},
	{
		provide: LOCALE_ID,
		useValue: 'cs'
	},
    {
      provide: RECAPTCHA_LANGUAGE,
      useValue: 'cs'
	}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
