import { RECAPTCHA_LANGUAGE } from "ng-recaptcha";

export interface IEnvironmentConfig {
  env: {
    production: boolean,
    debug: boolean,
    uriPrefix: string,
    uriPrefixV2: string,
    uriInbosPrefix: string,
    recaptchaSiteKey: string,
    damageLiabilityUrl: string,
    accidentInsuranceUrl: string,
    proprietaryInsuranceUrl: string,
    employeeLiabilityInsuranceUrl: string,
    combinatedCarInsuranceUrl: string,
    combinatedProprietaryInsuranceUrl: string,
    liabilityInsuranceUrl: string,
    HouseholdInsuranceUrl: string,
    getNewsUrl: string,
    captchaLanguage: string,
    amlUrl: string,
    getNewsById: string,
    getTechnicalReportUrl:string
  };
}
