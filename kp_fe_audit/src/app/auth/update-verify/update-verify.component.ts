import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation, ChangeDetectorRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AuthService } from '../../auth/shared/auth.service';
import { interpolate } from '../../utils/common.utils';
import { notBlankValidator, recaptchaValidator } from '../../validation/validation.validators';
import { trimToNull } from '../../validation/validation.utils';
import { LoggerService } from '../../shared/logger.service';
import { EnvironmentConfig } from '../../environment.config';
import { RecaptchaComponent } from 'ng-recaptcha';
import { SharedService } from '../../shared/shared.service';
declare function openQuestionMarkModal(modal): Function;
declare function openTechnicalReportModal(report, isAmlAlertNeeded): Function;

@Component({
    selector: 'app-update-verify',
    templateUrl: './update-verify.component.html',
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class UpdateVerifyComponent implements OnInit {

  i18NPrefix = 'portal.update-verify.';
  i18NErrorPrefix = 'global.errors.';

  @ViewChild(RecaptchaComponent) captchaComponentRef?: RecaptchaComponent;

  constructor(
    private authService: AuthService,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private log: LoggerService,
    private sharedService: SharedService
  ) { }

  alreadySubmitted = false;
  submitting = false;
  submitError = false;
  submitErrorString = 'submitError';
  form: FormGroup;
  verifyCode: string;
  invalidVerify = false;
  captchaRequired = false;
  captchaRequiredInfo = false;
  captchaError = false;
  recaptchaSiteKey: string;

  ngOnInit(): void {
    this.buildForm();
	this.recaptchaSiteKey = EnvironmentConfig.settings.env.recaptchaSiteKey;
  }

  private buildForm(): void {
	if (this.captchaRequired === true) {
		this.form = this.fb.group({
			verifyCode: [this.verifyCode, notBlankValidator],
			captcha:  [null, recaptchaValidator]
		});
	} else {
		this.form = this.fb.group({
			verifyCode: [this.verifyCode, notBlankValidator]
		});
	}
  }

  private rebuildForm(): void {
	this.verifyCode = this.form.get('verifyCode').value;
	this.buildForm();
  }

  switchDateFormat(dateString: string): string {
    let date = dateString.replace(/\s/g, "").split('.');
    return date[2] + '/' + date[1] + '/' + date[0];
  }

  needAMLRenew(date: string) {
    let today = new Date();
    today.setHours(0);
    let dateRenew = new Date(this.switchDateFormat(date));
    return dateRenew < today;
  }

  onSubmit(form: FormGroup): void {
    this.alreadySubmitted = true;
	this.captchaError = false;
	this.captchaRequiredInfo = false;
	this.invalidVerify = false;
    if (!form.valid || this.submitting) {
		return;
    }
	this.captchaRequired = false;
    this.submitting = true;
    this.submitError = false;
    this.submitErrorString = 'submitError';

    const value = this.form.value;
    value.verifyCode = trimToNull(value.verifyCode);

    this.authService.verifyConfirmationCode$(value.verifyCode, value.captcha).subscribe((res) => {
      this.log.debug('verifyConfirmationCode');
      this.log.debug(res);
      this.submitting = false;
      this.alreadySubmitted = false;
      if (res.amlRenewalDate && this.needAMLRenew(res.amlRenewalDate)) {
        this.sharedService.getTechnicalReport$().subscribe((report) => {
          console.log(report);
          if (report)
            openTechnicalReportModal(report, true);
          else {
            openQuestionMarkModal('amlModal');
          }
        },
          (e) => {
            openQuestionMarkModal('amlModal');
            console.log(e);
          });        
      }
      else {
        this.sharedService.getTechnicalReport$().subscribe((report) => {
          console.log(report);
          if (report)
            openTechnicalReportModal(report,false);
        },
          (e) => {
            console.log(e);
        });
      }
      this.router.navigate(['/profile']);
      this.cd.markForCheck();
    }, (e) => {		
		if (this.captchaComponentRef !== undefined && this.captchaComponentRef !== null) {
			this.captchaComponentRef.reset();
		}
		this.submitting = false;
		this.alreadySubmitted = false;
		if (e.status === 400) {
		  	if (this.authService.isCaptchaRequired(e)) {
				this.captchaRequired = true;
				this.captchaRequiredInfo = true;
		  	} else {
				this.invalidVerify = true;
				this.cd.markForCheck();
		  	}
      	} else if (this.authService.isCaptchaNotVerified(e)) {
			this.captchaError = true;
	  	} else {
			this.log.error(e);
			this.submitError = true;
      	}
		  this.rebuildForm();
		this.cd.markForCheck();
    });
    this.cd.markForCheck();
  }
}
