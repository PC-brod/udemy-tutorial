import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation, OnChanges, SimpleChanges, AfterViewChecked, AfterViewInit } from '@angular/core';
import { ChangeDetectorRef, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AuthService } from '../../auth/shared/auth.service';
import { showModal } from '../../utils/common.utils';
import { notBlankValidator, equalValidator, recaptchaValidator } from '../../validation/validation.validators';
import { trimToNull } from '../../validation/validation.utils';
import { PasswordChangeModel } from './password-change.model';
import { PasswordValidators } from 'ngx-validators/src/password/password-validators';
import { LoggerService } from '../../shared/logger.service';
import { LoaderIndicatorService } from '../../shared/LoaderIndicator.service';
import { SharedService } from '../../shared/shared.service';

@Component({
  selector: 'app-password-change',
  templateUrl: './password-change.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PasswordChangeComponent implements OnInit, AfterViewInit {
  validateNewPassword(): void {
    this.form.get('newPasswordRetyped').updateValueAndValidity();
    if (this.form.get('newPassword').status === "VALID") {
      this.passwordValid = true;
      this.cd.markForCheck();
    }
  }

  i18NPrefix = 'portal.password-change.';
  i18NErrorPrefix = 'global.errors.';

  constructor(
    private authService: AuthService,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private log: LoggerService,
    private loaderService: LoaderIndicatorService,
    private sharedService: SharedService
  ) { }

  alreadySubmitted = false;
  submitting = false;
  submitError = false;
  submitErrorString = 'submitError';
  form: FormGroup;
  captchaRequired: false;
  captchaRequiredToSend: boolean = false; // flag if "g-recaptcha-response" must be sent to api and recaptcha is shown
  captchaRequiredInfo: boolean = false; // flag if "captcha required" alert-info is shown
  passwordValid = true;

  newPasswordForm: PasswordChangeModel = {};

  ngOnInit(): void {
    this.buildForm();
   
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.loaderService.disableLoader();
    }, 500);
  }

  private buildForm(): void {
    if (this.captchaRequiredToSend) {
      this.form = this.fb.group({
        oldPassword: [this.newPasswordForm.oldPassword, notBlankValidator],
        newPassword: [this.newPasswordForm.newPassword,
        Validators.compose([
          notBlankValidator
          , PasswordValidators.alphabeticalCharacterRule(2)
          , PasswordValidators.digitCharacterRule(1),
          , PasswordValidators.lowercaseCharacterRule(1),
          , PasswordValidators.uppercaseCharacterRule(1),
          , PasswordValidators.repeatCharacterRegexRule(2),
          , Validators.minLength(8)])
        ],
        newPasswordRetyped: [this.newPasswordForm.newPasswordRetyped],
        "g-recaptcha-response": [null, recaptchaValidator]
      });
      this.form.setValidators(PasswordValidators.mismatchedPasswords('newPassword', 'newPasswordRetyped'));
    } else {
      this.form = this.fb.group({
        oldPassword: [this.newPasswordForm.oldPassword, notBlankValidator],
        newPassword: [this.newPasswordForm.newPassword,
        Validators.compose([
          notBlankValidator
          , PasswordValidators.alphabeticalCharacterRule(2)
          , PasswordValidators.digitCharacterRule(1),
          , PasswordValidators.lowercaseCharacterRule(1),
          , PasswordValidators.uppercaseCharacterRule(1),
          , PasswordValidators.repeatCharacterRegexRule(2),
          , Validators.minLength(8)])
        ],
        newPasswordRetyped: [this.newPasswordForm.newPasswordRetyped]
      });
      this.form.setValidators(PasswordValidators.mismatchedPasswords('newPassword', 'newPasswordRetyped'));
    }
  }



  onSubmit(form: FormGroup): void {
    this.alreadySubmitted = true;
    if (!form.valid || this.submitting) {
      if (this.form.get('newPassword').status === "INVALID") {
        this.passwordValid = false;
        this.cd.markForCheck();
      }
      return;
    }

    this.loaderService.showLoader();

    this.submitting = true;
    this.submitError = false;
    this.submitErrorString = 'submitError';
    this.captchaRequiredToSend = false;
    this.captchaRequiredInfo = false;

    const value: PasswordChangeModel = this.form.value;
    value.oldPassword = trimToNull(value.oldPassword);
    value.newPassword = trimToNull(value.newPassword);
    value.newPasswordRetyped = trimToNull(value.newPasswordRetyped);

    this.authService.changePassword$(value.oldPassword, value.newPassword, value["g-recaptcha-response"]).subscribe((res) => {
      this.submitting = false;
      this.alreadySubmitted = false;
      if (res && res.resultCode) {
        if (res.resultCode === 'OK') {
          this.cd.markForCheck();
          this.authService.setLoginInfo(res.accessTokenExpiresIn, res.accessTokenExpiration, this.authService.clientId, res.accessToken,
            false, this.authService.activated, res.sessionId, this.authService.username);
          this.togglePasswordSuccessfullyChanged();
          this.router.navigate(['/profile']);

        } else {
          this.submitError = true;
          this.submitErrorString = res.resultCode === 'FAIL_UNKNOWN_USER_OR_PASSWORD' ? 'wrongPassword' : res.resultCode;
          this.cd.markForCheck();
        }
      } else {
        this.submitError = true;
        this.submitErrorString = 'submitError';
        this.cd.markForCheck();
      }
      this.loaderService.disableLoader();
    }, (e) => {
      this.loaderService.disableLoader();
      this.submitError = true;
      this.submitting = false;
      this.alreadySubmitted = false;

      if (this.authService.isCaptchaRequired(e)) {
        this.submitError = false;
        this.captchaRequiredInfo = true;
        this.captchaRequiredToSend = true;
        this.rebuildForm();
      } else if (this.authService.isCaptchaNotVerified(e)) {
        this.submitErrorString = "captcha.error";
        this.captchaRequiredToSend = true;
        this.rebuildForm();
      }
      else if (e.status === 401) {
        this.submitErrorString = 'wrongPassword';
      }

      else {
        this.submitErrorString = e.error.resultCode === 'FAIL_UNKNOWN_USER_OR_PASSWORD' ? 'wrongPassword' : "submitError";
      }

      this.cd.markForCheck();
    });
  }
  public togglePasswordSuccessfullyChanged(): void {
    this.sharedService.toggleInfoAlert("portal.password-change.successfullyChanged", null);
  }

  private rebuildForm(): void {
    this.newPasswordForm.oldPassword = this.form.value.oldPassword;
    this.newPasswordForm.newPassword = this.form.value.newPassword;
    this.newPasswordForm.newPasswordRetyped = this.form.value.newPasswordRetyped;
    this.buildForm();
  }
}
