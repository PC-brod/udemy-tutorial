export interface PasswordChangeModel {
  	oldPassword?: string;
  	newPassword?: string;
  	newPasswordRetyped?: string;
  	"g-recaptcha-response"?: string;
}

export interface PasswordChangeResultModel {
	resultCode?: string,
	accessTokenExpiresIn?: string,
	accessToken?: string,
	accessTokenExpiration?: string,
	sessionId?: string;
}
