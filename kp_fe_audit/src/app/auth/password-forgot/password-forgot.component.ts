import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { ChangeDetectorRef, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { notBlankValidator, emailValidator, recaptchaValidator } from '../../validation/validation.validators';
import { trimToNull } from '../../validation/validation.utils';
import { AuthService } from '../shared/auth.service';
import { IdentificationStatus } from '../shared/models/identification-status.model';
import { ReqBodyUsername } from '../shared/models/req-body-username.model';
import { LoggerService } from '../../shared/logger.service';
import { LoaderIndicatorService } from '../../shared/LoaderIndicator.service';
import { EnvironmentConfig } from '../../environment.config';
import { RecaptchaComponent } from 'ng-recaptcha';

@Component({
  selector: 'app-password-forgot',
  templateUrl: './password-forgot.component.html'
})
export class PasswordForgotComponent implements OnInit {

  i18NPrefix = 'portal.password-forgot.';
  i18NErrorPrefix = 'global.errors.';

  constructor(
    private authService: AuthService,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private log: LoggerService,
    private loaderService: LoaderIndicatorService
  ) { }

  @ViewChild(RecaptchaComponent) captchaComponentRef?: RecaptchaComponent;
  alreadySubmitted = false;
  submitting = false;
  submitError = false;
  submitErrorString = 'submitError';
  captchaError = false;
  form: FormGroup;
  email: string;
  recaptchaSiteKey: string;

  ngOnInit(): void {
    this.buildForm();
    this.recaptchaSiteKey = EnvironmentConfig.settings.env.recaptchaSiteKey;
  }

  private buildForm(): void {
    this.form = this.fb.group({
      email: [this.email, Validators.compose([notBlankValidator, emailValidator])],
      captcha: [null, recaptchaValidator]
    });
  }

  onSubmit(form: FormGroup): void {
    this.alreadySubmitted = true;
    if (!form.valid || this.submitting) {
      return;
    }

    this.loaderService.showLoader();

    this.submitting = true;
    this.submitErrorString = 'submitError';
    this.submitError = false;
    this.captchaError = false;

    const value = this.form.value;
    value.email = trimToNull(value.email);

    const body: ReqBodyUsername = {
      userName: value.email,
      "g-recaptcha-response": value.captcha
    };

    this.authService.recoverPassword$(body).subscribe((res) => {
      this.submitting = false;
      this.alreadySubmitted = false;
      if (res.status === IdentificationStatus.EXISTINGCLIENTCPACTIVATED) {
        if (res.recoveryEmailSent) {
          this.authService.relogin = true;
          this.authService.loginInfo = true;
          this.authService.loginInfoString = 'infoEmailSent';
          this.cd.markForCheck();
          this.router.navigate(['/login']);
        } else {
          this.submitError = true;
          this.submitErrorString = 'recoveryEmailNotSent';
          this.cd.markForCheck();
        }
      } else if (res.status === IdentificationStatus.UNKNOWN) {
        this.captchaComponentRef.reset();
        this.submitErrorString = IdentificationStatus.UNKNOWNCLIENT;
        this.log.error("response status: " + res.status + ", recoveryEmailSent: " + res.recoveryEmailSent);
        this.submitError = true;
        this.submitting = false;
        this.alreadySubmitted = false;
        setTimeout(() => {
          this.submitError = false;
          this.cd.markForCheck();
        }, 5000);
        this.cd.markForCheck();
      } else {
        this.captchaComponentRef.reset();
        this.log.error("response status: " + res.status + ", recoveryEmailSent: " + res.recoveryEmailSent);
        this.submitError = true;
        this.submitting = false;
        this.alreadySubmitted = false;
        setTimeout(() => {
          this.submitError = false;
          this.cd.markForCheck();
        }, 5000);
        this.cd.markForCheck();
      }
      this.cd.markForCheck();
      this.loaderService.disableLoader();
    }, (e) => {
      this.captchaComponentRef.reset();
      setTimeout(() => {
        this.submitError = false;
        this.cd.markForCheck();
      }, 5000);
      this.loaderService.disableLoader();
      this.log.error(e);
      this.submitting = false;
      this.alreadySubmitted = false;
      if (e.status === 403) {
        this.captchaError = true;
      } else {
        this.submitError = true;
      }
      this.cd.markForCheck();
    });
  }
}
