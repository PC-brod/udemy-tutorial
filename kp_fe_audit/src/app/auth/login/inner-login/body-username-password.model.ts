import { NotLoged } from '../../../shared/models/not-loged.model';

export interface BodyUsernamePassword extends NotLoged {
  username?: string;
  password?: string;
}
export interface BodyUsernamePasswordCaptcha extends NotLoged {
	username?: string;
	password?: string;
	"g-recaptcha-response"?: string;
}

export interface ReqBodyUsernamePasswordWithBrowserInfo extends BodyUsernamePasswordCaptcha {
	userAgent?: string;
	clientIP?: string;
}
