import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { ChangeDetectorRef, EventEmitter, Input, Output } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute, Params } from '@angular/router';
import { AuthService } from '../../shared/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { showModal } from '../../../utils/common.utils';
import { notBlankValidator, recaptchaValidator } from '../../../validation/validation.validators';
import { trimToNull } from '../../../validation/validation.utils';
import { BodyUsernamePasswordCaptcha } from './body-username-password.model';
import { SharedService } from '../../../shared/shared.service';
import { LoggerService } from '../../../shared/logger.service';
import { LoaderIndicatorService } from '../../../shared/LoaderIndicator.service';
import { EnvironmentConfig } from '../../../environment.config';
import { RecaptchaComponent } from 'ng-recaptcha';
import { DatePipe } from '@angular/common';

declare function appAfterInit(): any;
declare function openQuestionMarkModal(modal): Function;
declare function openTechnicalReportModal(report, isAmlAlertNeeded): Function;

@Component({
  selector: 'app-login-inner-part',
  templateUrl: './inner-login.component.html',
  styleUrls: ['./inner-login.component.css'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InnerLoginComponent implements OnInit {

  i18NPrefix = 'login.';
  i18NErrorPrefix = 'global.errors.';
  i18NInfoPrefix = 'global.info.';

  @ViewChild(RecaptchaComponent) captchaComponentRef?: RecaptchaComponent;

  constructor(
    private authService: AuthService,
    private sharedService: SharedService,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private log: LoggerService,
    private loaderService: LoaderIndicatorService,
    private datePipe: DatePipe
  ) { }

  alreadySubmitted = false;
  submitting = false;
  submitError = false;
  submitErrorString = 'submitError';
  infoString = 'submitError';
  form: FormGroup;
  captchaRequiredToSend: boolean = false; // flag if "g-recaptcha-response" must be sent to api and recaptcha is shown
  captchaRequiredInfo: boolean = false; // flag if "captcha required" alert-info is shown
  recaptchaSiteKey: string;
  prevUsername: string; // previously submitted username. Important for cancelling captcha request for other user

  credentials: BodyUsernamePasswordCaptcha = {};

  ngOnInit(): void {
    this.buildForm();
    this.recaptchaSiteKey = EnvironmentConfig.settings.env.recaptchaSiteKey;
  }

  private buildForm(): void {
    if (this.captchaRequiredToSend === true) {
      this.form = this.fb.group({
        password: [this.credentials.password, notBlankValidator],
        username: [this.credentials.username, notBlankValidator],
        "g-recaptcha-response": [null, recaptchaValidator]
      });
    } else {
      this.form = this.fb.group({
        password: [this.credentials.password, notBlankValidator],
        username: [this.credentials.username, notBlankValidator]
      });
    }
  }

  showInfo() {
    if (this.authService.loginInfo) {
      this.infoString = this.authService.loginInfoString;
    }
    return this.authService.loginInfo;
  }

  switchDateFormat(dateString: string): string {
    let date = dateString.replace(/\s/g, "").split('.');
    return date[2] + '/' + date[1] + '/' + date[0];
  }

  needAMLRenew(date: string) {
    let today = new Date();
    today.setHours(0);
    if (date) {
      let dateRenew = new Date(this.switchDateFormat(date));
      return dateRenew < today;
    } else {
      return false;
    }
  }

  onSubmit(): void {
    this.alreadySubmitted = true;
    this.submitError = false;
    this.submitErrorString = 'submitError';
    this.captchaRequiredInfo = false; // reset flag - depends on submitted username

    if (this.prevUsername && this.prevUsername != trimToNull(this.form.value.username)) {
      // if username is different we need to rebuild form for cancelling captcha
      this.rebuildForm();
    }
    this.prevUsername = trimToNull(this.form.value.username);

    if (!this.form.valid || this.submitting) {
      this.cd.markForCheck();
      return;
    }
    this.captchaRequiredToSend = false; // reset flag - depends on submitted username

    this.loaderService.showLoader();
    this.submitting = true;

    const value: BodyUsernamePasswordCaptcha = this.form.value;
    value.password = trimToNull(value.password);
    value.username = trimToNull(value.username).toLocaleLowerCase();

    this.authService.loginUser$(value).subscribe((res) => {
      this.submitting = false;
      this.alreadySubmitted = false;
      if (res.resultCode === 'SUCCESS') {
        this.authService.setLoginInfo(res.accessTokenExpiresIn, res.accessTokenExpiration, res.clientId, res.accessToken,
          res.inAccessRecovery, res.activated, res.sessionId, value.username);
        if (res.requiresSMSVerification) {
          this.cd.markForCheck();
          this.router.navigate(['/verify']);
        } else {
          this.authService.login = true;
          this.cd.markForCheck();
          this.sharedService.loadConsultant();
          if (res.amlRenewalDate && this.needAMLRenew(res.amlRenewalDate)) {
            this.sharedService.getTechnicalReport$().subscribe((report) => {
              console.log(report);
              if (report)
                openTechnicalReportModal(report, true);
              else {
                openQuestionMarkModal('amlModal');
              }
            },
              (e) => {
                openQuestionMarkModal('amlModal');
                console.log(e);
              });
          }
          else {
            this.sharedService.getTechnicalReport$().subscribe((report) => {
              console.log(report);
              if (report)
                openTechnicalReportModal(report, false);
            },
              (e) => {
                console.log(e);
              });
          }
          this.router.navigate(['/onboarding']);
        }
      } else if (res.resultCode === 'PASSWORD_EXPIRED') {
        this.authService.setLoginInfo(res.accessTokenExpiresIn, res.accessTokenExpiration, res.clientId, res.accessToken,
          res.inAccessRecovery, res.activated, res.sessionId, value.username);
        this.authService.login = true;
        this.cd.markForCheck();
        this.router.navigate(['/changePassword']);
      } else {
        if (this.captchaComponentRef !== undefined && this.captchaComponentRef !== null) {
          this.captchaComponentRef.reset();
        }
        this.rebuildForm();
        this.loaderService.disableLoader();
        this.authService.login = false;
        this.submitError = true;
        this.submitErrorString = res.resultCode;
        this.cd.markForCheck();
      }
    }, (e) => {
      if (this.captchaComponentRef !== undefined && this.captchaComponentRef !== null) {
        this.captchaComponentRef.reset();
      }
      this.loaderService.disableLoader();
      this.log.debug(e);
      this.submitError = true;
      this.submitting = false;
      this.authService.login = false;
      this.alreadySubmitted = false;
      this.captchaRequiredToSend = false;
      this.captchaRequiredInfo = false;

      if (this.authService.isCaptchaRequired(e)) {
        this.submitError = false;
        this.captchaRequiredInfo = true;
        this.captchaRequiredToSend = true;
      } else if (this.authService.isCaptchaNotVerified(e)) {
        this.submitErrorString = "captcha.error";
        this.captchaRequiredToSend = true;
      } else {
        this.submitErrorString = "submitError";
      }

      this.rebuildForm();
      this.cd.markForCheck();
    });
  }

  relogin() { return this.authService.relogin; }

  private rebuildForm(): void {
    this.credentials.password = this.form.get("password").value;
    this.credentials.username = this.form.get("username").value;
    this.buildForm();
  }
}
