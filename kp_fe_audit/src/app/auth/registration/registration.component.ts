import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation, ViewChild, ElementRef, ComponentRef } from '@angular/core';
import { ChangeDetectorRef, EventEmitter, Input, Output } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute, Params } from '@angular/router';
import { AuthService } from '../shared/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { showModal } from '../../utils/common.utils';
import { notBlankValidator, birthNumberValidator, recaptchaValidator, divideBy } from '../../validation/validation.validators';
import { trimToNull } from '../../validation/validation.utils';
import { ReqBodyBirthnumber } from '../shared/models/req-body-birth-number.model';
import { IdentificationStatus } from '../shared/models/identification-status.model';
import { LoggerService } from '../../shared/logger.service';
import { EnvironmentConfig } from '../../environment.config';
import { RecaptchaComponent } from 'ng-recaptcha';

declare function appAfterInit(): any;

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RegistrationComponent implements OnInit {

  @ViewChild(RecaptchaComponent) captchaComponentRef?: RecaptchaComponent;

    i18NPrefix = 'registration.';
    i18NErrorPrefix = 'global.errors.';

    constructor(
      private authService: AuthService,
      private fb: FormBuilder,
      private cd: ChangeDetectorRef,
      private activatedRoute: ActivatedRoute,
      private router: Router,
      private log: LoggerService
    ) { }

  alreadySubmitted = false;
  submitting = false;
  submitError = false;
  submitErrorString = 'submitError';
  captchaError = false;
  form: FormGroup;
  birthNumber: string;
  approve: boolean;
  recaptchaSiteKey: string;

    ngOnInit(): void {
      this.buildForm();
      this.recaptchaSiteKey = EnvironmentConfig.settings.env.recaptchaSiteKey;
    }

    private buildForm(): void {
      this.form = this.fb.group({
        birthNumber: [this.birthNumber, Validators.compose([notBlankValidator, birthNumberValidator])],
        captcha: [null, recaptchaValidator]
      }, { validator: divideBy('birthNumber', 11, this.i18NErrorPrefix + 'validation.birthNumberDivided') });
    }

  onSubmit(form: FormGroup): void {
    this.alreadySubmitted = true;
    console.log(form.valid);
    if (!form.valid || this.submitting) {
      return;
    }
    this.submitting = true;
    this.submitError = false;
    this.submitErrorString = 'submitError';
	this.captchaError = false;

    const value = this.form.value;
    value.birthNumber = trimToNull(value.birthNumber);
    // value.approve = trimToNull(value.approve);

    const body: ReqBodyBirthnumber = {
      birthNumber: value.birthNumber,
      "g-recaptcha-response": value.captcha
    };
    // if (value.approve) {
    this.authService.identifyUser$(body).subscribe((res) => {
      this.submitting = false;
      this.alreadySubmitted = false;
      if (res.status === IdentificationStatus.EXISTINGCLIENTCPACTIVATED
        || res.status === IdentificationStatus.EXISTINGCLIENTCPNOTACTIVATED) {
        if (res.recoveryEmailSent) {
          this.authService.relogin = true;
          this.authService.loginInfo = true;
          this.authService.loginInfoString =
          res.status === IdentificationStatus.EXISTINGCLIENTCPACTIVATED ? 'infoEmailSentActivated' : 'infoEmailSent';
          this.cd.markForCheck();
          this.router.navigate(['/login']);
        } else {
          this.submitError = true;
          this.submitErrorString =
          res.status === IdentificationStatus.EXISTINGCLIENTCPACTIVATED ? 'recoveryEmailNotSent' : 'infoEmailNotSentActivated';
          this.cd.markForCheck();
        }
      } else {
        this.captchaComponentRef.reset();
        this.submitError = true;
        if (res.status === IdentificationStatus.UNKNOWNCLIENT) {

          this.submitErrorString = "registration." + res.status;
          setTimeout(() => {
            this.submitError = false;
            this.cd.markForCheck();
          }, 5000);
        }
        else {
          this.submitErrorString = res.status;
        }
        this.cd.markForCheck();
      }
    }, (e) => {
      this.captchaComponentRef.reset();
      this.log.error(e);
      this.submitting = false;
      this.authService.login = false;
	  this.alreadySubmitted = false;
	  if (e.status === 403) {
		this.captchaError = true;
	  } else {
		this.submitError = true;
	  }
      this.cd.markForCheck();
    });
}

}
