import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { ChangeDetectorRef, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { notBlankValidator, birthNumberValidator, recaptchaValidator, divideBy } from '../../validation/validation.validators';
import { trimToNull } from '../../validation/validation.utils';
import { AuthService } from '../shared/auth.service';
import { IdentificationStatus } from '../shared/models/identification-status.model';
import { LoggerService } from '../../shared/logger.service';
import { EnvironmentConfig } from '../../environment.config';
import { ReqBodyBirthnumber } from '../shared/models/req-body-birth-number.model';
import { LoaderIndicatorService } from '../../shared/LoaderIndicator.service';
import { RecaptchaComponent } from 'ng-recaptcha';

@Component({
  selector: 'app-username-recovery',
  templateUrl: './username-recovery.component.html',
  encapsulation: ViewEncapsulation.None
})
export class UsernameRecoveryComponent implements OnInit {

  i18NPrefix = 'portal.username-forgot.';
  i18NErrorPrefix = 'global.errors.';

  constructor(
    private authService: AuthService,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private log: LoggerService,
    private loaderService: LoaderIndicatorService
  ) { }

  alreadySubmitted = false;
  submitting = false;
  submitError = false;
  submitErrorString = 'submitError';
  captchaError = false;
  form: FormGroup;
  birthNumber: string;
  recaptchaSiteKey: string;

  @ViewChild(RecaptchaComponent) captchaComponentRef?: RecaptchaComponent;


  ngOnInit(): void {
    this.buildForm();
    this.recaptchaSiteKey = EnvironmentConfig.settings.env.recaptchaSiteKey;
  }

  private buildForm(): void {
    this.form = this.fb.group({
		birthNumber: [this.birthNumber, Validators.compose([notBlankValidator, birthNumberValidator])],
      captcha: [null, recaptchaValidator]
    }, { validator: divideBy('birthNumber', 11, this.i18NErrorPrefix + 'validation.birthNumberDivided') });
  }

  onSubmit(form: FormGroup): void {
    this.alreadySubmitted = true;
    if (!form.valid || this.submitting) {
      return;
    }

    this.loaderService.showLoader();

    this.submitting = true;
    this.submitError = false;
    this.submitErrorString = 'submitError';

    const value = this.form.value;
    value.birthNumber = trimToNull(value.birthNumber);

    const body: ReqBodyBirthnumber = {
      birthNumber: value.birthNumber,
      "g-recaptcha-response": value.captcha
    };

    this.authService.identifyUser$(body).subscribe((res) => {
      this.submitting = false;
      this.alreadySubmitted = false;
      if (res.status === IdentificationStatus.EXISTINGCLIENTCPACTIVATED || res.status === IdentificationStatus.EXISTINGCLIENTCPNOTACTIVATED) {
        if (res.recoveryEmailSent) {
          this.cd.markForCheck();
          this.authService.loginInfo = true;
		  this.authService.loginInfoString = 'infoEmailSent';
		  this.authService.relogin = true;
          this.router.navigate(['/login']);
        } else {
          this.log.error("response status: " + res.status + ", recoveryEmailSent: " + res.recoveryEmailSent);
          this.submitErrorValues();
        }
      } else if (res.status === IdentificationStatus.UNKNOWNCLIENT) {
        this.log.error("response status: " + res.status + ", recoveryEmailSent: " + res.recoveryEmailSent);
        this.submitErrorString = "registration." + res.status;
        this.submitErrorValues();
        setTimeout(() => {
          this.submitError = false;
          this.cd.markForCheck();
        }, 5000);
        this.captchaComponentRef.reset();
      } else {
        this.log.error("response status: " + res.status + ", recoveryEmailSent: " + res.recoveryEmailSent);
        this.submitErrorValues();
        this.captchaComponentRef.reset();
      }
      this.cd.markForCheck();
      this.loaderService.disableLoader();
    }, (e) => {
      this.captchaComponentRef.reset()
      this.loaderService.disableLoader();
      this.log.error(e);
      this.submitErrorValues();
      if (e.status === 403) {
        this.submitError = false;
        this.captchaError = true;
      }
      this.cd.markForCheck();
    });
  }

  private submitErrorValues(): void {
    this.submitError = true;
    this.submitting = false;
    this.authService.login = false;
    this.alreadySubmitted = false;
  }
}
