import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation, OnDestroy } from '@angular/core';
import { ChangeDetectorRef, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AuthService } from '../../auth/shared/auth.service';
import { showModal } from '../../utils/common.utils';
import { notBlankValidator, equalValidator } from '../../validation/validation.validators';
import { trimToNull } from '../../validation/validation.utils';
import { PasswordUpdateModel } from './password-update.model';
import { PasswordValidators } from 'ngx-validators/src/password/password-validators';
import { LoggerService } from '../../shared/logger.service';
import { SharedService } from '../../shared/shared.service';
import { LoaderIndicatorService } from '../../shared/LoaderIndicator.service';
import { ProfileIntegratedService } from '../../profile/shared/profile-integrated.service';
import { ProfileService } from '../../profile/shared/profile.service';
declare function openQuestionMarkModal(modal): Function;
declare function openTechnicalReportModal(report, isAmlAlertNeeded): Function;

@Component({
  selector: 'app-password-update',
  templateUrl: './password-update.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PasswordUpdateComponent implements OnInit {

  i18NPrefix = 'portal.password-change.';
  i18NErrorPrefix = 'global.errors.';

  constructor(
    private authService: AuthService,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private log: LoggerService,
    private sharedService: SharedService,
    private loaderService: LoaderIndicatorService,
    private clientService: ProfileService
  ) { }

  alreadySubmitted = false;
  submitting = false;
  submitError = false;
  submitErrorString = 'submitError';
  form: FormGroup;
  passwordValid = true;

  newPasswordForm: PasswordUpdateModel = {};

  ngOnInit(): void {
    this.authService.inUpdatePasswordProcess = true;
    this.buildForm();
  }

  private buildForm(): void {
    this.form = this.fb.group({
      newPassword: [this.newPasswordForm.newPassword,
      Validators.compose([
        notBlankValidator
        , PasswordValidators.alphabeticalCharacterRule(2)
        , PasswordValidators.digitCharacterRule(1),
        , PasswordValidators.lowercaseCharacterRule(1),
        , PasswordValidators.uppercaseCharacterRule(1),
        , PasswordValidators.repeatCharacterRegexRule(2),
        , Validators.minLength(8),
      ])
      ],
      newPasswordRetyped: [this.newPasswordForm.newPasswordRetyped]
    });
    this.form.setValidators(PasswordValidators.mismatchedPasswords('newPassword', 'newPasswordRetyped'));
  }

  validateNewPassword(): void {
    this.form.get('newPasswordRetyped').updateValueAndValidity();
    if (this.form.get('newPassword').status === "VALID") {
      this.passwordValid = true;
      this.cd.markForCheck();
    }
    console.log(this.form.get('newPassword').status);

  }

  switchDateFormat(dateString: string): string {
    let date = dateString.replace(/\s/g, "").split('.');
    return date[2] + '/' + date[1] + '/' + date[0];
  }

  needAMLRenew(date: string) {
    let today = new Date();
    today.setHours(0);
    if (date) {
      let dateRenew = new Date(date);
      return dateRenew < today;
    } else {
      return false;
    }
  }

  onSubmit(form: FormGroup): void {
    this.alreadySubmitted = true;
    if (!form.valid || this.submitting) {
      if (this.form.get('newPassword').status === "INVALID") {
        this.passwordValid = false;
        this.cd.markForCheck();
      }
      return;
    }


    this.loaderService.showLoader();

    this.submitting = true;
    this.submitError = false;
    this.submitErrorString = 'submitError';

    const value: PasswordUpdateModel = this.form.value;
    value.newPassword = trimToNull(value.newPassword);
    value.newPasswordRetyped = trimToNull(value.newPasswordRetyped);

    

    this.authService.updatePassword$(value.newPassword).subscribe((res) => {
      this.log.debug(res);
      this.authService.GDPRShowed = false;
      this.authService.carouselModalShowed = false;
      this.submitting = false;
      this.alreadySubmitted = false;
      if (res !== null
        && res.updatePasswordResult !== undefined
        && res.updatePasswordResult !== null
        && res.updatePasswordResult !== '') {
        if (res.updatePasswordResult === 'OK') {
          this.sharedService.loadConsultant();
          this.cd.markForCheck();
          this.authService.inUpdatePasswordProcess = false;

          this.sharedService.getOnboardingShown$().subscribe((res) => {
            if (res.onboarding_shown) {
              this.authService.carouselModalShowed = true;
              this.clientService.getClientById$().subscribe((clientInfoRes) => {
                if (clientInfoRes.amlRenewalDate && this.needAMLRenew(clientInfoRes.amlRenewalDate)) {
                  this.sharedService.getTechnicalReport$().subscribe((report) => {
                    console.log(report);
                    if (report)
                      openTechnicalReportModal(report, true);
                    else {
                      openQuestionMarkModal('amlModal');
                    }
                  },
                    (e) => {
                      openQuestionMarkModal('amlModal');
                      console.log(e);
                    });
                }
                else {
                  this.sharedService.getTechnicalReport$().subscribe((report) => {
                    console.log(report);
                    if (report)
                      openTechnicalReportModal(report, false);
                  },
                    (e) => {
                      console.log(e);
                    });
                }
              });
              this.router.navigate(['/dashboard']);
            } else {
              this.clientService.getClientById$().subscribe((clientInfoRes) => {
                if (clientInfoRes.amlRenewalDate && this.needAMLRenew(clientInfoRes.amlRenewalDate)) {
                  this.sharedService.getTechnicalReport$().subscribe((report) => {
                    console.log(report);
                    if (report)
                      openTechnicalReportModal(report, true);
                    else {
                      openQuestionMarkModal('amlModal');
                    }
                  },
                    (e) => {
                      openQuestionMarkModal('amlModal');
                      console.log(e);
                    });
                }
                else {
                  this.sharedService.getTechnicalReport$().subscribe((report) => {
                    console.log(report);
                    if (report)
                      openTechnicalReportModal(report, false);
                  },
                    (e) => {
                      console.log(e);
                    });
                }
              });
              this.router.navigate(['/onboarding']);
            }
          });

        } else {
          this.loaderService.disableLoader();
          this.submitError = true;
          this.submitErrorString = res.updatePasswordResult;
          this.cd.markForCheck();
        }
      } else {
        this.loaderService.disableLoader();
        this.submitError = true;
        this.submitErrorString = 'submitError';
        this.cd.markForCheck();
      }
    }, (e) => {
      this.loaderService.disableLoader();
      this.log.error(e);
      this.submitError = true;
      this.submitting = false;
      this.authService.login = false;
      this.alreadySubmitted = false;
      this.cd.markForCheck();
    });
  }

}

