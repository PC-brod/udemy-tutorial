export interface PasswordUpdateModel {
  newPassword?: string;
  newPasswordRetyped?: string;
}

export interface PasswordUpdateResultModel {
  updatePasswordResult?: string;
}
