import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ChangeDetectorRef, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AuthService } from '../../auth/shared/auth.service';
import { notBlankValidator, recaptchaValidator } from '../../validation/validation.validators';
import { trimToNull } from '../../validation/validation.utils';
import { LoggerService } from '../../shared/logger.service';
import { LoaderIndicatorService } from '../../shared/LoaderIndicator.service';
import { SharedService } from '../../shared/shared.service';
import { EnvironmentConfig } from '../../environment.config';
import { DatePipe } from '@angular/common';
declare function openQuestionMarkModal(modal): Function;

@Component({
    selector: 'app-password-verify',
    templateUrl: './password-verify.component.html',
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PasswordVerifyComponent implements OnInit {

  i18NPrefix = 'portal.password-verify.';
  i18NErrorPrefix = 'global.errors.';

  constructor(
    private authService: AuthService,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private log: LoggerService,
	private loaderService: LoaderIndicatorService,
    private sharedService: SharedService,
    private datePipe: DatePipe
  ) { }

  alreadySubmitted = false;
  submitting = false;
  submitError = false;
  submitErrorString = 'submitError';
  captchaRequired = false;
  captchaRequiredInfo = false;
  captchaError = false;
  recaptchaSiteKey: string;
  form: FormGroup;
  verifyCode: string;

  ngOnInit(): void {
	this.buildForm();
	this.recaptchaSiteKey = EnvironmentConfig.settings.env.recaptchaSiteKey;
	
	this.loaderService.disableLoader();
  }

  private buildForm(): void {
	  if (this.captchaRequired === true) {
		this.form = this.fb.group({
		  	verifyCode: [this.verifyCode, notBlankValidator],
		  	captcha:  [null, recaptchaValidator]
		});
	  } else {		  
		this.form = this.fb.group({
			verifyCode: [this.verifyCode, notBlankValidator]
		});
	  }
  }

  switchDateFormat(dateString: string): string {
    let date = dateString.replace(/\s/g, "").split('.');
    return date[2] + '/' + date[1] + '/' + date[0];
  }

  needAMLRenew(date :string) {
    let today = new Date();
    today.setHours(0);
    let dateRenew = new Date(this.switchDateFormat(date));
    return dateRenew < today; 
  }

  onSubmit(form: FormGroup): void {
	  this.alreadySubmitted = true;
	  this.captchaError = false;
	  this.captchaRequiredInfo = false;

	  this.submitError = false;
	  this.submitErrorString = 'submitError';
    if (!form.valid || this.submitting) {
		return;
	}
	
	this.captchaRequired = false;
	this.loaderService.showLoader();

    this.submitting = true;

    const value = this.form.value;
    value.verifyCode = trimToNull(value.verifyCode);

    this.authService.validateLoginConfirmationCode$(value.verifyCode, value.captcha).subscribe((res) => {
      this.submitting = false;
      this.alreadySubmitted = false;
      if (res.resultCode === 'SUCCESS') {
		this.authService.setLoginInfo(res.accessTokenExpiresIn, res.accessTokenExpiration, res.clientId, res.accessToken,
			res.inAccessRecovery, res.activated, res.sessionId, this.authService.username);
        this.authService.login = true;
        this.cd.markForCheck();
        this.sharedService.loadConsultant();
        if (res.amlRenewalDate && this.needAMLRenew(res.amlRenewalDate)) {
          openQuestionMarkModal('amlModal');
        }
        this.router.navigate(['/profile']);
      } else if (res.resultCode === 'PASSWORD_EXPIRED') {
		this.loaderService.disableLoader();
		this.authService.setLoginInfo(res.accessTokenExpiresIn, res.accessTokenExpiration, res.clientId, res.accessToken,
			res.inAccessRecovery, res.activated, res.sessionId, this.authService.username);
        this.authService.login = true;
        this.cd.markForCheck();
        this.router.navigate(['/updatePassword']);
      }
      else if (res.resultCode === 'UNKNOWN_USER_OR_PASSWORD') {
        this.rebuildForm();
        this.loaderService.disableLoader();
        this.submitError = true;
        this.submitErrorString = "verifyErrorString";
        this.cd.markForCheck();
      }
      else {
		this.rebuildForm();
		this.loaderService.disableLoader();
		this.submitError = true;
		this.submitErrorString = res.resultCode;
		this.cd.markForCheck();
	  }
    }, (e) => {
		this.loaderService.disableLoader();
		this.log.error(e);
		this.submitting = false;
		this.authService.login = false;
		this.alreadySubmitted = false;

		if (this.authService.isCaptchaRequired(e)) {
			this.captchaRequired = true;
			this.captchaRequiredInfo = true;
		} else if (this.authService.isCaptchaNotVerified(e)) {
			this.captchaError = true;
		} else {
			this.submitError = true;
		}

		this.cd.markForCheck();
		this.rebuildForm();
    });
    this.cd.markForCheck();
  }

	private rebuildForm(): void {	  
		this.verifyCode = this.form.get('verifyCode').value;
		this.buildForm();
  	}
}
