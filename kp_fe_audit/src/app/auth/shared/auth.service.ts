import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import 'rxjs/add/operator/toPromise';

import { AuthCheckModel } from './models/authcheck.model';
import { Observable } from 'rxjs/Observable';
import { AuthGuardModel } from './models/auth-guard.model';
import { RedirectLoginModel } from './models/redirect-login.model';
import { PasswordChangeResultModel } from '../password-change/password-change.model';
import { PasswordUpdateResultModel } from '../password-update/password-update.model';
import { NotLoged } from '../../shared/models/not-loged.model';
import { ResConsultant } from '../../shared/models/res-consultant.model';
import { LoginResult } from './models/login-result.model';
import { BodyUsernamePasswordCaptcha } from '../login/inner-login/body-username-password.model';
import { ReLoginModel } from './models/relogin.model';
import { InlineResponse } from './models/inline-response.model';
import { ReqBodyBirthnumber } from './models/req-body-birth-number.model';
import { ReqBodyUsername } from './models/req-body-username.model';
import { Subscription } from 'rxjs/Subscription';
import { RedirectLoginResponseModel } from './models/redirect-login-response.model';

@Injectable() 
export abstract class AuthService {

  GDPRShowed = true;
  carouselModalShowed = true;
  relogin = false; // flag if current url isss "/login" and only login form is shown
  login = false; // flag if user is logged in
  accessTokenExpiresIn: number;
  accessTokenExpiration: Date;
  clientId: string;
  username: string;
  resultCode: string;
  requiresSMSVerification: boolean;
  accessToken: string;
  inAccessRecovery: boolean;
  activated: boolean;
  loginInfo = false;
  inUpdatePasswordProcess = false;
  loginInfoString: string;
  consultant: ResConsultant;
  oauthCode: string;
  sessionTimerSubscription: Subscription;
  sessionIdLogout: string;

  abstract refreshToken(): Observable<any>;

    abstract identifyUser$(body?: ReqBodyBirthnumber): Observable<InlineResponse>;

	abstract setLoginInfo(accessTokenExpiresIn: string, accessTokenExpiration: string, clientId: string, accessToken: string,
							inAccessRecovery: boolean, activated: boolean, sessionId: string, username: string);

    abstract changePassword$(oldPass: string, newPass: string, gRecaptchaResponse: string): Observable<PasswordChangeResultModel>;

    abstract recoverPassword$(body?: ReqBodyUsername): Observable<InlineResponse>;

    abstract updatePassword$(newPass: string): Observable<PasswordUpdateResultModel>;

    abstract changeUsername$(body?: ReqBodyUsername): Observable<any>;

    abstract validateLoginConfirmationCode$(code: string, gRecaptchaResponse: string): Observable<LoginResult>;

    abstract verifyConfirmationCode$(code: string, gRecaptchaResponse: string): Observable<any>;

    abstract redirectLogin$(code: string): Observable<LoginResult>;

    abstract loginUser$(body: BodyUsernamePasswordCaptcha): Observable<LoginResult>;

    abstract getSessionId();

    abstract reloginUser$(body: ReLoginModel): Observable<LoginResult>;

    abstract logout$(): Observable<any>;

    // abstract injectAuthToBody<T extends NotLoged>(req: T): any;

    abstract get$(url);

    abstract post$(url, data, additionalOptions?);

    abstract put$(url, data);

    abstract delete$(url);

    abstract getUnauthorized(url);

    abstract postUnauthorized(url, data);

    abstract putUnauthorized(url, data);

    abstract deleteUnauthorized(url);

    abstract getHttpHeaders(accessToken?: string);

    abstract getClientId(): string;

    // abstract getAccesToken(): string;

    abstract initCheck(): Promise<AuthCheckModel>;

    abstract check(): /*Promise<AuthCheckModel>*/ boolean;

    abstract checkUrl(url: string): AuthGuardModel;

    abstract logout(): Promise<void>;

    abstract init(): Promise<void>;

    abstract getOAuthCode(): string;

    abstract getOAuthCode$(): Observable<RedirectLoginModel>;


    // private abstract handleErrors(error: any): Promise<any>;

    // private abstract handleError(error: Response): Promise<Response>;

  abstract validateBCID(): void;

    abstract sessionTimerStart(): void;

	abstract sessionTimerStop(): void;
	
	abstract isCaptchaRequired(e: HttpErrorResponse): boolean;

	abstract isCaptchaNotVerified(e: HttpErrorResponse): boolean;
}
