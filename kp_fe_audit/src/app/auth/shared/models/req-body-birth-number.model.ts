import { NotLoged } from '../../../shared/models/not-loged.model';

export interface ReqBodyBirthnumber extends BodyBirthnumber, NotLoged {}

export interface BodyBirthnumber {
  birthNumber?: string;
  "g-recaptcha-response"?: string;
}
