import { NotLoged } from '../../../shared/models/not-loged.model';

export interface ReqBodyUsername extends BodyUsername, NotLoged {}

export interface BodyUsername {
  userName?: string;
  "g-recaptcha-response"?: string;
}
