import { CPLoginResultCode } from './cp-login-result-code.model';

export interface LoginResult {
    sessionId?: string;
    accessTokenExpiresIn?: string;
    accessTokenExpiration?: string;
    clientId?: string;
    requiresSMSVerification?: boolean;
    resultCode?: string;
    accessToken?: string;
    activated?: boolean;
	inAccessRecovery?: boolean;
  CAPTCHA_REQUIRED?: boolean;
  hasAML?: boolean;
  amlRenewalDate?: string;
}
