﻿export class AuthCheckModel {
    defaultPrivatePage: string;
    isLogged: boolean;
    changingPasswordRequired: boolean;
    sessionTime: string;
}