﻿import { NotLoged } from '../../../shared/models/not-loged.model';

export interface ReLoginModel extends NotLoged {
  sessionId: string;
}
