export interface RefreshTokenResult {
    accessToken?: string;
    accessTokenExpiresIn?: number;
    accessTokenExpiration?: string;
}