import { IdentificationStatus } from './identification-status.model';

export interface InlineResponse {
    recoveryEmailSent?: boolean;
    status?: IdentificationStatus;
}
