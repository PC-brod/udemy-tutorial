export type CPLoginResultCode = 'success' | 'account_blocked' | 'password_expired' | 'unknown_user_or_password';

export const CPLoginResultCode = {
    Success: 'success' as CPLoginResultCode,
    AccountBlocked: 'account_blocked' as CPLoginResultCode,
    PasswordExpired: 'password_expired' as CPLoginResultCode,
    UnknownUserOrPassword: 'unknown_user_or_password' as CPLoginResultCode
};
