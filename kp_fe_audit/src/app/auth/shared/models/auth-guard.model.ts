﻿export interface AuthGuardModel {
  canAccessToUrl: boolean;
  defaultPrivatePage: string;
  changingPasswordRequired: boolean;
}
