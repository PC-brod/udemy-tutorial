import { LoginResult } from "./login-result.model";

export interface RedirectLoginResponseModel {
	redirectLoginResponse: {
		redirectLoginResult: LoginResult;
	}
}