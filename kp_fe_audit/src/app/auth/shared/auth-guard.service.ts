
import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild } from '@angular/router';
import 'rxjs/add/operator/toPromise';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs/Observable';
import { LoggerService } from '../../shared/logger.service';
import { SharedService } from '../../shared/shared.service';
import { LoaderIndicatorService } from '../../shared/LoaderIndicator.service';
import 'rxjs/add/operator/catch';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {

  constructor(private authService: AuthService,
    private router: Router,
    private log: LoggerService,
    private sharedService: SharedService,
    private loaderService: LoaderIndicatorService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    // window.scroll(0, 0);
    this.log.debug(state.url);

    if (this.authService.inUpdatePasswordProcess) {
      this.log.warn("Can't activate url. User in update-password process.");
      this.sharedService.toggleDangerAlert("global.errors.updatePasswordCancel", null);
      this.loaderService.disableLoader();
      return Observable.of(false);
    }

    this.log.debug('state.url: ' + state.url);
    const needLogin = this.checkUrl(state.url);
    if (needLogin === undefined) {
      window.scroll(0, 0);
      this.log.warn("Can't activate url. needLogin undefined.");
      this.router.navigate(['/portal']);
      return Observable.of(false);
    }

    this.log.debug('needlogin: ' + needLogin);

    if (!this.authService.login && needLogin) {
      this.log.debug('canActivate in if');
      const sessionId = this.authService.getSessionId();
      this.log.debug('canActivate sessionId: ' + sessionId);
      if (sessionId !== undefined && sessionId !== null) {
        return this.authService.reloginUser$({ sessionId: sessionId }).map((res) => {
          this.log.debug(JSON.stringify(res));
          this.log.debug('canActivate reloginUser: ' + res);
          this.log.debug('canActivate accessTokenExpiresIn: ' + res.accessTokenExpiresIn);
          this.log.debug('canActivate accessTokenExpiration: ' + res.accessTokenExpiration);
          this.log.debug('canActivate clientId: ' + res.clientId);
          this.log.debug('canActivate accessToken: ' + res.accessToken);
          this.log.debug('canActivate inAccessRecovery: ' + res.inAccessRecovery);
          this.log.debug('canActivate activated: ' + res.activated);
          this.log.debug('canActivate sessionId: ' + res.sessionId);

          if (res === null || res === undefined || res.clientId === undefined || res.accessToken === undefined
            || res.accessTokenExpiration === undefined) {
            this.authService.login = false;
            this.log.warn("Can't activate url. Incomplete login data: " + JSON.stringify(res));
            this.router.navigate(['/portal']);
            return false;
          }

          this.authService.setLoginInfo(res.accessTokenExpiresIn, res.accessTokenExpiration, res.clientId, res.accessToken,
            this.authService.inAccessRecovery, this.authService.activated, sessionId, null);
          this.log.debug('canActivate res: ' + res);
          this.authService.login = true;
          this.log.debug('canActivate login: ' + this.authService.login);
          window.scroll(0, 0);
          this.loaderService.disableLoader();
          this.sharedService.loadConsultant();
          return true;
        }).catch((e) => {
          this.log.error(e);
          this.authService.login = false;
          this.loaderService.disableLoader();
          this.log.warn("Can't activate url. Error occured during relogin.");
          this.router.navigate(['/portal']);
          return Observable.of(false);
        });
      } else {
        this.loaderService.disableLoader();
        this.log.warn("Can't activate url. User not logged.");
        this.router.navigate(['/portal']);
        return Observable.of(false);
      }
    } else {
      this.log.debug('canActivate else');
      if (this.authService.login !== needLogin) {
        this.log.debug('HEY' + this.authService.login + needLogin);
        this.loaderService.disableLoader();
        this.log.warn("Can't activate url. User not logged or redirected to non-client site.");
        this.router.navigate(['/portal']);
        return Observable.of(false);
      }
      return Observable.of(true);
    }

  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.canActivate(route, state);
  }

  checkUrl(url: string): boolean {
    this.log.debug('checkingURL = ' + url);
    if ((url === '/updatePassword')
      || (url === '/changePassword')
      || (url === '/verifyUpdate')
      || (url === '/binder')
      || (url === '/edit/:contractId')
      || (url === '/dashboard')
      || (url === '/finance')
      || (url === '/messages')
      || (url === '/onboarding')
      || (url === '/profile')
      || (url === '/selfservice')
      || (url === '/iframe')
      || (url === '/form')
      || (url === '/formAml')
      || (url.startsWith('/?nduri=%2Fselfservice'))
      || (url.startsWith('/redirect'))
      || (url.startsWith('/binder'))) {
      return true;
    } else if ((url.startsWith('/portal'))
      || (url === '/')
      || (url === '/login')
      || (url === '/login/:showlogin')
      || (url === '/verify')
      || (url === '/recover')
      || (url === '/forgot')
      || (url.startsWith('/news'))
      || (url.startsWith('/newsDetail'))
     /*|| (url === '/termConditions')*/) { // term conditions je mozne zobrazit odkudkoli :)
      return false;
    } else { return undefined; }
  }
}
