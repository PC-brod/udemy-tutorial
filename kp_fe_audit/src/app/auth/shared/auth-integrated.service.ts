import { Injectable, EventEmitter, isDevMode, Inject } from '@angular/core';
import { Http, Response, RequestMethod } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { assign, interpolate } from '../../../app/utils/common.utils';
import { AuthCheckModel } from './models/authcheck.model';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ChangeDetectorRef, Input, Output } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { TimerObservable } from 'rxjs/observable/TimerObservable';
import { AuthGuardModel } from './models/auth-guard.model';
import { AuthService } from './auth.service';
import { RedirectLoginModel } from './models/redirect-login.model';
import { PasswordChangeResultModel } from '../password-change/password-change.model';
import { PasswordUpdateResultModel } from '../password-update/password-update.model';
import { NotLoged } from '../../shared/models/not-loged.model';
import { ResConsultant } from '../../shared/models/res-consultant.model';
import { LoginResult } from './models/login-result.model';
import { BodyUsernamePasswordCaptcha, ReqBodyUsernamePasswordWithBrowserInfo } from '../login/inner-login/body-username-password.model';
import { ResGDPRConsentState } from '../../shared/models/res-gdpr-consent-state.model';
import { ReLoginModel } from './models/relogin.model';
import { CookieService } from 'ngx-cookie-service';
// import { CookieOptions } from 'angular2-cookie/services/base-cookie-options';
import { CookieOptionsArgs } from 'angular2-cookie/services/cookie-options-args.model';
import { ReqBodyBirthnumber } from './models/req-body-birth-number.model';
import { InlineResponse } from './models/inline-response.model';
import { ReqBodyUsername } from './models/req-body-username.model';
import { ApiConfig } from '../../api.model';
import { apiConfigToken } from '../../api.di';
import { EnvironmentConfig } from '../../environment.config';
import { RefreshTokenResult } from './models/refresh-token-result.model';
import { LoggerService } from '../../shared/logger.service';
import 'rxjs/add/operator/concatMap';
import 'rxjs/add/operator/mergeMap';
import { RedirectLoginResponseModel } from './models/redirect-login-response.model';

@Injectable()
export class AuthIntegratedService extends AuthService {

  constructor(
    @Inject(apiConfigToken) private config: ApiConfig,
    private http: HttpClient,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private cookieService: CookieService,
    private log: LoggerService
  ) {
    super();
  }

  identifyUser$(body?: ReqBodyBirthnumber): Observable<InlineResponse> {
    return this.http.post(EnvironmentConfig.settings.env.uriPrefixV2 + this.config.identifyUserUrl, body)
      .map((res: InlineResponse) => {
        if (!res) {
          throw new Error(`Client with id ${body.birthNumber} do not exists.`);
        }
        return res;
      });
  }

  setLoginInfo(accessTokenExpiresIn: string, accessTokenExpiration: string, clientId: string, accessToken: string,
    inAccessRecovery: boolean, activated: boolean, sessionId: string, username: string) {
    this.accessTokenExpiresIn = Number(accessTokenExpiresIn);
    // expected format of 'accessTokenExpiration' is dd.MM.yyyy hh:mm:ss
    let expirationDateTime: string[] = accessTokenExpiration.split(" ");
    // value of 'accessTokenExpiration' is converted to yyyy-MM-ddThh:mm:ss
    this.accessTokenExpiration = new Date(expirationDateTime[0].split(".").reverse().join("-") + 'T' + expirationDateTime[1]);
    this.clientId = clientId;
    this.username = username;
    this.accessToken = accessToken;
    this.inAccessRecovery = inAccessRecovery;
    this.GDPRShowed = true;
    this.activated = activated;
    this.setSessionCookie(sessionId);
  }

  changePassword$(oldPass: string, newPass: string, gRecaptchaResponse: string): Observable<PasswordChangeResultModel> {
    let body: any;
    if (gRecaptchaResponse) {
      body = { oldPassword: oldPass, newPassword: newPass, username: this.username, "g-recaptcha-response": gRecaptchaResponse };
    } else {
      body = { oldPassword: oldPass, newPassword: newPass, username: this.username };
    }
    return this.put$(interpolate(EnvironmentConfig.settings.env.uriPrefixV2 + this.config.changePasswordUrl, { clientId: this.clientId }), body)
      .map((res: PasswordChangeResultModel) => {
        if (!res) {
          throw new Error(`Password change unknown result.`);
        }
        return res;
      });
  }

  updatePassword$(newPass: string): Observable<PasswordUpdateResultModel> {
    return this.put$(interpolate(EnvironmentConfig.settings.env.uriPrefix + this.config.updatePasswordUrl, { clientId: this.clientId })
      , { newPassword: newPass })
      .map((res: PasswordUpdateResultModel) => {
        if (!res) {
          throw new Error(`Password update unknown result.`);
        }
        return res;
      });
  }

  changeUsername$(body?: ReqBodyUsername): Observable<any> {
    body = assign(body);
    return this.put$(interpolate(EnvironmentConfig.settings.env.uriPrefix + this.config.changeUsernameUrl,
      { clientId: this.clientId }), body).map(() => null);
  }

  recoverPassword$(body?: ReqBodyUsername): Observable<InlineResponse> {
    body = assign(body);
    return this.http.post(EnvironmentConfig.settings.env.uriPrefixV2 + this.config.recoverPasswordUrl, body);
  }

  validateLoginConfirmationCode$(code: string, gRecaptchaResponse: string): Observable<LoginResult> {
    let body: any;
    if (gRecaptchaResponse) {
      body = { code: code, username: this.username, "g-recaptcha-response": gRecaptchaResponse };
    } else {
      body = { code: code, username: this.username };
    }
    return this.post$(interpolate(EnvironmentConfig.settings.env.uriPrefixV2 + this.config.validateLoginConfirmationCodeUrl,
      { clientId: this.clientId }),
      body)
      .map((res: LoginResult) => {
        if (!res) {
          throw new Error(`These logins do not exists.`);
        }
        return res;
      });
  }

  verifyConfirmationCode$(code: string, gRecaptchaResponse: string): Observable<any> {
    let body: any;
    if (gRecaptchaResponse) {
      body = { code: code, clientId: this.clientId, "g-recaptcha-response": gRecaptchaResponse };
    } else {
      body = { code: code, clientId: this.clientId };
    }
    return this.post$(interpolate(EnvironmentConfig.settings.env.uriPrefixV2 + this.config.verifyConfirmationCodeUrl,
      { clientId: this.clientId }),
      body)
      .map((res: LoginResult) => {
        if (!res) {
          throw new Error(`These logins do not exists.`);
        }
        return res;
      });
  }

  redirectLogin$(code: string): Observable<LoginResult> {
    // return Observable.timer(100).map(() => {const resp: LoginResult =  {accessToken:"1547853769197_bc####testing15@mxs.sk_clientPortal_bVD26QRvKL7Ht37hQ5K9Ac82F4F746_5519917_KTRv2",accessTokenExpiresIn:"1799",activated:true,clientId:"Client_1034082","inAccessRecovery":false,requiresSMSVerification:false,resultCode:"SUCCESS", accessTokenExpiration:"19.01.2019 02:06:01"}; return resp;});
    const body = {
      code: code
    };
    this.log.debug('VOLANI POST: ' + EnvironmentConfig.settings.env.uriPrefix + this.config.redirectLoginUrl + ' BODY: ' + code);
    return this.http.post(EnvironmentConfig.settings.env.uriPrefix + this.config.redirectLoginUrl, body)
      .map((res: LoginResult) => {
        this.log.debug('RESPONSE: ' + res);
        if (!res) {
          throw new Error(`These logins do not exists.`);
        }
        return res;
      });
  }

  loginUser$(body: BodyUsernamePasswordCaptcha): Observable<LoginResult> {
    const extBody: ReqBodyUsernamePasswordWithBrowserInfo = {
      password: body.password,
      username: body.username,
      clientIP: '10.0.0.0', // musi si dotahovat WSO2
      userAgent: window.navigator.userAgent, // musi si dotahovat WSO2
      "g-recaptcha-response": body["g-recaptcha-response"]
    };

    return this.http.post(EnvironmentConfig.settings.env.uriPrefixV2 + this.config.loginUrl, extBody)
      .map((res: LoginResult) => {
        console.log(res);
        if (!res) {
          throw new Error(`These logins do not exists.`);
        }
        else if (res.resultCode != "UNKNOWN_USER_OR_PASSWORD") {
          this.sessionTimerStart();
        }
        return res;
      });
  }

  getUnauthorized(url) {
    return this.http.get(url, { headers: this.getUnauthorizedHttpHeaders() });
  }

  postUnauthorized(url, data) {
    return this.http.post(url, data, { headers: this.getUnauthorizedHttpHeaders() });
  }

  putUnauthorized(url, data) {
    return this.http.put(url, data, { headers: this.getUnauthorizedHttpHeaders() });
  }

  deleteUnauthorized(url) {
    return this.http.delete(url, { headers: this.getUnauthorizedHttpHeaders() });
  }

  get$(url) {
    this.sessionTimerStart();
    if (this.isAccessTokenInvalid()) {
      return this.refreshAccessToken(RequestMethod.Get, url, null);
    }

    return this.httpGet(url);
  }

  private httpGet(url): Observable<any> {
    this.log.debug("GET : " + url + "\n httpHeaders: " + JSON.stringify(this.getHttpHeaders()));
    return this.http.get(url, { headers: this.getHttpHeaders() });
  }

  post$(url, data, additionalOptions?) {
    this.sessionTimerStart();
    if (this.isAccessTokenInvalid()) {
      return this.refreshAccessToken(RequestMethod.Post, url, data);
    }
    return this.httpPost(url, data, additionalOptions);
  }

  private httpPost(url, data, additionalOptions?): Observable<any> {
    let options = { headers: this.getHttpHeaders() };
    if (additionalOptions) {
      options = { ...options, ...additionalOptions };
    }
    this.log.debug("POST : " + url + "\n data: " + JSON.stringify(data) + "\n httpHeaders: " + JSON.stringify(options));
    return this.http.post(url, data, options);
  }

  put$(url, data) {
    this.sessionTimerStart();
    if (this.isAccessTokenInvalid()) {
      return this.refreshAccessToken(RequestMethod.Put, url, data);
    }
    return this.httpPut(url, data);
  }

  private httpPut(url, data): Observable<any> {
    this.log.debug("PUT : " + url + "\n data: " + JSON.stringify(data) + "\n httpHeaders: " + JSON.stringify(this.getHttpHeaders()));
    return this.http.put(url, data, { headers: this.getHttpHeaders() });
  }

  delete$(url) {
    this.sessionTimerStart();
    if (this.isAccessTokenInvalid()) {
      return this.refreshAccessToken(RequestMethod.Delete, url, null);
    }
    return this.httpDelete(url);
  }

  private httpDelete(url): Observable<any> {
    this.log.debug("DELETE : " + url + "\n httpHeaders: " + JSON.stringify(this.getHttpHeaders()));
    return this.http.delete(url, { headers: this.getHttpHeaders() });
  }

  refreshToken(): Observable<any> {
    const refreshTokenBody = {
      sessionId: this.getSessionId()
    };
     return this.http
       .post(interpolate(EnvironmentConfig.settings.env.uriPrefix + this.config.refreshTokenUrl, { clientId: this.clientId }),
         refreshTokenBody).map((res: RefreshTokenResult) => {
        if (!res) {
          throw new Error(`Cannot refresh access token.`);
        }
        console.log(res);
        this.accessToken = res.accessToken;
        this.accessTokenExpiresIn = res.accessTokenExpiresIn;
        let expirationDateTime: string[] = res.accessTokenExpiration.split(" ");
        this.accessTokenExpiration = new Date(expirationDateTime[0].split(".").reverse().join("-") + 'T' + expirationDateTime[1]);

        // update BCID cookie
        this.setSessionCookie(refreshTokenBody.sessionId);
        
      });
  }

  private refreshAccessToken(httpMethod: RequestMethod, url: string, data: any): Observable<any> {
    const refreshTokenBody = {
      sessionId: this.getSessionId()
    };

    return this.http
      .post(interpolate(EnvironmentConfig.settings.env.uriPrefix + this.config.refreshTokenUrl, { clientId: this.clientId }),
        refreshTokenBody)
      .flatMap((res: RefreshTokenResult) => {
        if (!res) {
          throw new Error(`Cannot refresh access token.`);
        }
        this.accessToken = res.accessToken;
        this.accessTokenExpiresIn = res.accessTokenExpiresIn;
        let expirationDateTime: string[] = res.accessTokenExpiration.split(" ");
        this.accessTokenExpiration = new Date(expirationDateTime[0].split(".").reverse().join("-") + 'T' + expirationDateTime[1]);

        // update BCID cookie
        this.setSessionCookie(refreshTokenBody.sessionId);
        if (url !== null) {
          switch (httpMethod) {
            case RequestMethod.Get: return this.httpGet(url);
            case RequestMethod.Post: return this.httpPost(url, data);
            case RequestMethod.Put: return this.httpPut(url, data);
            case RequestMethod.Delete: return this.httpDelete(url);
            default: throw new Error(`Unknown HTTP method`);
          }
      }
      });
  }

  getHttpHeaders(accessToken?: string) {
    return { 'Content-Type': 'application/json; charset=utf-8', 'accessToken': accessToken ? accessToken : this.accessToken };
  }

  getUnauthorizedHttpHeaders() {
    return { 'Content-Type': 'application/json; charset=utf-8' };
  }

  getClientId(): string { return this.clientId; }

  isAccessTokenInvalid(): boolean {
    if (this.accessTokenExpiration != undefined) {
      return this.accessTokenExpiration.valueOf() < Date.now();
    }
    else {
      return false;
    }
  }

  initCheck(): Promise<AuthCheckModel> {
    return null;
  }

  check(): boolean {
    return this.login;
  }

  checkUrl(url: string): AuthGuardModel {
    return {
      canAccessToUrl: this.login,
      defaultPrivatePage: '',
      changingPasswordRequired: false
    };
  }

  setSessionCookie(sessionId: string) {
    this.log.debug('preparing cookie');
    const cookieDate = new Date();
    cookieDate.setMinutes(cookieDate.getMinutes() + 20);
    this.log.debug(cookieDate);
    // const options: CookieOptionsArgs = {
    // path: '' // The cookie will be available only for this path and its sub-paths.
    // By default, this is the URL that appears in your <base> tag.
    // domain: '' // The cookie will be available only for this domain and its sub-domains.
    // For security reasons the user agent will not accept the cookie
    // if the current domain is not a sub-domain of this domain or equal to it.
    // expires: cookieDate, // {string|Date} - String of the form "Wdy, DD Mon YYYY HH:MM:SS GMT"
    // or a Date object indicating the exact date/time this cookie will expire.
    // secure: true // If true, then the cookie will only be available through a secured connection.
    // };
    this.log.debug(sessionId);
    this.cookieService.set('BCID', sessionId, cookieDate/*, options*/);
    this.log.debug('after cookie');
  }

  getSessionId(): string {
    this.log.debug('get cookie');

    if (!this.cookieService.check('BCID')) {
      this.log.debug('session ID NENALEZENA');
      return null;
    }
    else {
      this.sessionIdLogout = this.cookieService.get('BCID');
      this.log.debug('SESSION ID = ' + this.sessionIdLogout);
      return this.sessionIdLogout;
    }
  }

  reloginUser$(body: ReLoginModel): Observable<LoginResult> {
    return this.http.post(EnvironmentConfig.settings.env.uriPrefix + this.config.reloginUrl, body)
      .map((res: LoginResult) => {
        if (!res) {
          throw new Error(`These logins do not exists.`);
        }
        return res;
      });
  }

  logout$(): Observable<any> {
    const res: Observable<any> = this.post$(interpolate(EnvironmentConfig.settings.env.uriPrefix + this.config.logoutUrl,
      { clientId: this.clientId }),
      { sessionId: this.getSessionId() }).map(() => { this.logout(); });

    this.sessionTimerStop();
    return res;
  }

  logout(): Promise<void> {
    this.login = false;
    this.relogin = false;
    this.accessTokenExpiresIn = null;
    this.clientId = null;
    this.resultCode = null;
    this.requiresSMSVerification = null;
    this.accessToken = null;
    this.loginInfo = false;
    if (this.cookieService.check('BCID')) {
      this.cookieService.delete('BCID');
    }
    this.sessionTimerStop();
    console.log('logging out...');
    return null;
  }

  init(): Promise<void> {
    return null;
  }

  getOAuthCode$(): Observable<RedirectLoginModel> {
    return this.get$(interpolate(EnvironmentConfig.settings.env.uriPrefix + this.config.accessTokenUrl, { clientId: this.clientId }))
      .map((res: RedirectLoginModel) => {
        if (!res) {
          throw new Error(`These logins do not exists.`);
        }
        return res;
      });
  }

  private initOAuthCode(): void {
    this.get$(interpolate(EnvironmentConfig.settings.env.uriPrefix + this.config.accessTokenUrl, { clientId: this.clientId }))
      .toPromise()
      .then((res: RedirectLoginModel) => {
        this.log.debug('getOAuthCode response: ' + res);
        this.oauthCode = res.code;
      })
      .catch((error) => {
        this.log.error('cannot load oauth access code');
        this.log.error(error);
      });
  }

  getOAuthCode(): string {
    this.initOAuthCode();
    return this.oauthCode;
  }

  validateBCID(): void {
    this.log.debug('VALIDATING BCID SESSION');
    this.log.debug(this.cookieService.get('BCID'));
    if (!this.cookieService.check('BCID') || this.cookieService.get('BCID') === 'undefined') {
        this.logout();
        this.router.navigate(['/portal/logout']);
      
    }

  }

  sessionTimerStart(): void {
    if (this.sessionTimerSubscription) {
      this.sessionTimerSubscription.unsubscribe();
    }

    // update BCID cookie
    this.setSessionCookie(this.getSessionId());
    let dateOfExpiration = new Date();
    dateOfExpiration.setMinutes(dateOfExpiration.getMinutes() + 20);
    // 20 minutes

    //setInterval(() => { if (this.login && dateOfExpiration < new Date()) { this.logout(); this.router.navigate(['/portal/logout']); } }, 1000)

    this.sessionTimerSubscription = Observable.timer(5000, 1000).subscribe(x => {
      if (this.login && dateOfExpiration < new Date()) {
        this.logout();
        this.router.navigate(['/portal/logout']);
      }
    });
  
  }

  sessionTimerStop(): void {
    if (this.sessionTimerSubscription) {
      this.sessionTimerSubscription.unsubscribe();
      this.sessionTimerSubscription = undefined;
    }
  }

  public isCaptchaRequired(e: HttpErrorResponse): boolean {
    return e
      && e.status === 400
      && e.error
      && e.error.CAPTCHA_REQUIRED;
  }

  public isCaptchaNotVerified(e: HttpErrorResponse): boolean {
    return e
      && e.status === 403;
  }
}
