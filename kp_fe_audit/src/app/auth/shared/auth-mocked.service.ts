import { Injectable, Inject } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { AuthCheckModel } from './models/authcheck.model';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthGuardModel } from './models/auth-guard.model';
import { AuthService } from './auth.service';
import { RedirectLoginModel } from './models/redirect-login.model';
import { LoginResult } from './models/login-result.model';
import { BodyUsernamePasswordCaptcha } from '../login/inner-login/body-username-password.model';
import { ReLoginModel } from './models/relogin.model';
import { ReqBodyBirthnumber } from './models/req-body-birth-number.model';
import { InlineResponse } from './models/inline-response.model';
import { IdentificationStatus } from './models/identification-status.model';
import { ReqBodyUsername } from './models/req-body-username.model';
import { ApiConfig } from '../../api.model';
import { apiConfigToken } from '../../api.di';
import { RedirectLoginResponseModel } from './models/redirect-login-response.model';

const delay = 500;

@Injectable()
export class AuthMockedService extends AuthService {

    refreshToken():Observable<any> {
        throw new Error("Method not implemented.");
    }

  constructor(
    @Inject(apiConfigToken) private config: ApiConfig,
    private http: HttpClient,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    super();
  }


    identifyUser$(body?: ReqBodyBirthnumber): Observable<InlineResponse> {
      return null; // TBD
    }

	setLoginInfo(accessTokenExpiresIn: string, accessTokenExpiration: string, clientId: string, accessToken: string,
		inAccessRecovery: boolean, activated: boolean, sessionId: string, username: string) {
      this.accessTokenExpiresIn = Number(accessTokenExpiresIn);
	  this.clientId = clientId;
	  this.username = username;
      this.accessToken = accessToken;
      // this.oauthCode = 'mockedOAuthCode';
      this.inAccessRecovery = inAccessRecovery;
      this.GDPRShowed = !inAccessRecovery;
      this.activated = activated;
  }
  validateBCID(): void {

  }

    changePassword$(oldPass: string, newPass: string, gRecaptchaResponse: string): Observable<any> {
      return Observable.timer(delay).map(() => {
        let resp = 'OK';
        if (newPass === 'Unknown1') {
          resp = 'FAIL_UNKNOWN_USER_OR_PASSWORD';
        } else if (newPass === 'Blocked1') {
          resp = 'FAIL_ACCOUNT_BLOCKED';
        } else if (newPass === 'Weak0001') {
          resp = 'FAIL_PASSWORD_IS_WEAK';
        }
        return resp;
      });
    }

    changeUsername$(body?: ReqBodyUsername): Observable<any> {
      return Observable.timer(delay).map(() => {
        let resp = 'OK';
        if (body.userName === 'invalid') {
          resp = 'FAIL_INVALID_USERNAME';
        } else if (body.userName === 'collision') {
          resp = 'FAIL_USERNAME_COLLISION';
        }
        return resp;
      });
    }

    recoverPassword$(body?: ReqBodyUsername): Observable<InlineResponse> {
      return Observable.timer(delay).map(() => {
        return {clientId: 100000000000001,
                recoveryEmailSent: true,
                status: IdentificationStatus.EXISTINGCLIENTCPACTIVATED };
      });
    }

    updatePassword$(newPass: string): Observable<any> {
      return Observable.timer(delay).map(() => {
        let resp = 'OK';
        if (newPass === 'Unknown1') {
          resp = 'FAIL_UNKNOWN_USER_OR_PASSWORD';
        } else if (newPass === 'Blocked1') {
          resp = 'FAIL_ACCOUNT_BLOCKED';
        } else if (newPass === 'Weak0001') {
          resp = 'FAIL_PASSWORD_IS_WEAK';
        }
        return resp;
      });
    }

    validateLoginConfirmationCode$(code: string, gRecaptchaResponse: string): Observable<LoginResult> {
      return Observable.timer(delay).map(() => {
        const resp: LoginResult = {
          accessToken: 'accessToken123456789qwerty',
          accessTokenExpiresIn: '5000',
          clientId: 'testID',
          requiresSMSVerification: false,
          resultCode: 'SUCCESS',
          inAccessRecovery: false,
          activated: true
        };
        if (code === 'blocked') {
          resp.resultCode = 'ACCOUNT_BLOCKED';
        } else if (code === 'expired') {
          resp.resultCode = 'PASSWORD_EXPIRED';
        } else if (code === 'uknown') {
          resp.resultCode = 'UNKNOWN_USER_OR_PASSWORD';
        } else if (code === 'sms') {
          resp.requiresSMSVerification = true;
        } else if (code === 'recovery') {
          resp.resultCode = 'PASSWORD_EXPIRED';
          resp.inAccessRecovery = true;
        } else if (code === 'notactivated') {
          resp.activated = false;
        }
        return resp;
      });
    }

    verifyConfirmationCode$(code: string, gRecaptchaResponse: string): Observable<any> {
      return Observable.timer(delay).map(() => {
        const resp: LoginResult = {
          accessToken: 'accessToken123456789qwerty',
          accessTokenExpiresIn: '5000',
          clientId: 'testID',
          requiresSMSVerification: false,
          resultCode: 'SUCCESS',
          inAccessRecovery: false,
          activated: true
        };
        if (code === 'blocked') {
          resp.resultCode = 'ACCOUNT_BLOCKED';
        } else if (code === 'expired') {
          resp.resultCode = 'PASSWORD_EXPIRED';
        } else if (code === 'uknown') {
          resp.resultCode = 'UNKNOWN_USER_OR_PASSWORD';
        } else if (code === 'sms') {
          resp.requiresSMSVerification = true;
        } else if (code === 'recovery') {
          resp.resultCode = 'PASSWORD_EXPIRED';
          resp.inAccessRecovery = true;
        } else if (code === 'notactivated') {
          resp.activated = false;
        }
        return resp;
      });
    }

    redirectLogin$(code: string): Observable<LoginResult> {
      return Observable.timer(delay).map(() => {
        const resp: LoginResult = {
          accessToken: 'accessToken123456789qwerty',
          accessTokenExpiresIn: '5000',
          clientId: 'testID',
          requiresSMSVerification: false,
          resultCode: 'SUCCESS',
          inAccessRecovery: false,
          activated: true
        };

		return resp;
      });
    }

    loginUser$(body: BodyUsernamePasswordCaptcha): Observable<LoginResult> {
      return Observable.timer(delay).map(() => {
        const resp: LoginResult = {
          accessToken: 'accessToken123456789qwerty',
          accessTokenExpiresIn: '5000',
          clientId: 'testID',
          requiresSMSVerification: false,
          resultCode: 'SUCCESS',
          inAccessRecovery: false,
          activated: true
        };
        if (body.username === 'blocked') {
          resp.resultCode = 'ACCOUNT_BLOCKED';
        } else if (body.username === 'expired') {
          resp.resultCode = 'PASSWORD_EXPIRED';
        } else if (body.username === 'uknown') {
          resp.resultCode = 'UNKNOWN_USER_OR_PASSWORD';
        } else if (body.username === 'sms') {
          resp.requiresSMSVerification = true;
        }

        // const idx = sessions.findIndex((c) => c.userName === body.username);
        // let clId = '';
        // if (idx === -1) {
        //   const idxUsrusers = sessions.findIndex((c) => c.userName === sessions[idx].userName);
        //   clId = users[idxUsrusers].clientId;
        // } else {
        //   const session: Session = {
        //     userName: body.username
        //   };
        //   sessions.push(session);
        // }
        return resp;
      });
    }

    getSessionId() {
      // get from cookies
    }

    reloginUser$(body: ReLoginModel): Observable<LoginResult> {
      return Observable.timer(delay).map(() => {
      const resp: LoginResult = {
        accessToken: 'accessToken123456789qwerty',
        accessTokenExpiresIn: '5000',
        clientId: 'testID',
        requiresSMSVerification: false,
        resultCode: 'SUCCESS',
        inAccessRecovery: false,
        activated: true
      };
      return resp;
    });
    }

    logout$(): Observable<any> {
      this.logout();
      return Observable.timer(delay).map(() => {
        return null;
      });
    }

    // injectAuthToBody<T extends NotLoged>(req: T): any {
    //     req.apiKey = EnvironmentConfig.settings.env.apiKey;
    //     req.systemId = EnvironmentConfig.settings.env.systemId;
    //     return req;
    // }

    get$(url) {
      return this.http.get(url, { headers: this.getHttpHeaders() });
    }

    post$(url, data, additionalOptions?) {
      return this.http.post(url, data, { headers: this.getHttpHeaders() });
    }

    put$(url, data) {
      return this.http.put(url, data, { headers: this.getHttpHeaders() });
    }

    delete$(url) {
      return this.http.delete(url, { headers: this.getHttpHeaders() });
    }

    getUnauthorized(url) {
      return this.http.get(url, { headers: this.getUnauthorizedHttpHeaders() });
    }

    postUnauthorized(url, data) {
      return this.http.post(url, data, { headers: this.getUnauthorizedHttpHeaders() });
    }

    putUnauthorized(url, data) {
      return this.http.put(url, data, { headers: this.getUnauthorizedHttpHeaders() });
    }

    deleteUnauthorized(url) {
      return this.http.delete(url, { headers: this.getUnauthorizedHttpHeaders() });
    }

    getUnauthorizedHttpHeaders() {
      // return {'Content-Type': 'application/json; charset=utf-8',
      // 'systemId': EnvironmentConfig.settings.env.systemId, 'apiKey': EnvironmentConfig.settings.env.apiKey};
      return {'Content-Type': 'application/json; charset=utf-8'};
    }

    getHttpHeaders() {
      return {'Content-Type': 'application/json; charset=utf-8', 'accessToken': this.accessToken};
    }

    getClientId(): string { return this.clientId; }

    getAccesToken(): string {
      console.log(this.accessToken);
      // if (this.accessTokenExpiresIn < 0) {
      //   this.refreshAccessToken();
      // }
      return this.accessToken;
    }

    refreshAccessToken() {
      // call refresh token
      // set up new access token
      // set up expiresIn
    }

    initCheck(): Promise<AuthCheckModel> {
        // return this.http
        //     .get(this.apiUrl + "/initcheck")
        //     .toPromise()
        //     .then((res: Response) => {
        //         let resJson = res.json();
        //         this.authCheckModel.isLogged = resJson.isLogged;
        //         if (this.authCheckModel.isLogged) {
        //             this.authCheckModel.defaultPrivatePage = resJson.defaultPrivatePage;
        //             this.authCheckModel.changingPasswordRequired = resJson.changingPasswordRequired;
        //             this.authCheckModel.sessionTime = resJson.sessionTime;
        //         } else {
        //             this.authCheckModel.defaultPrivatePage = "";
        //             this.authCheckModel.changingPasswordRequired = false;
        //         }
        //         return this.authCheckModel;
        //     })
        //     .catch((error) => {
        //         this.authCheckModel.isLogged = false;
        //         this.authCheckModel.defaultPrivatePage = "";
        //         this.authCheckModel.changingPasswordRequired = false;
        //         return this.authCheckModel;
        //     });
        return null;
    }

    check(): /*Promise<AuthCheckModel>*/ boolean {
      return this.login;
        // return this.http
        //     .post(this.apiUrl + "/check", JSON.stringify({ isLogged: this.authCheckModel.isLogged }), { headers: this.headers })
        //     .timeout(30000)
        //     .toPromise()
        //     .then((res: Response) => {
        //         let resJson = res.json();
        //         if (this.authCheckModel.isLogged != resJson.isLogged) {
        //             if (resJson.isLogged && !resJson.changingPasswordRequired) {
        //                 this.authCheckModel.isLogged = resJson.isLogged;
        //                 this.authCheckModel.defaultPrivatePage = resJson.defaultPrivatePage;
        //                 this.authCheckModel.changingPasswordRequired = resJson.changingPasswordRequired;
        //                 this.authCheckModel.sessionTime = resJson.sessionTime;
        //                 this.authChangedEvent.emit(this.authCheckModel);
        //             } else if (!resJson.isLogged) {
        //                 this.authCheckModel.isLogged = resJson.isLogged;
        //                 this.authCheckModel.defaultPrivatePage = "";
        //                 this.authCheckModel.changingPasswordRequired = false;
        //                 this.authChangedEvent.emit(this.authCheckModel);
        //             }
        //         }
        //         if (this.authCheckModel.isLogged) {
        //             this.authCheckModel.sessionTime = resJson.sessionTime;
        //         }
        //         return this.authCheckModel;
        //     })
        //     .catch((error) => {
        //         this.authCheckModel.isLogged = false;
        //         this.authCheckModel.defaultPrivatePage = "";
        //         this.authCheckModel.changingPasswordRequired = false;
        //         return this.authCheckModel;
        //     });
    }

    checkUrl(url: string): AuthGuardModel {
      return {
        canAccessToUrl: this.login,
        defaultPrivatePage: '',
        changingPasswordRequired: false
      };
        // return this.http
        //     .post(this.apiUrl + '/checkurl', JSON.stringify({ url: url }), { headers: this.headers })
        //     .toPromise()
        //     .then((res: Response) => {
        //         const resJson = res.json();
        //         this.authCheckModel.sessionTime = resJson.sessionTime;
        //         this.checkUrlEvent.emit(this.authCheckModel);
        //         return {
        //             canAccessToUrl: resJson.canAccessToUrl,
        //             defaultPrivatePage: resJson.defaultPrivatePage,
        //             changingPasswordRequired: resJson.changingPasswordRequired
        //         };
        //     })
        //     .catch((error) => {
        //         return {
        //             canAccessToUrl: false,
        //             defaultPrivatePage: '',
        //             changingPasswordRequired: false
        //         };
        //     });
        // return null;
    }

    logout(): Promise<void> {
        this.login = false;
        this.accessTokenExpiresIn = null;
        this.clientId = null;
        this.resultCode = null;
        this.requiresSMSVerification = null;
        this.accessToken = null;
        return null;
    }

    init(): Promise<void> {
        // return this.http
        //     .get(this.apiUrl + "/init")
        //     .toPromise()
        //     .then((res: Response) => {
        //         return;
        //     })
        //     .catch(this.handleError);
        return null;
    }

    // private handleErrors(error: any): Promise<any> {
    //     // let errorMessages = error.json().ModelState.Errors;
    //     // return Promise.reject(errorMessages);
    //     return null;
    // }

    // private handleError(error: Response): Promise<Response> {
    //     // if (isDevMode)
    //     //     console.error(error);
    //     // let errorMessage = error.json().Message;
    //     // if (isDevMode)
    //     //     console.error(errorMessage);
    //     // return Promise.reject(errorMessage);
    //     return null;
    // }

    getOAuthCode$(): Observable<RedirectLoginModel> {
      return Observable.timer(delay).map(() => {
        const resp: RedirectLoginModel = {code: '45646'};
        return resp;
      });
    }

    getOAuthCode(): string {
      const resp: RedirectLoginModel = {code: '45646'};
      this.oauthCode = resp.code;
      return this.oauthCode;
    }
    
    sessionTimerStart(): void {
      if (this.sessionTimerSubscription) {
        this.sessionTimerSubscription.unsubscribe();
      }
      this.sessionTimerSubscription = Observable.timer(5 * 1000).subscribe(x => {
        this.logout$();
        this.router.navigate(['/portal']);
      });
     }
    
     sessionTimerStop(): void {
      if (this.sessionTimerSubscription) {
        this.sessionTimerSubscription.unsubscribe();
      }
     }

	 public isCaptchaRequired(e: HttpErrorResponse): boolean {
		return e 
			&& e.status === 400
			&& e.error
			&& e.error.CAPTCHA_REQUIRED;
	}

	public isCaptchaNotVerified(e: HttpErrorResponse): boolean {
		return e 
			&& e.status === 403;
	}
}
