import { HttpClient, HttpClientModule } from '@angular/common/http';
import { CookieXSRFStrategy, XSRFStrategy, HttpModule } from '@angular/http';

import { AuthRoutingModule } from './auth-routing.module';
// import { SharedModule } from '../shared/shared.module';

import { LoginComponent } from './login/login.component';
import { BrowserModule } from '@angular/platform-browser';

import { AuthService } from './shared/auth.service';
import { AuthGuard } from './shared/auth-guard.service';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { AuthMockedService } from './shared/auth-mocked.service';
import { AuthIntegratedService } from './shared/auth-integrated.service';
import { InnerLoginComponent } from './login/inner-login/inner-login.component';
import { RegistrationComponent } from './registration/registration.component';
import { UsernameRecoveryComponent } from './username-recovery/username-recovery.component';
import { PasswordUpdateComponent } from './password-update/password-update.component';
import { UpdateVerifyComponent } from './update-verify/update-verify.component';
import { CookieService } from 'ngx-cookie-service';
import { CookieBackendService } from 'angular2-cookie/services/cookies.backend.service';
import { MomentModule } from 'angular2-moment';
import { SharedIntegratedService } from '../shared/shared-integrated.service';
import { SharedService } from '../shared/shared.service';
import { ProfileIntegratedService } from '../profile/shared/profile-integrated.service';
import { ProfileService } from '../profile/shared/profile.service';

@NgModule({
  imports: [
      CommonModule,
      FormsModule,
      HttpModule,
      HttpClientModule,
      AuthRoutingModule,
      TranslateModule,
      MomentModule,
      BrowserModule
      // SharedModule
  ],
  declarations: [
      // LoginComponent,
      // RegistrationComponent
    ],
  providers: [
      AuthGuard,
      {
        provide: AuthService,
          // useClass: AuthMockedService
        useClass: AuthIntegratedService
    },
    {
      provide: SharedService,
      //  useClass: SharedMockedService
      useClass: SharedIntegratedService
    },
      {
        provide: CookieService,
        useClass: CookieService
    },
    {
      provide: ProfileService,
      useClass: ProfileIntegratedService
    }
  ]
})
export class AuthModule { }
