import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AuthGuard } from './shared/auth-guard.service';

import { LoginComponent } from './login/login.component';
import { PasswordChangeComponent } from './password-change/password-change.component';
import { PasswordVerifyComponent } from './password-verify/password-verify.component';
import { PasswordForgotComponent } from './password-forgot/password-forgot.component';
import { UsernameRecoveryComponent } from './username-recovery/username-recovery.component';
import { PasswordUpdateComponent } from './password-update/password-update.component';
import { UpdateVerifyComponent } from './update-verify/update-verify.component';
import { PortalComponent } from '../portal/portal.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'login',
                component: LoginComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'login/:showlogin',
                component: LoginComponent,
                canActivate: [AuthGuard]
            },
            // {
            //     path: 'changepassword',
            //     component: ChangePasswordComponent,
            //     canActivate: [AuthGuard]
            // },
            {
                path: 'updatePassword',
                component: PasswordUpdateComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'changePassword',
                component: PasswordChangeComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'verify',
                component: PasswordVerifyComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'verifyUpdate',
                component: UpdateVerifyComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'forgot',
                component: PasswordForgotComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'recover',
                component: UsernameRecoveryComponent,
                canActivate: [AuthGuard]
        }, {
              path: 'portal',
              component: PortalComponent,
              canActivate: [AuthGuard]
        }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class AuthRoutingModule { }
