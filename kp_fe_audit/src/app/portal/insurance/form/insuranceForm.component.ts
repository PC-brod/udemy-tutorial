﻿import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ChangeDetectorRef, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
    selector: 'app-insurance-form',
    templateUrl: './insuranceForm.component.html'
})
export class InsuranceFormComponent {

    i18NPrefix = 'portal.insurance.form.';
    constructor(
      private fb: FormBuilder,
      private cd: ChangeDetectorRef,
      private activatedRoute: ActivatedRoute,
      private router: Router
    ) { }
}
