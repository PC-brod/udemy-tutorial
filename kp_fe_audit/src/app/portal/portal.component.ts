import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ChangeDetectorRef, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { notBlankValidator, phoneValidator } from '../validation/validation.validators';
import { trimToNull } from '../validation/validation.utils';
import { CallbackRequest } from '../shared/models/callback-request.model';
import { MessagesService } from '../shared/messages.service';
import { LoggerService } from '../shared/logger.service';
import { LoaderIndicatorService } from '../shared/LoaderIndicator.service';

declare function toggleLogoutModal(): Function;

@Component({
    selector: 'app-portal',
    templateUrl: './portal.component.html',
    styleUrls: ['./portal.component.css'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PortalComponent implements OnInit {

  i18NPrefix = 'root.';
  i18NPrefixHeader = 'header.after-login.';
  i18NPrefixFooter = 'footer.';
  i18NErrorPrefix = 'global.errors.';

  redirectLoginFailed = false;
  invalidated: boolean = false;

  constructor(
    private messagesService: MessagesService,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    private router: Router,
	private log: LoggerService,
	private loaderService: LoaderIndicatorService
  ) {

    this.activatedRoute.queryParams.subscribe(params => {
      if (params['redirectLogin'] === 'failed') {
        this.redirectLoginFailed = true;
      }
    });
  }

  alreadySubmitted = false;
  submitting = false;
  submitError = false;
  infoShow = false;
  submitErrorString = 'submitError';
  infoString = '';
  form: FormGroup;
  phoneNumber: string;

	ngOnInit(): void {
		this.buildForm();		

		this.invalidated = false;
		if (this.router.url.indexOf('/portal/logout') > -1) {
			this.loaderService.disableLoader();
			this.invalidated = true;
			toggleLogoutModal();
			return;
      }
	}

  private buildForm(): void {
    this.form = this.fb.group({
    phoneNumber: [this.phoneNumber, Validators.compose([notBlankValidator, phoneValidator])]
    });
  }

onSubmit(form: FormGroup): void {
  this.alreadySubmitted = true;
  if (!form.valid || this.submitting) {
    return;
  }
  this.submitting = true;
  this.submitError = false;
  this.submitErrorString = 'submitError';

  const value = this.form.value;
  value.phoneNumber = trimToNull(value.phoneNumber);

  const body: CallbackRequest = {
    phoneNumber: value.phoneNumber
  };

  this.messagesService.sendCallbackRequest$(body).subscribe((res) => {
    this.submitting = false;
    this.alreadySubmitted = false;
    this.infoShow = true;
    this.infoString = 'callbacksuccess';
    this.resetForm();
    this.cd.markForCheck();
  }, (e) => {
    this.log.error(e);
    this.submitError = true;
    this.submitting = false;
    this.alreadySubmitted = false;
    this.cd.markForCheck();
  });
  }

  resetForm() {
    if (this.form) {
      this.form.reset();
    }
    this.alreadySubmitted = false;
    this.submitting = false;
    this.submitError = false;
    this.cd.markForCheck();
  
  }

}
