﻿import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ChangeDetectorRef, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
    selector: 'app-carousel-cards',
    templateUrl: './carouselCards.component.html',
    styleUrls: ['./carouselCards.component.css']
})
export class CarouselCardsComponent {

  i18NPrefix = 'portal.carousel-cards.';
  constructor(
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }
}
