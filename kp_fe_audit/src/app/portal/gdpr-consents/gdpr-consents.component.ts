import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation, OnChanges, AfterViewInit
  , DoCheck, AfterContentInit, AfterContentChecked, AfterViewChecked, ViewChild, ElementRef } from '@angular/core';
import { ChangeDetectorRef, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DatePipe } from '@angular/common';

import { Router, ActivatedRoute, Params } from '@angular/router';
import { AuthService } from '../../auth/shared/auth.service';
import { GDPRConsents } from '../../../mocks/mock';
import { SharedService } from '../../shared/shared.service';
import { ResGDPRConsentState } from '../../shared/models/res-gdpr-consent-state.model';
import { LoggerService } from '../../shared/logger.service';
import { TranslateService } from '@ngx-translate/core';
import { LoaderIndicatorService } from '../../shared/LoaderIndicator.service';

declare function openModal(): Function;
declare function closeGdprModal(): Function;

@Component({
  selector: 'app-gdpr-consents',
  templateUrl: './gdpr-consents.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GdprConsentsComponent {
  i18NPrefix = 'gdpr.';
  @Output() modalClosed = new EventEmitter<any>();
  GDPRConsentsEdit: Array<ResGDPRConsentState> = [];
  editableMap: Map<string, boolean>;

  constructor(
    private sharedService: SharedService,
    private authService: AuthService,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private log: LoggerService,
    private translate: TranslateService,
	private loaderService: LoaderIndicatorService

  ) {
	this.loaderService.showLoader();
    this.sharedService.GDPRModalOpened.subscribe(() => {
      Object.assign(this.GDPRConsentsEdit, this.getGdprConsents());
      this.cd.markForCheck();
	  this.loaderService.disableLoader();
    });
  }

  consentsEditable(): boolean {
    return this.authService.login;
  }

  getGdprConsents() {
    this.editableMap = new Map<string, boolean>();
    this.sharedService.GDPRConsents.forEach(cons => {
      this.editableMap.set(cons.consentTypeId, this.consentsEditable() && !cons.accepted);
    });
    return this.sharedService.GDPRConsents;
  }

  isConsentEditable(consent: ResGDPRConsentState): boolean {
    return this.editableMap.get(consent.consentTypeId);
  }

  updateGDPR() {
	this.loaderService.showLoader();
    const datePipe = new DatePipe('en-US');

    this.sharedService.updateGDPR$({
        gdpr: this.GDPRConsentsEdit,
        clientIP: '10.0.0.0', // musi si dotahovat WSO2
        userAgent: window.navigator.userAgent,
        consentTimestamp: datePipe.transform(new Date(), 'yyyyMMddHHmmss')
      }).subscribe(() => {
        this.authService.GDPRShowed = true;
        this.cd.markForCheck();
        if (this.modalClosed) {
          this.modalClosed.emit();
          this.sharedService.scrollToTop();
          this.translate.get('gdpr.save.success').subscribe( text => {
            this.sharedService.toggleInfoAlert(text, null);
          });
        }
        this.cd.markForCheck();
		this.loaderService.disableLoader();
      }, (e) => {
		this.loaderService.disableLoader();
        this.log.error(e);
        this.close();
        this.cd.markForCheck();
        });
  }

  close() {
    if (this.modalClosed) {
      this.modalClosed.emit();
    }
    this.cd.markForCheck();
  }

}
