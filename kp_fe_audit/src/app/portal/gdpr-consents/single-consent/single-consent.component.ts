import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ChangeDetectorRef, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AuthService } from '../../../auth/shared/auth.service';
import { ResGDPRConsentState } from '../../../shared/models/res-gdpr-consent-state.model';

@Component({
  selector: 'app-consents-single-consent',
  templateUrl: './single-consent.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SingleConsentComponent implements OnInit {
    ngOnInit(): void {
      console.log(this.consent);
    }

  i18NPrefix = 'gdpr.single-consent.';

  constructor(
    private authService: AuthService,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    
  }

  @Input() consent: ResGDPRConsentState;
  @Input() editable: boolean;

  public acceptedChanged($event): void {
     this.consent.accepted = $event.target.checked;
   }

}
