import {
  ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation, OnChanges, AfterViewInit
  , DoCheck, AfterContentInit, AfterContentChecked, AfterViewChecked
} from '@angular/core';
import { ChangeDetectorRef, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AuthService } from '../../auth/shared/auth.service';
import { Location } from '@angular/common';
import { _localeFactory } from '@angular/core/src/application_module';
import { LoggerService } from '../../shared/logger.service';
declare function setActiveNav(id: string): Function;


@Component({
  selector: 'app-term-conditions-new-page',
  templateUrl: './term-conditions-new-page.component.html',
  styleUrls: ['./term-conditions-new-page.component.css'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TermConditionsNewPageComponent implements OnInit {
  i18NPrefix = 'termNewPage.';
  constructor(private _location: Location,
    private router: Router,
    private log: LoggerService,
  ) { }
  goBackRoute = '/';


  ngOnInit() {
    let href = window.location.href;
    const urlParams: URLSearchParams = new URLSearchParams(href.substring(href.indexOf("?") + 1));
    this.goBackRoute = urlParams.get('goBackRoute');
    if (this.goBackRoute && this.goBackRoute[0] === '%') {
      this.goBackRoute = '/' + this.goBackRoute.substr(3, this.goBackRoute.length - 1);
    }
    this.log.debug('gobackroute is ' + this.goBackRoute);
  }

  goBack() {
    if (!this.goBackRoute) {
      this._location.back();
    }
    else {
      this.router.navigate([this.goBackRoute]);
    }
  }

}
