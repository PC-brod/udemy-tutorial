import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation, OnChanges, AfterViewInit
  , DoCheck, AfterContentInit, AfterContentChecked, AfterViewChecked } from '@angular/core';
import { ChangeDetectorRef, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AuthService } from '../../auth/shared/auth.service';

@Component({
  selector: 'app-term-conditions',
  templateUrl: './term-conditions.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TermConditionsComponent {

  i18NPrefix = 'term-conditions.';
}
