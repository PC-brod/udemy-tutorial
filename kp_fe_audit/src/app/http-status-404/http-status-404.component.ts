import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-http-status-404',
  template: `<h1 class="my-4 text-center display-1">{{ 'http.' + '404' | translate }}</h1>`,
  encapsulation: ViewEncapsulation.None
})
export class HttpStatus404Component {

}
