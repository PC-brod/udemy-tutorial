import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, Input, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl, AbstractControl } from '@angular/forms';
import { notBlankValidator } from '../../validation/validation.validators';
import { AuthService } from '../../auth/shared/auth.service';
declare function amlCarouselN(): Function;
declare function amlCarouselP(): Function;
declare function scrollToValidationE(): Function;

@Component({
  selector: 'aml-second-page',
  templateUrl: './second-page.component.html',
  styleUrls: ['./second-page.component.css'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class SecondPageComponent implements OnInit, AfterViewInit {

  @Input() sourceOfFinances: FormGroup;
  i18NPrefix = "amlSecondPage.";
  alreadySubmitted: boolean = false;
  financialSources: Array<string> = ["Pravidelný zdroj z podnikání", "Dědictví",
    "Mzda od zaměstnavatele", "Dar", "Naspořený majetek"];
  financialSourcesControls: Array<string> = ['sourceRegularBusiness', 'sourceHeritage', 'sourceEmployeeSalary', 'sourceGift', 'sourceAccumulatedProperty']
  sourceOther: boolean = false;
  otherPositionSource: boolean = false;
  employeePositionSource: boolean = false;
  osvcPositionSource: boolean = false;
  positionSourcesError: boolean = false;
  positionSourcesCheckboxesError: boolean = true;

  constructor(private cd: ChangeDetectorRef,
    private authService: AuthService) { }

  ngAfterViewInit() {
    if (!this.isEmpty(this.sourceOfFinances.get('otherJob').value)) {
      (document.getElementById('positionOfSource-other') as HTMLInputElement).checked = true;
      this.updatingPositionsOfSource('other');
    }
    if (!this.isEmpty(this.sourceOfFinances.get('employeeJob').value)) {
      (document.getElementById('positionOfSource-employee') as HTMLInputElement).checked = true;
      this.updatingPositionsOfSource('employee');
    }
    if (!this.isEmpty(this.sourceOfFinances.get('osvcJob').value)) {
      (document.getElementById('positionOfSource-osvc') as HTMLInputElement).checked = true;
      this.updatingPositionsOfSource('osvc');
    }
    this.financialSourcesControls.forEach((control) => {
      (document.getElementById('form-' + control) as HTMLInputElement).checked = this.sourceOfFinances.get(control).value;
    });
    (document.getElementById('form-sourceOther') as HTMLInputElement).checked = this.sourceOfFinances.get('sourceOther').value;
    this.sourceOther = this.sourceOfFinances.get('sourceOther').value;
    this.cd.markForCheck();
  }

  ngOnInit() {

  }

  isEmpty(value): boolean {
    return (value === "" || value === null)
  }

  updatingPositionsOfSource(type: string) {
    switch (type) {
      case 'osvc':
        this.osvcPositionSource ? this.osvcPositionSource = false : this.osvcPositionSource = true;
        break;
      case 'employee':
        this.employeePositionSource ? this.employeePositionSource = false : this.employeePositionSource = true;
        break;
      default:
        this.otherPositionSource ? this.otherPositionSource = false : this.otherPositionSource = true;
        break;
    }
    if (!this.employeePositionSource && !this.otherPositionSource && !this.osvcPositionSource) {
      this.positionSourcesError = true;
    }
    else {
      this.positionSourcesError = false;
    }
    this.cd.markForCheck();
  }

  goBack() {
    this.authService.refreshToken().subscribe((res) => { console.log(res); });
  }

  updatingFinancialSources(index: number) {
    if (index === -1) {
      if (this.sourceOfFinances.get('sourceOther').value) {
        this.sourceOther = false;
        this.sourceOfFinances.get('sourceOther').setValue(false);
        this.sourceOfFinances.get('sourceOtherDetail').setValidators(null);
        this.sourceOfFinances.get('sourceOtherDetail').setValue(null);
      }
      else {
        this.positionSourcesCheckboxesError = false;
        this.sourceOther = true;
        this.sourceOfFinances.get('sourceOther').setValue(true);
        this.sourceOfFinances.get('sourceOtherDetail').setValidators(notBlankValidator);
      }
      this.sourceOfFinances.get('sourceOtherDetail').updateValueAndValidity();
    }
    else {
      if (this.sourceOfFinances.get(this.financialSourcesControls[index]).value) {
        this.sourceOfFinances.get(this.financialSourcesControls[index]).setValue(false);
      }
      else {
        this.sourceOfFinances.get(this.financialSourcesControls[index]).setValue(true);
        this.positionSourcesCheckboxesError = false;
      }
    }
    this.validateSourceCheckboxes();
    this.cd.markForCheck();
  }

  validateSourceCheckboxes() {
    let counter = 0;
    this.financialSourcesControls.forEach((control) => {
      if (this.sourceOfFinances.get(control).value || this.sourceOther) {
        this.positionSourcesCheckboxesError = false;
        counter++;
      }
      else {
        if (counter === 0) {
          this.positionSourcesCheckboxesError = true;
        }
      }
    });
  }

  validateSecondPage() {
    this.authService.refreshToken().subscribe((res) => { console.log(res); });
    this.alreadySubmitted = true;
    if (this.osvcPositionSource) {
      this.changeValidationSource('osvc', false);
    }
    else {
      this.changeValidationSource('osvc', true);
    }
    if (this.otherPositionSource) {
      this.changeValidationSource('other', false);
    }
    else {
      this.changeValidationSource('other', true);
    }
    if (this.employeePositionSource) {
      this.changeValidationSource('employee', false);
    }
    else {
      this.changeValidationSource('employee', true);
    }
    if (!this.employeePositionSource && !this.otherPositionSource && !this.osvcPositionSource) {
      this.positionSourcesError = true;
      scrollToValidationE();
      return;
    }
    else {
      this.positionSourcesError = false;
    }
    this.validateSourceCheckboxes();
    this.cd.markForCheck();
    if (!this.sourceOfFinances.valid || this.positionSourcesCheckboxesError) {
      scrollToValidationE();
      return;
    }
    amlCarouselN();
  }

  dateChanged($event) {
    setTimeout(() => {
      this.sourceOfFinances.get($event.target.name).setValue($event.target.value);
    }, 400);
  }

  changeValidationSource(source: string, del: boolean) {
    switch (source) {
      case 'osvc':
        if (del) {
          this.sourceOfFinances.get('expectedIncomeOsvc').setValidators(null);
          this.sourceOfFinances.get('osvcJob').setValidators(null);
          this.sourceOfFinances.get('expectedIncomeOsvc').setValue(null);
          this.sourceOfFinances.get('osvcJob').setValue(null);
        }
        else {
          this.sourceOfFinances.get('expectedIncomeOsvc').setValidators(notBlankValidator);
          this.sourceOfFinances.get('osvcJob').setValidators(notBlankValidator);
        }
        this.sourceOfFinances.get('expectedIncomeOsvc').updateValueAndValidity();
        this.sourceOfFinances.get('osvcJob').updateValueAndValidity();
        break;
      case 'employee':
        if (del) {
          this.sourceOfFinances.get('expectedIncomeEmployee').setValidators(null);
          this.sourceOfFinances.get('employeeJob').setValidators(null);
          this.sourceOfFinances.get('expectedIncomeEmployee').setValue(null);
          this.sourceOfFinances.get('employeeJob').setValue(null);
        }
        else {
          this.sourceOfFinances.get('expectedIncomeEmployee').setValidators(notBlankValidator);
          this.sourceOfFinances.get('employeeJob').setValidators(notBlankValidator);
        }
        this.sourceOfFinances.get('expectedIncomeEmployee').updateValueAndValidity();
        this.sourceOfFinances.get('employeeJob').updateValueAndValidity();
        break;
      case 'other':
        if (del) {
          this.sourceOfFinances.get('expectedIncomeOther').setValidators(null);
          this.sourceOfFinances.get('otherJob').setValidators(null);
          this.sourceOfFinances.get('expectedIncomeOther').setValue(null);
          this.sourceOfFinances.get('otherJob').setValue(null);
        }
        else {
          this.sourceOfFinances.get('expectedIncomeOther').setValidators(notBlankValidator);
          this.sourceOfFinances.get('otherJob').setValidators(notBlankValidator);
        }
        this.sourceOfFinances.get('expectedIncomeOther').updateValueAndValidity();
        this.sourceOfFinances.get('otherJob').updateValueAndValidity();
        break;
    }
  }

}
