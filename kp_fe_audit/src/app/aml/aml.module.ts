import { AmlComponent } from "./aml.component";
import { FirstPageComponent } from "./first-page/first-page.component";
import { SecondPageComponent } from "./second-page/second-page.component";
import { ThirdPageComponent } from "./third-page/third-page.component";
import { Component, NgModule, Input } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { TranslateModule } from "@ngx-translate/core";
import { ValidationModule } from "../validation/validation.module";
import { TooltipModule } from 'ngx-tooltip';
import { ModalsComponent } from './first-page/modals/modals.component';
import { AmlService } from "./aml.service";
import { AuthGuard } from "../auth/shared/auth-guard.service";
import { LoaderIndicatorService } from "../shared/LoaderIndicator.service";
import { SharedService } from "../shared/shared.service";
import { SharedIntegratedService } from "../shared/shared-integrated.service";
import { AuthService } from "../auth/shared/auth.service";
import { AuthIntegratedService } from "../auth/shared/auth-integrated.service";

@NgModule({
  imports: [
    TooltipModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    ValidationModule,
    FormsModule,
    ReactiveFormsModule],
  exports: [
    AmlComponent,
    FirstPageComponent,
    SecondPageComponent,
    ThirdPageComponent],
  declarations: [
    AmlComponent,
    FirstPageComponent,
    SecondPageComponent,
    ThirdPageComponent,
    ModalsComponent],
  providers: [
    AuthGuard,
    {
      provide: AmlService,
      // useClass: FinanceMockedService
      useClass: AmlService
    },
    {
      provide: SharedService,
      useClass: SharedIntegratedService
    },
    {
      provide: AuthService,
      useClass: AuthIntegratedService
    }
  ],
  bootstrap: [AmlComponent]
})
export class AmlModule { }
