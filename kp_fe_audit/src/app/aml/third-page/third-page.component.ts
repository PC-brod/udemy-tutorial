import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, Input, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { CPIdentDocumentType } from '../amlValues.model';
import { AuthService } from '../../auth/shared/auth.service';
import { LoaderIndicatorService } from '../../shared/LoaderIndicator.service';
declare function amlCarouselN(): Function;
declare function amlCarouselP(): Function;
declare function setDatepickerValidUntill(element, startDate): Function;
declare function openQuestionMarkModal(modal): Function;
declare function scrollToValidationE(): Function;

@Component({
  selector: 'aml-third-page',
  templateUrl: './third-page.component.html',
  styleUrls: ['./third-page.component.css'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ThirdPageComponent implements OnInit {

  approve: boolean = false;
  @Input() identityDocument: FormGroup;
  @Input() birthDate: string;
  @Input() renewalDate: string;
  @Input() dateAcquired: string;
  i18NPrefix = "amlThirdPage.";
  alreadySubmitted: boolean = false;
  hasPreviousValues = false;
  identDocumentNumberPast: string;
  identDocumentTypePast: string;
  datepickerValidE: boolean = false;

  constructor(private cd: ChangeDetectorRef,
    private authService: AuthService,
    private loaderService: LoaderIndicatorService) { }

  ngOnInit() {
    if (this.identityDocument.get('identDocumentNumber').value !== null) {
      this.hasPreviousValues = true;
      this.identDocumentTypePast = this.identityDocument.get('identDocumentType').value;
      this.identDocumentNumberPast = this.identityDocument.get('identDocumentNumber').value;
      this.identityDocument.get('identDocumentNumber').setValue(null);
      this.identityDocument.get('identDocumentType').setValue(null);
    }
    else {
      this.hasPreviousValues = false;
    }
    this.cd.markForCheck();
  }

  updatingApprove() {
    this.approve ? this.approve = false : this.approve = true;
    this.cd.markForCheck();
    return this.approve;
  }

  public identDocumentTypeList() {
    return Object.keys(CPIdentDocumentType);
  }

  cancelForm() {

  }

  switchDateFormat(dateString: string): string {
    let date = dateString.replace(/\s/g, "").split('.');
    return date[2] + '/' + date[1] + '/' + date[0];
  }

  isDateValid(date: string) {
    let today = new Date();
    today.setHours(0,0,0,0);
    let datepickerDate = new Date(this.switchDateFormat(date));
    return datepickerDate.getTime() >= today.getTime();
  }

  validateDateInPast(): boolean {

    if (this.identityDocument.get('identValidTill').value && this.identityDocument.get('identValidTill').value.length >= 8
      && (this.identityDocument.get('identValidTill').value as string).split('.').length - 1 === 2
      && (this.identityDocument.get('identValidTill').value as string).split('.')[2].length >= 4) {

      if (this.isDateValid(this.identityDocument.get('identValidTill').value)) {
        this.datepickerValidE = false;
        this.cd.markForCheck();
        return false;
      }
      else {
        this.datepickerValidE = true;
        this.identityDocument.get('identValidTill').setValue(null);
        this.cd.markForCheck();
        return true;
      }

    }
    else {
      this.datepickerValidE = false;

      this.cd.markForCheck();
      return false;
    }
  }

  validateDateValidTo() {
    if (this.isDateValid(this.identityDocument.get('identValidTill').value)) {
      this.datepickerValidE = false;

    }
    else {
      this.datepickerValidE = true;
    }
    this.cd.markForCheck();
  }

  goBack() {
    this.authService.refreshToken().subscribe((res) => { console.log(res); });
  }

  validateThirdPage() {
    this.authService.refreshToken().subscribe((res) => { console.log(res); });
    this.alreadySubmitted = true;
    this.validateDateInPast();
    this.loaderService.showLoader();
    setTimeout(() => {
      if (!this.identityDocument.valid || !this.approve || this.datepickerValidE) {
        scrollToValidationE();
        this.loaderService.disableLoader();
        return;
      }
      this.loaderService.disableLoader();
      openQuestionMarkModal('amlModalUpdate');
    }, 1200);
  }

  updateAml() {
    document.getElementById('amlSubmitButton').click();
  }

  ngAfterViewInit() {
    setDatepickerValidUntill('#form-identValidTill', new Date());
  }

  dateChanged($event) {
    setTimeout(() => {
      this.identityDocument.get($event.target.name).setValue($event.target.value);
      this.validateDateValidTo();
    }, 400);
  }
}
