import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, OnChanges, ChangeDetectorRef, AfterViewChecked } from '@angular/core';
import { SharedService } from '../../../shared/shared.service';

@Component({
  selector: 'app-aml-modals',
  templateUrl: './modals.component.html',
  styleUrls: ['./modals.component.css'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModalsComponent implements OnInit, AfterViewChecked {
    ngAfterViewChecked(): void {
      this.nonEditableProperty = this.sharedService.nonEditableProperty;
      this.cd.markForCheck();
    }
  constructor(private sharedService: SharedService,
    private cd: ChangeDetectorRef) { }
  nonEditableProperty = this.sharedService.nonEditableProperty;

  ngOnInit() {
  }
  getNonEditableProperty() {
    return this.sharedService.nonEditableProperty;
  }

}
