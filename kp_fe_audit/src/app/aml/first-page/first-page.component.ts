import {
  Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, Input,
  OnDestroy, ChangeDetectorRef, AfterViewInit, ViewChild, ElementRef, style, animate, transition, trigger, state
} from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { IsoCountry } from '../../shared/models/enums';
import { CPCountryRelationType, CPCountryRelation, CPCountryRelationFE, SensitiveActivity } from '../amlValues.model';
import { notBlankValidator } from '../../validation/validation.validators';
import { AmlService } from '../aml.service';
import { SharedService } from '../../shared/shared.service';
import { Router } from '@angular/router';
import { AuthService } from '../../auth/shared/auth.service';
import { LoaderIndicatorService } from '../../shared/LoaderIndicator.service';
declare function amlCarouselN(): Function;
declare function amlCarouselP(): Function;
declare function scrollToValidationE(): Function;

@Component({
  selector: 'aml-first-page',
  templateUrl: './first-page.component.html',
  styleUrls: ['./first-page.component.css'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class FirstPageComponent implements OnInit, AfterViewInit {
  @ViewChild('isPoliticallyExposed') isPoliticallyExposedDiv: HTMLElement;
  polliticallyExposedYes: HTMLInputElement;
  polliticallyExposedNo: HTMLInputElement;
  osvcNo: HTMLInputElement;
  osvcYes: HTMLInputElement;
  i18NPrefix = "amlFirstPage.";
  @Input() clientInformations: FormGroup;
  alreadySubmitted: boolean = false;
  hasPreviousValues: boolean = false;
  pepRelationPast: string;
  pepPositionPast: string;
  isPoliticallyExposed: boolean = false;
  isPoliticallyExposedE: boolean = true;
  isOsvc: boolean = false;
  sensitiveActivityIds: Array<string> = [];
  otherCountryRelations: Array<CPCountryRelationFE> = [];
  otherCountryRelationsBE: Array<CPCountryRelation> = [];
  activityForEnt: Array<SensitiveActivity> = [{ name: "směnárenská činnost", activityId: "AMLSensitiveBusinessActivity_101" },
  { name: "prodej či zprostředkování prodeje zájezdů", activityId: "AMLSensitiveBusinessActivity_106" },
  { name: "výkup a prodej použitého zboží", activityId: "AMLSensitiveBusinessActivity_111" },
  { name: "zprostředkování platebních transakcí", activityId: "AMLSensitiveBusinessActivity_102" },
  { name: "hedge fund", activityId: "AMLSensitiveBusinessActivity_107" },
  { name: "provozování kina", activityId: "AMLSensitiveBusinessActivity_112" },
  { name: "obchod s vozidly  a ojetými vozidly", activityId: "AMLSensitiveBusinessActivity_103" },
  { name: "zlatnictví a klenotnictví", activityId: "AMLSensitiveBusinessActivity_108" },
  { name: "provozování trafiky", activityId: "AMLSensitiveBusinessActivity_113" },
  { name: "realitní obchod", activityId: "AMLSensitiveBusinessActivity_104" },
  { name: "obchod s drahými kameny a ušlechtilými kovy", activityId: "AMLSensitiveBusinessActivity_109" },
  { name: "provozování aukčního domu", activityId: "AMLSensitiveBusinessActivity_114" },
  { name: "atomová energie", activityId: "AMLSensitiveBusinessActivity_105" },
  { name: "herní průmysl (sázkové kanceláře, herny)", activityId: "AMLSensitiveBusinessActivity_110" },
  { name: "obchod se zbranemi a střelivem", activityId: "AMLSensitiveBusinessActivity_115" }];
  getSensitiveActivitiesIdsResult: Array<SensitiveActivity> = [];

  constructor(private cd: ChangeDetectorRef,
    private amlService: AmlService,
    private sharedService: SharedService,
    private router: Router,
    private authService: AuthService,
    private loaderService: LoaderIndicatorService
  ) { }

  ngOnInit() {
    this.getAmlIds();
    if (this.clientInformations.get('pepRelation').value !== null) {
      this.hasPreviousValues = true;
      this.pepPositionPast = this.clientInformations.get('pepPosition').value;
      this.pepRelationPast = this.clientInformations.get('pepRelation').value;
      this.clientInformations.get('pepPosition').setValue(null);
      this.clientInformations.get('pepRelation').setValue(null);
    }
    else {
      this.hasPreviousValues = false;
    }
    if (this.clientInformations.get('otherCountryRelations').value.length > 0) {
      this.clientInformations.get('otherCountryRelations').value.forEach((item) => {
        this.otherCountryRelations.push({ countryCode: item.countryCode, relationTypeE: false, countryCodeE: false, relationType:item.relationType });
      });
    }
    this.cd.markForCheck();

  }

  setNonEditableProperty(value) {
    this.sharedService.nonEditableProperty = value;
    this.cd.markForCheck();
  }

  getAmlIds() {
    this.amlService.getAMLSensitiveActivities().subscribe((res) => {
      this.activityForEnt = res.getAMLSensitiveActivitiesResult;
      console.log(res);
      this.sensitiveActivityIds = this.clientInformations.get('sensitiveActivityIds').value;
       
      this.cd.markForCheck();
    },
      (e) => {
        console.log(e);
      });
  }

  ngAfterViewInit() {
    this.setCheckboxes();
    this.clientInformations.get('sensitiveActivityIds').value.forEach((item) => {
      (document.getElementById('form-' + item) as HTMLInputElement).checked = true;
      this.cd.markForCheck();
    });
    setTimeout(() => {
          if (this.router.url === '/aml') {
            this.setCheckboxesActivityIds();
          }
      this.loaderService.disableLoader();
    },1000);
  }

  setCheckboxesActivityIds() {
    this.clientInformations.get('sensitiveActivityIds').value.forEach((item) => {
      (document.getElementById('form-' + item) as HTMLInputElement).checked = true;
      this.cd.markForCheck();
    });
  }

  addOtherCountryRelations() {
    this.otherCountryRelations.push({ relationTypeE: true, countryCodeE: true });
    this.cd.markForCheck();
  }

  deleteOtherCountryRelations(index: number) {
    this.otherCountryRelations.splice(index, 1);
    this.cd.markForCheck();
  }

  updatingOtherCountryRelationsState(event, index: number) {
    this.otherCountryRelations[index].countryCode = event.target.value;
    this.otherCountryRelations[index].countryCodeE = false;
    this.cd.markForCheck();
  }

  updatingOtherCountryRelationsType(event, index: number) {
    this.otherCountryRelations[index].relationType = event.target.value;
    this.otherCountryRelations[index].relationTypeE = false;
    this.cd.markForCheck();
  }

  validateOtherCR(): boolean {
    let valueToReturn = false;
    this.otherCountryRelations.forEach((country) => {
      if (valueToReturn) {
        return;
      }
      else {
        valueToReturn = (country.relationTypeE || country.countryCodeE);
      }
    });
    this.cd.markForCheck();
    return valueToReturn;
  }

  setCheckboxes() {
    this.polliticallyExposedYes = document.getElementById('polliticallyExposed-yes') as HTMLInputElement;
    this.polliticallyExposedNo = document.getElementById('polliticallyExposed-no') as HTMLInputElement;
    this.osvcNo = document.getElementById('osvc-no') as HTMLInputElement;
    this.osvcYes = document.getElementById('osvc-yes') as HTMLInputElement;

    if (this.clientInformations.get('isOsvc') && this.clientInformations.get('isOsvc').value) {
      this.isOsvc = true;
      this.osvcYes.checked = true;
      this.osvcNo.checked = false;
      document.getElementById('isOsvc').classList.add('animatedScroll');
    }
    else {
      this.clientInformations.get('isOsvc').setValue(false);
      this.isOsvc = false;
      this.osvcYes.checked = false;
      this.osvcNo.checked = true;
      document.getElementById('isOsvc').classList.remove('animatedScroll');
    }

    //if (this.clientInformations.get('isPoliticallyExposed') && this.clientInformations.get('isPoliticallyExposed').value) {
    //  this.isPoliticallyExposed = true;
    //  this.polliticallyExposedYes.checked = true;
    //  this.polliticallyExposedNo.checked = false;
    //  document.getElementById('isPoliticallyExposed').classList.add('animatedScroll');
    //}
    //else {
    //  this.isPoliticallyExposed = false;
    //  this.polliticallyExposedYes.checked = false;
    //  this.polliticallyExposedNo.checked = true;
    //  this.clientInformations.get('isPoliticallyExposed').setValue(false);
    //  document.getElementById('isPoliticallyExposed').classList.remove('animatedScroll');
    //}
    this.isPoliticallyExposed = false;
    //this.polliticallyExposedYes.checked = false;
    //this.polliticallyExposedNo.checked = true;
    this.clientInformations.get('isPoliticallyExposed').setValue(false);
      //document.getElementById('isPoliticallyExposed').classList.remove('animatedScroll');
    this.cd.markForCheck();
  }

  public typeOfRelationList() {
    return Object.keys(CPCountryRelationType);
  }

  public countryCodeList() {
    return Object.keys(IsoCountry);
  }

  updatingCitizenshipCountryCode() {
    this.clientInformations.get('citizenshipCountryCode').setValue((document.getElementById('form-citizenshipCountryCode') as HTMLInputElement).value);
  }

  updatingIsOsvc($event) {
    if ($event.target.id === 'osvc-yes' && this.isOsvc) {
      this.osvcYes.checked = true;
      return;
    }
    else if ($event.target.id === 'osvc-no' && !this.isOsvc) {
      this.osvcNo.checked = true;
      return;
    }
    if (this.isOsvc) {
      this.isOsvc = false;
      this.osvcYes.checked = false;
      this.osvcNo.checked = true;
      this.clientInformations.get('isOsvc').setValue(false);
      document.getElementById('isOsvc').classList.remove('animatedScroll');
    }
    else {
      this.isOsvc = true;
      this.osvcYes.checked = true;
      this.osvcNo.checked = false;
      this.clientInformations.get('isOsvc').setValue(true);
      document.getElementById('isOsvc').classList.add('animatedScroll');
    }

    this.cd.markForCheck();
  }

  updatingIsPoliticallyExposed($event) {
    this.isPoliticallyExposedE = false;
    if ($event.target.id === 'polliticallyExposed-yes' && this.isPoliticallyExposed) {
      this.polliticallyExposedYes.checked = true;
      return;
    }
    else if ($event.target.id === 'polliticallyExposed-no' && !this.isPoliticallyExposed) {
      this.polliticallyExposedNo.checked = true;
      return;
    }
    if (this.isPoliticallyExposed) {
      this.isPoliticallyExposed = false;
      this.polliticallyExposedYes.checked = false;
      this.polliticallyExposedNo.checked = true;
      this.clientInformations.get('isPoliticallyExposed').setValue(false);
      document.getElementById('isPoliticallyExposed').classList.remove('animatedScroll');
    }
    else {
      this.isPoliticallyExposed = true;
      this.polliticallyExposedYes.checked = true;
      this.polliticallyExposedNo.checked = false;
      this.clientInformations.get('isPoliticallyExposed').setValue(true);
      document.getElementById('isPoliticallyExposed').classList.add('animatedScroll');
    }

    this.cd.markForCheck();
  }

  updatingOtherActivities($event, index) {
    if ($event.target.checked) {
      this.sensitiveActivityIds.push(this.activityForEnt[index].activityId);
    }
    else {
      this.sensitiveActivityIds.splice(this.sensitiveActivityIds.indexOf(this.activityForEnt[index].activityId), 1);
    }
  }

  validateFirstPage() {
    this.authService.refreshToken().subscribe((res) => { console.log(res); });
    this.alreadySubmitted = true;
    if (this.isPoliticallyExposedE) {
      return;
    }
    if (this.isPoliticallyExposed) {
      this.changeValidationPoliticallyExposed(false);
    }
    else {
      this.changeValidationPoliticallyExposed(true);
    }
    if (this.isOsvc) {
      this.changeValidationIsOsvc(false);
    }
    else {
      this.changeValidationIsOsvc(true);
    }
    let otherCountriesValidation = this.validateOtherCR();
    if (!this.clientInformations.valid || otherCountriesValidation) {
      scrollToValidationE();
      return;
    }
    if (this.sensitiveActivityIds.length > 0) {
      this.clientInformations.get('sensitiveActivityIds').setValue(this.sensitiveActivityIds);
    }
    if (this.otherCountryRelations.length > 0) {
      this.otherCountryRelations.forEach((item) => {
        this.otherCountryRelationsBE.push({ countryCode: item.countryCode, relationType: item.relationType });
      });
      this.clientInformations.get('otherCountryRelations').setValue(this.otherCountryRelationsBE);
      this.otherCountryRelationsBE = [];
    }
    amlCarouselN();
  }

  changeValidationPoliticallyExposed(del: boolean) {
    if (del) {
      this.clientInformations.get('pepRelation').setValidators(null);
      this.clientInformations.get('pepPosition').setValidators(null);
      this.clientInformations.get('pepRelation').setValue(null);
      this.clientInformations.get('pepPosition').setValue(null);
    }
    else {
      this.clientInformations.get('pepRelation').setValidators(notBlankValidator);
      this.clientInformations.get('pepPosition').setValidators(notBlankValidator);
    }
    this.clientInformations.get('pepRelation').updateValueAndValidity();
    this.clientInformations.get('pepPosition').updateValueAndValidity();
    this.cd.markForCheck();
  }

  changeValidationIsOsvc(del: boolean) {
    if (del) {
      this.clientInformations.get('subjectOfMainActivity').setValidators(null);
      this.clientInformations.get('osvcRegistrationNumber').setValidators(null);
      this.clientInformations.get('companyAddress').get('countryCode').setValidators(null);
      this.clientInformations.get('companyAddress').get('postalCode').setValidators(null);
      this.clientInformations.get('companyAddress').get('city').setValidators(null);
      this.clientInformations.get('companyAddress').get('streetNumber').setValidators(null);
      this.clientInformations.get('companyAddress').get('street').setValidators(null);
      this.clientInformations.get('companyName').setValidators(null);
      this.clientInformations.get('subjectOfMainActivity').setValue(null);
      this.clientInformations.get('osvcRegistrationNumber').setValue(null);
      this.clientInformations.get('companyAddress').get('countryCode').setValue(null);
      this.clientInformations.get('companyAddress').get('postalCode').setValue(null);
      this.clientInformations.get('companyAddress').get('city').setValue(null);
      this.clientInformations.get('companyAddress').get('streetNumber').setValue(null);
      this.clientInformations.get('companyAddress').get('street').setValue(null);
      this.clientInformations.get('companyName').setValue(null);
    }
    else {
      this.clientInformations.get('subjectOfMainActivity').setValidators(notBlankValidator);
      this.clientInformations.get('osvcRegistrationNumber').setValidators(notBlankValidator);
      this.clientInformations.get('companyAddress').get('countryCode').setValidators(notBlankValidator);
      this.clientInformations.get('companyAddress').get('postalCode').setValidators(notBlankValidator);
      this.clientInformations.get('companyAddress').get('city').setValidators(notBlankValidator);
      this.clientInformations.get('companyAddress').get('streetNumber').setValidators(notBlankValidator);
      this.clientInformations.get('companyAddress').get('street').setValidators(notBlankValidator);
      this.clientInformations.get('companyName').setValidators(notBlankValidator);
    }
    this.clientInformations.get('subjectOfMainActivity').updateValueAndValidity();
    this.clientInformations.get('osvcRegistrationNumber').updateValueAndValidity();
    this.clientInformations.get('companyAddress').get('countryCode').updateValueAndValidity();
    this.clientInformations.get('companyAddress').get('postalCode').updateValueAndValidity();
    this.clientInformations.get('companyAddress').get('city').updateValueAndValidity();
    this.clientInformations.get('companyAddress').get('streetNumber').updateValueAndValidity();
    this.clientInformations.get('companyAddress').get('street').updateValueAndValidity();
    this.clientInformations.get('companyName').updateValueAndValidity();
    this.cd.markForCheck();
  }

  dateChanged($event) {
    setTimeout(() => {
      this.clientInformations.get($event.target.name).setValue($event.target.value);
    }, 400);
  }

}


