import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { notBlankValidator } from '../validation/validation.validators';
import { AmlValues, AmlRequest } from './amlValues.model';
import { AmlService } from './aml.service';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { SharedService } from '../shared/shared.service';
import { LoaderIndicatorService } from '../shared/LoaderIndicator.service';

@Component({
  selector: 'app-aml',
  templateUrl: './aml.component.html',
  styleUrls: ['./aml.component.css'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AmlComponent implements OnInit, AfterViewInit {

  i18NPrefix = 'aml.';
  i18NErrorPrefix = 'global.errors.';

  renewalDate: string;
  dateAcquired: string;
  amlForm: FormGroup;
  getAMLDataResult: AmlRequest = {
  };

  constructor(private fb: FormBuilder,
    private amlService: AmlService,
    private cd: ChangeDetectorRef,
    private datePipe: DatePipe,
    private router: Router,
    private sharedService: SharedService,
    private loaderService: LoaderIndicatorService
  ) { }

  ngOnInit() {
    this.loaderService.showLoader();
    this.amlService.getAMLData().subscribe((res) => {
      console.log(res);
      Object.assign(this.getAMLDataResult, res.getAMLDataResult);
      if (!this.getAMLDataResult.companyAddress) {
        this.getAMLDataResult.companyAddress = { streetNumber: null, street: null, postalCode: null, houseNumber: null, countryCode: null, city:null }
      }
      if (!this.getAMLDataResult.permanentAddress) {
        this.getAMLDataResult.permanentAddress = { streetNumber: null, street: null, postalCode: null, houseNumber: null, countryCode: null, city: null }
      }
      this.buildForm();
      this.renewalDate = res.getAMLDataResult.renewalDate;
      this.dateAcquired = res.getAMLDataResult.dateAcquired;
      //this.amlForm.get('clientInformations').get('birthDate').setValue("22.01.2010");
      //this.amlForm.get('identityDocument').get('identDocumentNumber').setValue('125478547');
      //this.amlForm.get('identityDocument').get('identDocumentType').setValue("PASSPORT");
      //this.amlForm.get('clientInformations').get('pepPosition').setValue("nejaka funkce test");
      //this.amlForm.get('clientInformations').get('pepRelation').setValue("test");
      this.cd.markForCheck();
    },
      (e) => {
        console.log(e);
        this.loaderService.disableLoader();
      });
    
  }

  private buildForm() {
    this.amlForm = this.fb.group({
      clientInformations: this.fb.group({
        registrationNumber: [this.getAMLDataResult.registrationNumber],
        firstName: [this.getAMLDataResult.firstName],
        surname: [this.getAMLDataResult.surname],
        birthPlace: [this.getAMLDataResult.birthPlace, notBlankValidator],
        gender: [this.getAMLDataResult.gender],
        birthDate: [this.getAMLDataResult.birthDate],
        citizenshipCountryCode: [this.getAMLDataResult.citizenshipCountryCode, notBlankValidator],
        otherCountryRelations: [this.getAMLDataResult.otherCountryRelations],
        isPoliticallyExposed: [this.getAMLDataResult.isPoliticallyExposed],
        pepPosition: [this.getAMLDataResult.pepPosition, notBlankValidator],
        pepRelation: [this.getAMLDataResult.pepRelation, notBlankValidator],
        permanentAddress: this.fb.group({
          street: [this.getAMLDataResult.permanentAddress.street, notBlankValidator],
          streetNumber: [this.getAMLDataResult.permanentAddress.streetNumber, notBlankValidator],
          city: [this.getAMLDataResult.permanentAddress.city, notBlankValidator],
          postalCode: [this.getAMLDataResult.permanentAddress.postalCode, notBlankValidator],
          countryCode: [this.getAMLDataResult.permanentAddress.countryCode, notBlankValidator]
        }),
        osvcRegistrationNumber: [this.getAMLDataResult.osvcRegistrationNumber, notBlankValidator],
        isOsvc: [this.getAMLDataResult.isOsvc],
        companyName: [this.getAMLDataResult.companyName, notBlankValidator],
        companyAddress: this.fb.group({
          street: [this.getAMLDataResult.companyAddress.street, notBlankValidator],
          streetNumber: [this.getAMLDataResult.companyAddress.streetNumber, notBlankValidator],
          city: [this.getAMLDataResult.companyAddress.city, notBlankValidator],
          postalCode: [this.getAMLDataResult.companyAddress.postalCode, notBlankValidator],
          countryCode: [this.getAMLDataResult.companyAddress.countryCode, notBlankValidator]
        }),
        subjectOfMainActivity: [this.getAMLDataResult.subjectOfMainActivity, notBlankValidator],
        sensitiveActivityIds: [this.getAMLDataResult.sensitiveActivityIds],
      }),
      sourceOfFinances: this.fb.group({
        employeeJob: [this.getAMLDataResult.employeeJob],
        expectedIncomeEmployee: [this.getAMLDataResult.expectedIncomeEmployee, notBlankValidator],
        osvcJob: [this.getAMLDataResult.osvcJob],
        expectedIncomeOsvc: [this.getAMLDataResult.expectedIncomeOsvc, notBlankValidator],
        otherJob: [this.getAMLDataResult.otherJob],
        expectedIncomeOther: [this.getAMLDataResult.expectedIncomeOther, notBlankValidator],
        sourceRegularBusiness: [this.getAMLDataResult.sourceRegularBusiness],
        sourceEmployeeSalary: [this.getAMLDataResult.sourceEmployeeSalary],
        sourceAccumulatedProperty: [this.getAMLDataResult.sourceAccumulatedProperty],
        sourceHeritage: [this.getAMLDataResult.sourceHeritage],
        sourceGift: [this.getAMLDataResult.sourceGift],
        sourceOther: [this.getAMLDataResult.sourceOther],
        sourceOtherDetail: [this.getAMLDataResult.sourceOtherDetail]
      }),
      identityDocument: this.fb.group({
        identDocumentType: [this.getAMLDataResult.identDocumentType, notBlankValidator],
        identDocumentNumber: [this.getAMLDataResult.identDocumentNumber, notBlankValidator],
        identIssuedBy: [this.getAMLDataResult.identIssuedBy, notBlankValidator],
        identValidTill: [this.getAMLDataResult.identValidTill, notBlankValidator]
      })
    })
  }

  ngAfterViewInit() {
  }

  updateAml() {
    document.getElementById('amlSubmitButton').click();
  }

  submit() {
    this.setAmlRequest();
    console.log(this.amlForm);
    console.log(this.getAMLDataResult);
    console.log('SUBMIT');
    this.amlService.setAMLData(this.getAMLDataResult).subscribe((res) => {
      console.log(res);
      if (res.setAMLDataResult) {
        window.history.back();
        this.sharedService.toggleInfoAlert("aml.save.success", null);
      }
      else {
        this.sharedService.toggleDangerAlert("aml.save.error", null);
      }
    },
      (e) => {
        this.sharedService.toggleDangerAlert("aml.save.error", null);
        console.log(e);
      });
  }

  getTodayDate(): string {
    return this.datePipe.transform(new Date(), 'dd.MM.yyyy');
  }

  setAmlRequest() {
    this.getAMLDataResult = {
      dateAcquired: this.getTodayDate(),
      sourceAMLDataId: this.getAMLDataResult.sourceAMLDataId,
      birthDate: this.amlForm.controls.clientInformations.get('birthDate').value,
      birthPlace: this.amlForm.controls.clientInformations.get('birthPlace').value,
      citizenshipCountryCode: this.amlForm.controls.clientInformations.get('citizenshipCountryCode').value,
      companyAddress: (this.amlForm.controls.clientInformations.get('isOsvc').value ? this.amlForm.controls.clientInformations.get('companyAddress').value : null),
      companyName: this.amlForm.controls.clientInformations.get('companyName').value,
      employeeJob: this.amlForm.controls.sourceOfFinances.get('employeeJob').value,
      expectedIncomeEmployee: this.amlForm.controls.sourceOfFinances.get('expectedIncomeEmployee').value,
      expectedIncomeOsvc: this.amlForm.controls.sourceOfFinances.get('expectedIncomeOsvc').value,
      expectedIncomeOther: this.amlForm.controls.sourceOfFinances.get('expectedIncomeOther').value,
      firstName: this.amlForm.controls.clientInformations.get('firstName').value,
      gender: this.amlForm.controls.clientInformations.get('gender').value,
      identDocumentNumber: this.amlForm.controls.identityDocument.get('identDocumentNumber').value,
      identDocumentType: this.amlForm.controls.identityDocument.get('identDocumentType').value,
      identIssuedBy: this.amlForm.controls.identityDocument.get('identIssuedBy').value,
      identValidTill: this.amlForm.controls.identityDocument.get('identValidTill').value,
      isOsvc: this.amlForm.controls.clientInformations.get('isOsvc').value,
      isPoliticallyExposed: this.amlForm.controls.clientInformations.get('isPoliticallyExposed').value,
      osvcJob: this.amlForm.controls.sourceOfFinances.get('osvcJob').value,
      osvcRegistrationNumber: this.amlForm.controls.clientInformations.get('osvcRegistrationNumber').value,
      otherCountryRelations: this.amlForm.controls.clientInformations.get('otherCountryRelations').value,
      otherJob: this.amlForm.controls.sourceOfFinances.get('otherJob').value,
      pepPosition: this.amlForm.controls.clientInformations.get('pepPosition').value,
      pepRelation: this.amlForm.controls.clientInformations.get('pepRelation').value,
      permanentAddress: this.amlForm.controls.clientInformations.get('permanentAddress').value,
      registrationNumber: this.amlForm.controls.clientInformations.get('registrationNumber').value,
      sensitiveActivityIds: this.amlForm.controls.clientInformations.get('sensitiveActivityIds').value,
      sourceAccumulatedProperty: this.amlForm.controls.sourceOfFinances.get('sourceAccumulatedProperty').value,
      sourceEmployeeSalary: this.amlForm.controls.sourceOfFinances.get('sourceEmployeeSalary').value,
      sourceGift: this.amlForm.controls.sourceOfFinances.get('sourceGift').value,
      sourceHeritage: this.amlForm.controls.sourceOfFinances.get('sourceHeritage').value,
      sourceOther: this.amlForm.controls.sourceOfFinances.get('sourceOther').value,
      sourceOtherDetail: this.amlForm.controls.sourceOfFinances.get('sourceOtherDetail').value,
      sourceRegularBusiness: this.amlForm.controls.sourceOfFinances.get('sourceRegularBusiness').value,
      subjectOfMainActivity: this.amlForm.controls.clientInformations.get('subjectOfMainActivity').value,
      surname: this.amlForm.controls.clientInformations.get('surname').value
    }
  }
}
