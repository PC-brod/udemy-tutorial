import { Injectable, Inject } from '@angular/core';
import { AuthService } from '../auth/shared/auth.service';
import { Observable } from 'rxjs';
import { EnvironmentConfig } from '../environment.config';
import { assign, interpolate } from '../../app/utils/common.utils';
import { ApiConfig } from '../api.model';
import { apiConfigToken } from '../api.di';
import { AmlRequest } from './amlValues.model';

@Injectable()
export class AmlService {

  constructor(
    @Inject(apiConfigToken) private config: ApiConfig,
    private authService: AuthService
  ) { }


  getAMLData(): Observable<any> {
    return this.authService.get$(interpolate(EnvironmentConfig.settings.env.amlUrl + this.config.getAMLDataUrl,
      { clientId: this.authService.clientId }))
      .map((res) => {
        if (!res) {
          throw new Error();
        }
        return res;
      });
  }

  getAMLSensitiveActivities(): Observable<any> {
    return this.authService.get$(interpolate(EnvironmentConfig.settings.env.amlUrl + this.config.getAMLSensitiveActivitiesUrl,
      null))
      .map((res) => {
        if (!res) {
          throw new Error();
        }
        return res;
      });
  }

  setAMLData(amlData): Observable<any> {
    return this.authService.post$(interpolate(EnvironmentConfig.settings.env.amlUrl + this.config.setAMLDataUrl,
      { clientId: this.authService.clientId }), amlData)
      .map((res) => {
        if (!res) {
          throw new Error();
        }
        return res;
      });
  }

}
