import { CPAddress } from "../profile/shared/models/cp-address.model";
import { CPGender } from "../shared/models/cp-gender.model";

export interface AmlValues {
  firstPage: AmlFirstPageValues,
  secondPage: AmlSecondPageValues,
  thirdPage: AmlThirdPageValues
}

export interface AmlFirstPageValues {
  registrationNumber?:string,
  firstName?:string,
  surname?:string,
  birthPlace?:string,
  gender?: CPGenderAml,
  birthDate?:string,
  citizenshipCountryCode?:string,
  otherCountryRelations?: Array<CPCountryRelation>,
  isPoliticallyExposed?: boolean,
  pepPosition?:string,
  pepRelation?:string,
  permanentAddress?: CPAddress,
  osvcRegistrationNumber?: string,
  isOsvc?:boolean,
  companyName?:string,
  companyAddress?: CPAddress,
  subjectOfMainActivity?:string,
  sensitiveActivityIds?: Array<string>
}

export interface SensitiveActivity {
  activityId?:string,
  name?:string,
  seq?:number
}

export interface AmlSecondPageValues {

  employeeJob?: string,
  expectedIncomeEmployee?: number,
  osvcJob?: string,
  expectedIncomeOsvc?: number,
  otherJob?: string,
  expectedIncomeOther?: number,
  sourceRegularBusiness?: boolean,
  sourceEmployeeSalary?: boolean,
  sourceAccumulatedProperty?: boolean,
  sourceHeritage?: boolean,
  sourceGift?: boolean,
  sourceOther?: boolean,
  sourceOtherDetail?: string //Poznámka pro jiný zdroj
}

export interface AmlThirdPageValues {
  identDocumentType?: CPIdentDocumentType,
  identDocumentNumber?: string,
  identIssuedBy?: string,
  identValidTill?: string
  //datumy posledni aktualizace, platnosti do a prohlášení klienta
}

export enum CPIdentDocumentType {
  IDENTIFICATION_CARD = "IDENTIFICATION_CARD",
  IDENTIFICATION_CARD_EU = "IDENTIFICATION_CARD_EU",
  PASSPORT = "PASSPORT",
  PERMIT = "PERMIT",
  PERMIT_TEMP = "PERMIT_TEMP",
  OTHER = "OTHER"
}

export enum CPCountryRelationType {
  CITIZENSHIP = "CITIZENSHIP",
  PERMANENT_RESIDENCE = "PERMANENT_RESIDENCE"
}

export interface CPCountryRelation {
  countryCode?: string,
  relationType?: CPCountryRelationType
}

export interface CPCountryRelationFE {
  countryCode?: string,
  relationType?: CPCountryRelationType,
  countryCodeE?: boolean,
  relationTypeE?: boolean
}

export enum CPGenderAml {
  MALE = "MALE",
  FEMALE = "FEMALE"
}

export interface AmlRequest {
  dateAcquired?:string,
  sourceAMLDataId?: string,
  registrationNumber?: string,
  firstName?: string,
  surname?: string,
  birthPlace?: string,
  gender?: CPGenderAml,
  birthDate?: string,
  citizenshipCountryCode?: string,
  otherCountryRelations?: Array<CPCountryRelation>,
  isPoliticallyExposed?: boolean,
  pepPosition?: string,
  pepRelation?: string,
  permanentAddress?: CPAddress,
  osvcRegistrationNumber?: string,
  isOsvc?: boolean,
  companyName?: string,
  companyAddress?: CPAddress,
  subjectOfMainActivity?: string,
  sensitiveActivityIds?: Array<string>,
  employeeJob?: string,
  expectedIncomeEmployee?: number,
  osvcJob?: string,
  expectedIncomeOsvc?: number,
  otherJob?: string,
  expectedIncomeOther?: number,
  sourceRegularBusiness?: boolean,
  sourceEmployeeSalary?: boolean,
  sourceAccumulatedProperty?: boolean,
  sourceHeritage?: boolean,
  sourceGift?: boolean,
  sourceOther?: boolean,
  sourceOtherDetail?: string,
  identDocumentType?: CPIdentDocumentType,
  identDocumentNumber?: string,
  identIssuedBy?: string,
  identValidTill?: string
}
