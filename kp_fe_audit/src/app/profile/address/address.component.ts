import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation, AfterContentInit } from '@angular/core';
import { ChangeDetectorRef, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { trimToNull } from '../../validation/validation.utils';
import { notBlankValidator, emailValidator, phoneValidator } from '../../validation/validation.validators';
import { Client } from '../shared/models/res-client.model';
import { CPPurpose } from '../shared/models/cp-purpose.model';
import { ProfileService } from '../shared/profile.service';
import { CPAddress } from '../shared/models/cp-address.model';
import { LoggerService } from '../../shared/logger.service';
import { IsoCountry } from '../../shared/models/enums';

@Component({
  selector: 'app-profile-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.css'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddressComponent implements OnInit, AfterContentInit {

  i18NPrefix = 'profile.';
  i18NErrorPrefix = 'global.errors.';

  constructor(
    private profileService: ProfileService,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private log: LoggerService
  ) { }

  addressEdited = true;
  @Input() clientAddress: CPAddress;
  @Input() isCorresponding: boolean;
  @Input() alreadySubmitted: boolean;
  @Input() submitting: boolean;
  @Input() correspondingDifferent: boolean;
  @Output() correspondingIsDifferent = new EventEmitter<boolean>();
  @Output() addressSave = new EventEmitter<FormGroup>();
  form: FormGroup;
  public isoCountryList() { return Object.keys(IsoCountry); }

  ngOnInit() {
    // this.buildForms();
    this.log.debug('address ngoninit');
    this.cd.markForCheck();
  }

  ngAfterContentInit() {
    this.log.debug('address ngAfterContentInit');
    this.log.debug('address buildAddressForm');
    this.log.debug(this.clientAddress);
    this.form = this.buildAddressForm(this.clientAddress);
    this.log.debug('address buildAddressForm after');
    this.cd.markForCheck();
  }

  correspondingIsDifferentCheck(): void {
    this.correspondingIsDifferent.emit(!this.correspondingDifferent);
    this.cd.markForCheck();
  }

  addressEdit() {
    this.addressEdited = false;
    this.cd.markForCheck();
  }

  addressEditSave() {
    this.addressSave.emit(this.form);
    this.cd.markForCheck();
  }

  addressEditCancel() {
    this.form.patchValue({
      street: this.clientAddress.street,
      streetNumber: this.clientAddress.streetNumber,
      city: this.clientAddress.city,
      postalCode: this.clientAddress.postalCode,
      // addressdescription: this.clientEdit.permanentAddress.addressdescription,
      countryCode: this.clientAddress.countryCode
    });
    this.addressEdited = true;
    this.cd.markForCheck();
  }

  private buildAddressForm(address: CPAddress) {
    return this.fb.group({
      street: [address.street, notBlankValidator],
      streetNumber: [address.streetNumber, notBlankValidator],
      city: [address.city, notBlankValidator],
      postalCode: [address.postalCode, notBlankValidator],
      // addressdescription: [addressdescription],
      countryCode: [address.countryCode, notBlankValidator]
    });
  }

}
