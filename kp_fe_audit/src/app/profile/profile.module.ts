import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { CookieXSRFStrategy, XSRFStrategy, HttpModule } from '@angular/http';

import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileComponent } from './profile.component';
import { AuthGuard } from '../auth/shared/auth-guard.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ValidationModule } from '../validation/validation.module';
import { AddressComponent } from './address/address.component';
import { ProfileService } from './shared/profile.service';
import { ProfileIntegratedService } from './shared/profile-integrated.service';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    HttpClientModule,
    ProfileRoutingModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    ValidationModule
  ],
  declarations: [ProfileComponent, AddressComponent],
  providers: [
      AuthGuard,
      {
        provide: ProfileService,
          // useClass: ProfileMockedService
        useClass: ProfileIntegratedService
      },
  ]
})
export class ProfileModule { }
