import { Injectable, EventEmitter, isDevMode, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { HttpClient } from '@angular/common/http';

import { assign, interpolate } from '../../../app/utils/common.utils';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ChangeDetectorRef, Input, Output } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { GDPRConsents, codelistpartners, codelistproductgroups,
  attachments, contractsDetails, contracts, notificationPrescripts, clients } from '../../../mocks/mock';
import { ProfileService } from './profile.service';
import { ResClient } from './models/res-client.model';
import { AuthService } from '../../auth/shared/auth.service';
import { ApiConfig } from '../../api.model';
import { apiConfigToken } from '../../api.di';

const delay = 500;

@Injectable()
export class ProfileMockedService extends ProfileService {

  constructor(
    @Inject(apiConfigToken) private config: ApiConfig,
    private http: HttpClient,
    private activatedRoute: ActivatedRoute,
    private authService: AuthService,
    // private cd: ChangeDetectorRef,
    private router: Router
  ) {
    super();
  }

  getClientById$(): Observable<ResClient> {
    return Observable.timer(delay).map(() => {
      return clients[0];
    });
  }

  updateClient$(body: ResClient): Observable<ResClient> {
    return Observable.timer(delay).map(() => {
      const idx = clients.findIndex((c) => c.clientId === "100000000000001");
      if (idx === -1) {
        throw new Error(`Item with id ${this.authService.clientId} not exists.`);
      }
      const client: ResClient = null;
      assign(clients[idx], body);
      return client;
    });
  }
}
