export interface EmailAddress {
  emailAddress?: string;
  emailAwaitingApproval?: boolean;
}
