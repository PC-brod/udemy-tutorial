import { CPGender } from '../../../shared/models/cp-gender.model';
import { CPAddress } from './cp-address.model';
import { CPPurpose } from './cp-purpose.model';
import { NotLoged } from '../../../shared/models/not-loged.model';


export interface Client extends CPGender {
      permanentAddress?: CPAddress;
      correspondingAddress?: CPAddress;
      mobilePhone?: string;
    purpose?: CPPurpose;
      clientId?: string;
      dateOfBirth?: string;
      emailAwaitingApproval?: boolean;
      mobilePhoneAwaitingApproval?: boolean;
      username?: string;
      firstName?: string;
      lastName?: string;
      titlesInFront?: string;
      titlesBehind?: string;
      hasPrimaryConsultant?: boolean;
      email?: string;
}
export interface ResClient extends Client, NotLoged {
      permanentAddress?: CPAddress;
      correspondingAddress?: CPAddress;
      mobilePhone?: string;
    purpose?: CPPurpose;
      clientId?: string;
      dateOfBirth?: string;
      emailAwaitingApproval?: boolean;
      mobilePhoneAwaitingApproval?: boolean;
      username?: string;
      firstName?: string;
      lastName?: string;
      titlesInFront?: string;
      titlesBehind?: string;
      hasPrimaryConsultant?: boolean;
  email?: string;
  hasAML?: boolean;
  amlRenewalDate?: string;
}
export namespace Client {
}

export interface ClientUpdates {
  permanentAddress?: CPAddress;
  correspondingAddress?: CPAddress;
  mobilePhone?: string;
  email?: string;
}

export interface UpdateClient {
  purpose?: CPPurpose;
  updates?: ClientUpdates;
}
