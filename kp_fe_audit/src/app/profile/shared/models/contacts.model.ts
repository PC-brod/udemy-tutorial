import { CPAddress } from './cp-address.model';
import { CPPurpose } from './cp-purpose.model';
import { EmailAddress } from './email-address.model';
import { MobilePhone } from './mobile-phone.model';

export interface Contacts {
  permanentAddress?: CPAddress;
  correspondingAddress?: CPAddress;
  mobilePhone?: MobilePhone;
  emailAddress?: EmailAddress;
  purpose?: CPPurpose;
}
