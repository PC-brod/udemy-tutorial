export interface MobilePhone {
  mobilePhone?: string;
  mobilePhoneAwaitingApproval?: boolean;
}
