export interface CPAddress {
  street?: string;
  city?: string;
  streetNumber?: string;
  postalCode?: string;
  // addressId?: string;
  // addressdescription?: string;
  awaitingApproval?: boolean;
  countryCode?: string;
  houseNumber?: string;
}
