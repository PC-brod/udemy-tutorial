import { Injectable, EventEmitter, isDevMode, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { HttpClient } from '@angular/common/http';

import { assign, interpolate } from '../../../app/utils/common.utils';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ChangeDetectorRef, Input, Output } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ProfileService } from './profile.service';
import { AuthService } from '../../auth/shared/auth.service';
import { ApiConfig } from '../../api.model';
import { apiConfigToken } from '../../api.di';

import { getUpdateFromDetail, getCreateFromDetail, getUpdateFromClient } from '../../utils/transform.utils';
import { ResClient } from './models/res-client.model';
import { EnvironmentConfig } from '../../environment.config';
import { LoggerService } from '../../shared/logger.service';

@Injectable()
export class ProfileIntegratedService extends ProfileService {

  constructor(
    @Inject(apiConfigToken) private config: ApiConfig,
    private http: HttpClient,
    private authService: AuthService,
    private activatedRoute: ActivatedRoute,
    // private cd: ChangeDetectorRef,
    private router: Router,
    private log: LoggerService
  ) {
    super();
  }

  getClientById$(): Observable<ResClient> {

    return this.authService.get$(interpolate(EnvironmentConfig.settings.env.uriPrefix + this.config.getClientByIdUrl,
      { clientId: this.authService.clientId }))
      .map((res: ResClient) => {
        console.log(res);
      if (!res) {
        throw new Error(`Client with id ${this.authService.clientId} do not exists.`);
      }
      return res;
    });
  }

  updateClient$(body: ResClient): Observable<ResClient> {
    body = assign(body);
    const updateBody = getUpdateFromClient(body);
    this.log.debug('updateClient');
    this.log.debug(updateBody);
    this.log.debug(interpolate(EnvironmentConfig.settings.env.uriPrefix + this.config.updateClientUrl,
      { clientId: this.authService.clientId }));
    return this.authService.put$(interpolate(EnvironmentConfig.settings.env.uriPrefix + this.config.updateClientUrl,
      { clientId: this.authService.clientId }), updateBody)
    .mergeMap(() => {
      return this.getClientById$();
    });
  }
}
