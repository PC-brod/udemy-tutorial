import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ChangeDetectorRef, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { trimToNull } from '../validation/validation.utils';
import { notBlankValidator, emailValidator, phoneValidator } from '../validation/validation.validators';
import { Client } from './shared/models/res-client.model';
import { CPPurpose } from './shared/models/cp-purpose.model';
import { ProfileService } from './shared/profile.service';
import { CPAddress } from './shared/models/cp-address.model';
import { AddressComponent } from './address/address.component';
import { ViewChildren, QueryList } from '@angular/core';
import { AuthService } from '../auth/shared/auth.service';
import { LoggerService } from '../shared/logger.service';
import { LoaderIndicatorService } from '../shared/LoaderIndicator.service';
declare function setActiveNav(id: string): Function;

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileComponent implements OnInit {

  i18NPrefix = 'profile.';
  i18NErrorPrefix = 'global.errors.';

  constructor(
    private profileService: ProfileService,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
	private log: LoggerService,
	private loaderService: LoaderIndicatorService
  ) { }


  @ViewChildren('perm') permanentAddressComponent: AddressComponent;

  @ViewChildren('corr') correspondingAddressComponent: AddressComponent;

  client: Client = {};
  clientEdit: Client = {};
  loadError = false;

  alreadySubmitted = false;
  submitting = false;
  submitError = false;
  submitErrorString = 'submitError';
  formAddressSubmitError = false;
  formCorrespondingAddressSubmitError = false;
  formAddressSubmitting = false;
  formCorrespondingAddressSubmitting = false;

  contactEdited = true;
  correspondingIsDifferent = false;
  formContacts: FormGroup;
  formPermanentAddress: FormGroup;
  formCorrespondingAddress: FormGroup;
  formCorrespondingAddressInitialized = false;
  formPermanentAddressInitialized = false;

  setCorrespondingIsDifferent(isDifferent: boolean) {
    this.log.debug('setCorrespondingIsDifferent');
    this.log.debug(isDifferent);
    this.correspondingIsDifferent = isDifferent;
    if (isDifferent && !this.clientEdit.correspondingAddress) {
      this.clientEdit.correspondingAddress = {
        street: '',
        city: '',
        streetNumber: '',
        postalCode: '',
        countryCode: '',
        houseNumber: ''};
    }
    this.cd.markForCheck();
  }

  ngOnInit() {
    setActiveNav('delete');
		this.profileService.getClientById$().subscribe((client) => {
			this.client = client;
			Object.assign(this.clientEdit, this.client);
			this.log.debug(client);
			this.log.debug(this.client);
			this.log.debug(this.clientEdit);
			this.loadError = false;
			this.buildForms();
			this.correspondingIsDifferent = this.clientEdit.correspondingAddress ? true : false;
			this.formCorrespondingAddressInitialized = true;
			this.formPermanentAddressInitialized = true;
			this.cd.markForCheck();
			this.loaderService.disableLoader();
		}, (e) => {
			this.loaderService.disableLoader();
			this.log.error(e);
			this.loadError = true;
			this.cd.markForCheck();
		});
  	}

  private buildAddressForm(address: CPAddress) {
    return this.fb.group({
      street: [address.street, notBlankValidator],
      streetNumber: [address.streetNumber, notBlankValidator],
      city: [address.city, notBlankValidator],
      postalCode: [address.postalCode, notBlankValidator],
      // addressdescription: [addressdescription],
      countryCode: [address.countryCode, notBlankValidator]
    });
  }

  contactEdit() {
    this.authService.sessionTimerStart();
    this.contactEdited = false;
  }

  contactEditSave() {
    this.onSubmitContacts();
    if (!this.alreadySubmitted) {
      this.contactEdited = true;
    }
  }

  contactEditCancel() {
    this.authService.sessionTimerStart();
    Object.assign(this.clientEdit, this.client);
    this.contactEdited = true;
    this.formContacts.get('mobilePhone').setValue(this.clientEdit.mobilePhone);
    this.formContacts.get('email').setValue(this.clientEdit.email);
    this.cd.markForCheck();
  }

  getFullName() {
    let fullName = this.clientEdit.firstName + ' ' + this.clientEdit.lastName;
    if (this.clientEdit.titlesInFront !== undefined) {
      fullName = this.clientEdit.titlesInFront + ' ' + fullName;
    }
    if (this.clientEdit.titlesBehind !== undefined) {
      fullName = fullName + ' ' + this.clientEdit.titlesBehind;
    }
    return fullName;
  }

  private buildForms(): void {
    this.formContacts = this.fb.group({
      mobilePhone: [this.clientEdit.mobilePhone, Validators.compose([notBlankValidator, phoneValidator])],
      email: [this.clientEdit.email, Validators.compose([notBlankValidator, emailValidator])]
    });
  }

  onSubmitCorrespondingAddress(address: FormGroup): void {
    this.alreadySubmitted = true;
    if (this.isAnyFormSubmitting()) {
      return;
    }

    const value: Client = this.clientEdit;
    if (!this.correspondingIsDifferent) {
      // clear corresponding address
      this.submitting = true;
      address.value.submitError = false;

      value.correspondingAddress = null;
    } else {

      if (!this.canSubmitForm(address)) {
        return;
      }
      this.submitting = true;
      address.value.submitError = false;

      value.correspondingAddress.street = address.value.street;
      value.correspondingAddress.city = address.value.city; // trimToNull(address.value);
      value.correspondingAddress.streetNumber = address.value.streetNumber;
      // trimToNull(address.value);
      value.correspondingAddress.postalCode = address.value.postalCode;
      // parseInt(trimToNull(value.power as any), 10);
      // value.correspondingAddress.addressdescription = address.value.addressdescription;
      value.correspondingAddress.countryCode = address.value.countryCode;
      // trimToNull(value.description);
      value.purpose = CPPurpose.Update;
    }

    this.log.debug(value);
    // this.corrAddressEdited = true;
    this.profileService.updateClient$(value).subscribe((client) => {
      this.submitting = false;
      this.client = client;
      Object.assign(this.clientEdit, this.client);
      this.alreadySubmitted = false;
      this.cd.markForCheck();
      this.router.navigate(['/verifyUpdate']);
    }, (e) => {
      this.log.error(e);
      address.value.submitError = true;
      this.submitting = false;
      Object.assign(this.clientEdit, this.client);
      this.alreadySubmitted = false;
      this.cd.markForCheck();
    });
  }

  onSubmitAddress(address: FormGroup): void {
    this.alreadySubmitted = true;
    if (!this.canSubmitForm(address)) {
      return;
    }
    this.submitting = true;
    address.value.submitError = false;

    const value: Client = this.clientEdit;
    value.permanentAddress.street = address.value.street;
    value.permanentAddress.city = address.value.city;
    value.permanentAddress.streetNumber = address.value.streetNumber;
    value.permanentAddress.postalCode = address.value.postalCode;
    value.permanentAddress.countryCode = address.value.countryCode;
    value.purpose = CPPurpose.Update;

    if (!this.correspondingIsDifferent) {
      value.correspondingAddress = undefined;
    }

    // this.addressEdited = true;
    this.log.debug(value);
    this.profileService.updateClient$(value).subscribe((client) => {
      this.submitting = false;
      this.client = client;
      Object.assign(this.clientEdit, this.client);
      this.alreadySubmitted = false;
      this.cd.markForCheck();
      this.router.navigate(['/verifyUpdate']);
    }, (e) => {
      this.log.error(e);
      this.formAddressSubmitError = true;
      this.submitting = false;
      Object.assign(this.clientEdit, this.client);
      this.alreadySubmitted = false;
      this.cd.markForCheck();
    });
  }

  onSubmitContacts(): void {
    this.alreadySubmitted = true;
    if (!this.canSubmitForm(this.formContacts)) {
      return;
    }
    this.submitting = true;
    this.formContacts.value.submitError = false;

    const value: Client = this.clientEdit;
	value.mobilePhone = this.formContacts.value.mobilePhone.replace(/\s/g,'');
    value.email = this.formContacts.value.email;
    value.purpose = CPPurpose.Update;

    this.log.debug(value);
    this.profileService.updateClient$(value).subscribe((client) => {
      this.submitting = false;
      this.client = client;
      Object.assign(this.clientEdit, this.client);
      this.alreadySubmitted = false;
      this.cd.markForCheck();
      this.router.navigate(['/verifyUpdate']);
    }, (e) => {
      this.log.error(e);
      this.formContacts.value.submitError = true;
      this.submitting = false;
      Object.assign(this.clientEdit, this.client);
      this.alreadySubmitted = false;
      this.cd.markForCheck();
    });
  }

  correspondingIsDifferentCheck(isDifferent: boolean): void {
    this.authService.sessionTimerStart();
    this.correspondingIsDifferent = !this.correspondingIsDifferent;
    // this.corrAddressEditCancel();
  }

  private canSubmitForm(form: FormGroup): boolean {
    return form.valid && !this.isAnyFormSubmitting();
  }

  private isAnyFormSubmitting(): boolean {
    return this.submitting;
  }

  public changePassword() {
    this.authService.sessionTimerStart();
    this.router.navigate(['/changePassword']);
  }

}
