import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProfileComponent } from './profile.component';
import { AuthGuard } from '../auth/shared/auth-guard.service';

@NgModule({
  imports: [
    RouterModule.forChild(
      [
        // {
        //     path: '',
        //     redirectTo: 'profile',
        //     pathMatch: 'full'
        // },
        {
          path: 'profile',
          component: ProfileComponent,
          canActivate: [AuthGuard]
        },
      ]// ,
      // {
      //   enableTracing: false
      // }
    )
  ],
  exports: [
    RouterModule
  ]
})
export class ProfileRoutingModule { }
