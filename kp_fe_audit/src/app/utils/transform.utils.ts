import { NotificationPrescriptCreate } from '../binder/notification/models/notification-prescript-create.model';
import { ResNotificationPrescript } from '../binder/notification/models/res-notification-prescript.model';
import { ResContractDetail } from '../binder/contract/models/res-contract-detail.model';
import { ContractCreate } from '../binder/contract/models/contract-create.model';
import { ContractUpdate } from '../binder/contract/models/contract-update.model';
import { UpdateClient, ResClient, ClientUpdates } from '../profile/shared/models/res-client.model';

export function getCreateFromRes(not: ResNotificationPrescript, contractName: string, contractPartnerName: string): NotificationPrescriptCreate {
  const notCreate: NotificationPrescriptCreate = {};
  notCreate.contractId = not.contractId;
  notCreate.dateOfStart = not.dateOfStart;
  notCreate.description = not.description;
  notCreate.enabled = not.enabled;
  notCreate.frequency = not.frequency;
  notCreate.paymentAmount = not.paymentAmount;
  notCreate.paymentCurrency = not.paymentCurrency;
  notCreate.standingOrder = not.standingOrder;
  notCreate.terminationDate = not.terminationDate;
  notCreate.typeOfNotification = not.typeOfNotification;
  notCreate.contractName = contractName;
  notCreate.contractPartnerName = contractPartnerName;
  return notCreate;
}

export function getCreateFromDetail(contr: ResContractDetail): ContractCreate {
  const contrUpd: ContractCreate = {};
  // Michal posila u upd i contractId
    contrUpd.clientContractRole = contr.clientContractRole;
    contrUpd.closedBC = contr.closedBC;
    contrUpd.note = contr.note;
    contrUpd.partnerName = contr.partnerName;
    contrUpd.productGroupName = contr.productGroupName;
    contrUpd.signDate = contr.signDate;
    contrUpd.validFrom = contr.validFrom;
  contrUpd.validTo = contr.validTo;
    contrUpd.accessBC = contr.accessBC;
    contrUpd.contractNumber = contr.contractNumber;
    contrUpd.contractOwner = contr.contractOwner;
    contrUpd.contractOwnerBirthDate = contr.contractOwnerBirthDate;
    contrUpd.hasAttachments = contr.hasAttachments;
    contrUpd.partnerId = contr.partnerId;
    contrUpd.productGroupId = contr.productGroupId;
    contrUpd.state = contr.state === undefined || contr.state === null || contr.state === '' ? 'PROPOSED' : contr.state;
  // contrUpd.clientId = contr.clientId;
  return contrUpd;
}

export function getUpdateFromDetail(contr: ResContractDetail): ContractUpdate {
  const contrUpd: ContractUpdate = {};
  // Michal posila u upd i contractId
    contrUpd.contractId = contr.contractId;
    contrUpd.clientContractRole = contr.clientContractRole;
    contrUpd.closedBC = contr.closedBC;
    contrUpd.note = contr.note;
    contrUpd.partnerName = contr.partnerName;
    contrUpd.productGroupName = contr.productGroupName;
    contrUpd.signDate = contr.signDate;
    contrUpd.validFrom = contr.validFrom;
  contrUpd.validTo = contr.validTo;
    contrUpd.accessBC = contr.accessBC;
    contrUpd.contractNumber = contr.contractNumber;
    contrUpd.contractOwner = contr.contractOwner;
    contrUpd.contractOwnerBirthDate = contr.contractOwnerBirthDate;
    contrUpd.hasAttachments = contr.hasAttachments;
    contrUpd.partnerId = contr.partnerId;
    contrUpd.productGroupId = contr.productGroupId;
  contrUpd.state = contr.state === undefined || contr.state === null || contr.state === '' ? 'PROPOSED' : contr.state;
  contrUpd.markedAsTerminated = contr.markedAsTerminated;
  // contrUpd.clientId = contr.clientId;
  return contrUpd;
}

export function getUpdateFromClient(client: ResClient): UpdateClient {
  const clientUpd: UpdateClient = {};
  clientUpd.purpose = client.purpose;
  const updates: ClientUpdates = {};
  updates.correspondingAddress = client.correspondingAddress;
  updates.email = client.email;
  updates.mobilePhone = client.mobilePhone;
  updates.permanentAddress = client.permanentAddress;
  clientUpd.updates = updates;
  return clientUpd;
}
