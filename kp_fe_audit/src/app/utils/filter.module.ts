import { NgModule } from '@angular/core';
import { FilterPipe } from '../utils/filter.pipe';

@NgModule({
    declarations: [ FilterPipe ],
    exports: [FilterPipe]
})

export class FilterModule { }