import { Pipe, PipeTransform } from '@angular/core';
import { FilterContract } from './filterContract.model';
import { CPContractAttachment } from "../binder/contract/models/cp-contract-attachment.model";
import { CPMessage } from '../shared/models/cp-message.model';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {
  filterMessage: CPMessage = { body: "", subject: "", sender: "", recipient: "", dateSent:"", attachments: [] };
  filterContract: FilterContract = { contractOwner: "", validTo: "", contractNumber: "", contractOwnerBirthDate: "", partnerName: "", productGroupName: "", validFrom: "", signDate: "", note: "", attachments:[] };
  contractFields: string[] = Object.keys(this.filterContract);
  messageFields: string[] = Object.keys(this.filterMessage);
  transform(items: any[], component:string,args: any): any[] {
    if (args === '') {
      return items;
    }
    const isSearch = (data: any): boolean => {
	  let isAll = false;
	  if (data === undefined || data === null) {
		  return isAll;
	  }
      if (typeof data === 'object') {
        if (data.name != undefined || data.name != null) {
          isAll = isSearch(data['name']);
        }
        if (component === 'binder') {
          for (const z in this.contractFields) {
            if (isAll = isSearch(data[this.contractFields[z]])) {
              break;
            }
          }
        }
        else {
          for (const z in this.messageFields) {
            if (isAll = isSearch(data[this.messageFields[z]])) {
              break;
            }
          }
        }
      } else {
        if (typeof args === 'number') {
          isAll = data === args;
        } else {
          isAll = data.toString().toLowerCase().indexOf(args.toLowerCase()) !== -1;
        }
      }

      return isAll;
    };

    return items.filter(isSearch);
  }
}
