import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { FilterContract } from './filterContract.model';
import { ResContract } from '../binder/contract/models/res-contract.model';

@Pipe({
  name: 'searchfilter'
})

@Injectable()
export class SearchFilterPipe implements PipeTransform {
  filterContract: FilterContract = { contractOwner: "", validTo: "", contractNumber: "", contractOwnerBirthDate: "", partnerName: "", productGroupName: "", validFrom: "", signDate: "", note: "" };
  fields: string[] = Object.keys(this.filterContract);
  transform(items: any[], value: string): ResContract[] {
   
    if (!items) return [];
    if (value === '') {
      return items;
    }
    return this.filterTest(items,value);
  }

  filterTest(data: any[], value) {
    let test: any[] = [];
    for (var field in this.fields) {
      let dataToPush = data.filter(it => it[this.fields[field]] == value);
      if (dataToPush.length > 0) {
        dataToPush.forEach((d) => test.push(d));
      }
    }
    console.log(test);
    return test;
  }
}
