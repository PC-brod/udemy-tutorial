import { CPContractAttachment } from "../binder/contract/models/cp-contract-attachment.model";

export function isDefined(arg: any): boolean {
  return arg !== null && typeof arg !== 'undefined';
}

export function isNotDefined(arg: any): boolean {
  return !isDefined(arg);
}

export function isEmpty(val: string | number | boolean) {
  return isNotDefined(val) || (typeof val === 'string' && val.length === 0);
}

export function interpolate(template: string, values: Object) {
  return template.replace(/#{([\w0-9]+)}/g, (val, match) => {
    return isEmpty(values[match]) ? val : values[match];
  });
}

export function assign(target, ...sources) {
  return Object.assign({}, target, ...sources);
}

export function showModal(modalName: string) {
  /*$('#' + modalName).modal('show');
  $('#globalModal').modal('show');*/
  document.getElementById(modalName).click();
}

/**
 * Convert base64 string to Blob object
 * @param b64Data string to convert
 * @param contentType content type of object, i.e "application/pdf"
 */
export function b64toBlob(b64Data, contentType) {
  contentType = contentType || '';
  let sliceSize = 512;

  var byteCharacters = atob(b64Data);
  var byteArrays = [];

  for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    var slice = byteCharacters.slice(offset, offset + sliceSize);

    var byteNumbers = new Array(slice.length);
    for (var i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i);
    }

    var byteArray = new Uint8Array(byteNumbers);

    byteArrays.push(byteArray);
  }

  var blob = new Blob(byteArrays, { type: contentType });
  return blob;
}

/**
 * Create file from input Blob object a download it.
 * @param blob object to file creation
 * @param filename name of created file
 */
export function createAndDownloadBlobFile(blob, filename) {
  if (navigator.msSaveBlob) {
    // IE 10+
    navigator.msSaveBlob(blob, filename);
  } else {
    var link = document.createElement('a');
    // Browsers that support HTML5 download attribute
    if (link.download !== undefined) {
      //var url = URL.createObjectURL(blob);
      link.setAttribute('href', 'data:image/png;base64,' + blob);

      link.setAttribute('download', filename);
      link.style.visibility = 'hidden';
      console.log(link);
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
      return 'data:image/png;base64,' + blob;
    }
  }
}

export function downloadMobileFile(attachment,isSafari) {
  
    var link = document.createElement('a');
    if (link.download !== undefined) {
      //var url = URL.createObjectURL(blob);
      link.setAttribute('href', attachment.img);
      link.setAttribute('target', isSafari ? '_blank' : '_self');
      link.setAttribute('download', attachment.name);
      link.style.visibility = 'hidden';
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
     
    }
}

export function formatDecimalStringToNumber(value: string): number {
  let valueString: string = value.toString();
  
  return +valueString.replace(',', '.').replace(/\s/g, "");;
}
