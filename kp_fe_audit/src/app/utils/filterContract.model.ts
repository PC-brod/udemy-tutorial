import { CPContractAttachment } from "../binder/contract/models/cp-contract-attachment.model";

export interface FilterContract {
  contractNumber?: string;
  partnerName?: string;
  productGroupName?: string;
  signDate?: string;
  validFrom?: string;
  validTo?: string;
  note?: string;
  contractOwner?: string;
  contractOwnerBirthDate?: string;
  attachments?: CPContractAttachment[];
}
