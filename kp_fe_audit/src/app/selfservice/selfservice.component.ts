import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { ChangeDetectorRef, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AuthService } from '../auth/shared/auth.service';
import { interpolate } from '../utils/common.utils';
import { SharedService } from '../shared/shared.service';
import { ApiConfig } from '../api.model';
import { apiConfigToken } from '../api.di';
import { EnvironmentConfig } from '../environment.config';
import { LoggerService } from '../shared/logger.service';
import { LoaderIndicatorService } from '../shared/LoaderIndicator.service';
declare function setActiveNav(id: string): Function;

@Component({
  selector: 'app-selfservice',
  templateUrl: './selfservice.component.html',
  styleUrls: ['../../../src/assets/content/styles/main-style.css'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelfserviceComponent implements OnInit {
  i18NPrefix = 'selfservice.';

  constructor(
    @Inject(apiConfigToken) private config: ApiConfig,
    private authService: AuthService,
    private sharedService: SharedService,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private log: LoggerService,
	private loaderService: LoaderIndicatorService
  ) {}

  ngOnInit() {
    setActiveNav('selfservice');
  }

  private redirectTo(url: string) {
    this.authService.getOAuthCode$().subscribe(
      res => {
		const oauthCode = encodeURIComponent(res.code);
        window.location.href = interpolate(url, { code: oauthCode });
      }, e => { this.log.error(e); }
    );
  }

  public redirectToDamageliability() {
    this.redirectTo(EnvironmentConfig.settings.env.uriInbosPrefix + EnvironmentConfig.settings.env.damageLiabilityUrl);
  }

  public redirectToAccidentInsurance() {
    this.redirectTo(EnvironmentConfig.settings.env.uriInbosPrefix + EnvironmentConfig.settings.env.accidentInsuranceUrl);
  }

  public redirectToProprietaryInsurance() {
    this.redirectTo(EnvironmentConfig.settings.env.uriInbosPrefix + EnvironmentConfig.settings.env.proprietaryInsuranceUrl);
  }

  public redirectToemployeeLiabilityInsurance() {
    this.redirectTo(EnvironmentConfig.settings.env.uriInbosPrefix + EnvironmentConfig.settings.env.employeeLiabilityInsuranceUrl);
  }

  public redirectTocombinatedCarInsurance() {
    this.redirectTo(EnvironmentConfig.settings.env.uriInbosPrefix + EnvironmentConfig.settings.env.combinatedCarInsuranceUrl);
  }

  public redirectTocombinatedProprietaryInsurance() {
    this.redirectTo(EnvironmentConfig.settings.env.uriInbosPrefix + EnvironmentConfig.settings.env.combinatedProprietaryInsuranceUrl);
  }

  public redirectToliabilityInsurance() {
    this.redirectTo(EnvironmentConfig.settings.env.uriInbosPrefix + EnvironmentConfig.settings.env.liabilityInsuranceUrl);
  }

  public redirectToHouseholdInsurance() {
    this.redirectTo(EnvironmentConfig.settings.env.uriInbosPrefix + EnvironmentConfig.settings.env.HouseholdInsuranceUrl);
  }
}
