import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { SelfserviceRoutingModule } from './selfservice-routing.module';
import { SelfserviceComponent } from './selfservice.component';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { AuthGuard } from '../auth/shared/auth-guard.service';
import { ConsultantMessageBoxModule } from '../messages/consultant-message-box/consultant-message-box.module';

@NgModule({
  imports: [
    CommonModule,
    SelfserviceRoutingModule,
    TranslateModule,
    ConsultantMessageBoxModule,
    ScrollToModule.forRoot()
  ],
  declarations: [SelfserviceComponent],
  providers: [AuthGuard]
})
export class SelfserviceModule { }
