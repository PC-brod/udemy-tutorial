import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SelfserviceComponent } from './selfservice.component';
import { AuthGuard } from '../auth/shared/auth-guard.service';

@NgModule({
  imports: [
    RouterModule.forChild(
      [
        // {
        //     path: '',
        //     redirectTo: 'selfservice',
        //     pathMatch: 'full'
        // },
        {
          path: 'selfservice',
          component: SelfserviceComponent,
          canActivate: [AuthGuard]
        },
      ]// ,
      // {
      //   enableTracing: false
      // }
    )
  ],
  exports: [
    RouterModule
  ]
})
export class SelfserviceRoutingModule { }
