import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { OnboardingComponent } from './onboarding.component';
import { AuthGuard } from '../auth/shared/auth-guard.service';

@NgModule({
  imports: [
    RouterModule.forChild(
      [
        // {
        //     path: '',
        //     redirectTo: 'onboarding',
        //     pathMatch: 'full'
        // },
        {
          path: 'onboarding',
          component: OnboardingComponent,
          canActivate: [AuthGuard]
        },
      ]// ,
      // {
      //   enableTracing: false
      // }
    )
  ],
  exports: [
    RouterModule
  ]
})
export class OnboardingRoutingModule { }
