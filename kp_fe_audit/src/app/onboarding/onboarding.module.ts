import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { OnboardingRoutingModule } from './onboarding-routing.module';
import { OnboardingComponent } from './onboarding.component';
import { OverviewComponent } from './overview/overview.component';
import { AuthGuard } from '../auth/shared/auth-guard.service';
import { GdprConsentsComponent } from '../portal/gdpr-consents/gdpr-consents.component';
import { SingleConsentComponent } from '../portal/gdpr-consents/single-consent/single-consent.component';
import { SharedIntegratedService } from '../shared/shared-integrated.service';
import { SharedService } from '../shared/shared.service';
import { ProfileIntegratedService } from '../profile/shared/profile-integrated.service';
import { ProfileService } from '../profile/shared/profile.service';

@NgModule({
  imports: [
    CommonModule,
    OnboardingRoutingModule,
    TranslateModule
  ],
  declarations: [OnboardingComponent, OverviewComponent],
  providers: [AuthGuard,
    {
      provide: SharedService,
      // useClass: SharedMockedService
      useClass: SharedIntegratedService
    },
    {
      provide: ProfileService,
      useClass: ProfileIntegratedService
    }
  ]
})
export class OnboardingModule { }
