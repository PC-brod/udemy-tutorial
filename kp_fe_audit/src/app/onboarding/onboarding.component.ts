import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';
import { ChangeDetectorRef, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AuthService } from '../auth/shared/auth.service';
import { GdprConsentsComponent } from '../portal/gdpr-consents/gdpr-consents.component';
declare function openModal(): Function;
declare function openGdprModal(): Function;
import { SharedService } from '../shared/shared.service';
import { LoggerService } from '../shared/logger.service';
import { LoaderIndicatorService } from '../shared/LoaderIndicator.service';
declare function setActiveNav(id: string): Function;

@Component({
  selector: 'app-onboarding',
  templateUrl: './onboarding.component.html',
  encapsulation: ViewEncapsulation.None
})
export class OnboardingComponent implements OnInit {
  // GDPRConsents: ResGDPRConsentState[];
  agreed = false;
  i18NPrefix = 'onboarding.';


  constructor(
    private authService: AuthService,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private sharedService: SharedService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private log: LoggerService,
	private loaderService: LoaderIndicatorService
  ) {
    this.sharedService.GDPRModalClosed.subscribe(() => {
      if (this.sharedService.GDPRIsOnboarding === true) {
        this.gdprModalClosed();
      }
    });
  }

  ngOnInit() {
    setActiveNav('delete');

      this.log.debug('onboarding GDPR');
      this.log.debug(this.sharedService.GDPRConsents);
      this.sharedService.getGDPR$().subscribe((res) => {
        this.sharedService.GDPRConsents = res;
        this.sharedService.GDPRConsents.forEach((c) => { if (c.accepted) { this.agreed = true; } });

        this.log.debug('inAccessRecovery: ' + this.authService.inAccessRecovery);
        this.log.debug('GDPRShowed: ' + this.authService.GDPRShowed);
        this.log.debug('activated: ' + this.authService.activated);
        this.log.debug('GDPRShowed: ' + this.authService.GDPRShowed); 
        this.log.debug('inAccessRecovery: ' + this.authService.inAccessRecovery);
		this.log.debug('agreed: ' + this.agreed);
		this.loaderService.disableLoader();
        // this.authService.GDPRShowed je nastaven na false po provolání servisy v password-update.component.ts
        if (!this.agreed && !this.authService.GDPRShowed) {
          this.sharedService.GDPRIsOnboarding = true;
          this.sharedService.openGDPRModal();
        } else {
          this.authService.GDPRShowed = true;
          this.sharedService.openCarouselModal();
        }        
      });

      this.sharedService.getOnboardingShown$().subscribe((res) => {
        if (res && res.onboarding_shown === false) {
			this.sharedService.postOnboardingShown$().subscribe((res) => { 
				this.log.debug(res); 
			});
        }
      });
  }

  gdprModalClosed() {
    this.sharedService.GDPRIsOnboarding = false;
    this.log.debug(this.authService.GDPRShowed);
    this.sharedService.openCarouselModal();
  }

}
