import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation, AfterViewInit, ViewChild, ContentChild } from '@angular/core';
import { ChangeDetectorRef, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AuthService } from '../../../auth/shared/auth.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { notBlankValidator, dateLessThan, notBlankDateValidator } from '../../../validation/validation.validators';
import { trimToNull } from '../../../validation/validation.utils';
import { BinderService } from '../../shared/binder.service';
import { ResContractDetail } from '../models/res-contract-detail.model';
import { ResNotificationPrescript } from '../../notification/models/res-notification-prescript.model';
import { ResCPProductGroup } from '../../shared/models/res-cp-product-group.model';
import { ResCPPartner } from '../../shared/models/res-cp-partner.model';
import { ContractNotificationComponent } from '../contract-notification/contract-notification.component';
import { ContractPaymentComponent } from '../contract-payment/contract-payment.component';
import { ViewChildren, QueryList } from '@angular/core';
import {
  DropzoneComponent, DropzoneDirective,
  DropzoneConfigInterface
} from 'ngx-dropzone-wrapper';
import { ResAttachmentUpload } from '../../shared/models/res-attachment-upload.model';
import { DatePipe } from '@angular/common';
import { LoggerService } from '../../../shared/logger.service';
import { SharedService } from '../../../shared/shared.service';
import { LoaderIndicatorService } from '../../../shared/LoaderIndicator.service';
import { ContractSavedEventData } from '../models/contractSavedEventData.model';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/empty';
import 'rxjs/add/observable/from';
import 'rxjs/add/observable/forkJoin';
import { all } from 'q';
import { TranslateService } from '@ngx-translate/core';
import { NotificationFrequency } from '../../../shared/models/enums';

declare function setMaxToday(date: string): Function;
declare function closeNewContractModal(): Function;
declare function resetNewContractDatepickers(): Function;
declare function setDatepicker(id): Function;

@Component({
  selector: 'app-binder-new-contract',
  templateUrl: './new-contract.component.html',
  styleUrls: ['./new-contract.component.css'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NewContractComponent implements OnInit, AfterViewInit {

  i18NPrefix = 'binder.contract.new.';
  i18NErrorPrefix = 'global.errors.';
  i18NInfoPrefix = 'global.info.';

  // ?? ano nebo ne?? nejak musime vyvolat metody uvnitr, ale budou tohle skutecne stejne komponenty jako mame v html??
  @ViewChildren('newnot') notificationComponents: QueryList<ContractNotificationComponent>;

  // ?? ano nebo ne?? nejak musime vyvolat metody uvnitr, ale budou tohle skutecne stejne komponenty jako mame v html??
  @ViewChildren('newpay') paymentComponents: QueryList<ContractPaymentComponent>;

  alreadySubmitted = false;
  submitting = false;
  submitError = false;
  submitWarning = false;
  notError = false;
  submitErrorString = 'saveError';
  form: FormGroup;
  contract: ResContractDetail = {};
  clientId: string;
  payments: ResNotificationPrescript[] = [];
  otherNotifications: ResNotificationPrescript[] = [];
  toAddAttachments: ResAttachmentUpload[] = [];
  newContractId: string = null;
  @Output() added = new EventEmitter<ContractSavedEventData>();
  @Output() dataChanged = new EventEmitter<any>();
  maxNumOfFiles: boolean = false;
  maxFileSize: boolean = false;

  paymentCurrencies: string[] = [
    'CZK', 'EUR', 'USD', 'GBP'
  ];

  updatePaymentsWarn: Array<ResNotificationPrescript> = new Array<ResNotificationPrescript>();
  updateNotificationsWarn: Array<ResNotificationPrescript> = new Array<ResNotificationPrescript>();
  addAttWarn: Array<ResAttachmentUpload> = new Array<ResAttachmentUpload>();

  constructor(
    private binderService: BinderService,
    private authService: AuthService,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private datePipe: DatePipe,
    private log: LoggerService,
    private sharedService: SharedService,
    private loaderService: LoaderIndicatorService,
    private translate: TranslateService
  ) { }

  //#region Dropzone Wrapper

  public type = 'component';

  public disabled = false;
  public useDropzoneClass = 'dropzone';
  public message = `<span id=\"dropzone-span\" class=\"glyphicon glyphicon-cloud-upload\"></span> <p>Nahrát dokumenty</p> <p id=\"dropzone-p\">Přetáhněte sem potřebné dokumenty nebo <a class=\"aRed\">vyberte z vašeho počítače</a>.</p>`;

  public config: DropzoneConfigInterface = {
    clickable: true,
    maxFiles: 10,
    maxFilesize: 5,
    autoReset: null,
    errorReset: null,
    cancelReset: null,
    acceptedFiles: 'application/pdf,image/jpeg,image/png,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.presentation'
  };

  @ViewChild(DropzoneComponent) componentRef?: DropzoneComponent;
  //#endregion

  public onUploadError(args: any): void {
    this.componentRef.directiveRef.dropzone().removeFile(args[0]);
    if (args[1] === "You can not upload any more files.") {
      this.maxNumOfFiles = true;
      this.cd.markForCheck();
    }
    else if (this.toAddAttachments.length === 10) {
      this.maxNumOfFiles = true;
      this.cd.markForCheck();
    }
    else if (new RegExp('File is too big.*').test(args[1])) {
      this.maxFileSize = true;
      this.cd.markForCheck();
    }
  }

  public onUploadSuccess(args: any): void {

    const hasAttachments = true;
    const filename = args[0].name;
    const content = args[1].files.file;
    let toSearch = "base64,";
    let index = content.indexOf(toSearch);
    let base64: string;
    if (index !== -1) {
      base64 = content.substring(content.indexOf(toSearch) + toSearch.length);
    } else {
      base64 = btoa(content);
    }
    const attachment: ResAttachmentUpload = {
      fileData: base64,
      fileName: filename
    };
    this.toAddAttachments.push(attachment);
  
  }

  onCancel(error) {
    if (error === "maxFileSize") {
      this.maxFileSize = false;
    }
    else {
      this.maxNumOfFiles = false;
    }
    this.cd.markForCheck();
  }
//#endregion

ngOnInit() {
  this.contract = {};
  this.payments = [];
  this.otherNotifications = [];
  this.buildForm();
  this.cd.markForCheck();
}

ngAfterViewInit() {
  setMaxToday('#form-contractOwnerBirthDate');
}

addNotification() {
  this.authService.sessionTimerStart();
  this.otherNotifications.push({});
  this.cd.markForCheck();
}

addPayment() {
  this.authService.sessionTimerStart();
  this.payments.push({});
  this.cd.markForCheck();
}

handleFileSuccess(event) {

}

handleFileFailure(event) {

}

switchDateFormat(dateString): string {
  let date = dateString.replace(/\s/g, "");
  return date.substr(6, 4) + '.' + date.substr(3, 2) + '.' + date.substr(0, 2);
}

getContractState(): string {
  if (this.form.value.validTo === null) {
    return 'ACTIVE';
  }
  let dateValidFrom = new Date(this.switchDateFormat(trimToNull(this.form.value.validFrom)));
  let dateValidTo = new Date(this.switchDateFormat(trimToNull(this.form.value.validTo)));
  let dateNow = new Date(this.datePipe.transform(new Date(), 'yyyy-MM-dd'));
  dateNow.setHours(0);

  return dateNow < dateValidFrom ? 'PROPOSED' : dateNow > dateValidTo ? 'TERMINATED' : 'ACTIVE';

}

  private buildForm(): void {

  this.form = this.fb.group({
    partnerName: [this.contract.partnerName, notBlankValidator],
    partnerId: [this.contract.partnerId],
    productGroupName: [this.contract.productGroupName, notBlankValidator],
    productGroupId: [this.contract.productGroupId],
    clientContractRole: [this.contract.clientContractRole],
    signDate: [this.contract.signDate, notBlankDateValidator],
    validFrom: [this.contract.validFrom, notBlankDateValidator],
    validTo: [this.contract.validTo],
    note: [this.contract.note],
    accessBC: [this.contract.accessBC],
    closedBC: [this.contract.closedBC],
    contractOwner: [this.contract.contractOwner, notBlankValidator],
    contractOwnerBirthDate: [this.contract.contractOwnerBirthDate, notBlankDateValidator],
    state: [this.contract.state],
    hasAttachments: false,
    attachments: [this.contract.attachments],
    changes: [this.contract.changes],
    contractChange: [this.contract.contractChange],
    contractId: [this.contract.contractId],
    systemId: [this.contract.systemId],
    contractNumber: [this.contract.contractNumber, notBlankValidator],
    submitting: false
  }, { validator: dateLessThan('validFrom', 'validTo', this.i18NPrefix + 'validation.dateLessThan') });
}

cancel() {
  this.authService.sessionTimerStart();
  this.contract = {};
  this.payments = [];
  this.otherNotifications = [];
}

onSubmit(form: FormGroup): void {
  this.alreadySubmitted = true;

  this.paymentComponents.forEach((item) => item.preSubmit());
  this.notificationComponents.forEach((item) => item.preSubmit());

  if(!this.form.valid || !this.isNotificationsValid() || !this.isPaymentsValid() || this.submitting) {
  return;
}

this.loaderService.showLoader();

this.submitting = true;
this.submitError = false;
this.submitErrorString = 'saveError';
this.submitWarning = false;
this.notError = false;

const value: ResContractDetail = this.getFormValue();

// if (this.newContractId) {
// 	this.submitNotifsAndAttachments(value);
// } else {
this.submitAll(value);
// }

this.alreadySubmitted = false;
this.cd.markForCheck();
  }

  public toggleNewContractAddedSuccess(): void {
  this.sharedService.toggleInfoAlert("binder.contract.edit.success.saveContract", null);
  this.clearForm();
}

	public toggleNewContractAddedWithErrors(): void {
  let content: string;
  let msgObservables: Array<Observable< any >> = new Array<Observable<any>>();

let contractTitle: string;
msgObservables.push(this.translate.get("binder.contract.warn.contractTitle").map(msg => contractTitle = msg));

let paymentsTitle: string;
let payments: Array<string> = new Array<string>();
let notificationsTitle: string;
let notifications: Array<string> = new Array<string>();

if (this.updatePaymentsWarn && this.updatePaymentsWarn.length > 0) {
  msgObservables.push(this.translate.get("binder.contract.warn.paymentsTitle").map(msg => paymentsTitle = msg));
  this.updatePaymentsWarn.forEach(paymentWarn => {
    msgObservables.push(this.translate.get("binder.contract.warn.payments",
      { dateOfStart: paymentWarn.dateOfStart, frequency: this.binderService.freqMap.get(NotificationFrequency[paymentWarn.frequency]), paymentAmount: paymentWarn.paymentAmount, paymentCurrency: paymentWarn.paymentCurrency })
      .map(msg => payments.push(msg)));
  });
}

if (this.updateNotificationsWarn && this.updateNotificationsWarn.length > 0) {
  msgObservables.push(this.translate.get("binder.contract.warn.notificationsTitle").map(msg => notificationsTitle = msg));
  this.updateNotificationsWarn.forEach(notifWarn => {
    msgObservables.push(this.translate.get("binder.contract.warn.notifications",
      { dateOfStart: notifWarn.dateOfStart, frequency: this.binderService.freqMap.get(NotificationFrequency[notifWarn.frequency]), description: notifWarn.description })
      .map(msg => notifications.push(msg)));
  });
}

let attachmentTitle: string;
if (this.addAttWarn && this.addAttWarn.length > 0) {
  msgObservables.push(this.translate.get("binder.contract.warn.attachmentTitle").map(msg => attachmentTitle = msg));
}

Observable.forkJoin(msgObservables).subscribe(val => {
  let attachments: Array<string> = this.addAttWarn.map(att => att.fileName);
  content = this.binderService.buildWarnMsgContent(contractTitle, paymentsTitle, payments, notificationsTitle, notifications, attachmentTitle, attachments);
  this.sharedService.toggleAlertCustom(content, null, "alert-warning", false);

  this.clearForm();
});
	}

	

  public removeNot(
  notification: ResNotificationPrescript
): void {
  this.authService.sessionTimerStart();
  this.removeNotification(notification, false);
}

  public removePayment(
  notification: ResNotificationPrescript
): void {
  this.authService.sessionTimerStart();
  this.removeNotification(notification, true);
}

  public notificationError(
  notError: boolean
): void {
  this.notError = notError;
}

getContractNames(): Array < string > {
  const products: ResCPProductGroup[] = this.binderService.getListOfCodesProductGroups();
  return products.map(x => x.name);
}

getPartnerNames(): Array < string > {
  const partners: ResCPProductGroup[] = this.binderService.getListOfCodesPartners();
  return partners.map(x => x.name);
}

getContractId(name ?: string): string {
  const products: ResCPProductGroup[] = this.binderService.getListOfCodesProductGroups();
  const product = products.find(x => x.name === name);
  return product ? product.productGroupId : null;
}

getPartnerId(name ?: string): string {
  const partners: ResCPPartner[] = this.binderService.getListOfCodesPartners();
  const partner = partners.find(x => x.name === name);
  return partner ? partner.partnerId : null;
}

  public removeNotification(notification: ResNotificationPrescript, isPayment: boolean): void {
  const notifs: ResNotificationPrescript[] = isPayment ? this.payments : this.otherNotifications;
  notifs.splice(notifs.indexOf(notification), 1);
  this.cd.markForCheck();
}

/** Workaround pro nefunkcni datepickery. Docasne. */
dateChanged($event) {
  setTimeout(() => {
    this.form.get($event.target.name).setValue($event.target.value);
  }, 400);

}

  private reset(): void {
  this.authService.sessionTimerStart();
  this.newContractId = null;
  this.contract = {};
  this.payments = [];
  this.otherNotifications = [];
  this.updatePaymentsWarn = new Array<ResNotificationPrescript>();
  this.updateNotificationsWarn = new Array<ResNotificationPrescript>();
  this.addAttWarn = new Array<ResAttachmentUpload>();
}

  public clearForm(): void {
  if(this.form) {
    this.form.reset();
  }
	this.alreadySubmitted = false;
  this.submitting = false;
  this.submitError = false;
  this.submitWarning = false;
  this.notError = false;
  this.newContractId = null;
  this.contract = {};
  this.payments = [];
  this.otherNotifications = [];
  this.toAddAttachments = [];
  if(this.componentRef && this.componentRef.directiveRef) {
  this.componentRef.directiveRef.dropzone().removeAllFiles(true);
}
this.updatePaymentsWarn = new Array<ResNotificationPrescript>();
this.updateNotificationsWarn = new Array<ResNotificationPrescript>();
this.addAttWarn = new Array<ResAttachmentUpload>();
resetNewContractDatepickers();
setDatepicker('form-contractOwnerBirthDate');
setDatepicker('form-signDate');
this.cd.markForCheck();
  }


	private isPaymentsValid(): boolean {
  let valid: boolean = true;
  this.paymentComponents.forEach((item) => {
    valid = valid && item.isFormValid();
  });
  return valid;
}

	private isNotificationsValid(): boolean {
  let valid: boolean = true;
  this.notificationComponents.forEach((item) => {
    valid = valid && item.isFormValid();
  });
  return valid;
}

	private getFormValue(): ResContractDetail {
  let value: ResContractDetail = this.form.value;
  value.partnerName = trimToNull(this.form.value.partnerName);
  value.partnerId = this.getPartnerId(value.partnerName);
  value.productGroupName = trimToNull(this.form.value.productGroupName);
  value.productGroupId = this.getContractId(value.productGroupName);
  value.clientContractRole = this.form.value.clientContractRole;
  value.signDate = trimToNull(this.form.value.signDate);
  value.validFrom = trimToNull(this.form.value.validFrom);
  value.validTo = trimToNull(this.form.value.validTo);
  value.note = trimToNull(this.form.value.note);
  value.accessBC = this.form.value.accessBC;
  value.contractOwner = trimToNull(this.form.value.contractOwner);
  value.contractOwnerBirthDate = trimToNull(this.form.value.contractOwnerBirthDate);
  value.state = trimToNull(this.getContractState());
  value.hasAttachments = this.form.value.hasAttachments;
  value.attachments = this.form.value.attachments;
  value.changes = this.form.value.changes;
  value.contractChange = this.form.value.contractChange;
  value.contractId = trimToNull(this.form.value.contractId);
  value.systemId = trimToNull(this.form.value.systemId);
  value.closedBC = false;
  value.contractNumber = trimToNull(this.form.value.contractNumber);
  value.clientId = this.authService.clientId;

  return value;
}

	private getUpdatePaymentsObservables(newContractId: string, contractName: string, contractPartnerName): Array < Observable < any >> {
  let result: Array<Observable< any >> = new Array<Observable<any>>();
if (this.paymentComponents) {
  result = this.paymentComponents.map(
    (item) => {
      item.contractId = newContractId;
      item.contractName = contractName;
      item.contractPartnerName = contractPartnerName;
      return item.getSubmitObservable().catch(err => {
        this.log.warn(err);
        this.updatePaymentsWarn.push(item.getFormValue());
        this.submitWarning = true;
        return Observable.of(null);
      })
    }
  );
}
return result;
	}

	private getUpdateNotifsObservables(newContractId: string, contractName: string, contractPartnerName): Array < Observable < any >> {
  let result: Array<Observable< any >> = new Array<Observable<any>>();
if (this.notificationComponents) {
  result = this.notificationComponents.map(
    (item) => {
      item.contractId = newContractId;
      item.contractName = contractName;
      item.contractPartnerName = contractPartnerName;
      return item.getSubmitObservable().catch(err => {
        this.log.warn(err);
        this.updateNotificationsWarn.push(item.getFormValue());
        this.submitWarning = true;
        return Observable.of(null);
      })
    }
  );
}
return result;
	}

	private getAddAttObservables(newContractId: string): Array < Observable < any >> {
  let result: Array<Observable< any >> = new Array<Observable<any>>();
if (this.toAddAttachments) {
  result = this.toAddAttachments.map(
    (att) => this.binderService.createAttachment$(newContractId, att).catch(err => {
      this.log.warn(err);
      this.addAttWarn.push(att);
      this.submitWarning = true;
      return Observable.of(null);
    })
  );
}
return result;
	}

	private buildSubmitObservables(newContractId: string, contractName: string, contractPartnerName): Array < Observable < any >> {
  // updatePrescriptWarn and deletePrescriptWarn are used for payments and notifications together
  this.updatePaymentsWarn = new Array<ResNotificationPrescript>();
  this.updateNotificationsWarn = new Array<ResNotificationPrescript>();
  this.addAttWarn = new Array<ResAttachmentUpload>();

  let updatePaymentsObservables: Array<Observable< any >> = this.getUpdatePaymentsObservables(newContractId, contractName, contractPartnerName);
let updateNotifsObservables: Array<Observable<any>> = this.getUpdateNotifsObservables(newContractId, contractName, contractPartnerName);
let addAttObservables: Array<Observable<any>> = this.getAddAttObservables(newContractId);
return updatePaymentsObservables.concat(updateNotifsObservables, addAttObservables);
	}

	private submitAll(value: ResContractDetail): void {
  this.binderService.createContract$(value).subscribe((contr) => {
    this.newContractId = contr.contractId;
    this.submitNotifsAndAttachments(value);
  }, (e) => {
    this.loaderService.disableLoader();
    this.log.error(e);
    this.submitError = true;
    this.submitWarning = false;
    this.submitting = false;
    this.alreadySubmitted = false;
    this.reset();
    this.cd.markForCheck();
  });
}

	private submitNotifsAndAttachments(value: ResContractDetail): void {
  let allObservables = this.buildSubmitObservables(this.newContractId, value.productGroupName, value.partnerName);
  if(allObservables.length > 0) {
  Observable.forkJoin(allObservables).subscribe(val => {
    this.submitting = false;
    this.alreadySubmitted = false;
    this.form.value.submitting = false;
    if (!this.submitError && !this.submitWarning) {
      this.submitError = false;
      this.submitWarning = false;
      this.reset();
      closeNewContractModal();
      this.added.emit({ added: true, callback: this.toggleNewContractAddedSuccess, callbackComponent: this });
    } else {
      // this.dataChanged.emit();
      closeNewContractModal();
      this.added.emit({ added: true, callback: this.toggleNewContractAddedWithErrors, callbackComponent: this });
    }
    this.cd.markForCheck();
  });
} else {
  this.submitting = false;
  this.alreadySubmitted = false;
  this.form.value.submitting = false;
  this.submitError = false;
  this.submitWarning = false;
  this.reset();
  closeNewContractModal();
  this.added.emit({ added: true, callback: this.toggleNewContractAddedSuccess, callbackComponent: this });
  this.cd.markForCheck();
}
	}
}
