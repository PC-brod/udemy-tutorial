import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ContractComponent } from './contract.component';
import { EditContractComponent } from './edit-contract/edit-contract.component';
import { NewContractComponent } from './new-contract/new-contract.component';
import { AuthGuard } from '../../auth/shared/auth-guard.service';
import { ErrorsComponent } from '../../validation/errors/errors.component';
import { ValidationModule } from '../../validation/validation.module';
import { ContractNotificationComponent } from './contract-notification/contract-notification.component';
import { ContractPaymentComponent } from './contract-payment/contract-payment.component';
import { DropzoneModule } from 'ngx-dropzone-wrapper';
import { DROPZONE_CONFIG } from 'ngx-dropzone-wrapper';
import { DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
import { TooltipModule } from 'ngx-tooltip';

// using https://github.com/zefoy/ngx-dropzone-wrapper
const DEFAULT_DROPZONE_CONFIG: DropzoneConfigInterface = {
  // Change this to your upload POST address:
   url: 'https://httpbin.org/post',
   maxFilesize: 5000,
   acceptedFiles: 'application/pdf'
 };

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    ValidationModule,
    DropzoneModule,
    TooltipModule
  ],
  declarations: [
    ContractComponent,
    NewContractComponent,
    ContractNotificationComponent,
    ContractPaymentComponent
    ],
  exports: [ContractComponent, NewContractComponent, ContractNotificationComponent, ContractPaymentComponent],
  providers: [
    AuthGuard,
    {
      provide: DROPZONE_CONFIG,
      useValue: DEFAULT_DROPZONE_CONFIG
    }
  ]
})
export class ContractModule {}
