import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation, AfterViewInit, AfterViewChecked, ViewChild, OnChanges, SimpleChanges, Sanitizer } from '@angular/core';
import { ChangeDetectorRef, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { notBlankValidator, dateLessThan, notBlankDateValidator } from '../../../validation/validation.validators';
import { AuthService } from '../../../auth/shared/auth.service';
import { trimToNull } from '../../../validation/validation.utils';
import { BinderService } from '../../shared/binder.service';
import { ResContractDetail } from '../models/res-contract-detail.model';
import { ResNotificationPrescript } from '../../notification/models/res-notification-prescript.model';
import { ResCPProductGroup } from '../../shared/models/res-cp-product-group.model';
import { ResCPPartner } from '../../shared/models/res-cp-partner.model';
import { ContractNotificationComponent } from '../contract-notification/contract-notification.component';
import { ContractPaymentComponent } from '../contract-payment/contract-payment.component';
import { ViewChildren, QueryList } from '@angular/core';
import { CPContractAttachment } from '../models/cp-contract-attachment.model';
import { b64toBlob, createAndDownloadBlobFile } from '../../../utils/common.utils';
import {
  DropzoneComponent, DropzoneDirective,
  DropzoneConfigInterface
} from 'ngx-dropzone-wrapper';
import { ReqBodyClientAttachment } from '../../shared/models/req-body-client-attachment.model';
import { ResAttachmentUpload } from '../../shared/models/res-attachment-upload.model';
import { DatePipe, TranslationWidth } from '@angular/common';
import { CPContractChange } from '../models/cp-contract-change.model';
import { CPContractChangeType } from '../models/cp-contract-change-type.model';
import { LoggerService } from '../../../shared/logger.service';
import { SharedService } from '../../../shared/shared.service';
import { ScrollToService, ScrollToConfigOptions } from '@nicky-lenaers/ngx-scroll-to';
import { LoaderIndicatorService } from '../../../shared/LoaderIndicator.service';
import { ShowDetailEventData } from '../models/showDetail.model';
import { Observable } from 'rxjs/Observable';
import { NotificationFrequency } from '../../../shared/models/enums';
import 'rxjs/add/observable/empty';
import 'rxjs/add/observable/from';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/mergeMap';
import { forEach } from '@angular/router/src/utils/collection';
import { DomSanitizer } from '@angular/platform-browser';
import { EnvironmentConfig } from '../../../environment.config';

declare function setMaxToday(date: string): Function;
declare function loadDZedit(): Function;
declare function toggleDeleteAttachment(): Function;
declare function toggleDeleteContract(): Function;
declare function isSafari(): Function;


@Component({
  selector: 'app-binder-edit-contract',
  templateUrl: './edit-contract.component.html',
  styleUrls: ['./edit-contract.component.css'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditContractComponent implements OnInit, AfterViewInit, AfterViewChecked {

  i18NPrefix = 'binder.contract.edit.';
  i18NErrorPrefix = 'global.errors.';
  i18NInfoPrefix = 'global.info.';



  constructor(
    private binderService: BinderService,
    private authService: AuthService,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private translateService: TranslateService,
    private datePipe: DatePipe,
    private log: LoggerService,
    private sharedService: SharedService,
    private _scrollToService: ScrollToService,
    private loaderService: LoaderIndicatorService,
    private translate: TranslateService,
    private sanitizer: DomSanitizer
  ) { }

  //#region Dropzone Wrapper

  public type = 'component';

  public disabled = false;
  public useDropzoneClass = 'dropzone';
  public message = `<span id=\"dropzone-span\" class=\"glyphicon glyphicon-cloud-upload\"></span> <p>Nahrát dokumenty</p> <p id=\"dropzone-p\">Přetáhněte sem potřebné dokumenty nebo <a class=\"aRed\">vyberte z vašeho počítače</a>.</p>`;

  public config: DropzoneConfigInterface = {
    clickable: true,
    maxFiles: 10,
    maxFilesize: 5,
    autoReset: null,
    errorReset: null,
    cancelReset: null,
    acceptedFiles: 'application/pdf,image/jpeg,image/png,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.presentation'
  };

  @ViewChild(DropzoneComponent) componentRef?: DropzoneComponent;
  //#endregion

  // ?? ano nebo ne?? nejak musime vyvolat metody uvnitr, ale budou tohle skutecne stejne komponenty jako mame v html??
  @ViewChildren('not') notificationComponents: QueryList<ContractNotificationComponent>;

  // ?? ano nebo ne?? nejak musime vyvolat metody uvnitr, ale budou tohle skutecne stejne komponenty jako mame v html??
  @ViewChildren('pay') paymentComponents: QueryList<ContractPaymentComponent>;

  actualBlobUrl = '';
  alreadySubmitted = false;
  submitting = false;
  submitError = false;
  submitWarning = false;
  noUpdate = true;
  submitErrorString = 'submitError';
  form: FormGroup;
  @Input() contractId: string;
  contractsHistory: CPContractChange[] = [];
  contract: ResContractDetail = {};
  @Input() notifications: ResNotificationPrescript[];
  toRemoveAttachments: CPContractAttachment[] = [];
  @Output() showDetail = new EventEmitter<ShowDetailEventData>();
  payments: ResNotificationPrescript[] = [];
  otherNotifications: ResNotificationPrescript[] = [];
  toRemoveNotifications: ResNotificationPrescript[] = [];
  toAddAttachments: ResAttachmentUpload[] = [];
  toRemoveNotificationsComponents: ContractNotificationComponent[] = [];
  toRemovePaymentsComponents: ContractPaymentComponent[] = [];
  loadError = false;
  contractUpdated = false;
  notError = false;
  deletedAttachment = false;
  maxNumOfFiles = false;
  maxFileSize = false;
  IEorME = false;
  loaded = false;

  attachmentToDelete: CPContractAttachment;
  attachmentsDownloadMap: Map<string, boolean>;

  updatePaymentsWarn: Array<ResNotificationPrescript> = new Array<ResNotificationPrescript>();
  removePaymentsWarn: Array<ResNotificationPrescript> = new Array<ResNotificationPrescript>();
  updateNotificationsWarn: Array<ResNotificationPrescript> = new Array<ResNotificationPrescript>();
  removeNotificationsWarn: Array<ResNotificationPrescript> = new Array<ResNotificationPrescript>();
  addAttWarn: Array<ResAttachmentUpload> = new Array<ResAttachmentUpload>();
  removeAttWarn: Array<CPContractAttachment> = new Array<CPContractAttachment>();


  //#region Dropzone Wrapper
  public onUploadError(args: any): void {
    this.componentRef.directiveRef.dropzone().removeFile(args[0]);
    if (args[1] === "You can not upload any more files.") {
      this.maxNumOfFiles = true;
      this.cd.markForCheck();
    }
    else if (this.contract.attachments != undefined && this.contract.attachments.length + this.toAddAttachments.length === 10) {
      this.maxNumOfFiles = true;
      this.cd.markForCheck();
    }
    else if (new RegExp('File is too big.*').test(args[1])) {
      this.maxFileSize = true;
      this.cd.markForCheck();
    }
  }

  onCancel(error) {
    if (error === "maxFileSize") {
      this.maxFileSize = false;
    }
    else {
      this.maxNumOfFiles = false;
    }
    this.cd.markForCheck();
  }

  getFileUrl(attachment: CPContractAttachment) {
    let attachmentName = attachment.name.replace(/č/g, 'c').replace(/Č/g, 'C').replace(/Ě/g, 'E').replace(/ě/g, 'e').replace(/\(/g, '').replace(/\)/g, '').replace(/\+/g, '').replace(/ď/g, 'd').replace(/Ď/g, 'D');
    return this.sanitizer.bypassSecurityTrustUrl(EnvironmentConfig.settings.env.uriPrefix + '/contracts/attachments/' +
      attachment.contractAttachmentId + '/'
      + this.authService.clientId + '/'
      + encodeURIComponent(attachmentName)
      + '?accessToken=' + encodeURIComponent(this.authService.accessToken));
  }

  public onUploadSuccess(args: any): void {

    if (this.contract.attachments != undefined && this.contract.attachments.length + this.toAddAttachments.length === 10) {
      this.componentRef.directiveRef.dropzone().removeFile(args[0]);
      this.maxNumOfFiles = true;
      this.cd.markForCheck();
    }
    else {
      const hasAttachments = true;
      const filename = args[0].name;
      const content = args[1].files.file;
      let toSearch = "base64,";
      let index = content.indexOf(toSearch);
      let base64: string;
      if (index !== -1) {
        base64 = content.substring(content.indexOf(toSearch) + toSearch.length);
      } else {
        base64 = btoa(content);
      }
      const attachment: ResAttachmentUpload = {
        fileData: base64,
        fileName: filename
      };
      this.toAddAttachments.push(attachment);
    }
  }
  //#endregion

  ngAfterViewInit() {
    loadDZedit();
  }

  getColorOfState() {
    return this.contract.state === 'TERMINATED' ? '#E43024' : this.contract.state === 'PROPOSED' ? '#da8d00' : '#30BB97'
  }

  ngAfterViewChecked() {
    loadDZedit();
  }
  backToBinder(callback: () => void) {
    this.authService.sessionTimerStart();
    this.showDetail.emit({ showDetail: false, callback: callback, callbackComponent: this });
  }

  getPayments(): any {
    if (this.notifications) {
      return this.notifications.filter(x => (x.typeOfNotification === 'Payment' && x.contractId === this.contractId));
    }
    return [];
  }

  triggerScrollTo(destination: string) {

    const config: ScrollToConfigOptions = {
      target: destination
    };

    this._scrollToService.scrollTo(config);
  }

  getOtherNotifications(): any {
    if (this.notifications) {
      return this.notifications.filter(x => (x.typeOfNotification === 'Other' && x.contractId === this.contractId));
    }
    return [];
  }

  getIsSafari() {
    return isSafari();
  }

  ngOnInit(): void {
    this.log.debug(window.navigator);
    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      this.log.debug(window.navigator);
      this.IEorME = true;
    }
    this.contract = {};
    this.payments = [];
    this.otherNotifications = [];
    this.contractsHistory = [];
    this.payments = this.getPayments();
    this.otherNotifications = this.getOtherNotifications();
    this.getContractDetail();
    loadDZedit();
    this.deletedAttachment = false;


    // Naplneni testovacich dat
    // this.getContractsHistory();
    // if (this.contractsHistory === null || this.contractsHistory.length === 0) {
    //   this.noUpdate = true;
    // }

    // this.files = this.contract.attachments[0].
  }

  getImg(attachment: CPContractAttachment) {

    this.binderService.getAttachmentContent$(attachment.contractAttachmentId).subscribe((resp) => {

      //console.log(resp.attachment + this.sharedService.getContentType(attachment.name.split('.')[1]));
      attachment.img = this.sanitizer.bypassSecurityTrustUrl('data:' + this.sharedService.getContentType(attachment.name.split('.')[1]) + ';' + 'base64,' + resp.fileData);
    }, (e) => {
      this.log.error(e);
      this.cd.markForCheck();
    });

  }

  updatingNotification(updating: boolean) {
    if (updating) {
      this.deletedAttachment = true;
    }


  }

  downloadIEorME(attachment: CPContractAttachment) {

    if (this.attachmentsDownloadMap.get(attachment.name)) {
      return;
    }
    this.attachmentsDownloadMap.set(attachment.name, true);
    this.binderService.getAttachmentContent$(attachment.contractAttachmentId).subscribe((resp) => {
      const blob = b64toBlob(resp.fileData, this.sharedService.getContentType(attachment.name.split('.')[1]));

      //createAndDownloadBlobFile(blob, attachment.name);
      this.attachmentsDownloadMap.set(attachment.name, false);
      this.cd.markForCheck();
      this.log.debug(this.sharedService.openBlobInNewTab(blob));

      window.navigator.msSaveOrOpenBlob(blob);


    }, (e) => {
      this.log.error(e);
      this.attachmentsDownloadMap.set(attachment.name, false);
      this.sharedService.toggleDangerAlert("binder.contract.edit.error.fileDownload", { filename: attachment.name });
      this.cd.markForCheck();

    });
    this.cd.markForCheck();
  }

  addNotification() {
    this.authService.sessionTimerStart();
    this.otherNotifications.push({});
  }

  getContractPartnerName() {
    return this.form.get('partnerName').value;
  }

  getContractName() {
    return this.form.get('productGroupName').value;
  }

  uploadAttach() {
    this.authService.sessionTimerStart();
    loadDZedit();
  }

  addPayment() {
    this.authService.sessionTimerStart();
    this.payments.push({});
  }

  getDate(control: any, date: HTMLInputElement) {
    control.value = date.value;
  }

  getContractDetail() {
    this.loaderService.showLoader();
    this.binderService.getContract$(this.contractId).subscribe(
      contract => {
        Object.assign(this.contract, contract);

        if (this.contract.changes && this.contract.changes.length > 0) {
          this.noUpdate = false;
        }

        if (this.contract.attachments) {
          this.attachmentsDownloadMap = new Map();
          if (!(this.contract.attachments instanceof Array)) {
            const attArr: Array<CPContractAttachment> = new Array<CPContractAttachment>();
            attArr.push(this.contract.attachments)
            this.contract.attachments = attArr;
          }
          this.contract.attachments.forEach(att => {
            if (att) {
              this.attachmentsDownloadMap.set(att.name, false);
              this.getImg(att);
              this.loaderService.showLoader();
              setTimeout(() => { this.loaded = true; this.loaderService.disableLoader(); this.cd.markForCheck(); }, 3000);
            }
          });
          if (!this.IEorME) {
            this.contract.attachments.forEach((x) => this.downloadAttachment(x));
          }

        }




        this.loadError = false;
        this.buildForm();
        this.cd.markForCheck();
        this.loaderService.disableLoader();
        this.triggerScrollTo('edit_contract_dest');
      },
      e => {
        this.log.error(e);
        this.loadError = true;
        this.cd.markForCheck();
        this.loaderService.disableLoader();
      }
    );
  }

  switchDateFormat(dateString: string): string {
    let date = dateString.replace(/\s/g, "").split('.');
    return date[2] + '/' + date[1] + '/' + date[0];
  }



  getContractState(): string {
    if (this.form.value.validTo === null) {
      return 'ACTIVE';
    }
    let dateValidFrom = new Date(this.switchDateFormat(trimToNull(this.form.value.validFrom)));
    let dateValidTo = new Date(this.switchDateFormat(trimToNull(this.form.value.validTo)));
    let dateNow = new Date(this.datePipe.transform(new Date(), 'yyyy-MM-dd'));
    dateNow.setHours(0);

    return dateNow < dateValidFrom ? 'PROPOSED' : dateNow > dateValidTo ? 'TERMINATED' : 'ACTIVE';

  }

  private buildForm(): void {
    this.form = this.fb.group({
      partnerName: [this.contract.partnerName, notBlankValidator],
      partnerId: [this.contract.partnerId],
      productGroupName: [this.contract.productGroupName, notBlankValidator],
      productGroupId: [this.contract.productGroupId],
      clientContractRole: [this.contract.clientContractRole],
      signDate: [this.contract.signDate, notBlankDateValidator],
      validFrom: [this.contract.validFrom, notBlankDateValidator],
      closedBC: [this.contract.closedBC],
      validTo: [this.contract.validTo],
      note: [this.contract.note],
      accessBC: [this.contract.accessBC],
      contractOwner: [this.contract.contractOwner, notBlankValidator],
      contractOwnerBirthDate: [this.contract.contractOwnerBirthDate, notBlankDateValidator],
      state: [this.contract.state],
      hasAttachments: [this.contract.hasAttachments],
      attachments: [this.contract.attachments],
      changes: [this.contract.changes],
      contractChange: [this.contract.contractChange],
      contractId: [this.contract.contractId],
      contractNumber: [this.contract.contractNumber, notBlankValidator],
      submitting: false,
      markedAsTerminated: [this.contract.markedAsTerminated]
    }, { validator: dateLessThan('validFrom', 'validTo', this.i18NPrefix + 'validation.dateLessThan') });
  }

  onSubmit(form: FormGroup): void {
    this.loaderService.showLoader();
    //TIMER!!!!
    setTimeout(() => {
      this.alreadySubmitted = true;
      this.paymentComponents.forEach((item) => { if (!item.preSubmit()) { return; } });
      this.notificationComponents.forEach((item) => { if (!item.preSubmit()) { return; } });

      if (!this.form.valid || !this.isNotificationsValid() || !this.isPaymentsValid() || this.submitting) {
        this.loaderService.disableLoader();
        return;
      }

      

      this.submitting = true;
      this.submitError = false;
      this.submitErrorString = 'saveError';
      this.submitWarning = false;
      this.notError = false;
    const value: ResContractDetail = this.getFormValue();

    this.binderService.updateContract$(this.contractId, value).subscribe(() => {
      // updatePrescriptWarn and deletePrescriptWarn are used for payments and notifications together
      this.updatePaymentsWarn = new Array<ResNotificationPrescript>();
      this.removePaymentsWarn = new Array<ResNotificationPrescript>();
      this.updateNotificationsWarn = new Array<ResNotificationPrescript>();
      this.removeNotificationsWarn = new Array<ResNotificationPrescript>();
      this.addAttWarn = new Array<ResAttachmentUpload>();
      this.removeAttWarn = new Array<CPContractAttachment>();

      let updatePaymentsObservables: Array<Observable<any>> = new Array<Observable<any>>();
      if (this.paymentComponents) {
        updatePaymentsObservables = this.paymentComponents.map(
          (item) => item.getSubmitObservable().catch(err => {
            this.log.warn(err);
            this.updatePaymentsWarn.push(item.getFormValue());
            this.submitWarning = true;
            return Observable.of(null);
          })
        );
      }

      let updateNotifsObservables: Array<Observable<any>> = new Array<Observable<any>>();
      if (this.notificationComponents) {

        updateNotifsObservables = this.notificationComponents.map(
          (item) => item.getSubmitObservable().catch(err => {

            this.log.warn(err);
            this.updateNotificationsWarn.push(item.getFormValue());
            this.submitWarning = true;
            return Observable.of(null);
          })
        );
      }

      let addAttObservables: Array<Observable<any>> = new Array<Observable<any>>();
      if (this.toAddAttachments) {
        addAttObservables = this.toAddAttachments.map(
          (att) => this.binderService.createAttachment$(this.contractId, att).catch(err => {
            this.log.warn(err);
            this.addAttWarn.push(att);
            this.submitWarning = true;
            return Observable.of(null);
          })
        );
      }

      let removeAttObservables: Array<Observable<any>> = new Array<Observable<any>>();
      if (this.toRemoveAttachments) {
        removeAttObservables = this.toRemoveAttachments.map(
          (att) => this.binderService.deleteAttachment$(att.contractAttachmentId).catch(err => {
            this.log.warn(err);
            this.removeAttWarn.push(att);
            this.submitWarning = true;
            return Observable.of(null);
          })
        );
      }

      let removePaymentsObservables: Array<Observable<any>> = new Array<Observable<any>>();
      if (this.toRemovePaymentsComponents) {
        removePaymentsObservables = this.toRemovePaymentsComponents.map(
          (item) =>

            this.binderService.deleteNotification$(item.notification.prescriptId).catch(err => {
              this.log.warn(err);
              this.removePaymentsWarn.push(item.getFormValue());
              this.submitWarning = true;
              return Observable.of(null);
            })


        );
      }

      let removeNotifsObservables: Array<Observable<any>> = new Array<Observable<any>>();
      if (this.toRemoveNotificationsComponents) {
        removeNotifsObservables = this.toRemoveNotificationsComponents.map(
          (item) => this.binderService.deleteNotification$(item.notification.prescriptId).catch(err => {
            this.log.warn(err);
            this.removeNotificationsWarn.push(item.getFormValue());
            this.submitWarning = true;
            return Observable.of(null);
          })
        );
      }

      let allObservables = updatePaymentsObservables.concat(updateNotifsObservables,
        addAttObservables,
        removeAttObservables,
        removePaymentsObservables,
        removeNotifsObservables);

      if (allObservables.length > 0) {
        Observable.forkJoin(allObservables).subscribe(val => {
          this.submitting = false;
          this.alreadySubmitted = false;
          if (!this.submitError && !this.submitWarning) {
            this.showDetail.emit({ showDetail: false, callback: this.toggleContractSavedSuccess, callbackComponent: this });
          } else {
            this.showDetail.emit({ showDetail: false, callback: this.toggleContractSavedWithErrors, callbackComponent: this });
          }
          this.submitError = false;
          this.submitWarning = false;
          this.cd.markForCheck();
        });
      } else {
        this.submitting = false;
        this.alreadySubmitted = false;
        this.submitError = false;
        this.submitWarning = false;
        this.showDetail.emit({ showDetail: false, callback: this.toggleContractSavedSuccess, callbackComponent: this });
        this.cd.markForCheck();
      }
    }, (e) => {
      this.loaderService.disableLoader();
      this.log.error(e);
      this.submitError = true;
      this.submitWarning = false;
      this.submitting = false;
      this.alreadySubmitted = false;
      this.cd.markForCheck();
    }
      );
    }, 900);
  }

  public toggleContractSavedSuccess(): void {
    this.sharedService.toggleInfoAlert("binder.contract.edit.success.saveContract", null);
  }

  public toggleContractSavedWithErrors(): void {
    let content: string;
    let msgObservables: Array<Observable<any>> = new Array<Observable<any>>();

    let contractTitle: string;
    msgObservables.push(this.translate.get("binder.contract.warn.contractTitle").map(msg => contractTitle = msg));

    let paymentsTitle: string;
    let payments: Array<string> = new Array<string>();
    let notificationsTitle: string;
    let notifications: Array<string> = new Array<string>();
    let removePaymentsTitle: string;
    let removePayments: Array<string> = new Array<string>();
    let removeNotificationsTitle: string;
    let removeNotifications: Array<string> = new Array<string>();
    let attachmentTitle: string;
    let attachments: Array<string>;
    let removeAttachmentTitle: string;
    let removeAttachments: Array<string>;

    if (this.updatePaymentsWarn && this.updatePaymentsWarn.length > 0) {
      msgObservables.push(this.translate.get("binder.contract.warn.paymentsTitle").map(msg => paymentsTitle = msg));
      this.updatePaymentsWarn.forEach(paymentWarn => {
        msgObservables.push(this.translate.get("binder.contract.warn.payments",
          { dateOfStart: paymentWarn.dateOfStart, frequency: this.binderService.freqMap.get(NotificationFrequency[paymentWarn.frequency]), paymentAmount: paymentWarn.paymentAmount, paymentCurrency: paymentWarn.paymentCurrency })
          .map(msg => payments.push(msg)));
      });
    }
    if (this.updateNotificationsWarn && this.updateNotificationsWarn.length > 0) {
      msgObservables.push(this.translate.get("binder.contract.warn.notificationsTitle").map(msg => notificationsTitle = msg));
      this.updateNotificationsWarn.forEach(notifWarn => {
        msgObservables.push(this.translate.get("binder.contract.warn.notifications",
          { dateOfStart: notifWarn.dateOfStart, frequency: this.binderService.freqMap.get(NotificationFrequency[notifWarn.frequency]), description: notifWarn.description })
          .map(msg => notifications.push(msg)));
      });
    }
    if (this.addAttWarn && this.addAttWarn.length > 0) {
      attachments = this.addAttWarn.map(att => att.fileName);
      msgObservables.push(this.translate.get("binder.contract.warn.attachmentTitle").map(msg => attachmentTitle = msg));
    }

    if (this.removePaymentsWarn && this.removePaymentsWarn.length > 0) {
      msgObservables.push(this.translate.get("binder.contract.warn.removePaymentsTitle").map(msg => removePaymentsTitle = msg));
      this.removePaymentsWarn.forEach(paymentWarn => {
        msgObservables.push(this.translate.get("binder.contract.warn.removePayments",
          { dateOfStart: paymentWarn.dateOfStart, frequency: this.binderService.freqMap.get(NotificationFrequency[paymentWarn.frequency]), paymentAmount: paymentWarn.paymentAmount, paymentCurrency: paymentWarn.paymentCurrency })
          .map(msg => removePayments.push(msg)));
      });
    }
    if (this.removeNotificationsWarn && this.removeNotificationsWarn.length > 0) {
      msgObservables.push(this.translate.get("binder.contract.warn.removeNotificationsTitle").map(msg => removeNotificationsTitle = msg));
      this.removeNotificationsWarn.forEach(notifWarn => {
        msgObservables.push(this.translate.get("binder.contract.warn.removeNotifications",
          { dateOfStart: notifWarn.dateOfStart, frequency: this.binderService.freqMap.get(NotificationFrequency[notifWarn.frequency]), description: notifWarn.description })
          .map(msg => removeNotifications.push(msg)));
      });
    }
    if (this.removeAttWarn && this.removeAttWarn.length > 0) {
      removeAttachments = this.removeAttWarn.map(att => att.name);
      msgObservables.push(this.translate.get("binder.contract.warn.removeAttachmentTitle").map(msg => removeAttachmentTitle = msg));
    }

    Observable.forkJoin(msgObservables).subscribe(val => {
      content = this.binderService.buildWarnMsgContent(contractTitle,
        paymentsTitle, payments,
        notificationsTitle, notifications,
        attachmentTitle, attachments,
        removePaymentsTitle, removePayments,
        removeNotificationsTitle, removeNotifications,
        removeAttachmentTitle, removeAttachments);
      this.sharedService.toggleAlertCustom(content, null, "alert-warning", false);
    });
  }

  cancel() {
    this.authService.sessionTimerStart();
    this.contract = {};
    this.payments = [];
    this.otherNotifications = [];
    this.showDetail.emit({ showDetail: false });
  }

  getContractNames(): Array<string> {
    const products: ResCPProductGroup[] = this.binderService.getListOfCodesProductGroups();
    return products.map(x => x.name);
  }

  getPartnerNames(): Array<string> {
    const partners: ResCPPartner[] = this.binderService.getListOfCodesPartners();
    return partners.map(x => x.name);
  }

  getContractId(name?: string): string {
    const products: ResCPProductGroup[] = this.binderService.getListOfCodesProductGroups();
    const product = products.find(x => x.name === name);
    return product ? product.productGroupId : null;
  }

  getPartnerId(name?: string): string {
    const partners: ResCPPartner[] = this.binderService.getListOfCodesPartners();
    const partner = partners.find(x => x.name === name);
    return partner ? partner.partnerId : null;
  }

  public removeNotification(
    notification: ResNotificationPrescript,
    isPayment: boolean
  ): void {
    this.deletedAttachment = true;
    this.authService.sessionTimerStart();
    const notifs: ResNotificationPrescript[] = isPayment
      ? this.payments
      : this.otherNotifications;
    if (notification.prescriptId) {
      this.toRemoveNotifications.push(notification);

      // we need to keep payments/notification components which has to be removed to get their Observable objects in this.onSubmit()
      if (isPayment) {
        for (let comp of this.paymentComponents.toArray()) {
          if (comp.notification == notification) {
            this.toRemovePaymentsComponents.push(comp);
            break;
          }
        }
      } else {
        for (let comp of this.notificationComponents.toArray()) {
          if (comp.notification == notification) {
            this.toRemoveNotificationsComponents.push(comp);
            break;
          }
        }
      }
    }

    notifs.splice(notifs.indexOf(notification), 1);
    this.cd.markForCheck();
  }

  public removeAttachmentModal(event, attachment: CPContractAttachment): void {
    event.stopPropagation();
    this.attachmentToDelete = attachment;
    toggleDeleteAttachment();
  }

  public removeAttachment(): void {
    this.authService.sessionTimerStart();
    this.toRemoveAttachments.push(this.attachmentToDelete);
    this.contract.attachments.splice(this.contract.attachments.indexOf(this.attachmentToDelete), 1);

    // modify maximum amount of files
    this.config.maxFiles = this.config.maxFiles + 1;
    this.deletedAttachment = true;
    this.cd.markForCheck();
  }

  public downloadAttachment(attachment: CPContractAttachment): void {
    if (this.attachmentsDownloadMap.get(attachment.name)) {
      return;
    }
    this.attachmentsDownloadMap.set(attachment.name, true);
    this.binderService.getAttachmentContent$(attachment.contractAttachmentId).subscribe((resp) => {
      const blob = b64toBlob(resp.fileData, this.sharedService.getContentType(attachment.name.split('.')[1]));

      //createAndDownloadBlobFile(blob, attachment.name);
      this.attachmentsDownloadMap.set(attachment.name, false);
      this.cd.markForCheck();
      this.log.debug(this.sharedService.openBlobInNewTab(blob));

      attachment.blobUrl = this.sanitizer.bypassSecurityTrustUrl(this.sharedService.openBlobInNewTab(blob));

      this.log.debug(attachment.blobUrl);
    }, (e) => {
      this.log.error(e);
      this.attachmentsDownloadMap.set(attachment.name, false);
      this.sharedService.toggleDangerAlert("binder.contract.edit.error.fileDownload", { filename: attachment.name });
      this.cd.markForCheck();

    });
    this.cd.markForCheck();

  }

  public removeNot(
    notification: ResNotificationPrescript
  ): void {
    //if (notification.description && notification.frequency && notification.dateOfStart) {
    //  this.notificationToDelete = notification;
    //  this.authService.sessionTimerStart();
    //  toggleDeleteNotification();
    //}
    //else {
    //  this.otherNotifications.splice(this.otherNotifications.indexOf(notification), 1);
    //}
    this.authService.sessionTimerStart();
    this.removeNotification(notification, false);


  }

  public removePayment(
    notification: ResNotificationPrescript
  ): void {

    this.authService.sessionTimerStart();
    this.removeNotification(notification, true);
    //  if (notification.paymentAmount && notification.frequency && notification.paymentCurrency && notification.dateOfStart) {
    //    this.paymentToDelete = notification;
    //    this.authService.sessionTimerStart();
    //    toggleDeletePayment();
    //  }
    //  else {
    //    this.payments.splice(this.payments.indexOf(notification), 1);
    //  }

    //}

    //public modalRemoveNotification() {
    //  this.deletedAttachment = true;
    //  this.removeNotification(this.notificationToDelete, false);
    //}

    //public modalRemovePayment() {
    //  this.deletedAttachment = true;
    //  this.removeNotification(this.paymentToDelete, true);
  }

  public notificationError(
    notError: boolean
  ): void {
    this.notError = notError;
  }

  public deleteContractModal(): void {
    toggleDeleteContract();
  }

  public deleteContract(): void {
    this.alreadySubmitted = true;
    if (!this.form.valid || this.form.value.submitting) {
      return;
    }

    this.loaderService.showLoader();

    this.submitting = true;
    this.submitError = false;
    this.submitErrorString = 'deleteError';

    let notifIdErr: Array<number> = new Array<number>();
    let attIdErr: Array<string> = new Array<string>();
    let attNameErr: Array<string> = new Array<string>();
    let notifObservables: Array<Observable<any>> = new Array<Observable<any>>();
    let attObservables: Array<Observable<any>> = new Array<Observable<any>>();

    if (this.notifications) {
      notifObservables = this.notifications.map(notif =>
        this.binderService.deleteNotification$(notif.prescriptId).catch((err) => {
          this.log.error(err);
          notifIdErr.push(notif.prescriptId);
          return Observable.of(null);
        })
      );
    }

    if (this.contract.attachments) {
      attObservables = this.contract.attachments.map(att =>
        this.binderService.deleteAttachment$(att.contractAttachmentId).catch((err) => {
          this.log.error(err);
          attNameErr.push(att.name);
          attIdErr.push(att.contractAttachmentId);
          return Observable.of(null);
        })
      );
    }

    let allObservables = notifObservables.concat(attObservables);
    if (allObservables.length > 0) {
      Observable.forkJoin(allObservables).subscribe(val => {
        if (notifIdErr.length > 0 || attIdErr.length > 0) {
          this.deleteContractError(notifIdErr, attIdErr, attNameErr);
        } else {
          this.doDeleteContract();
        }
      });
    } else {
      this.doDeleteContract();
    }
  }

  private doDeleteContract(): void {
    this.binderService.deleteContract$(this.contract.contractId).subscribe(() => {
      this.submitting = false;
      this.alreadySubmitted = false;
      this.backToBinder(this.toggleDeleteContractSuccess);
      this.cd.markForCheck();
    }, e => {
      // remove deleted notifications or attachments from page
      this.notifications = [];
      this.payments = this.getPayments();
      this.otherNotifications = this.getOtherNotifications();
      this.contract.attachments = [];

      this.loaderService.disableLoader();
      this.log.error(e);
      this.submitError = true;
      this.submitting = false;
      this.alreadySubmitted = false;
      this.cd.markForCheck();
    });
  }

  private deleteContractError(notifIdErr: Array<number>, attIdErr: Array<string>, attNameErr: Array<string>): void {
    this.submitting = false;
    this.alreadySubmitted = false;
    if (notifIdErr.length > 0 && attNameErr.length > 0) {
      this.sharedService.toggleDangerAlert("binder.contract.edit.error.deleteContract.all", { attachments: attNameErr.join(", ") });
    } else if (notifIdErr.length === 0 && attNameErr.length > 0) {
      this.sharedService.toggleDangerAlert("binder.contract.edit.error.deleteContract.attachments", { attachments: attNameErr.join(", ") });
    } else if (notifIdErr.length > 0 && attNameErr.length === 0) {
      this.sharedService.toggleDangerAlert("binder.contract.edit.error.deleteContract.notifications", null);
    }

    // remove deleted notifications or attachments from page
    if (this.notifications) {
      this.notifications = this.notifications.filter(notif => notifIdErr.indexOf(notif.prescriptId) > -1);
    }
    this.payments = this.getPayments();
    this.otherNotifications = this.getOtherNotifications();

    if (this.contract.attachments) {
      this.contract.attachments = this.contract.attachments.filter(att => attIdErr.indexOf(att.contractAttachmentId) > -1);
    }

    this.cd.markForCheck();
    this.loaderService.disableLoader();
  }

  public toggleDeleteContractSuccess(): void {
    this.sharedService.toggleInfoAlert("binder.contract.edit.success.deleteContract", null);
  }

  dateChanged($event) {
    setTimeout(() => {
      this.form.get($event.target.name).setValue($event.target.value);
    }, 400);
  }

  cutFilename(filename: String) {
    return filename.length > 10
      ? (filename.substring(0, 11) + '...')
      : filename;
  }

  private isPaymentsValid(): boolean {
    let valid: boolean = true;
    this.paymentComponents.forEach((item) => {
      valid = valid && item.isFormValid();
    });
    return valid;
  }

  private isNotificationsValid(): boolean {
    let valid: boolean = true;
    this.notificationComponents.forEach((item) => {
      valid = valid && item.isFormValid();
    });
    return valid;
  }

  private getFormValue(): ResContractDetail {
    let value: ResContractDetail = this.form.value;
    value.partnerName = trimToNull(this.form.value.partnerName);
    value.partnerId = this.getPartnerId(value.partnerName);
    value.productGroupName = trimToNull(this.form.value.productGroupName);
    value.productGroupId = this.getContractId(value.productGroupName);
    value.clientContractRole = 'CLIENT';
    value.signDate = trimToNull(this.form.value.signDate);
    value.validFrom = trimToNull(this.form.value.validFrom);
    value.validTo = trimToNull(this.form.value.validTo);
    value.note = trimToNull(this.form.value.note);
    value.accessBC = this.form.value.accessBC;
    value.contractOwner = trimToNull(this.form.value.contractOwner);
    value.contractOwnerBirthDate = trimToNull(this.form.value.contractOwnerBirthDate);
    if (!this.form.value.closedBC) {
      value.state = this.getContractState();
    }
    else {
      value.state = null;
    }
    value.hasAttachments = this.form.value.hasAttachments;
    value.attachments = this.form.value.attachments;
    value.changes = this.form.value.changes;
    value.contractChange = this.form.value.contractChange;
    value.closedBC = this.form.value.closedBC;
    value.contractId = trimToNull(this.form.value.contractId);
    value.contractNumber = trimToNull(this.form.value.contractNumber);
    value.clientId = this.authService.clientId;
    value.markedAsTerminated = this.form.value.markedAsTerminated;

    return value;
  }

  //MOCKY
  getContractsHistory() {
    this.contractsHistory.push(
      {
        attachments: null,
        contractChangeId: "1",
        contractId: "1",
        description: "test 1 tqw",
        newPayment: false,
        signDate: "03.01.2017",
        state: "PROPOSED",
        type: CPContractChangeType.DECREASE,
        validFrom: "01.03.2018",
        validTo: "19.10.2019"
      },
      {
        attachments: null,
        contractChangeId: "4",
        contractId: "4",
        description: "test 4 u",
        newPayment: false,
        signDate: "13.01.2018",
        state: "PROPOSED",
        type: CPContractChangeType.OTHER,
        validFrom: "10.03.2018",
        validTo: "12.06.2018"
      },
      {
        attachments: null,
        contractChangeId: "3",
        contractId: "3",
        description: "test 3 a ",
        newPayment: false,
        signDate: "13.03.2018",
        state: "PROPOSED",
        type: CPContractChangeType.INCREASE,
        validFrom: "31.07.2018",
        validTo: "12.09.2018"
      }
    );
  }

}
