import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, Input, Output } from '@angular/core';
import { CPContractChange } from '../../models/cp-contract-change.model';

@Component({
  selector: 'app-contract-history',
  templateUrl: './contract-history.component.html',
  styleUrls: ['./contract-history.component.css'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ContractHistoryComponent implements OnInit {

  constructor() { }

  @Input() change: CPContractChange;

  ngOnInit() {
    
  }

}
