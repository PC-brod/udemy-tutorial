import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, ChangeDetectorRef, EventEmitter, Input, Output, LOCALE_ID, Inject, OnChanges } from '@angular/core';
import { getCreateFromRes } from '../../../utils/transform.utils';
import { BinderService } from '../../shared/binder.service';
import { AuthService } from '../../../auth/shared/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ResNotificationPrescript } from '../../notification/models/res-notification-prescript.model';
import { trimToNull } from '../../../validation/validation.utils';
import { notBlankValidator, dateInPast } from '../../../validation/validation.validators';
import { positiveNumberValidator } from '../../../validation/validation.validators';
import { NotificationFrequency, PaymentCurrency } from '../../../shared/models/enums';
import { LoggerService } from '../../../shared/logger.service';
import { Observable } from 'rxjs/Observable';
import { DecimalPipe, DatePipe } from '@angular/common';
import { formatDecimalStringToNumber } from '../../../utils/common.utils';

declare function toggleDeletePayment(id): Function;


@Component({
  selector: 'app-binder-contract-payment',
  templateUrl: './contract-payment.component.html',
  styleUrls: ['./contract-payment.component.css'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ContractPaymentComponent implements OnInit {
  i18NPrefix = 'binder.contract.payment.';
  i18NErrorPrefix = 'global.errors.';
  i18NInfoPrefix = 'global.info.';

  constructor(
    private binderService: BinderService,
    private authService: AuthService,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private translate: TranslateService,
    private log: LoggerService,
    private decimalPipe: DecimalPipe,
    private datePipe: DatePipe
  ) { }

  wasEdited = false;
  alreadySubmitted = false;
  submitting = false;
  submitError = false;
  submitErrorString = 'submitError';
  form: FormGroup;
  @Input() notification: ResNotificationPrescript;
  @Input() contractId: string;
  @Input() contractName: string;
  @Input() contractPartnerName: string;
  
  @Output() errorOccured = new EventEmitter<boolean>();
  @Output() remove = new EventEmitter<ResNotificationPrescript>();
  @Output() update = new EventEmitter<boolean>();

  deleteNot = false;
  dateInPastError = false;

  ngOnInit() {
    this.buildForm();
  }

  public notificationFrequencyList() { return Object.keys(NotificationFrequency); }
  public paymentCurrencyList() { return Object.keys(PaymentCurrency); }

  private buildForm(): void {
    this.form = this.fb.group({
      contractId: this.contractId,
      dateOfStart: [this.notification.dateOfStart, notBlankValidator],
      description: [this.notification.description],
      enabled: true,
      frequency: [this.notification.frequency, notBlankValidator],
      paymentAmount: [this.decimalPipe.transform(this.notification.paymentAmount), Validators.compose([positiveNumberValidator])],
      paymentCurrency: [this.notification.paymentCurrency, notBlankValidator],
      prescriptId: [this.notification.prescriptId],
      standingOrder: [this.notification.standingOrder],
      terminationDate: [this.notification.terminationDate],
      typeOfNotification: 'Payment',
      submitting: false
    }/*, { validator: dateInPast('dateOfStart', this.datePipe, this.i18NPrefix + 'validation.dateInPast') }*/);
  }

  removeNewNotification() {
    this.remove.emit(this.notification);
  }

  modalToRemovePayment(id) {
    toggleDeletePayment(id);
  }

  removeNotification() {
    this.authService.sessionTimerStart();
    this.deleteNot = true;
    this.remove.emit(this.notification);
  }

  public isFormValid(): boolean {

    return this.form.valid;
  }

  public preSubmit(): boolean {
    if (this.form.get('dateOfStart').value === null) {
      this.dateInPastError = true;
      this.cd.markForCheck();

    }
    this.alreadySubmitted = true;
    this.cd.markForCheck();
    if (this.wasEdited) {
      if (!this.validateDateInPast()) {
        this.form.get('dateOfStart').setValue('');
        this.cd.markForCheck();
      }
      return this.validateDateInPast();
    }
    else {
      return true;
    }



  }

  submit() {
    this.onSubmit(this.form);
  }


  switchDateFormat(dateString: string): string {
    let date = dateString.replace(/\s/g, "").split('.');
    return date[2] + '/' + date[1] + '/' + date[0];
  }

  validateDateInPast(): boolean {
    this.wasEdited = true;
    if (this.form.get('dateOfStart').value.length >= 8
      && (this.form.get('dateOfStart').value as string).split('.').length - 1 === 2
      && (this.form.get('dateOfStart').value as string).split('.')[2].length >= 4) {
      let startDate: Date = new Date(this.switchDateFormat(trimToNull(this.form.get('dateOfStart').value)));
      let todayDate: Date = new Date(this.datePipe.transform(new Date(), 'yyyy-MM-dd'));
      todayDate.setHours(0);
      if (startDate < todayDate || startDate.toString() === 'Invalid Date') {
        this.dateInPastError = true;
        this.form.get('dateOfStart').setValue('');
        this.cd.markForCheck();
        return false;
      }
      else {
        
        this.dateInPastError = false;
        this.cd.markForCheck();
        return true;
      }

    }
    else {
      this.dateInPastError = true;
      this.cd.markForCheck();
      return false;
    }
  }

  getSubmitObservable(): Observable<any> {
    this.alreadySubmitted = true;
    if (!this.form.valid || this.form.value.submitting) {
      this.cd.markForCheck();
      return null;
    }
    this.submitting = true;
    this.submitError = false;
    this.submitErrorString = 'saveError';

    if (this.deleteNot) {
      return this.getDeleteNotificationObservable();
    }
    return this.getUpdateNotificationObservable();
  }

  onSubmit(form: FormGroup): void {
    this.alreadySubmitted = true;
    if (!this.form.valid || this.form.value.submitting) {
      if (this.form.errors.dateInPast) {
        this.dateInPastError = true;
        this.cd.markForCheck();
        setTimeout(() => { this.dateInPastError = false; this.cd.markForCheck(); }, 3000);
      }
      this.cd.markForCheck();
      return;
    }
    this.submitting = true;
    this.submitError = false;
    this.submitErrorString = 'saveError';

    if (this.deleteNot) {
      this.deleteNotification();
    } else {
      this.updateNotification();
    }
  }

  getUpdateNotificationObservable(): Observable<any> {
    const value: ResNotificationPrescript = this.getFormValue();
    if (value.prescriptId === undefined || value.prescriptId === null) {
      return this.binderService.createNotificationPrescript$(getCreateFromRes(value, this.contractName, this.contractPartnerName));
    } else {
      return this.binderService.updateNotificationPrescript$(value.prescriptId, getCreateFromRes(value, this.contractName, this.contractPartnerName));
    }
  }

  getDeleteNotificationObservable(): Observable<any> {
    const value: ResNotificationPrescript = this.getFormValue();
    if (this.notification.prescriptId !== undefined && this.notification.prescriptId !== null) {
      return this.binderService.deleteNotification$(this.notification.prescriptId);
    }
    return null;
  }

  updatingNotification() {
    this.update.emit(true);
  }

  updateNotification() {
    const value: ResNotificationPrescript = this.getFormValue();

    if (value.prescriptId === undefined || value.prescriptId === null) {
      this.binderService.createNotificationPrescript$(getCreateFromRes(value, this.contractName, this.contractPartnerName)).subscribe(() => {
        this.submitting = false;
        this.cd.markForCheck();
      }, (e) => {
        this.log.error(e);
        this.submitError = true;
        this.submitting = false;
        this.alreadySubmitted = false;
        this.cd.markForCheck();
      }
      );
    } else {
      this.binderService.updateNotificationPrescript$(value.prescriptId, getCreateFromRes(value, this.contractName, this.contractPartnerName)).subscribe(() => {
        this.submitting = false;
        this.cd.markForCheck();
      }, (e) => {
        this.log.error(e);
        this.submitError = true;
        this.submitting = false;
        this.alreadySubmitted = false;
        this.cd.markForCheck();
      });
    }
  }
  
  deleteNotification() {
    if (this.notification.prescriptId !== undefined && this.notification.prescriptId !== null) {
      this.binderService.deleteNotification$(this.notification.prescriptId).subscribe(() => {
        this.submitting = false;
        this.cd.markForCheck();
      }, (e) => {
        this.log.error(e);
        this.submitError = true;
        this.submitting = false;
        this.alreadySubmitted = false;
        this.cd.markForCheck();
      });
    }
  }

  /** Workaround pro nefunkcni datepickery. Docasne. */
  dateChanged($event) {
    this.authService.sessionTimerStart();
    setTimeout(() => {
      this.form.get($event.target.name).setValue($event.target.value);
      this.validateDateInPast();
      this.updatingNotification();
    }, 400);
  }

  public getFormValue(): ResNotificationPrescript {
    let value: ResNotificationPrescript = this.form.value;
    value.contractId = this.contractId;
    value.dateOfStart = trimToNull(this.form.value.dateOfStart);
    value.description = trimToNull(this.form.value.description);
    value.enabled = this.form.value.enabled;
    value.frequency = trimToNull(this.form.value.frequency);
    value.paymentAmount = formatDecimalStringToNumber(this.form.value.paymentAmount);
    value.paymentCurrency = trimToNull(this.form.value.paymentCurrency);
    value.prescriptId = this.form.value.prescriptId;
    value.standingOrder = this.form.value.standingOrder;
    value.terminationDate = trimToNull(this.form.value.terminationDate);
    value.typeOfNotification = trimToNull(this.form.value.typeOfNotification);

    return value;
  }
}
