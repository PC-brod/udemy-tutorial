import { ResNotificationPrescript } from '../notification/models/res-notification-prescript.model';

export interface ContractNotificationLabel {
  isFirst: boolean;
  notification: ResNotificationPrescript;
}
