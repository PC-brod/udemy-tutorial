import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation, AfterContentInit } from '@angular/core';
import { ChangeDetectorRef, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ResContract } from './models/res-contract.model';
import { ResNotificationPrescript } from '../notification/models/res-notification-prescript.model';
import { ContractNotificationLabel } from './contract-notification-label.model';
import { forEach } from '@angular/router/src/utils/collection';
import { assign } from '../../utils/common.utils';
import { LoggerService } from '../../shared/logger.service';
import { ShowDetailEventData } from './models/showDetail.model';
import { LoaderIndicatorService } from '../../shared/LoaderIndicator.service';


@Component({
  selector: 'app-binder-contract',
  templateUrl: './contract.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ContractComponent implements OnInit, AfterContentInit {

  i18NPrefix = 'binder.contract.';

  constructor(
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    private router: Router,
	private log: LoggerService,
	private loaderService: LoaderIndicatorService
  ) { }

  @Input() contract: ResContract;
  editShow = false; // true; // TBR
  hasAttachments = false;
  payments: ContractNotificationLabel[] = [];
  otherNotifications: ContractNotificationLabel[] = [];
  @Input() notifications: ResNotificationPrescript[];
  firstPayment = true;
  firstNotification = true;
  loadError = false;
  @Output() showDetail = new EventEmitter<ShowDetailEventData>();
  @Output() detailId = new EventEmitter<string>();

  getPayments(): ResNotificationPrescript[] {
    // mozna ma byt podle type of payment nebo type of notification
      return this.notifications.filter(x => x.typeOfNotification === 'Payment');
  }

  getOtherNotifications(): ResNotificationPrescript[] {
      return this.notifications.filter(x => x.typeOfNotification === 'Other');
  }

  ngOnInit(): void {
    this.hasAttachments = this.contract.attachments && this.contract.attachments.length > 0 ? true : false;
    this.payments = [];
    this.otherNotifications = [];
    this.log.debug('notifications');
    this.log.debug(this.notifications);
    this.log.debug('end notifications');
    this.setNotificationLists();
    this.cd.markForCheck();
        
  }

  ngAfterContentInit(): void {
    this.payments = [];
    this.otherNotifications = [];
    this.log.debug('notifications');
    this.log.debug(this.notifications);
    this.log.debug('end notifications');
    this.setNotificationLists();
      this.cd.markForCheck();
  }

  setNotificationLists() {
    // this.log.debug('setNotificationLists');
    // this.log.debug('notifications');
    // this.log.debug(this.notifications);
    // this.log.debug('setNotificationLists');
    let isFirst = true;
    let nots = this.getPayments();
    // this.log.debug(nots);
    nots.forEach(x => {
      const not: ContractNotificationLabel = {
        isFirst: isFirst,
        notification: x
      };
      assign(not.notification, x);
      this.payments.push(not);
      isFirst = false;
    });
    isFirst = true;
    nots = this.getOtherNotifications();
    // this.log.debug(nots);
    nots.forEach(x => {
      const not: ContractNotificationLabel = {
        isFirst: isFirst,
        notification: x
      };
      assign(not.notification, x);
      this.otherNotifications.push(not);
      isFirst = false;
    });
    // this.log.debug('before lists setNotificationLists');
    // this.log.debug(this.payments);
    // this.log.debug(this.otherNotifications);
    // this.log.debug('after lists setNotificationLists');
  }

  hasNotifications(): any {
    return this.getPayments().length > 0 || this.getOtherNotifications().length > 0;
      // return this.notifications.length > 0;
  }

  	showEdit() {
	  	this.loaderService.showLoader();
		this.showDetail.emit({ showDetail: true });
		this.detailId.emit(this.contract.contractId);
  	}
}
