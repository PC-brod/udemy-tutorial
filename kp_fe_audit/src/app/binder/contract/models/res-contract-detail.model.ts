import { CPContractChange } from './cp-contract-change.model';
import { CPContractAttachment } from './cp-contract-attachment.model';
import { CPClientRole } from './cp-client-role.model';
import { NotLoged } from '../../../shared/models/not-loged.model';

export interface ResContractDetail extends NotLoged {
    // clientId?: string;
    contractId?: string;
    partnerName?: string;
    partnerId?: string;
    productGroupName?: string;
    productGroupId?: string;
    clientContractRole?: CPClientRole;
    closedBC?: boolean;
    signDate?: string;
    validFrom?: string;
    validTo?: string;
    contractNumber?: string;
    note?: string;
    accessBC?: boolean;
    contractOwner?: string;
    contractOwnerBirthDate?: string;
    state?: string;
    markedAsTerminated?: boolean;
    attachments?: CPContractAttachment[];
    contractChange?: CPContractChange;
    changes?: CPContractChange[]; // TBD
    hasAttachments?: boolean;
    clientId?: string;
}
