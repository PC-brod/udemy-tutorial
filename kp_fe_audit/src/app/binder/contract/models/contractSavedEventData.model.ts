export interface ContractSavedEventData {
	added: boolean;
	callback?: () => void;
	callbackComponent?;
}