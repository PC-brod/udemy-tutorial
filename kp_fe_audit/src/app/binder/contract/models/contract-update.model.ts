export interface ContractUpdate {
  partnerName?: string;
  productGroupId?: string;
  contractNumber?: string;
  contractId?: string;
  productGroupName?: string;
  accessBC?: boolean;
  clientContractRole?: string;
  contractOwnerBirthDate?: string;
  closedBC?: boolean;
  contractOwner?: string;
  signDate?: string;
  markedAsTerminated?: boolean;
  partnerId?: string;
  validFrom?: string;
  state?: string;
  validTo?: string;
  hasAttachments?: boolean;
  note?: string;
  // clientId?: string;
}
