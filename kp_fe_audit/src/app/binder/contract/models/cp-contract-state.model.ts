export type CPContractState = 'PROPOSED' | 'ACTIVE' | 'TERMINATED';

export const CPContractState = {
    PROPOSED: 'PROPOSED' as CPContractState,
    ACTIVE: 'ACTIVE' as CPContractState,
    TERMINATED: 'TERMINATED' as CPContractState
};
