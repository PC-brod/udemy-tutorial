export interface ShowDetailEventData {
	showDetail: boolean;
	callback?: () => void;
  callbackComponent?;
}
