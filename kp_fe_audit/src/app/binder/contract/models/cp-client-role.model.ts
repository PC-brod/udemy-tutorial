export type CPClientRole = 'CLIENT' | 'COCLIENT' | 'SUBMITTER';

export const CPClientRole = {
    CLIENT: 'CLIENT' as CPClientRole,
    COCLIENT: 'COCLIENT' as CPClientRole,
    SUBMITTER: 'SUBMITTER' as CPClientRole
};
