import { SafeUrl } from "@angular/platform-browser";

export interface CPContractAttachment {
  contractAttachmentId?: string;
  contractId?: string;
  contractChangeId?: string;
  name?: string;
  relevantDate?: string;
  createdByClient?: boolean;
  blobUrl?: SafeUrl;
  img?: SafeUrl;
}
