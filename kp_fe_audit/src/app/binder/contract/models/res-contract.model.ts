import { CPContractChange } from './cp-contract-change.model';
import { CPContractAttachment } from './cp-contract-attachment.model';
import { CPClientRole } from './cp-client-role.model';

export interface ResContract {
    clientId?: string;
    contractId?: string;
    contractNumber?: string;
      partnerName?: string;
    partnerId?: string;
    productGroupName?: string;
    productGroupId?: string;
    clientContractRole?: CPClientRole;
    closedBC?: boolean;
    signDate?: string;
    validFrom?: string;
    validTo?: string;
  note?: string;
  markedAsTerminated?: boolean;
    accessBC?: boolean;
    contractOwner?: string;
    contractOwnerBirthDate?: string;
    state?: string;
      attachments?: CPContractAttachment[];
    changes?: CPContractChange[]; // TBD
    hasAttachments?: boolean;
}
