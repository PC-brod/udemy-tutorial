import { CPContractChangeType } from './cp-contract-change-type.model';
import { CPContractAttachment } from './cp-contract-attachment.model';
import { CPContractState } from './cp-contract-state.model';

export interface CPContractChange {
    contractChangeId?: string;
    contractId?: string;
    state?: CPContractState;
    signDate?: string;
    validFrom?: string;
    validTo?: string;
    attachments?: Array<CPContractAttachment>;
    type?: CPContractChangeType;
    description?: string;
    newPayment?: boolean;
}
