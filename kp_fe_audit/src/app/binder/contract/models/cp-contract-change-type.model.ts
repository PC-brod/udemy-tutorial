export type CPContractChangeType = 'DECREASE' | 'INCREASE' | 'INDEXATION' | 'OTHER';

export const CPContractChangeType = {
    DECREASE: 'DECREASE' as CPContractChangeType,
    INCREASE: 'INCREASE' as CPContractChangeType,
    INDEXATION: 'INDEXATION' as CPContractChangeType,
    OTHER: 'OTHER' as CPContractChangeType
};
