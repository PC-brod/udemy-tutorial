export interface ContractCreate {
  partnerName?: string;
  productGroupId?: string;
  contractNumber?: string;
  productGroupName?: string;
  accessBC?: boolean;
  clientContractRole?: string;
  contractOwnerBirthDate?: string;
  closedBC?: boolean;
  contractOwner?: string;
  signDate?: string;
  partnerId?: string;
  validFrom?: string;
  state?: string;
  validTo?: string;
  hasAttachments?: boolean;
  note?: string;
  // clientId?: string;
}
