export interface ResCPPartner {
  partnerId?: string;
  name?: string;
  isBC?: boolean;
}
