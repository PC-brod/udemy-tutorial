export interface ResAttachmentUpload {
  contractId?: string;
  contractChangeId?: string;
  fileName?: string;
  fileData?: string;
}
