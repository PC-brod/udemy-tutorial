import { NotLoged } from '../../../shared/models/not-loged.model';
import { ResAttachmentUpload } from './res-attachment-upload.model';

export interface ReqBodyClientAttachment extends BodyClientAttachment, NotLoged {}

export interface BodyClientAttachment {
    clientId?: string;
    attachment?: ResAttachmentUpload;
}
