﻿import { Injectable, EventEmitter, isDevMode, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { HttpClient } from '@angular/common/http';

import { assign, interpolate } from '../../../app/utils/common.utils';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ChangeDetectorRef, Input, Output } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { GDPRConsents, codelistpartners, codelistproductgroups,
  attachments, contractsDetails, contracts, notificationPrescripts } from '../../../mocks/mock';
import { BinderService } from './binder.service';
import { ResCPProductGroup } from './models/res-cp-product-group.model';
import { ResCPPartner } from './models/res-cp-partner.model';
import { ResContract } from '../contract/models/res-contract.model';
import { ResContractDetail } from '../contract/models/res-contract-detail.model';
import { ResNotificationPrescript } from '../notification/models/res-notification-prescript.model';
import { NotificationPrescriptCreate } from '../notification/models/notification-prescript-create.model';
import { ReqBodyClientId } from '../../shared/models/req-body-client.model';
import { FileResponse } from '../../shared/models/file-response.model';
import { ResAttachmentUpload } from './models/res-attachment-upload.model';
import { ReqBodyClientAttachment } from './models/req-body-client-attachment.model';
import { ApiConfig } from '../../api.model';
import { apiConfigToken } from '../../api.di';

const delay = 500;

@Injectable()
export class BinderMockedService extends BinderService {

  constructor(
    @Inject(apiConfigToken) private config: ApiConfig,
    private http: HttpClient,
    private activatedRoute: ActivatedRoute,
    // private cd: ChangeDetectorRef,
    private router: Router
  ) {
    super();
  }

  private productGroups: Array<ResCPProductGroup>;
  private partners: Array<ResCPPartner>;

  getContracts$(body?: ReqBodyClientId): Observable<Array<ResContract>> {
    return Observable.timer(delay).map(() => contracts);
  }

  createContract$(body?: ResContractDetail): Observable<ResContract> {
    return Observable.timer(delay).map(() => {
      body.contractId = `${new Date().getTime()}`;
      contracts.push(body);
      const contractDet: ResContractDetail = body; // TBD
      contractsDetails.push(contractDet);
      return body;
    });
  }

  getContract$(contractId: string, body?: any): Observable<ResContractDetail> {
    return Observable.timer(delay).map(() => {
      const idx = contractsDetails.findIndex((c) => c.contractId === contractId);
      if (idx === -1) {
        throw new Error(`Item with id ${contractId} not exists.`);
      }
      return contractsDetails[idx];
    });
  }

  updateContract$(contractId: string, body: ResContractDetail): Observable<ResContract> {
    return Observable.timer(delay).map(() => {
      const idx = contracts.findIndex((c) => c.contractId === contractId);
      if (idx === -1) {
        throw new Error(`Item with id ${contractId} not exists.`);
      }
      const idxDet = contractsDetails.findIndex((c) => c.contractId === contractId);
      if (idxDet === -1) {
        throw new Error(`Item with id ${contractId} not exists.`);
      }
      const contract: ResContract = null;
      const contractDet: ResContractDetail = null;
      assign(contract, contracts[idx]);
      assign(contract, contractsDetails[idxDet]);
      // TBD
      return contract;
    });
  }

  deleteContract$(contractId: string): Observable<any> {
    return Observable.timer(delay).map(() => {
      const idx = contracts.findIndex((c) => c.contractId === contractId);
      if (idx === -1) {
        throw new Error(`Item with id ${contractId} not exists.`);
      }
      contracts.splice(idx, 1);
      const idxDet = contractsDetails.findIndex((c) => c.contractId === contractId);
      if (idx === -1) {
        throw new Error(`Item with id ${contractId} not exists.`);
      }
      contractsDetails.splice(idxDet, 1);
      return null;
    });
  }

  createAttachment$(contractId: string, body?: ResAttachmentUpload): Observable<ResAttachmentUpload> {
    return Observable.timer(delay).map(() => {
      // attachments.push(body);
      return body;
    });
  }

  getAttachmentContent$(attachmentId: string): Observable<FileResponse> {
    return null;
  }

  deleteAttachment$(attachmentId: string): Observable<any> {
    return Observable.timer(delay).map(() => {
      // Nebude fungovat
      return null;
    });
  }

  getNotificationsList$(): Observable<Array<ResNotificationPrescript>> {
    return Observable.timer(delay).map(() => {
      return notificationPrescripts;
    });
  }

  createNotificationPrescript$(body: NotificationPrescriptCreate): Observable<any> {
    return Observable.timer(delay).map(() => {
      return null;
    });
  }

  deleteNotification$(notPrescriptId: number): Observable<any> {
    return Observable.timer(delay).map(() => {
      const notif = notificationPrescripts.filter(n => n.prescriptId === notPrescriptId)[0];
      if (notif) {
        notificationPrescripts.splice(notificationPrescripts.indexOf(notif), 1);
      }
      return null;
    });
  }

  getNotificationPrescript$(notPrescriptId: number): Observable<ResNotificationPrescript> {
    return Observable.timer(delay).map(() => {
      return notificationPrescripts[0];
    });
  }

  updateNotificationPrescript$(notPrescriptId: number, body: NotificationPrescriptCreate): Observable<ResNotificationPrescript> {
    return Observable.timer(delay).map(() => {
      const notif: ResNotificationPrescript = notificationPrescripts.filter(n => n.prescriptId === notPrescriptId)[0];
      if (notif) {
        return {
          prescriptId: notif.prescriptId,
          conctractId: notif.contractId,
          dateOfStart: body.dateOfStart,
          description: body.description,
          enabled: body.enabled,
          frequency: body.frequency,
          terminationDate: body.terminationDate,
          paymentAmount: body.paymentAmount,
          paymentCurrency: body.paymentCurrency,
          standingOrder: body.standingOrder,
          typeOfNotification: body.typeOfNotification
        };
      }
      return null;
    });
  }

  getListOfCodesProductGroups(): Array<ResCPProductGroup> {
    // if (this.productGroups === undefined || this.productGroups === null || this.productGroups.length === 0) {
      Observable.timer(delay).map(() => {
        return codelistproductgroups;
      }).subscribe((res) => {
        this.productGroups = res;
        // this.cd.markForCheck();
      }, (e) => {
        console.error(e);
        this.productGroups = [];
        // this.cd.markForCheck();
      });
    // }
    return this.productGroups;
  }

  getListOfCodesPartners(): Array<ResCPPartner> {
    // if (this.partners === undefined || this.partners === null || this.partners.length === 0) {
      Observable.timer(delay).map(() => {
        return codelistpartners;
      }).subscribe((res) => {
        this.partners = res;
        // this.cd.markForCheck();
      }, (e) => {
        console.error(e);
        this.partners = [];
        // this.cd.markForCheck();
      });
    // }
    return this.partners;
  }


  	public buildWarnMsgContent(contractTitle: string, paymentsTitle?: string, payments?: Array<string>,
								notificationsTitle?: string, notifications?: Array<string>,
								attachmentTitle?: string, attachments?: Array<string>,
								removePaymentsTitle?: string, removePayments?: Array<string>,
								removeNotificationsTitle?: string, removeNotifications?: Array<string>,
								removeAttachmentTitle?: string, removeAttachments?: Array<string>): string {
		let content = "<div>";
		content += "<span style=\"font-weight: bold\">" + contractTitle + "</span>";

		// Update/create Payments 
		if (paymentsTitle && payments) {
			content += "<div class=\"contract-warn-inner\">";
			content += "<span>" + paymentsTitle + "</span>";
			content += "<ul>";
			payments.forEach(payment => {
				content += "<li><span>" + payment + "</span></li>";
			});
			content += "</ul>";
			content += "</div>";
		}

		// Update/create Notifications
		if (notificationsTitle && notifications) {
			content += "<div class=\"contract-warn-inner\">";
			content += "<span>" + notificationsTitle + "</span>";
			content += "<ul>";
			notifications.forEach(notif => {
				content += "<li><span>" + notif + "</span></li>";
			});
			content += "</ul>";
			content += "</div>";
		}

		// Create Attachments
		if (attachmentTitle && attachments) {
			content += "<div class=\"contract-warn-inner\">";
			content += "<span>" + attachmentTitle + "</span>";
			content += "<ul>";
			attachments.forEach(att => {
				content += "<li><span>" + attachments + "</span></li>";
			});
			content += "</ul>";
			content += "</div>";
		}

		// Remove Payments 
		if (removePaymentsTitle && removePayments) {
			content += "<div class=\"contract-warn-inner\">";
			content += "<span>" + removePaymentsTitle + "</span>";
			content += "<ul>";
			removePayments.forEach(payment => {
				content += "<li><span>" + payment + "</span></li>";
			});
			content += "</ul>";
			content += "</div>";
		}

		// Remove Notifications
		if (removeNotificationsTitle && removeNotifications) {
			content += "<div class=\"contract-warn-inner\">";
			content += "<span>" + removeNotificationsTitle + "</span>";
			content += "<ul>";
			removeNotifications.forEach(notif => {
				content += "<li><span>" + notif + "</span></li>";
			});
			content += "</ul>";
			content += "</div>";
		}

		// Remove Attachments
		if (removeAttachmentTitle && removeAttachments) {
			content += "<div class=\"contract-warn-inner\">";
			content += "<span>" + removeAttachmentTitle + "</span>";
			content += "<ul>";
			removeAttachments.forEach(att => {
				content += "<li><span>" + attachments + "</span></li>";
			});
			content += "</ul>";
			content += "</div>";
		}

		content += "</div>";
		return content;
	}
}
