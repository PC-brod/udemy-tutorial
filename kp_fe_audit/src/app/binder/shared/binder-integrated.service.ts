import { Injectable, Inject } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { HttpClient } from '@angular/common/http';

import { assign, interpolate } from '../../../app/utils/common.utils';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { BinderService } from './binder.service';
import { AuthService } from '../../auth/shared/auth.service';

import { getUpdateFromDetail, getCreateFromDetail } from '../../utils/transform.utils';
import { ResCPProductGroup } from './models/res-cp-product-group.model';
import { ResCPPartner } from './models/res-cp-partner.model';
import { ResContract } from '../contract/models/res-contract.model';
import { ResContractDetail } from '../contract/models/res-contract-detail.model';
import { ResNotificationPrescript } from '../notification/models/res-notification-prescript.model';
import { NotificationPrescriptCreate } from '../notification/models/notification-prescript-create.model';
import { ContractCreate } from '../contract/models/contract-create.model';
import { ContractUpdate } from '../contract/models/contract-update.model';
import { ResAttachmentUpload } from './models/res-attachment-upload.model';
import { FileResponse } from '../../shared/models/file-response.model';
import { ApiConfig } from '../../api.model';
import { apiConfigToken } from '../../api.di';
import { EnvironmentConfig } from '../../environment.config';
import { LoggerService } from '../../shared/logger.service';
import { NotificationFrequency } from '../../shared/models/enums';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class BinderIntegratedService extends BinderService {

	private productGroups: Array<ResCPProductGroup> = [];
	private partners: Array<ResCPPartner> = [];

	constructor(
		@Inject(apiConfigToken) private config: ApiConfig,
		private http: HttpClient,
		private authService: AuthService,
		private activatedRoute: ActivatedRoute,
		private router: Router,
		private log: LoggerService,
		private translate: TranslateService
	) {
		super();
		let freqObservables: Array<Observable<any>> = new Array<Observable<any>>();
		Object.keys(NotificationFrequency).forEach(freq => {
			freqObservables.push(this.translate.get("enum.NotificationFrequency." + freq).map(msg => this.freqMap.set(NotificationFrequency[freq], msg)));
		});
		Observable.forkJoin(freqObservables).subscribe(val => { });
	}

      getContracts$(): Observable<Array<ResContract>> {
      return this.authService.get$(interpolate(EnvironmentConfig.settings.env.uriPrefix + this.config.getContractsUrl,
        { clientId: this.authService.clientId }))
      .map((res) => {
        if (!res) {
          throw new Error(`These contracts do not exists.`);
        }
        return res;
      });
    }

    createContract$(body?: ResContractDetail): Observable<ResContract> {
      const contractUpdate: ContractCreate = getCreateFromDetail(body);
      return this.authService.post$(interpolate(EnvironmentConfig.settings.env.uriPrefix + this.config.createContractUrl,
        { clientId: this.authService.clientId }),
      contractUpdate).map((res: ResContract) => {
        if (!res) {
          throw new Error(`ContractId do not exists.`);
        }
        return res;
      });
    }

    getContract$(contractId: string): Observable<ResContractDetail> {
      return this.authService.get$(interpolate(EnvironmentConfig.settings.env.uriPrefix + this.config.getContractUrl,
        { clientId: this.authService.clientId, contractId }))
    .map((res) => {
        if (!res) {
          throw new Error(`Contract ${contractId} do not exists.`);
        }
        return res;
      });
    }

    updateContract$(contractId: string, body: ResContractDetail): Observable<any> {
      body = assign(body);
      const contractUpdate: ContractUpdate = getUpdateFromDetail(body);
      return this.authService.put$(interpolate(EnvironmentConfig.settings.env.uriPrefix + this.config.updateContractUrl,
        { clientId: this.authService.clientId, contractId }), contractUpdate)
      .map(() => {
        return null;
      });
    }

    deleteContract$(contractId: string): Observable<any> {
      return this.authService.delete$(interpolate(EnvironmentConfig.settings.env.uriPrefix + this.config.deleteContractUrl,
        { clientId: this.authService.clientId, contractId })).map(() => null);
    }

    createAttachment$(contractId: string, body?: ResAttachmentUpload): Observable<ResAttachmentUpload> {
      return this.authService.post$(interpolate(EnvironmentConfig.settings.env.uriPrefix + this.config.createAttachmentUrl,
        { clientId: this.authService.clientId, contractId }), body)
      .map((res: ResAttachmentUpload) => {
        if (!res) {
          throw new Error(`Attachment was not uploaded.`);
        }
        return res;
      });
    }

    getAttachmentContent$(attachmentId: string): Observable<FileResponse> {
      return this.authService.get$(interpolate(EnvironmentConfig.settings.env.uriPrefix + this.config.getAttachmentContentUrl,
        { attachmentId: attachmentId, clientId: this.authService.clientId}))
      .map((res: FileResponse) => {
        if (!res) {
          throw new Error(`Attachment ${attachmentId} do not exists.`);
        }
        return res;
      });
    }

    deleteAttachment$(attachmentId: string): Observable<any> {
      return this.authService.delete$(interpolate(EnvironmentConfig.settings.env.uriPrefix + this.config.deleteAttachmentUrl,
        { attachmentId: attachmentId, clientId: this.authService.clientId }))
        .map((res) => {
          this.log.debug(res);
          return res;
        });
    }

    getNotificationsList$(): Observable<Array<ResNotificationPrescript>> {
      return this.authService.get$(interpolate(EnvironmentConfig.settings.env.uriPrefix + this.config.getNotificationsListUrl,
        { clientId: this.authService.clientId}))
    .map((res) => {
        if (res === undefined) {
          throw new Error(`This notifications list do not exists.`);
        }
        if (res === null) {
          return [];
        }
        return res;
      });
    }

    getNotificationPrescript$(notPrescriptId: number): Observable<any> {
      return this.authService.get$(interpolate(EnvironmentConfig.settings.env.uriPrefix + this.config.getNotificationPrescriptUrl,
        { notPrescriptId, clientId: this.authService.clientId }))
      .map((res: any) => {
        if (!res) {
          throw new Error(`This notification prescript do not exists.`);
        }
        return res;
      });
    }

    createNotificationPrescript$(body: NotificationPrescriptCreate): Observable<any> {	
      return this.authService.post$(interpolate(EnvironmentConfig.settings.env.uriPrefix + this.config.createNotificationPrescriptUrl,
        {clientId: this.authService.clientId})
      , body).map(() => null);
    }

    deleteNotification$(notPrescriptId: number): Observable<any> {
      return this.authService.delete$(interpolate(EnvironmentConfig.settings.env.uriPrefix + this.config.deleteNotificationUrl,
        {clientId: this.authService.clientId, notPrescriptId}))
      .map((res) => () => null );
    }

    updateNotificationPrescript$(notPrescriptId: number, body: NotificationPrescriptCreate): Observable<any> {
      body = assign(body);
      return this.authService.put$(interpolate(EnvironmentConfig.settings.env.uriPrefix + this.config.updateNotificationPrescriptUrl,
      { notPrescriptId: notPrescriptId, clientId: this.authService.clientId }), body)
      .mergeMap(() => {
        return this.getNotificationPrescript$(notPrescriptId);
      });
    }

  getListOfCodesProductGroups(): Array<ResCPProductGroup> {
    if (this.productGroups === undefined || this.productGroups === null || this.productGroups.length === 0) {
      this.getListOfCodesProductGroups$().subscribe((res) => {
        this.productGroups = res;
        return this.productGroups;
      }, (e) => {
        this.log.error(e);
        this.productGroups = [];
      });
    }
    return this.productGroups;
  }

  getListOfCodesPartners(): Array<ResCPPartner> {
    if (this.partners === undefined || this.partners === null || this.partners.length === 0) {
      this.getListOfCodesPartners$().subscribe((res) => {
        this.partners = res;
        return this.partners;
      }, (e) => {
        this.log.error(e);
        this.partners = [];
      });
    }
    return this.partners;
  }

  private getListOfCodesProductGroups$(): Observable<Array<ResCPProductGroup>> {
    return this.authService.getUnauthorized(EnvironmentConfig.settings.env.uriPrefix + this.config.getListOfCodesProductGroupsUrl)
    .map((res: Array<ResCPProductGroup>) => {
      if (!res) {
        throw new Error(`List of codes product do not exists.`);
      }
      this.log.debug(res);
      return res;
    });
  }

  private getListOfCodesPartners$(): Observable<Array<ResCPPartner>> {
    return this.authService.getUnauthorized(EnvironmentConfig.settings.env.uriPrefix + this.config.getListOfCodesPartnersUrl)
    .map((res: Array<ResCPPartner>) => {
      if (!res) {
        throw new Error(`List of codes partners do not exists.`);
      }
      this.log.debug(res);
      return res;
    });
  }

  	public buildWarnMsgContent(contractTitle: string, paymentsTitle?: string, payments?: Array<string>,
								notificationsTitle?: string, notifications?: Array<string>,
								attachmentTitle?: string, attachments?: Array<string>,
								removePaymentsTitle?: string, removePayments?: Array<string>,
								removeNotificationsTitle?: string, removeNotifications?: Array<string>,
								removeAttachmentTitle?: string, removeAttachments?: Array<string>): string {
		let content = "<div>";
		content += "<span style=\"font-weight: bold\">" + contractTitle + "</span>";

		// Update/create Payments 
		if (paymentsTitle && payments) {
			content += "<div class=\"contract-warn-inner\">";
			content += "<span>" + paymentsTitle + "</span>";
			content += "<ul>";
			payments.forEach(payment => {
				content += "<li><span>" + payment + "</span></li>";
			});
			content += "</ul>";
			content += "</div>";
		}

		// Update/create Notifications
		if (notificationsTitle && notifications) {
			content += "<div class=\"contract-warn-inner\">";
			content += "<span>" + notificationsTitle + "</span>";
			content += "<ul>";
			notifications.forEach(notif => {
				content += "<li><span>" + notif + "</span></li>";
			});
			content += "</ul>";
			content += "</div>";
		}

		// Create Attachments
		if (attachmentTitle && attachments) {
			content += "<div class=\"contract-warn-inner\">";
			content += "<span>" + attachmentTitle + "</span>";
			content += "<ul>";
			attachments.forEach(att => {
				content += "<li><span>" + att + "</span></li>";
			});
			content += "</ul>";
			content += "</div>";
		}

		// Remove Payments 
		if (removePaymentsTitle && removePayments) {
			content += "<div class=\"contract-warn-inner\">";
			content += "<span>" + removePaymentsTitle + "</span>";
			content += "<ul>";
			removePayments.forEach(payment => {
				content += "<li><span>" + payment + "</span></li>";
			});
			content += "</ul>";
			content += "</div>";
		}

		// Remove Notifications
		if (removeNotificationsTitle && removeNotifications) {
			content += "<div class=\"contract-warn-inner\">";
			content += "<span>" + removeNotificationsTitle + "</span>";
			content += "<ul>";
			removeNotifications.forEach(notif => {
				content += "<li><span>" + notif + "</span></li>";
			});
			content += "</ul>";
			content += "</div>";
		}

		// Remove Attachments
		if (removeAttachmentTitle && removeAttachments) {
			content += "<div class=\"contract-warn-inner\">";
			content += "<span>" + removeAttachmentTitle + "</span>";
			content += "<ul>";
			removeAttachments.forEach(att => {
				content += "<li><span>" + att + "</span></li>";
			});
			content += "</ul>";
			content += "</div>";
		}

		content += "</div>";
		return content;
	}
}
