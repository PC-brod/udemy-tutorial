import { Injectable, EventEmitter, isDevMode, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { HttpClient } from '@angular/common/http';

import { assign, interpolate } from '../../../app/utils/common.utils';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ChangeDetectorRef, Input, Output } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ResCPProductGroup } from './models/res-cp-product-group.model';
import { ResCPPartner } from './models/res-cp-partner.model';
import { ResContract } from '../contract/models/res-contract.model';
import { ResContractDetail } from '../contract/models/res-contract-detail.model';
import { ResNotificationPrescript } from '../notification/models/res-notification-prescript.model';
import { NotificationPrescriptCreate } from '../notification/models/notification-prescript-create.model';
import { ReqBodyClientId } from '../../shared/models/req-body-client.model';
import { FileResponse } from '../../shared/models/file-response.model';
import { ResAttachmentUpload } from './models/res-attachment-upload.model';
import { ReqBodyClientAttachment } from './models/req-body-client-attachment.model';
import { NotificationFrequency } from '../../shared/models/enums';

@Injectable()
export abstract class BinderService {
	
	public freqMap: Map<NotificationFrequency, String> = new Map<NotificationFrequency, String>();

  abstract getContracts$(): Observable<Array<ResContract>>;

  abstract createContract$(body?: ResContractDetail): Observable<ResContract>;

  abstract getContract$(contractId: string): Observable<ResContractDetail>;

  abstract updateContract$(contractId: string, body: ResContractDetail): Observable<any>;

  abstract deleteContract$(contractId: string): Observable<any>;

  abstract createAttachment$(contractId: string, body?: ResAttachmentUpload): Observable<ResAttachmentUpload>;

  abstract getAttachmentContent$(attachmentId: string): Observable<FileResponse>;

  abstract deleteAttachment$(attachmentId: string): Observable<any>;

  abstract getNotificationsList$(): Observable<Array<ResNotificationPrescript>>;

  abstract createNotificationPrescript$(body: NotificationPrescriptCreate): Observable<any>;

  abstract deleteNotification$(notPrescriptId: number): Observable<any>;

  abstract getNotificationPrescript$(notPrescriptId: number): Observable<any>;

  abstract updateNotificationPrescript$(notPrescriptId: number, body: NotificationPrescriptCreate): Observable<any>;

  abstract getListOfCodesProductGroups(): Array<ResCPProductGroup>;

  abstract getListOfCodesPartners(): Array<ResCPPartner>;

  abstract buildWarnMsgContent(contractTitle: string, paymentsTitle?: string, payments?: Array<string>,
								notificationsTitle?: string, notifications?: Array<string>,
								attachmentTitle?: string, attachments?: Array<string>,
								removePaymentsTitle?: string, removePayments?: Array<string>,
								removeNotificationsTitle?: string, removeNotifications?: Array<string>,
								removeAttachmentTitle?: string, removeAttachments?: Array<string>): string;
}
