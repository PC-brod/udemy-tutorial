import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { NewNotificationComponent } from './new-notification/new-notification.component';
import { NotificationComponent } from './notification.component';
import { AuthGuard } from '../../auth/shared/auth-guard.service';

import { HttpClient, HttpClientModule } from '@angular/common/http';
import { CookieXSRFStrategy, XSRFStrategy, HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ValidationModule } from '../../validation/validation.module';
import { TooltipModule } from 'ngx-tooltip';

@NgModule({
  imports: [
    HttpModule,
    CommonModule,
    TranslateModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    ValidationModule,
    TooltipModule
  ],
  declarations: [NotificationComponent, NewNotificationComponent],
  exports: [NotificationComponent, NewNotificationComponent],
  providers: [AuthGuard]
})
export class NotificationModule {}
