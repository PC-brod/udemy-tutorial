export interface ResNotificationPrescript {
  dateOfStart?: string;
  description?: string;
  standingOrder?: boolean;
  terminationDate?: string;
  prescriptId?: number;
  frequency?: string;
  paymentAmount?: number;
  paymentCurrency?: string;
  typeOfNotification?: string;
  contractId?: string;
  enabled?: boolean;
}
