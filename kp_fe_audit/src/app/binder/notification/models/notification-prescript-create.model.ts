export interface NotificationPrescriptCreate {
  dateOfStart?: string;
  description?: string;
  standingOrder?: boolean;
  terminationDate?: string;
  frequency?: string;
  paymentAmount?: number;
  paymentCurrency?: string;
  typeOfNotification?: string;
  contractId?: string;
  enabled?: boolean;
  contractName?: string;
  contractPartnerName?: string;
}
