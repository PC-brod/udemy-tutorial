import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ChangeDetectorRef, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { BinderService } from '../../shared/binder.service';
import { notBlankValidator, dateInPast } from '../../../validation/validation.validators';
import { NotificationPrescriptCreate } from '../models/notification-prescript-create.model';
import { ResNotificationPrescript } from '../models/res-notification-prescript.model';
import { NotificationFrequency } from '../../../shared/models/enums';
import { LoggerService } from '../../../shared/logger.service';
import { LoaderIndicatorService } from '../../../shared/LoaderIndicator.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-binder-new-notification',
  templateUrl: './new-notification.component.html',
  styleUrls: ['./new-notification.component.css'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NewNotificationComponent implements OnInit {

  i18NPrefix = 'binder.notification.new.';

  constructor(
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private binderService: BinderService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private log: LoggerService,
    private loaderService: LoaderIndicatorService,
    private datePipe: DatePipe
  ) { }

  notification: NotificationPrescriptCreate = {};
  @Input() edited: boolean;
  @Output() edit = new EventEmitter<boolean>();
  @Output() saveNotification = new EventEmitter<ResNotificationPrescript>();
  alreadySubmitted = false;
  submitting = false;
  submitError = false;
  submitErrorString = 'submitError';
  form: FormGroup;
  dateInPastError = false;

  ngOnInit(): void {
    this.notification = {};
    this.buildForm();
  }

  private buildForm(): void {
     this.form = this.fb.group({
       contractId: [this.notification.contractId],
       dateOfStart: [this.notification.dateOfStart, notBlankValidator],
       description: [this.notification.description, Validators.compose([
        notBlankValidator, Validators.maxLength(100)])
        ],
       frequency: [this.notification.frequency, notBlankValidator],
       paymentAmount: [this.notification.paymentAmount],
       paymentCurrency: [this.notification.paymentCurrency],
       standingOrder: [this.notification.standingOrder],
       terminationDate: [this.notification.terminationDate],
       typeOfNotification: [this.notification.typeOfNotification],
       contractName: null,
       contractPartnerName: null,
       submitting: false
     }, { validator: dateInPast('dateOfStart', this.datePipe, this.i18NPrefix + 'validation.dateInPast') });
   }

  reset() {
    this.edited = false;
    this.edit.emit(false);
    this.notification = {};
    this.buildForm();
    this.cd.markForCheck();
  }

  validateDateInPast() {
    if (this.form.errors != null && this.form.errors.dateInPast != null) {
      this.dateInPastError = true;
      this.cd.markForCheck();

    }
    else {
      this.dateInPastError = false;
      this.cd.markForCheck();
    }
  }

  onSubmit(form: FormGroup): void {
    
    this.alreadySubmitted = true;
    if (!this.form.valid || this.form.value.submitting) {
      if (this.form.errors != null && this.form.errors.dateInPast != null) {
        this.dateInPastError = true;
        this.cd.markForCheck();
        setTimeout(() => { this.dateInPastError = false; this.cd.markForCheck(); }, 3000);
      }
      return;
    }

	this.loaderService.showLoader();

    this.submitting = true;
    this.submitError = false;
    this.submitErrorString = 'saveError';

    const value: NotificationPrescriptCreate = this.form.value;
	value.enabled = true;
	value.typeOfNotification = 'Other';

    this.binderService.createNotificationPrescript$(value).subscribe(() => {
      this.submitting = false;
      this.alreadySubmitted = false;
      this.saveNotification.emit(this.attachFromEdit(value));
      this.reset();
    }, (e) => {
		this.loaderService.disableLoader();
      this.log.error(e);
      this.submitError = true;
      this.submitting = false;
      this.alreadySubmitted = false;
      this.cd.markForCheck();
        });
    
  }

  attachToEdit(notification: ResNotificationPrescript, notificationEdit: NotificationPrescriptCreate) {
    notificationEdit = {
    contractId: notification.contractId,
    dateOfStart: notification.dateOfStart,
    description: notification.description,
    enabled: notification.enabled,
    frequency: notification.frequency,
    paymentAmount: notification.paymentAmount,
    paymentCurrency: notification.paymentCurrency,
    standingOrder: notification.standingOrder,
    terminationDate: notification.terminationDate,
    typeOfNotification: notification.typeOfNotification
    };
  }

  attachFromEdit(notificationEdit: NotificationPrescriptCreate) {
    return {
    contractId: notificationEdit.contractId,
    dateOfStart: notificationEdit.dateOfStart,
    description: notificationEdit.description,
    enabled: notificationEdit.enabled,
    frequency: notificationEdit.frequency,
    paymentAmount: notificationEdit.paymentAmount,
    paymentCurrency: notificationEdit.paymentCurrency,
    standingOrder: notificationEdit.standingOrder,
    terminationDate: notificationEdit.terminationDate,
    typeOfNotification: notificationEdit.typeOfNotification
  };
  }

  /** Workaround pro nefunkcni datepickery. Docasne. */
  dateChanged($event) {
    setTimeout(() => {
      this.form.get($event.target.name).setValue($event.target.value);
      this.validateDateInPast();
    }, 400);
  }

  public notificationFrequencyList() {
    return Object.keys(NotificationFrequency);
  }
}
