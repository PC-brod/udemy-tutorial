import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ChangeDetectorRef, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { BinderService } from '../shared/binder.service';
import { notBlankValidator, dateInPast } from '../../validation/validation.validators';
import { ResNotificationPrescript } from './models/res-notification-prescript.model';
import { NotificationPrescriptCreate } from './models/notification-prescript-create.model';
import { AuthService } from '../../auth/shared/auth.service';
import { NotificationFrequency } from '../../shared/models/enums';
import { LoggerService } from '../../shared/logger.service';
import { SharedService } from '../../shared/shared.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-binder-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NotificationComponent implements OnInit {

  i18NPrefix = 'binder.notification.';

  constructor(
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private binderService: BinderService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
	private log: LoggerService,
    private sharedService: SharedService,
    private datePipe: DatePipe
  ) { }

  @Input() notification: ResNotificationPrescript;
  @Output() notificationDeleted = new EventEmitter<ResNotificationPrescript>();
  @Output() notificationEdited = new EventEmitter<ResNotificationPrescript>();
  notificationEdit: NotificationPrescriptCreate = {};
  edited = false;
  alreadySubmitted = false;
  submitting = false;
  submitError = false;
  submitErrorString = 'submitError';
  form: FormGroup;
  dateInPastError = false;

  ngOnInit(): void {
    this.attachToEdit(this.notification, this.notificationEdit);
    this.buildForm();
    this.cd.markForCheck();
  }

  private buildForm(): void {
     this.form = this.fb.group({
       contractId: [this.notificationEdit.contractId],
       dateOfStart: [this.notificationEdit.dateOfStart, notBlankValidator],
       description: [this.notificationEdit.description, notBlankValidator],
       frequency: [this.notificationEdit.frequency, notBlankValidator],
       paymentAmount: [this.notificationEdit.paymentAmount],
       paymentCurrency: [this.notificationEdit.paymentCurrency],
       standingOrder: [this.notificationEdit.standingOrder],
       terminationDate: [this.notificationEdit.terminationDate],
       typeOfNotification: [this.notificationEdit.typeOfNotification],
       enabled: [this.notificationEdit.enabled],
       contractName: null,
       contractPartnerName: null,
       submitting: false
     }, { validator: dateInPast('dateOfStart', this.datePipe, this.i18NPrefix + 'validation.dateInPast') });
   }

  showEdit() {
     
    this.edited = !this.edited;
    this.form.reset();
    this.buildForm();
  }

  validateDateInPast() {
    if (this.form.errors != null && this.form.errors.dateInPast != null) {
      this.dateInPastError = true;
      this.cd.markForCheck();

    }
    else {
      this.dateInPastError = false;
      this.cd.markForCheck();
    }
  }

  onSubmit(form: FormGroup): void {

    this.alreadySubmitted = true;
    if (!this.form.valid || this.form.value.submitting) {
      if (this.form.errors != null && this.form.errors.dateInPast != null) {
        this.dateInPastError = true;
        this.cd.markForCheck();
        setTimeout(() => { this.dateInPastError = false; this.cd.markForCheck(); }, 3000);
      }
      return;
    }
    this.submitting = true;
    this.submitError = false;
    this.submitErrorString = 'saveError';

    const value: NotificationPrescriptCreate = this.form.value;

    this.binderService.updateNotificationPrescript$(this.notification.prescriptId, value).subscribe((not) => {
      this.submitting = false;
      let prescriptId = this.notification.prescriptId;
      this.notification = not;
      this.notification.prescriptId = prescriptId;

      this.attachToEdit(this.notification, value);
      this.notificationEdited.emit(this.notification);
      this.edited = false;
      this.alreadySubmitted = false;
      this.cd.markForCheck();
    }, (e) => {
      this.log.error(e);
      this.submitError = true;
      this.submitting = false;
      this.alreadySubmitted = false;
      this.cd.markForCheck();
    });
  }

  deleteNotification() {
    this.alreadySubmitted = true;
    this.submitting = true;
    this.submitError = false;
    this.submitErrorString = 'saveError';
    this.binderService.deleteNotification$(this.notification.prescriptId).subscribe(() => {
      this.submitting = false;
      this.notification.enabled = false;
      this.alreadySubmitted = false;
      this.notificationDeleted.emit(this.notification);
      this.cd.markForCheck();
    }, (e) => {
		this.sharedService.toggleDangerAlert("binder.notification.error.delete", null);
      this.log.error(e);
      this.submitError = true;
      this.submitting = false;
      this.alreadySubmitted = false;
      this.cd.markForCheck();
    });
  }

  attachToEdit(notification: ResNotificationPrescript, notificationEdit: NotificationPrescriptCreate) {
    notificationEdit.contractId = notification.contractId;
    notificationEdit.dateOfStart = notification.dateOfStart;
    notificationEdit.description = notification.description;
    notificationEdit.enabled = notification.enabled;
    notificationEdit.frequency = notification.frequency;
    notificationEdit.paymentAmount = notification.paymentAmount;
    notificationEdit.paymentCurrency = notification.paymentCurrency;
    notificationEdit.standingOrder = notification.standingOrder;
    notificationEdit.terminationDate = notification.terminationDate;
    notificationEdit.typeOfNotification = notification.typeOfNotification;

  }

  addPayAlert() { }

  public solvedChanged(): void {
    // this.notification.Enabled = this.form.get('enabled').value;
    // this.onSubmit(this.form);
  }
  
  /** Workaround pro nefunkcni datepickery. Docasne. */
  dateChanged($event) {
    this.authService.sessionTimerStart();
    setTimeout(() => {
      this.form.get($event.target.name).setValue($event.target.value);
      this.validateDateInPast();
    }, 400);
  }

  public notificationFrequencyList() {
    return Object.keys(NotificationFrequency);
  }
}
