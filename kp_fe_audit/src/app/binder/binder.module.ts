import { HttpClient, HttpClientModule } from '@angular/common/http';
import { CookieXSRFStrategy, XSRFStrategy, HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { BinderRoutingModule } from './binder-routing.module';
import { BinderComponent } from './binder.component';
import { ContractModule } from './contract/contract.module';
import { NotificationModule } from './notification/notification.module';
import { AuthGuard } from '../auth/shared/auth-guard.service';
import { BinderIntegratedService } from './shared/binder-integrated.service';
import { BinderMockedService } from './shared/binder-mocked.service';
import { BinderService } from './shared/binder.service';
import { EditContractComponent } from './contract/edit-contract/edit-contract.component';
import { ValidationModule } from '../validation/validation.module';
// import { FilterPipe } from '../utils/filter.pipe';
import { DropzoneModule } from 'ngx-dropzone-wrapper';
import { DROPZONE_CONFIG } from 'ngx-dropzone-wrapper';
import { DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
import { TooltipModule } from 'ngx-tooltip';

// using https://github.com/zefoy/ngx-dropzone-wrapper
const DEFAULT_DROPZONE_CONFIG: DropzoneConfigInterface = {
  // Change this to your upload POST address:
   url: 'https://httpbin.org/post',
   maxFilesize: 5000,
   acceptedFiles: 'application/pdf'
 };
import { FilterModule } from '../utils/filter.module';
import { ContractHistoryComponent } from './contract/edit-contract/contract-history/contract-history.component';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { SearchFilterPipe } from '../utils/contractFilter.pipe';

@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    CommonModule,
    ContractModule,
    NotificationModule,
    BinderRoutingModule,
    TranslateModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    ValidationModule,
    DropzoneModule,
    FilterModule,
    TooltipModule,
    ScrollToModule.forRoot()
  ],
  declarations: [BinderComponent, EditContractComponent, ContractHistoryComponent, SearchFilterPipe],
  providers: [
      AuthGuard,
      {
        provide: BinderService,
          // useClass: BinderMockedService
        useClass: BinderIntegratedService
      },
      {
        provide: DROPZONE_CONFIG,
        useValue: DEFAULT_DROPZONE_CONFIG
      }
  ]
})
export class BinderModule { }
