import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation, ViewChild, ElementRef, OnDestroy, AfterViewInit, AfterViewChecked } from '@angular/core';
import { ChangeDetectorRef, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { URLSearchParams } from '@angular/http';
import { BinderService } from './shared/binder.service';
import { ResContract } from './contract/models/res-contract.model';
import { ResNotificationPrescript } from './notification/models/res-notification-prescript.model';
import { AuthService } from '../auth/shared/auth.service';
import { LoggerService } from '../shared/logger.service';
import { NewContractComponent } from './contract/new-contract/new-contract.component';
import { LoaderIndicatorService } from '../shared/LoaderIndicator.service';
import { ShowDetailEventData } from './contract/models/showDetail.model';
import { ContractSavedEventData } from './contract/models/contractSavedEventData.model';
import { SharedService } from '../shared/shared.service';
import { ScrollToService, ScrollToConfigOptions } from '@nicky-lenaers/ngx-scroll-to';
import { FilterContract } from '../utils/filterContract.model';
declare function setActiveNav(id: string): Function;

@Component({
  selector: 'app-binder',
  templateUrl: './binder.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BinderComponent implements OnInit, AfterViewInit {

  constructor(
    private binderService: BinderService,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private log: LoggerService,
    private loaderService: LoaderIndicatorService,
    private sharedService: SharedService,
    private scrollService: ScrollToService
  ) { }

  @ViewChild('newContract') newContractComponent: NewContractComponent;
  @ViewChild('filterFulltextInput') filterFulltextInput: ElementRef;
  @ViewChild('mobileSelect') mobileSelect: ElementRef;
  @ViewChild('fulltextFilterMobile') fulltextFilterMobile: ElementRef;

  i18NPrefix = 'binder.';

  loadError = false;
  filterText = '';
  showedContracts: ResContract[] = [];
  contracts: ResContract[] = [];

  notifications: ResNotificationPrescript[] = [];
  notContractNotifications: ResNotificationPrescript[] = [];

  showDetail = false;
  detailId = '';

  addingNewNotification = false;
  newNotification: ResNotificationPrescript = {};
  notificationsInited = false;

  filter = 0;

  ngOnInit() {
    setActiveNav('binder');

    this.getData();
  }


  ngAfterViewInit(): void {
    let href = window.location.href;
    const urlParams: URLSearchParams = new URLSearchParams(href.substring(href.indexOf("?") + 1));
    const elementArr = urlParams.paramsMap.get('element');
    let element = elementArr ? elementArr[0] : undefined;
    if (element && element === "setNotifications") {
      this.loaderService.showLoader();
      setTimeout(() => { this.loaderService.disableLoader(); this.scrollToSetNotifications(); }, 1500);
    }
    this.loaderService.disableLoader();
  }

  scrollToSetNotifications() {
        let scrollConfig: ScrollToConfigOptions = {
          target: 'setNotifications'
        };
        this.scrollService.scrollTo(scrollConfig);
  }

  mobileFilter($event) {
    switch ($event.target.selectedIndex) {
      case 0:
        this.filterAll();
        break;
      case 1:
        this.filterActive();
        break;
      case 2:
        this.filterEnded();
        break;
      case 3:
        this.filterBrokerConsulting(true);
        break;
      case 4:
        this.filterBrokerConsulting(false);
        break;
    }
  }

  onShowDetailChange(data: ShowDetailEventData) {
    if (!data) {
      return;
    }
    this.showDetail = data.showDetail;
    if (!this.showDetail) {
      this.getData(data.callback, data.callbackComponent);
      //this.showedContracts = [];
      this.sharedService.scrollToTop();
      this.cd.markForCheck();

    }
  }

  onContractCreation(data: ContractSavedEventData) {
    if (!data) {
      return;
    }
    if (data.added) {
      this.getData(data.callback, data.callbackComponent);
      this.cd.markForCheck();
    }
  }

  saveNotification(not) {
    this.notifications.push(not);
    this.notContractNotifications.push(not);
    this.getData(this.toggleSaveNotificationSuccess, this);
    this.cd.markForCheck();
  }

  public toggleSaveNotificationSuccess() {
    this.sharedService.toggleInfoAlert("binder.notification.success.save", null);
  }

  notificationAdding(adding) {
    this.addingNewNotification = adding;
  }

  onDetailIdSelect(selectedContractId) {
    this.detailId = selectedContractId;
  }

  filterActive() {
    this.authService.sessionTimerStart();
    this.showDetail = false;
    Object.assign(this.showedContracts, this.contracts);
    let index = 0;
    for (let i = 0; i < this.contracts.length; i++) {
      if (this.contracts[i].state !== 'ACTIVE') {
        this.showedContracts.splice(index, 1);
      } else {
        index++;
      }
    }
    this.filter = 1;
    this.cd.markForCheck();
  }

  filterAll() {
    this.authService.sessionTimerStart();
    this.showDetail = false;
    Object.assign(this.showedContracts, this.contracts);
    this.filter = 0;
    this.cd.markForCheck();
  }

  filterEnded() {
    this.authService.sessionTimerStart();
    this.showDetail = false;
    Object.assign(this.showedContracts, this.contracts);
    let index = 0;
    for (let i = 0; i < this.contracts.length; i++) {
      if (this.contracts[i].state !== 'TERMINATED') {
        this.showedContracts.splice(index, 1);
      } else {
        index++;
      }
    }
    this.filter = 2;
    this.cd.markForCheck();
  }

  filterBrokerConsulting(isClosedBC: boolean) {
    this.authService.sessionTimerStart();
    this.showDetail = false;
    Object.assign(this.showedContracts, this.contracts);
    let index = 0;
    for (let i = 0; i < this.contracts.length; i++) {
      if (this.contracts[i].closedBC === isClosedBC) {
        index++;
      } else {
        this.showedContracts.splice(index, 1);
      }
    }
    if (isClosedBC) {
      this.filter = 3;
    } else {
      this.filter = 4;
    }
    this.cd.markForCheck();
  }

  returnNotifications(contractId: string): any {
    this.authService.sessionTimerStart();
    return []; // this.notifications.filter((n) => n.ContractId === contractId);
  }

  filterContracts() {


    switch (this.filter) {
      case 1: this.filterActive();
        break;
      case 2: this.filterEnded();
        break;
      case 3: this.filterBrokerConsulting(true);
        break;
      case 4: this.filterBrokerConsulting(false);
        break;
      default: this.filterAll();
        break;
    }
  }

	getContracts(callback?: () => void, callbackComponent?) {
		this.binderService.getContracts$().subscribe((contracts) => {
			this.contracts = contracts;
			Object.assign(this.showedContracts, this.contracts);
			this.log.debug(this.showedContracts);
			this.log.debug(this.contracts);
			this.loadError = false;
			this.filterContracts();
			this.cd.markForCheck();
			this.loaderService.disableLoader();
			if (callback && callbackComponent) {
				callback.apply(callbackComponent);
			}
		}, (e) => {
			this.log.error(e);
			this.loadError = true;
			this.cd.markForCheck();
		});
	}

  getContractNotifications(contractId: string) {
    const contractNotifications: ResNotificationPrescript[] = this.notifications.filter(x => x.contractId === contractId && x.enabled);
    return contractNotifications;
  }

  getData(callback?: () => void, callbackComponent?): void {
    this.loaderService.showLoader();
    this.binderService.getNotificationsList$().subscribe((notifications) => {
      this.notifications = [];
      this.notContractNotifications = [];
      if (!this.notifications || !(this.notifications instanceof Array)) {
        this.loadError = false;
        this.cd.markForCheck();
        return;
      }
      this.notifications = notifications;
      notifications.filter((n) => (n.contractId === undefined || n.contractId === null || n.contractId === ''))
        .map(x => this.notContractNotifications.push(x));

      this.loadError = false;
      this.notificationsInited = true;

      this.getContracts(callback, callbackComponent);

      this.cd.markForCheck();
    }, (e) => {
      this.log.error(e);
      this.loadError = true;
      this.cd.markForCheck();
    });
  }

  addNotification() {
    this.addingNewNotification = true;
  }

  public notificationDeleted(notif: ResNotificationPrescript): void {
    this.authService.sessionTimerStart();
    if (!notif) {
      this.loaderService.disableLoader();
      return;
    }

    let idx = this.notifications.indexOf(notif);
    if (idx >= 0) {
      this.notifications.splice(idx, 1);
    }
    idx = this.notContractNotifications.indexOf(notif);
    if (idx >= 0) {
      this.notContractNotifications.splice(idx, 1);
    }

    this.cd.markForCheck();
    this.loaderService.disableLoader();
    this.sharedService.toggleInfoAlert("binder.notification.success.delete", null);
  }

  public notificationEdited(notif: ResNotificationPrescript): void {
    this.authService.sessionTimerStart();
    if (!notif) {
      this.loaderService.disableLoader();
      return;
    }

    let filteredNotif: ResNotificationPrescript = this.notifications.filter(n => n.prescriptId === notif.prescriptId)[0];
    if (filteredNotif) {
      const idx = this.notifications.indexOf(filteredNotif);
      this.notifications[idx] = notif;
    }
    filteredNotif = null;
    filteredNotif = this.notContractNotifications.filter(n => n.prescriptId === notif.prescriptId)[0];
    if (filteredNotif) {
      const idx = this.notContractNotifications.indexOf(filteredNotif);
      this.notContractNotifications[idx] = notif;
    }

    this.cd.markForCheck();
    this.loaderService.disableLoader();
    this.sharedService.toggleInfoAlert("binder.notification.success.save", null);
  }

  public initNewContractForm(): void {
    this.newContractComponent.clearForm();
  }

  public onDataChanged() {
    this.getData();
  }
}
