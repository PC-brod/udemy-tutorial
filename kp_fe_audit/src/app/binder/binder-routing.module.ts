import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BinderComponent } from './binder.component';
import { EditContractComponent } from './contract/edit-contract/edit-contract.component';
import { AuthGuard } from '../auth/shared/auth-guard.service';

@NgModule({
  imports: [
    RouterModule.forChild(
      [
        // {
        //     path: '',
        //     redirectTo: 'binder',
        //     pathMatch: 'full'
        // },
        {
          path: 'binder',
          component: BinderComponent,
          canActivate: [AuthGuard],
          children: [
            {
              path: 'edit/:contractId',
              component: EditContractComponent,
              canActivate: [AuthGuard]
            }
          ]
        },
      ]// ,
      // {
      //   enableTracing: false
      // }
    )
  ],
  exports: [
    RouterModule
  ]
})
export class BinderRoutingModule { }
