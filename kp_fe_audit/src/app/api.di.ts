import { InjectionToken } from '@angular/core';

import { ApiConfig } from './api.model';

export const apiConfigToken = new InjectionToken<ApiConfig>('api.config');
