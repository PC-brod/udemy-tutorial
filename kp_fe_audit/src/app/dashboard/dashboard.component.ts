import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation, OnChanges, SimpleChanges } from '@angular/core';
import { ChangeDetectorRef, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ResNotificationInstance } from './shared/models/notification-instance.model';
import { DashboardService } from './shared/dashboard.service';
import { LoggerService } from '../shared/logger.service';
import { LoaderIndicatorService } from '../shared/LoaderIndicator.service';
declare function setActiveNav(id: string): Function;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  encapsulation: ViewEncapsulation.None
})
export class DashboardComponent implements OnInit {

  i18NPrefix = 'dashboard.';

  constructor(
    private dashboardService: DashboardService,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private log: LoggerService,
    private loaderService: LoaderIndicatorService
  ) { }

  loadError = false;

  numberToShow = 5;

  allPayments = false;
  allNotifications = false;
  payments: ResNotificationInstance[] = [];
  fullPayments: ResNotificationInstance[] = [];
  notifications: ResNotificationInstance[];
  fullNotifications: ResNotificationInstance[];
  messages: any[] = []; // TBD
  messageDetail = false;

  ngOnInit() {
    setActiveNav('dashboard');
    this.loaderService.showLoader();
    this.getNotifications();
  }

  getNotifications() {
    this.allPayments = true;
    this.allNotifications = true;
    this.dashboardService.getNotificationInstances$().subscribe((notifications) => {
      this.payments = [];
      this.fullPayments = [];
      this.notifications = [];
      this.fullNotifications = [];

      if (!this.notifications || !(this.notifications instanceof Array)) {
        this.loadError = false;
        this.cd.markForCheck();
        this.loaderService.disableLoader();
        return;
      }

      this.fullNotifications = notifications.filter((n) => (!n.paid && n.typeofNotification === 'Other')).sort((a, b) => {
        if (this.dateFormatSwitch(a.dateofEfficiency) < this.dateFormatSwitch(b.dateofEfficiency)) {
          return -1;
        } else if (this.dateFormatSwitch(a.dateofEfficiency) > this.dateFormatSwitch(b.dateofEfficiency)) {
          return 1;
        } else {
          return 0;
        }
      });

      this.fullPayments = notifications.filter((n) => (!n.paid && n.typeofNotification === 'Payment')).sort((a, b) => {
        if (this.dateFormatSwitch(a.dateofEfficiency) < this.dateFormatSwitch(b.dateofEfficiency)) {
          return -1;
        } else if (this.dateFormatSwitch(a.dateofEfficiency) > this.dateFormatSwitch(b.dateofEfficiency)) {
          return 1;
        } else {
          return 0;
        }
      });

      if (this.fullNotifications === undefined) { this.fullNotifications = []; }
      if (this.fullPayments === undefined) { this.fullPayments = []; }

      this.filterStandingOrder();

      this.getFirstPayments();
      this.getFirstNotifications();
      this.loadError = false;
      this.cd.markForCheck();
      this.loaderService.disableLoader();
    }, (e) => {
      this.loaderService.disableLoader();
      this.log.error(e);
      this.loadError = true;
      this.cd.markForCheck();
    });
  }

  solvedPayment() {
    this.getNotifications();
    this.getFirstPayments();
    this.cd.markForCheck();
   
  }

  solvedNotification() {
    this.getNotifications();
    this.getFirstNotifications();
    this.cd.markForCheck();
  }

  // Trvale prikazy nesmime zobrazit pokud dateOfEfficiency je starsi nez aktualni den.
  // Takove prikazy by z backendu nemely vubec prichazet, 
  // takze toto je spis workaround pro pripad backendove chyby.
  private filterStandingOrder(): void {
    if (!this.fullPayments) {
      return;
    }
    this.fullPayments = this.fullPayments.filter(p => {
      if (!p.standingOrder) {
        return true;
      }

      let yesterdayDate = new Date();
      yesterdayDate.setHours(0);
      yesterdayDate.setDate(yesterdayDate.getDate() - 1);
      return this.dateFormatSwitch(p.dateofEfficiency) >= yesterdayDate;
    });
  }

  dateFormatSwitch(dateToConvert) {
    return new Date(dateToConvert.split('.').reverse().join('-'));
  }

  getFirstPayments() {
    let counter = 0;
    let index = 0;
    while (counter != this.numberToShow) {
      if (index === this.fullPayments.length) {
        break;
      }
      if (!this.fullPayments[index].paid) {
        this.payments.push(this.fullPayments[index]);
        counter++;
      }
      index++;
    }
    this.allPayments = this.fullPayments.length < 6;
  }

  getFirstNotifications() {
    let counter = 0;
    let index = 0;
    while (counter != this.numberToShow) {
      if (index === this.fullNotifications.length) {
        break;
      }
      if (!this.fullNotifications[index].paid) {
        this.notifications.push(this.fullNotifications[index]);
        counter++;
      }
      index++;
    }
   
    this.allNotifications = this.fullNotifications.length < 6;
  }

  getAllPayments() {
    //for (let i = this.numberToShow; i < this.fullPayments.length; i++) {
    //  this.payments.push(this.fullPayments[i]);
    //}
    this.payments = this.fullPayments;
    this.allPayments = true;
  }

  getAllNotifications() {
    //for (let i = this.numberToShow; i < this.fullNotifications.length; i++) {
    //  this.notifications.push(this.fullNotifications[i]);
    //}
    this.notifications = this.fullNotifications;
    this.allNotifications = true;
  }

  hasNotifications(): any {
    return this.fullNotifications !== undefined && this.fullNotifications.length > 0;
  }

  hasPayments(): any {
    return this.fullPayments !== undefined && this.fullPayments.length > 0;
  }

  messageDetailShow() {
    if (this.messageDetail) {
      this.messageDetail = false;
    } else {
      this.messageDetail = true;
    }
  }
}
