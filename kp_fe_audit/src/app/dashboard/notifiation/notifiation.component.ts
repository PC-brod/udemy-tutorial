import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import { ChangeDetectorRef, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ResNotificationInstance } from '../shared/models/notification-instance.model';
import { DashboardService } from '../shared/dashboard.service';
import { LoggerService } from '../../shared/logger.service';
import { LoaderIndicatorService } from '../../shared/LoaderIndicator.service';
import { SharedService } from '../../shared/shared.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-dashboard-notification',
  templateUrl: './notifiation.component.html',
  styleUrls: ['./notification.component.css'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NotifiationComponent implements OnInit {
  i18NPrefix = 'dashboard.notification.';

  constructor(
    private dashboardService: DashboardService,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private datePipe: DatePipe,
    private log: LoggerService,
    private loaderService: LoaderIndicatorService,
    private sharedService: SharedService
  ) { }

  loadError: boolean = false;
  submitting: boolean = false;

  @Input() notification: ResNotificationInstance;
  @Output() solvedNotification = new EventEmitter();

  ngOnInit() { }

  showNotification(): any {
    return !this.notification.paid;
  }

  afterDueDate(): any {
    // expected format of 'this.notification.dateofEfficiency' is dd.MM.yyyy
    // value of 'this.notification.dateofEfficiency' is converted to yyyy-MM-dd
    return new Date(this.notification.dateofEfficiency.split('.').reverse().join('-')) <
      new Date(this.datePipe.transform(new Date(), "yyyy-MM-dd"));
  }

  solved() {
    if (this.submitting) {
      return;
    }
    this.submitting = true;
    this.loaderService.showLoader();
    this.dashboardService.updateNotificationInstance$(this.notification.notificationId, { paid: true }).subscribe(() => {
      this.submitting = false;
      this.notification.paid = true;
      this.loadError = false;
      this.loaderService.disableLoader();
      this.sharedService.toggleInfoAlert("dashboard.submit.success.notification", null);
      this.cd.markForCheck();
      this.solvedNotification.emit(null);
    }, e => {
      this.submitting = false;
      this.log.error(e);
      this.loadError = true;
      this.loaderService.disableLoader();
      this.sharedService.toggleDangerAlert("dashboard.submit.error.notification", null);
      this.cd.markForCheck();
    });
  }
}
