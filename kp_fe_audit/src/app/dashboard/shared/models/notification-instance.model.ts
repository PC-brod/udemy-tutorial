export interface ResNotificationInstance {
  notPrescriptId?: string; // proc neni pritomne?

  typeofNotification?: string;
  paid?: boolean;
  description?: string;
  notificationId?: string;
  paymentAmount?: string;
  paymentCurrency?: string;
  dateofEfficiency?: string;
  contractName?: string;
  contractPartnerName?: string;
  frequency?: string;
  standingOrder?: boolean;
}
