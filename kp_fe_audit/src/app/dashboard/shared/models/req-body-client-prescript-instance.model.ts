import { ResNotificationInstance } from './notification-instance.model';
import { NotLoged } from '../../../shared/models/not-loged.model';

export interface ReqBodyClientPrescriptInstance extends BodyClientPrescriptInstance, NotLoged {}

export interface BodyClientPrescriptInstance {
    notPrescriptId?: string;
    clientId?: string;
    notInstance?: Array<ResNotificationInstance>;
}
