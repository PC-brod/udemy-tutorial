import { NotLoged } from '../../../shared/models/not-loged.model';

export interface ReqBodyPaid extends BodyPaid, NotLoged {}

export interface BodyPaid {
    paid?: boolean;
}
