import { Injectable, Inject } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { HttpClient } from '@angular/common/http';

import { assign, interpolate } from '../../../app/utils/common.utils';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { DashboardService } from './dashboard.service';
import { AuthService } from '../../auth/shared/auth.service';
import { ResNotificationInstance } from './models/notification-instance.model';
import { ReqBodyPaid } from './models/req-body-paid.model';
import { ApiConfig } from '../../api.model';
import { apiConfigToken } from '../../api.di';

import { EnvironmentConfig } from '../../environment.config';
import { LoggerService } from '../../shared/logger.service';

@Injectable()
export class DashboardIntegratedService extends DashboardService {

  constructor(
    @Inject(apiConfigToken) private config: ApiConfig,
    private http: HttpClient,
    private authService: AuthService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private log: LoggerService
  ) {
    super();
  }

  getNotificationInstances$(): Observable<Array<ResNotificationInstance>> {
    this.log.debug(interpolate(EnvironmentConfig.settings.env.uriPrefix + this.config.getNotificationInstancesUrl,
      { clientId: this.authService.clientId }));
    return this.authService.get$(interpolate(EnvironmentConfig.settings.env.uriPrefix + this.config.getNotificationInstancesUrl,
      { clientId: this.authService.clientId }))
    .map((res) => {
      if (!res) {
        this.log.debug('Tento klient nemá žádné instance notifikací.');
        return new Array<ResNotificationInstance>();
      }
      return res;
    });
  }

  updateNotificationInstance$(notificationId: string, body: ReqBodyPaid): Observable<ResNotificationInstance> {
    body = assign(body);
    return this.authService.put$(interpolate(EnvironmentConfig.settings.env.uriPrefix + this.config.updateNotificationInstanceUrl,
      							{ notificationId: notificationId, clientId: this.authService.clientId }), body)
	.map(() => {
		return null;
	});
  }
}
