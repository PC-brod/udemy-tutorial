import { Injectable, EventEmitter, isDevMode, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { HttpClient } from '@angular/common/http';

import { assign, interpolate } from '../../../app/utils/common.utils';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ChangeDetectorRef, Input, Output } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { GDPRConsents, codelistpartners, codelistproductgroups,
  attachments, contractsDetails, contracts, notificationPrescripts, notificationInstances } from '../../../mocks/mock';
import { DashboardService } from './dashboard.service';
import { ResNotificationInstance } from './models/notification-instance.model';
import { ReqBodyPaid } from './models/req-body-paid.model';
import { ReqBodyClientPrescriptInstance } from './models/req-body-client-prescript-instance.model';
import { ApiConfig } from '../../api.model';
import { apiConfigToken } from '../../api.di';

const delay = 500;

@Injectable()
export class DashboardMockedService extends DashboardService {

  constructor(
    @Inject(apiConfigToken) private config: ApiConfig,
    private http: HttpClient,
    private activatedRoute: ActivatedRoute,
    // private cd: ChangeDetectorRef,
    private router: Router
  ) {
    super();
  }

  updateNotificationInstance$(notificationId: string, body: ReqBodyPaid): Observable<ResNotificationInstance> {
    //return null; // TBD
     return Observable.timer(delay).map(() => {
       const idx = notificationInstances.findIndex((c) => c.notificationId === notificationId);
       if (idx === -1) {
         throw new Error(`Item with id ${notificationId} not exists.`);
       }
       const notification: ResNotificationInstance = null;
       assign(notification, notificationInstances[idx]);
       // TBD
       return notification;
     });
  }

  getNotificationInstances$(): Observable<Array<ResNotificationInstance>> {
    //return null; // TBD
     return Observable.timer(delay).map(() => notificationInstances);
  }
}
