import { Injectable, EventEmitter, isDevMode, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { HttpClient } from '@angular/common/http';

import { assign, interpolate } from '../../../app/utils/common.utils';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ChangeDetectorRef, Input, Output } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ResNotificationInstance } from './models/notification-instance.model';
import { ReqBodyPaid } from './models/req-body-paid.model';
import { ReqBodyClientPrescriptInstance } from './models/req-body-client-prescript-instance.model';

@Injectable()
export abstract class DashboardService {

  abstract getNotificationInstances$(): Observable<Array<ResNotificationInstance>>;

  abstract updateNotificationInstance$(notificationId: string, body: ReqBodyPaid): Observable<ResNotificationInstance>;
}
