import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { CookieXSRFStrategy, XSRFStrategy, HttpModule } from '@angular/http';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { NotifiationComponent } from './notifiation/notifiation.component';
import { PaymentComponent } from './payment/payment.component';
import { AuthGuard } from '../auth/shared/auth-guard.service';
import { DashboardService } from './shared/dashboard.service';
import { DashboardIntegratedService } from './shared/dashboard-integrated.service';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    HttpClientModule,
    DashboardRoutingModule,
    TranslateModule
  ],
  declarations: [DashboardComponent, NotifiationComponent, PaymentComponent],
  providers: [
      AuthGuard,
      {
        provide: DashboardService,
          // useClass: DashboardMockedService
        useClass: DashboardIntegratedService
      },
  ]
})
export class DashboardModule { }
