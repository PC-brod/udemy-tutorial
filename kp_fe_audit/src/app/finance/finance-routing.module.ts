import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FinanceComponent } from './finance.component';
import { AuthGuard } from '../auth/shared/auth-guard.service';

@NgModule({
  imports: [
    RouterModule.forChild(
      [
        // {
        //     path: '',
        //     redirectTo: 'finance',
        //     pathMatch: 'full'
        // },
        {
          path: 'finance',
          component: FinanceComponent,
          canActivate: [AuthGuard]
        },
      ]// ,
      // {
      //   enableTracing: false
      // }
    )
  ],
  exports: [
    RouterModule
  ]
})
export class FinanceRoutingModule { }
