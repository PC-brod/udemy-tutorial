  import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';
import { ChangeDetectorRef, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, FormsModule } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AuthService } from '../auth/shared/auth.service';
import { forEach } from '@angular/router/src/utils/collection';
import { Observable } from '../../../node_modules/rxjs/Observable';
import { createRendererV1 } from '../../../node_modules/@angular/core/src/view/refs';
import { FinanceService } from './shared/finance.service';
import { ResFinance } from './shared/models/finance.model';
import { FinanceRecord } from './shared/models/finance-record.model';
import { RegularExpense } from './shared/models/regular-expense.model';
import { FinancialSituationBalance } from './shared/models/financial-situation-balance.model';
import { ReqBodyFinancialSituation } from './shared/models/req-body-financial-situation.model';
import { Attachment } from './shared/models/attachment.model';
import { RegularIncome } from './shared/models/regular-income.model';
import { b64toBlob, createAndDownloadBlobFile } from './../utils/common.utils';
import { FinancePlan } from './shared/models/finance-plan.model';
import { LoggerService } from '../shared/logger.service';
import { LoaderIndicatorService } from '../shared/LoaderIndicator.service';
import { DecimalPipe } from '@angular/common';
import { formatDecimalStringToNumber } from '../utils/common.utils';
import { positiveNumberValidatorValue } from '../validation/validation.validators';
import { SharedService } from '../shared/shared.service';
declare function setActiveNav(id: string): Function;

@Component({
  selector: 'app-finance',
  templateUrl: './finance.component.html',
  styleUrls: ['./finance.component.css'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FinanceComponent implements OnInit {

  i18NPrefix = 'finance.';

  @ViewChild('incomeInput') incomeInput: ElementRef;

  constructor(
    private financeService: FinanceService,
    private authService: AuthService,
    private sharedService: SharedService,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private log: LoggerService,
    private loaderService: LoaderIndicatorService,
    private decimalPipe: DecimalPipe
  ) { }
  finances: ResFinance = {};

  newFinanceRecord: FinanceRecord = {};
  categories: any[] = [
    { Name: 'jiné' },
    { Name: 'potraviny' },
    { Name: 'sport' },
    { Name: 'zábava' },
    { Name: 'pojištění' },
    { Name: 'úvěry' },
  ];

  financePlanRequestSent: boolean;
  financePlanRequestSuccess: boolean;

  newCostError: boolean = false;
  incomeError: boolean = false;
  addNew = false;
  edit = false;
  income = 0;
  incomeId: number;
  loadError = false;
  expenses: RegularExpense[] = [];
  financialPlanVersions: any[];
  selectedPlanVersion: any;

  ngOnInit() {
    setActiveNav('finance');
    this.loaderService.showLoader();
    this.getFinances();
  }

  getIncome() :string {
    return this.decimalPipe.transform(this.income);
  }

  existsFinancialSituations() {
    return this.finances.financialSituations
      && this.finances.financialSituations.financialSituation
      && (this.finances.financialSituations.financialSituation.dateCreated
        || this.finances.financialSituations.financialSituation.lastUpdated)
      ? true : false;

  }

  public getFinances() {
    this.financeService.getFinances$().subscribe((finances) => {
      this.finances = finances;
      this.log.debug("Retrieved finances from GET: " + JSON.stringify(finances));
      this.initFinancialPlanVersions();
      this.updateFinancesValues();
      this.loadError = false;
      this.cd.markForCheck();
      this.loaderService.disableLoader();
    }, (e) => {
      this.log.error(e);
      this.loadError = true;
      this.cd.markForCheck();
      this.loaderService.disableLoader();
    });
  }

  public getRegularSpendingSum(): string {
    return this.decimalPipe.transform(this.regularSpendingSum());
  }

  public financeSum(): string {
    if (this.finances && this.finances.financialSituations && this.finances.financialSituations.financialSituation) {
      return this.decimalPipe.transform(+this.finances.financialSituations.financialSituation.financialBalance);
    }
    return '0';
  }

  public financeSumNumber() {
    if (this.finances && this.finances.financialSituations && this.finances.financialSituations.financialSituation) {
      return +this.finances.financialSituations.financialSituation.financialBalance;
    }
    return 0;
  }

  public regularSpendingSum(): number {
    let finSum = 0;
    this.expenses.forEach(x => finSum -= x.regExpAmount);
    return finSum;
  }

  public addNewCost_v2() {
    this.addNew = true;
  }

  public saveNewCost(describe: string, cost: string, category: string) {
    this.newCostError = false;
    if (this.addNew && describe !== '' && positiveNumberValidatorValue.apply(this, [cost, true]) === null) {
      this.loaderService.showLoader();

      let costValue: number = formatDecimalStringToNumber(cost);
      this.addNew = false;
      const balance: FinancialSituationBalance = {
        financialBalance: this.calcNewBalance(costValue, 0),
        clientId: this.authService.clientId
      };
      const body: ReqBodyFinancialSituation = {
        financialSituation: balance,
        regularExpense: this.createExpense(costValue, describe, category)
      };

      this.sendRegularExpenseRequest(body).subscribe(() => {
        this.getFinances();
        this.loadError = false;
      }, (e) => {
        this.log.error(e);
        this.loadError = true;
        this.cd.markForCheck();
        this.loaderService.disableLoader();
      });
    } else {
      this.newCostError = true;
      this.addNew = true;
    }
  }

  public deleteCost(expense: RegularExpense) {
    if (!expense) {
      return;
    }

    this.loaderService.showLoader();
    this.financeService.financesRegularExpensesDelete$(expense.regExpenseId).subscribe(() => {
      this.expenses.splice(this.expenses.indexOf(expense), 1);
      const balance: FinancialSituationBalance = { financialBalance: this.calcNewBalance(0, 0) };
      const body: ReqBodyFinancialSituation = {
        financialSituation: balance,
      };
      this.financeService
        .financesFinancialSituationIdPut$(this.finances.financialSituations.financialSituation.financialSituationId, body)
        .subscribe(() => {
          this.loadError = false;
          this.getFinances();
          this.cd.markForCheck();
        }, (e) => {
          this.log.error(e);
          this.loadError = true;
          this.cd.markForCheck();
          this.loaderService.disableLoader();
        });
    }, (e) => {
      this.log.error(e);
      this.loadError = true;
      this.cd.markForCheck();
      this.loaderService.disableLoader();
    });
  }

  public editIncome() {
    this.incomeError = false;
    let income = this.incomeInput ? this.incomeInput.nativeElement.value : "0";
    if (positiveNumberValidatorValue.apply(this, [income, true]) === null) {
      let incomeValue: number = formatDecimalStringToNumber(income);
      if (incomeValue < 0) {
        incomeValue = 0;
      }
      if (this.edit) {
        this.updateIncome(incomeValue);
      }
      this.edit = !this.edit;
    }
    else {
      this.incomeError = true;
    }
  }

  public updateIncome(income: number) {
    this.loaderService.showLoader();
    if (income < 0) {
      this.income = 0;
    } else {
      this.income = +income;
    }

    const balance: FinancialSituationBalance = {
      financialBalance: this.calcNewBalance(0, 0),
      clientId: this.authService.clientId
    };
    const body: ReqBodyFinancialSituation = {
      financialSituation: balance,
      regularIncome: this.createIncome(income, this.incomeId),
      regularExpense: null
    };

    this.sendRegularIncomeRequest(body).subscribe(() => {
      this.getFinances();
    }, (e) => {
      this.log.error(e);
      this.loadError = true;
      this.cd.markForCheck();
      this.loaderService.disableLoader();
    });
  }

  getTodayDate() {
    return new Date();
  }

  existsLastDateUpdated() {
    return this.finances.financialSituations.financialSituation.lastUpdated
      ? true : false;
  }

  public downloadAttachment(attachment: Attachment): void {
    if (!attachment) {
      return;
    }
    this.financeService.getFinancialPlanAttachment(attachment.fpAttachmentId).subscribe((resp) => {
      const blob = b64toBlob(resp.file, this.sharedService.getContentType(attachment.name.split('.')[1]));

      this.sharedService.openBlobInNewTab(blob);
      //createAndDownloadBlobFile(blob, attachment.name);
    }, (e) => {
      this.log.error(e);
      this.loadError = true;
      this.cd.markForCheck();
    });
  }

  updateFinancePlanModalOpened() {
    this.financePlanRequestSent = false;
    this.financePlanRequestSuccess = false;
  }

  public updateFinancePlan(): void {
    this.loaderService.showLoader();
    this.financeService.postRequestFinancialPlan().subscribe((resp) => {
      this.financePlanRequestSent = true;
      this.financePlanRequestSuccess = resp.planRequestResult;
      this.cd.markForCheck();
      this.loaderService.disableLoader();
    }, (e) => {
      this.financePlanRequestSent = true;
      this.log.error(e);
      this.loadError = true;
      this.cd.markForCheck();
      this.loaderService.disableLoader();
    });
  }

  public planEqualsSelected(plan: FinancePlan): boolean {
    if (!this.selectedPlanVersion || !plan) {
      return false;
    }
    const value = this.buildFinancialPlanValue(plan);
    return value === this.selectedPlanVersion.value;
  }

  public buildFinancialPlanValue(plan: FinancePlan): string {
    return plan.dateOfRelevance + ' - ' + plan.financialPlanId + ' - ' + plan.consultant;
  }

  public financialPlanSelected($event) {
    this.selectedPlanVersion = $event.target.value;
    this.cd.markForCheck();
  }

  private sendRegularExpenseRequest(body: ReqBodyFinancialSituation): Observable<any> {
    if (this.finances.financialSituations) {
      return this.financeService.financesRegularExpensesPost$(
        this.finances.financialSituations.financialSituation.financialSituationId, body);
    }
    // if financial situation does not exists, we need to create one by callin POST method with zero-income and expense object
    body.regularIncome = this.createIncome(0, null);
    return this.financeService.financesPost$(body);
  }

  private sendRegularIncomeRequest(body: ReqBodyFinancialSituation): Observable<any> {
    if (this.finances.financialSituations) {
      return this.financeService.financesFinancialSituationIdPut$(
        this.finances.financialSituations.financialSituation.financialSituationId, body);
    }
    // if financial situation does not exists, we need to create one by callin POST method with income object
    return this.financeService.financesPost$(body);
  }


  private updateFinancesValues(): void {
    this.setIncome(this.finances.regularIncomes);
    this.setExpenses(this.finances.regularExpenses);
  }

  private setIncome(incomes: any): void {
    this.log.debug("setting incomes" + JSON.stringify(incomes));
    if (!incomes) {
      this.incomeId = null;
      return;
    }
    if (incomes.regularIncome instanceof Array) {
      this.income = incomes.regularIncome[0].regIncAmount;
      this.incomeId = incomes.regularIncome[0].regIncomeId;
    } else {
      this.income = incomes.regularIncome.regIncAmount;
      this.incomeId = incomes.regularIncome.regIncomeId;
    }
  }

  private setExpenses(expenses: any): void {
    this.log.debug("setting expenses" + JSON.stringify(expenses));
    this.expenses = [];
    if (!expenses) {
      return;
    }

    if (expenses.regularExpense instanceof Array) {
      expenses.regularExpense.forEach(e => this.expenses.push(e));
    } else {
      this.expenses.push(expenses.regularExpense);
    }
    this.expenses.sort((o1, o2) => o1.regExpenseId - o2.regExpenseId);
  }

  private createIncome(amount: number, regIncomeId: number): RegularIncome {
    if (regIncomeId) {
      return {
        regIncomeId: regIncomeId,
        regIncAmount: amount,
        regIncCategory: 'Mzda',
        regIncDescription: 'plat',
        regIncFrequency: 'MONTHLY',
        validFrom: this.currentDateString(),
        validTo: '20990101'
      };
    }
    return {
      regIncAmount: amount,
      regIncCategory: 'Mzda',
      regIncDescription: 'plat',
      regIncFrequency: 'MONTHLY',
      validFrom: this.currentDateString(),
      validTo: '20990101'
    };
  }

  private createExpense(amount: number, desc: string, category: string): RegularExpense {
    return {
      regExpAmount: amount,
      regExpDescription: desc,
      regExpFrequency: 'MONTHLY',
      regExpCategory: category,
      validFrom: this.currentDateString(),
      validTo: '20990101'
    };
  }

  private currentDateString(): string {
    const date: Date = new Date();

    const month: number = date.getMonth() + 1;
    const monthStr: string = (month < 10) ? '0' + String(month) : String(month);
    const day: number = date.getDate();
    const dayStr: string = (day < 10) ? '0' + String(day) : String(day);

    return String(date.getFullYear()) + monthStr + dayStr;
  }

  private calcNewBalance(expense: number, removedExpense: number): number {
    let balance: number = this.income;
    if (this.expenses) {
      this.expenses.forEach(e => balance -= e.regExpAmount);
    }

    if (expense) {
      balance -= expense;
    }

    if (removedExpense) {
      balance += removedExpense;
    }
    return balance;
  }

  private initFinancialPlanVersions(): void {
    if (!this.finances.financialPlans || this.finances.financialPlans.length === 0) {
      return;
    }

    this.log.debug("financial plans init");

    this.financialPlanVersions = [];

    this.log.debug("Finanční plány \n " + JSON.stringify(this.finances.financialPlans));

    this.finances.financialPlans.forEach(plan => {
      this.financialPlanVersions.push({
        value: this.buildFinancialPlanValue(plan),
        dateOfRelevance: plan.dateOfRelevance
      });
    });


    this.financialPlanVersions = Array.from(new Set(this.financialPlanVersions));

    this.log.debug("retrieved financial plans: " + JSON.stringify(this.financialPlanVersions));
    // sort desc
    this.financialPlanVersions.sort((a, b) => {
      // expected date format is dd.MM.yyyy
      a = a.dateOfRelevance.split('.').reverse().join('');
      b = b.dateOfRelevance.split('.').reverse().join('');
      return b.localeCompare(a);
    });

    this.log.debug("financial plans sorted by dateOfRelevance " + JSON.stringify(this.financialPlanVersions));

    this.selectedPlanVersion = this.financialPlanVersions[0];
  }

}
