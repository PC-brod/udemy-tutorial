import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { CookieXSRFStrategy, XSRFStrategy, HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { FinanceRoutingModule } from './finance-routing.module';
import { FinanceComponent } from './finance.component';
import { AuthGuard } from '../auth/shared/auth-guard.service';
import { BlockCopyPasteDirective } from '../shared/block-copy-paste.directive';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { FinanceService } from './shared/finance.service';
import { FinanceIntegratedService } from './shared/finance-integrated.service';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    HttpClientModule,
    FinanceRoutingModule,
    TranslateModule,
    ScrollToModule.forRoot(),
    FormsModule
  ],
  providers: [
      AuthGuard,
      {
        provide: FinanceService,
          // useClass: FinanceMockedService
        useClass: FinanceIntegratedService
      },
  ],
  declarations: [FinanceComponent, BlockCopyPasteDirective]
})
export class FinanceModule {}
