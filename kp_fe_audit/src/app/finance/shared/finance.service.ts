import { Injectable, EventEmitter, isDevMode, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { HttpClient } from '@angular/common/http';

import { assign, interpolate } from '../../../app/utils/common.utils';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ChangeDetectorRef, Input, Output } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ResFinance } from './models/finance.model';
import { ReqBodyFinancialSituation } from './models/req-body-financial-situation.model';
import { ReqBodyExpenseIncomeId } from './models/req-body-expense-income-id.model';

@Injectable()
export abstract class FinanceService {

  abstract financesFinancialSituationIdDelete$(financialSituationId: string, body: ReqBodyExpenseIncomeId): Observable<any>;

  abstract financesFinancialSituationIdPut$(financialSituationId: number, body: ReqBodyFinancialSituation): Observable<any>;

  abstract financesPost$(body: ReqBodyFinancialSituation): Observable<any>;

  abstract financesRegularExpensesPost$(financialSituationId: number, body: ReqBodyFinancialSituation): Observable<any>;

  abstract financesRegularExpensesDelete$(regularExpenseId: number): Observable<any>;

  abstract getFinances$(): Observable<ResFinance>;

  abstract getFinancialPlanAttachment(attachmentId: string): Observable<any>;

  abstract postRequestFinancialPlan(): Observable<any>;
}
