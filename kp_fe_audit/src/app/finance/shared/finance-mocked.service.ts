﻿import { Injectable, EventEmitter, isDevMode, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { HttpClient } from '@angular/common/http';

import { assign, interpolate } from '../../../app/utils/common.utils';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ChangeDetectorRef, Input, Output } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { GDPRConsents, codelistpartners, codelistproductgroups, finances,
  attachments, contractsDetails, contracts, notificationPrescripts } from '../../../mocks/mock';
import { FinanceService } from './finance.service';
import { ResFinance } from './models/finance.model';
import { ReqBodyFinancialSituation } from './models/req-body-financial-situation.model';
import { ReqBodyExpenseIncomeId } from './models/req-body-expense-income-id.model';
import { ApiConfig } from '../../api.model';
import { apiConfigToken } from '../../api.di';

const delay = 500;

@Injectable()
export class FinanceMockedService extends FinanceService {

  constructor(
    @Inject(apiConfigToken) private config: ApiConfig,
    private http: HttpClient,
    private activatedRoute: ActivatedRoute,
    // private cd: ChangeDetectorRef,
    private router: Router
  ) {
    super();
  }

  getFinances$(): Observable<ResFinance> {
    return Observable.timer(delay).map(() => finances);
  }

  financesPost$(body: ReqBodyFinancialSituation): Observable<any> {
    return null; // TBD
  }

  financesFinancialSituationIdPut$(financialSituationId: number, body: ReqBodyFinancialSituation): Observable<any> {
    if (body.regularExpense) {
      finances.regularExpenses.regularExpense.push({
        regExpenseId: Math.random() * 1000000 + 100,
        regExpAmount: +body.regularExpense.regExpAmount,
        regExpFrequency: null,
        regExpDescription: body.regularExpense.regExpDescription,
        regExpCategory: body.regularExpense.regExpCategory,
        validFrom: null,
        validTo: null
      });
    }

    if (body.regularIncome) {
      finances.regularIncomes[0] = {
        regIncomeId: Math.random() * 1000000 + 100,
        regIncAmount: +body.regularIncome.regIncAmount,
        regIncFrequency: null,
        regIncomeDescription: body.regularIncome.regIncDescription,
        validFrom: null,
        validTo: null
      };
    }

    return Observable.timer(delay).map(() => null);
  }

  financesFinancialSituationIdDelete$(financialSituationId: string, body: ReqBodyExpenseIncomeId): Observable<any> {
    const expense = finances.regularExpenses.regularExpense.filter(exp => exp.regExpenseId === +body.regExpenseId)[0];
    if (expense) {
      if (finances.regularExpenses.regularExpense.length === 1) {
        finances.regularExpenses.regularExpense = [];
      } else {
        finances.regularExpenses.regularExpense.splice(finances.regularExpenses.regularExpense.indexOf(expense), 1);
      }
    }
    return Observable.timer(delay).map(() => null);
  }

  financesRegularExpensesPost$(financialSituationId: number, body: ReqBodyFinancialSituation): Observable<any> {
    if (body.regularExpense) {
      finances.regularExpenses.regularExpense.push({
        regExpenseId: Math.random() * 1000000 + 100,
        regExpAmount: +body.regularExpense.regExpAmount,
        regExpFrequency: null,
        regExpDescription: body.regularExpense.regExpDescription,
        regExpCategory: body.regularExpense.regExpCategory,
        validFrom: null,
        validTo: null
      });
    }
    return Observable.timer(delay).map(() => null);
  }

  financesRegularExpensesDelete$(regularExpenseId: number): Observable<any> {
    const expense = finances.regularExpenses.regularExpense.filter(exp => exp.regExpenseId === regularExpenseId)[0];
    if (expense) {
      if (finances.regularExpenses.regularExpense.length === 1) {
        finances.regularExpenses.regularExpense = [];
      } else {
        finances.regularExpenses.regularExpense.splice(finances.regularExpenses.regularExpense.indexOf(expense), 1);
      }
    }
    return Observable.timer(delay).map(() => null);
  }  

  getFinancialPlanAttachment(attachmentId: string): Observable<any> {
    return Observable.timer(delay).map(() => null);
  }

  postRequestFinancialPlan(): Observable<any> {
    return Observable.timer(delay).map(() => null);
  }
}
