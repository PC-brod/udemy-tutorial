﻿import { Injectable, EventEmitter, isDevMode, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { HttpClient } from '@angular/common/http';

import { assign, interpolate } from '../../../app/utils/common.utils';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ChangeDetectorRef, Input, Output } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { FinanceService } from './finance.service';
import { AuthService } from '../../auth/shared/auth.service';
import { ResFinance } from './models/finance.model';
import { ReqBodyFinancialSituation } from './models/req-body-financial-situation.model';
import { ReqBodyExpenseIncomeId } from './models/req-body-expense-income-id.model';
import { ApiConfig } from '../../api.model';
import { apiConfigToken } from '../../api.di';
import { EnvironmentConfig } from '../../environment.config';

@Injectable()
export class FinanceIntegratedService extends FinanceService {

  constructor(
    @Inject(apiConfigToken) private config: ApiConfig,
    private http: HttpClient,
    private authService: AuthService,
    private activatedRoute: ActivatedRoute,
    // private cd: ChangeDetectorRef,
    private router: Router
  ) {
    super();
  }

  getFinances$(): Observable<ResFinance> {
    // return this.financesService.getFinances(clientId).map((res: ResFinance) => {
    //   return res;
    // });
    return this.authService.get$(interpolate(EnvironmentConfig.settings.env.uriPrefix + this.config.getFinancesUrl,
      { clientId: this.authService.clientId }))
    .map((res: ResFinance) => {
      if (!res) {
        throw new Error(`Client ${this.authService.clientId} finances do not exists.`);
	  }
	  return res;
    });
  }

  financesPost$(body: ReqBodyFinancialSituation): Observable<any> {
    // return this.financesService.financesPost(clientId, body).map(() => null);
    return this.authService.post$(interpolate(EnvironmentConfig.settings.env.uriPrefix + this.config.financesPostUrl,
      { clientId: this.authService.clientId }), body)
    .mergeMap(({ name: clientId }: { name: string; }) => {
      return  this.getFinances$();
    });
  }

  financesRegularExpensesPost$(financialSituationId: number, body: ReqBodyFinancialSituation): Observable<any> {
    return this.authService.post$(interpolate(EnvironmentConfig.settings.env.uriPrefix + this.config.financesRegularExpensesPostUrl,
      { financialSituationId: financialSituationId, clientId: this.authService.clientId  }), body)
    .map(() => null);
  }

  financesRegularExpensesDelete$(regularExpenseId: number): Observable<any> {
    return this.authService.delete$(interpolate(EnvironmentConfig.settings.env.uriPrefix + this.config.financesRegularExpensesDeleteUrl,
      { regularExpenseId: regularExpenseId, clientId: this.authService.clientId }))
    .map(() => null);
  }

  financesFinancialSituationIdPut$(financialSituationId: number, body: ReqBodyFinancialSituation): Observable<any> {
    // return this.financesService.financesFinancialSituationIdPut(financialSituationId, body).map(() => null);
    body = assign(body);
    return this.authService.put$(interpolate(EnvironmentConfig.settings.env.uriPrefix + this.config.financesFinancialSituationIdPutUrl,
      { financialSituationId: financialSituationId, clientId: this.authService.clientId  }), body)
    .map(() => null);
  }

  financesFinancialSituationIdDelete$(financialSituationId: string, body: ReqBodyExpenseIncomeId): Observable<any> {
    // return this.financesService.financesFinancialSituationIdDelete(financialSituationId, body).map(() => null);
    return this.authService.delete$(interpolate(EnvironmentConfig.settings.env.uriPrefix + this.config.financesFinancialSituationIdDeleteUrl,
      { financialSituationId: financialSituationId, clientId: this.authService.clientId  })).map(() => null);
  }

  getFinancialPlanAttachment(attachmentId: string): Observable<any> {
    return this.authService.get$(interpolate(EnvironmentConfig.settings.env.uriPrefix + this.config.getFinancialPlanAttachmentUrl,
                                           { attachmentId: attachmentId, clientId: this.authService.clientId }))
      .map((res) => {
        if (!res) {
          throw new Error(`Attachment ${attachmentId} does not exist.`);
        }
        return res;
      });
  }

  postRequestFinancialPlan(): Observable<any> {
    return this.authService.post$(interpolate(EnvironmentConfig.settings.env.uriPrefix + this.config.postRequestFinancialPlanUrl,
      { clientId: this.authService.clientId }), null)
            .map((res) => {
              if (!res) {
                throw new Error(`Financial plan request error.`);
              }
              return res;
            });
  }
}
