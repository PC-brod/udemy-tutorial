export interface OneTimeIncome {
  regIncomeId?: string;
  financialSituationId?: string;
  dateOfOTIncome?: string;
  regIncomeAmount?: number;
  regIncDescription?: string;
}
