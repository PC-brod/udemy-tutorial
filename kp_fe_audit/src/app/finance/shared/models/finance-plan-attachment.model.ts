export interface FinancePlanAttachment {
    financialPlanId?: string;
    fpAttachmentId?: string;
    fpAttachmentName?: string;
    fpAttachmentFile?: string;
    urlAddress?: string;
}
