export interface RegularIncome {
    regIncomeId?: number;
    regIncCategory?: string;
    regIncAmount?: number;
    regIncFrequency?: string;
    regIncDescription?: string;
    validFrom?: string;
    validTo?: string;
}
