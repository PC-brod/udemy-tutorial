import { Attachment } from './attachment.model';

export interface FinancePlan {
    financialPlanId?: number;
    clientId?: string;
    dateOfRelevance?: string;
    consultant?: string;
    attachments?: Attachment[];
}
