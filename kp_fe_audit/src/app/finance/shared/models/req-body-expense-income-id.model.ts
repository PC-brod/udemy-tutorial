import { NotLoged } from '../../../shared/models/not-loged.model';

export interface ReqBodyExpenseIncomeId extends BodyExpenseIncomeId, NotLoged {
  regExpenseId?: string;
  regIncomeId?: string;
}

export interface BodyExpenseIncomeId {
  regExpenseId?: string;
  regIncomeId?: string;
}
