import { FinancePlan } from './finance-plan.model';
import { FinancePlanAttachment } from './finance-plan-attachment.model';
import { FinancialSituations } from './financial-situations.model';
import { OneTimeIncome } from './one-time-income.model';
import { OneTimeExpense } from './one-time-expense.model';

export interface ResFinance {
    financialPlans?: FinancePlan[];
    financialPlanAttachment?: FinancePlanAttachment;
    financialSituations?: FinancialSituations;
    regularExpenses?: { regularExpense?: any };
    regularIncomes?: { regularIncome?: any };
    oneTimeExpense?: OneTimeExpense;
    oneTimeIncome?: OneTimeIncome;
}
