export interface FinanceRecord {
    value?: number;
    describe?: string;
    id?: string;
    category?: string;
  }
