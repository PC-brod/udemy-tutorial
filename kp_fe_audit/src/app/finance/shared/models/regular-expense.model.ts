export interface RegularExpense {
    regExpenseId?: number;
    regExpAmount?: number;
    regExpDescription?: string;
    regExpFrequency?: string;
    regExpCategory?: string;
    validFrom?: string;
    validTo?: string;
}
