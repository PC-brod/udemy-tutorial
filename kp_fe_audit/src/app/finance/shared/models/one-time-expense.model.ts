export interface OneTimeExpense {
  oneTimeIncomeId?: string;
  financialSituationId?: string;
  dateOfOTIncome?: string;
  oneTimeIncAmount?: number;
  oneTimeIncDescription?: string;
}
