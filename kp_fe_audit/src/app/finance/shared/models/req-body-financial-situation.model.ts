import { NotLoged } from '../../../shared/models/not-loged.model';
import { FinancialSituationBalance } from './financial-situation-balance.model';
import { RegularExpense } from './regular-expense.model';
import { RegularIncome } from './regular-income.model';

export interface ReqBodyFinancialSituation extends BodyFinancialSituation, NotLoged {}

export interface BodyFinancialSituation {
    financialSituation?: FinancialSituationBalance;
    regularExpense?: RegularExpense;
    regularIncome?: RegularIncome;
}
