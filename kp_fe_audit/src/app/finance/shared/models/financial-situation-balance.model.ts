export interface FinancialSituationBalance {
    financialBalance?: number;
    clientId?: string;
}
