export interface FinancialSituations {
    financialSituation?: FinancialSituation;
}

export interface FinancialSituation {
  clientId?: string;
  financialBalance?: string;
  dateCreated?: string;
  lastUpdated?: string;
  financialSituationId?: number;
}
