export interface Attachment {
  attachmentType?: string;
  fpAttachmentId?: string;
  name?: string;
}
