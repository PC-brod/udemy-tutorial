function isMinus(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode !== 46 && charCode > 31
        && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

function digitsAndComma(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    return charCode === 44 || (charCode >= 48 && charCode <= 57);
}

function openModal() {
    $('#globalModal').modal('toggle');
    $('#nextButton, #nextButton1, #nextButton2, #nextButton3').click(function() {
        $('.carousel').carousel('next');
    });
    $('#nextButton4, .closeModal').click(function() {
        $('#globalModal').modal('toggle');
    });
}

function setDatepicker(id) {
  $('#' + id).datepicker('update','');
}

function showPassword(id) {
  if (document.getElementById(id).attributes["type"].value === "text") {
    document.getElementById(id).attributes["type"].value = "password";
  }
  else {
    document.getElementById(id).attributes["type"].value = "text";
  }
}

function scrollToValidationE() {
  setTimeout(() => {

    let item = document.getElementsByClassName("alert-danger").item(0);

    if (item) {
      const yCoordinate = item.getBoundingClientRect().top + window.pageYOffset;
      const yOffset = -100;

      window.scrollTo({
        top: yCoordinate + yOffset,
        behavior: 'smooth'
      });
    }

  }, 500);

}

function removeGlobalModal() {
	if ($('#globalModal')) {
		$('#globalModal').remove();
	}
}

function setActivePage(id, pages) {
  if (document.getElementById("page" + id)) {
    for (let num = 1; num <= pages.length; num++) {
      document.getElementById("page" + num).classList.remove('activeMessagePage');
    }
    document.getElementById("page" + id).classList.add('activeMessagePage');
  }

}

function isSafari() {
  var ua = window.navigator.userAgent;
  var iOS = !!ua.match(/iPad/i) || !!ua.match(/iPhone/i);
  var webkit = !!ua.match(/WebKit/i);
  return iOSSafari = iOS && webkit && !ua.match(/CriOS/i);
}

function openQuestionMarkModal(modal) {
  $('#'+modal).modal('toggle');
}

function openGdprModal() {
    console.log('openGdprModal');
    $('#gdprConsents').modal('toggle');
    console.log('end openGdprModal');
}

function closeGdprModal() {
    console.log('closeGdprModal');
    $('#gdprConsents').modal('toggle');
    console.log('end closeGdprModal');
}

function closeNewContractModal() {
    console.log('closeNewContractModal');
    $('#newContract').modal('toggle');
    console.log('end closeNewContractModal');
}

function toggleLogoutModal() {
    $('#modalLogout').modal('toggle');
}

function maxDate(datumDo, edit) {
  if (datumDo === '') {
    if (edit) {
      $('#form-validFromEdit').datepicker('setEndDate', Infinity);
    }
    else {
      $('#form-validFrom').datepicker('setEndDate', Infinity);
    }
  }
  else {
    if (edit) {
        $('#form-validFromEdit').datepicker('setEndDate', datumDo);
    }
    else {
        $('#form-validFrom').datepicker('setEndDate', datumDo);
    }
  }
}

function setDatepickerValidUntill(element,startDate) {
  $(element).datepicker('setStartDate', startDate);
}

function minDate(datumOd, edit) {
  if (datumOd === '') {
    if (edit) {
      $('#form-validToEdit').datepicker('setStartDate', -Infinity);
    }
    else {
      $('#form-validTo').datepicker('setStartDate', -Infinity);
    }
  }
  else {
    if (datumOd !== "") {
      if (edit) {
        $('#form-validToEdit').datepicker('setStartDate', datumOd);
      }
      else {
        $('#form-validTo').datepicker('setStartDate', datumOd);
      }
    }
  }
}

function setMaxToday(datepicker) {
  $(datepicker).datepicker('setEndDate', new Date());
}

function setMinToday(datepicker) {
  $(datepicker).datepicker('setStartDate', new Date());
}

function setEighteen(datepicker) {
    $(datepicker).datepicker('setEndDate', '-18y');
}

function resetNewContractDatepickers() {
    $('#form-validFrom').val("").datepicker("update");
    $('#form-validTo').val("").datepicker("update");
}

function setToday(datepicker) {
    $(datepicker).datepicker('setStartDate', new Date());

}

function updatePlan() {
    alert("Pokud chcete aktualizovat finanční plán, kontaktujte svého konzultanta.");
}

function setDatepickers() {
    $('#datepickerOfBirth').datepicker('setEndDate', new Date());
    $('.datepickersNotify').datepicker('setStartDate', new Date());
    $('#datepickerSign').datepicker('setEndDate', new Date());
}

function loadDZ(name, url) {
    return $("div#" + name).dropzone({ url: url });
}

function loadDZedit() {
    return $("div#myDropzoneEdit").dropzone({ url: "/file/post" });
}

function toggleDZMaxFilesize() {
    $('#modalDZMaxFilesize').modal('toggle');
}

function toggleDeleteAttachment() {
    $('#modalDeleteAttachment').modal('toggle');
}

function toggleDeleteContract() {
  $('#modalDeleteContract').modal('toggle');
}

function amlCarouselN() {
  $('#amlCarousel').carousel('next');
  $('.carousel').carousel('pause');
}
function amlCarouselP() {
  $('#myCarousel').carousel('prev');
  $('.carousel').carousel('pause');
}

function setActiveNav(id) {

  document.getElementById("binder").classList.remove('active');
  document.getElementById("selfservice").classList.remove('active');
  document.getElementById("messages").classList.remove('active');
  document.getElementById("finance").classList.remove('active');
  document.getElementById("dashboard").classList.remove('active');
  if (id === 'delete') {
    return;
  }
  else {
    document.getElementById(id).classList.add("active");
  }
}

function toggleDeleteNotification(id) {
  $('#' + id).modal('toggle');
}

function toggleDeletePayment(id) {
  $('#'+ id).modal('toggle');
}

// Global alerts with text message. Message is wrapped to <span> tag.
function toggleGlobalAlert(alertClass, message, autohide) {
	var content = "<span>" + message + "</span>";
	toggleGlobalAlertCustomContent(alertClass, "global-alert", content, autohide);
}

// Global alerts with custom content. Content is pasted "as is" into alert div.
function toggleGlobalAlertCustomContent(alertClass, globalAlertClass, content, autohide) {
	$('.main-style').remove();
    var d = new Date();
	var alertId = alertClass + d.getMilliseconds() + Math.floor(Math.random() * 1000);
    var alert = "<div id=\"" + alertId + "\" class=\"" + globalAlertClass + " alert main-style " + alertClass + "\"><button type=\"button\" class=\"close close-alert\" aria-hidden=\"true\" onclick=\"$(\'#" + alertId + "\').remove()\">&times;</button>" + content + "</div>";

    $('body').prepend(alert);

    $newAlert = $("#" + alertId);
    $newAlert.show();
    if (autohide) {
        setTimeout(function() {
            $newAlert.fadeOut();
        }, 3000);

        setTimeout(function() {
            $newAlert.remove();
        }, 4000);
    }
}

function openTechnicalReportModal(report, isAmlAlertNeeded) {
  console.log(report);
  $('#technicalReportTitle').text(report.title);
  $('#technicalReportMessage').text(report.message);
  $('#technicalReportModal').modal('toggle');
  if (isAmlAlertNeeded) {
    $('#technicalReportModal').on("hide.bs.modal", technicalReportTrigger);
  }

}

function technicalReportTrigger() {
  openQuestionMarkModal('amlModal');
  $('#technicalReportModal').off('hide.bs.modal', technicalReportTrigger);
}
