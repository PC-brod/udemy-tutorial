import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.LinkedList;

public class UrlLibrary implements Iterable<String> {
    private LinkedList<String> urlList = new LinkedList<String>();

    private class UtlIterator implements Iterator<String> {

        private int index = 0;

        @Override
        public boolean hasNext() {
            return index < urlList.size();
        }

        @Override
        public String next() {

            StringBuilder sb = new StringBuilder();

            try {
                URL url = new URL(urlList.get(index));

                BufferedReader br = new BufferedReader(
                        new InputStreamReader(url.openStream())
                );

                String line = null;

                while ( (line = br.readLine()) != null) {
                    sb.append(line);
                    sb.append("\n");
                }

                br.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            index++;

            return sb.toString();
        }

        @Override
        public void remove() {
            urlList.remove(index);

        }
    }

    public UrlLibrary() {
        urlList.add("https://www.seznam.cz");
        urlList.add("https://www.google.cz");
        urlList.add("https://www.dvtv.cz");
    }

    @Override
    public Iterator<String> iterator() {
        return new UtlIterator();
    }



    /*
    @Override
    public Iterator<String> iterator() {
        return urlList.iterator();
    }

     */
}
