import java.io.*;

public class App {
    public static void main(String[] args) {
        File file = new File("test.text");
        /*
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
         */
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
            bw.write("New content");
            bw.newLine();
            bw.write("More data");
            bw.newLine();
            bw.write("Last line");
        } catch (IOException e) {
            System.out.println("Unable to write to file: " + file.toString());
            e.printStackTrace();
        }
    }
}
