public class Car extends Machine {
    @Override
    public void start() {
        System.out.println("Car started!");
    }

    @Override
    public void reset() {
        System.out.println("Car reset!");
    }

    @Override
    public void shutdown() {
        System.out.println("Car shouting down!");
    }
}
