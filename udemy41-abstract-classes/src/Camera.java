public class Camera extends Machine {
    @Override
    public void start() {
        System.out.println("Camera is starting...");
    }

    @Override
    public void reset() {
        System.out.println("Camera is resetting...");
    }

    @Override
    public void shutdown() {
        System.out.println("Camera is turning down...");
    }
}
