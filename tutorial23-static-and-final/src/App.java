class Thing {
	public final static int LUCKY_NUMBER = 7; // final means that you can`t reassign its value
												// value must be set when created

	public String name;
	public static String description; // static variable
	public static int count = 0;
	public int id;

	public Thing() { // when class Thing is called, this constructor Thing is called as well
		id = count;
		count++;
		System.out.println("Count = " + count);
	}

	public void showName() { // instance method can access static data
		System.out.println("instance method");
		System.out.println("Obj. id: " + id);
		System.out.println(name);
		System.out.println(description);
	}

	public static void showInfo() { // static method
		System.out.println("Hi");
		System.out.println(description); // can access data
		// System.out.println(name); // can`t output instance variables like name.
		// Cannot make a static reference to the non-static field name
	}
}

public class App {

	public static void main(String[] args) {

		Thing.description = "I`m something special, doing strange things.";
		System.out.println(Thing.description);

		Thing.showInfo();

		System.out.println("Before creating objects, count is: " + Thing.count);

		Thing thing1 = new Thing();
		Thing thing2 = new Thing();

		System.out.println("After creating objects, count is: " + Thing.count);

		thing1.name = "Bob";
		thing2.name = "Lue";

		System.out.println(thing1.name);
		System.out.println(thing2.name);

		thing1.showName();
		thing2.showName();

		System.out.println(Math.PI);

		// Math.PI = 3; // error

		System.out.println(Thing.LUCKY_NUMBER);
	}

}
