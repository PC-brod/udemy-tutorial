class Frog {
	/*
	public String toString() {
		return "Hello";
	}
	// if there is no method, system will return name and hash code: Frog@762efe5d
	// can be useful for debugging
	*/
	
	private int id;
	private String name;
	
	public Frog(int id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb
		.append(id)
		.append(" : ")
		.append(name);
		
		return sb.toString();
	}
}

public class App {

	public static void main(String[] args) {
		/*
		Object smallObj = new Object();
		smallObj.
*/
		/*
		Frog frog1 = new Frog();
		System.out.println(frog1);
		*/
		Frog frog2 = new Frog(5, "Bill");
		Frog frog3 = new Frog(3, "Paul");
		System.out.println(frog2);
		System.out.println(frog3);
	}

}
