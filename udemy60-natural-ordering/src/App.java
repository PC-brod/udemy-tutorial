import java.util.*;

public class App {

    public static void main(String[] args) {
        List<String> list = new ArrayList<String>();
        SortedSet<String> set = new TreeSet<String>();

        List<Person> listPerson = new ArrayList<Person>();
        SortedSet<Person> setPerson = new TreeSet<Person>();

        addElements(list);
        addElements(set);

        showElements(list);
        System.out.println("============");
        showElements(set);
        System.out.println("============");
        Collections.sort(list);
        showElements(list);

        System.out.println("+++ ppppppppppppppppppppppp +++");

        addPerson(listPerson);
        addPerson(setPerson);

        showPerson(listPerson);
        System.out.println("*************");
        showPerson(setPerson);
        System.out.println("*************");
        Collections.sort(listPerson);
        showPerson(listPerson);
    }

    private static void addElements(Collection<String> col){
        col.add("Joe");
        col.add("Jim");
        col.add("Jack");
        col.add("Jill");
        col.add("Jacob");
        col.add("Joseph");
    }

    private static void showElements(Collection<String> col) {
        for (String element: col){
            System.out.println(element);
        }
    }

    private static void addPerson(Collection<Person> col){
        col.add(new Person("Joseph"));
        col.add(new Person("Joe"));
        col.add(new Person("Jim"));
        col.add(new Person("Jacob"));
        col.add(new Person("Jack"));
        col.add(new Person("Jill"));
    }

    private static void showPerson(Collection<Person> col) {
        for (Person person: col){
            System.out.println(person);
        }
    }
}
