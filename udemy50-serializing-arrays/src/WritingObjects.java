import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;

public class WritingObjects {
    public static void main(String[] args) {
        System.out.println("Writing objects ... ");

        Person[] people = {
                new Person(1, "Don"),
                new Person(2, "Ron"),
                new Person(3, "Tom")
        };

        ArrayList<Person> peopleList = new ArrayList<Person>(Arrays.asList(people));

        // will automatically call close method
        try (FileOutputStream fs = new FileOutputStream("people.bin")) {

            ObjectOutputStream os = new ObjectOutputStream(fs);

            os.writeObject(people);

            os.writeObject(peopleList);
            System.out.println(peopleList);

            os.writeInt(peopleList.size());

            for (Person person : peopleList) {
                os.writeObject(person);
            }

            os.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
