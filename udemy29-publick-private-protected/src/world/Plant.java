package world;

public class Plant {
    // Bad practice
    public String name;

    // Ok practice -- it is final
    public final static int ID = 5; // visible from anywhere

    private String type; // class visibility

    protected String size; // class, subclass and same package visibility

    int height; // no visibility specification -> package visibility

    public Plant() {
        name = "Big plant";
        type = "plant";
        size = "big";
        height = 100;
    }
}
