package world;

public class Fiala extends Plant {
    public Fiala() {
        // Not working type is private
        // type = "Kytka";

        // works - size is protected, Fiala is subclass
        size = "small";
        // works, no access specifier; Oak and Plant in same package
        height = 10;
    }
}
