package packageName;

import world.Plant;

public class App {
    public static void main(String[] args) {
        Plant plant = new Plant();

        System.out.println(plant.name);
        System.out.println(plant.ID);

        plant.name = "Kren";
        System.out.println(plant.name);
        /*
        plant.ID = 10; //Cannot assign a value to final variable 'ID'
        */
/*
        System.out.println(plant.type); // not working type is private
 */
        // size is protected, App is not in the same package as Plant
        // System.out.println(plant.size);

        // not working, App and Plant in different packages, height has package-level visibility
        // System.out.println(plant.height);
    }
}
