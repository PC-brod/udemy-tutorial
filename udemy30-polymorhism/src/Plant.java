public class Plant {
    public void grow() {
        System.out.println("Plant is growing.");
        Tree tree = new Tree();
        tree.shedLeaves();
    }
}
