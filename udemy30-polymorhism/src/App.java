public class App {
    public static void main(String[] args) {
        Plant plant1 = new Plant();
        Tree tree = new Tree();

        Plant plant2 = tree;
        plant2.grow();
        // plant2.shedLeaves(); not gonna work
        tree.shedLeaves();

        plant1.grow();

        doGrow(plant1);
        doGrow(plant2);
        doGrow(tree);
    }

    public static void doGrow(Plant plant) {
        System.out.println("doGrow");
        plant.grow();
    }
}
