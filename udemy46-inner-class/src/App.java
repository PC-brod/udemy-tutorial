public class App {
    public static void main(String[] args) {
        Robot robot = new Robot(7);
        robot.start();
/*
        // class Brain must be public, than we can use this:
        Robot.Brain braineee = robot.new Brain();
        braineee.think();
 */
        Robot.Battery battery = new Robot.Battery();
        battery.charge();
    }
}
