public class Robot {
    private int id;

    // nested class
    private class Brain { // it can be also public
        public void think() {
            System.out.println("Robot " + id + " is thinking.");
        }
    }

    public static class Battery {
        public void charge() {
            System.out.println("Battery charging...");
        }
    }

    public Robot(int id) {
        this.id = id;
    }

    public void start() {
        System.out.println("Robot " + id + " is starting...");

        Brain brain = new Brain();
        brain.think();

        final String name = "Komp";

        class Temp {
            public  void doProcess() {
                System.out.println("R. num.: " + id + " is processing.");
                System.out.println("My name is " + name);
            }
        }

        Temp temp = new Temp();
        temp.doProcess();
    }
}
