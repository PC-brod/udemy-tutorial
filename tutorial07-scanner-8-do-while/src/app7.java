import java.util.Scanner;
//Ctr+Shift+o -eclipse will add all necessary imports. Or click on error icon on the left.
public class app7 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);			// Creating new scanner object and
														// passing in to it standard predefined system.in
														// input stream object
		System.out.println("Enter a line of text: ");	// Output the prompt
		
		String line = input.nextLine();					// Wait for the user to enter a line of text
		
		System.out.println("You entered: " + line);		// Tell them what they entered
		
		Scanner numIn = new Scanner(System.in);
		System.out.println("Give me a number: ");
		int userNumber = numIn.nextInt();
		System.out.println("Your number is: " + userNumber);
		
		Scanner floatIn = new Scanner(System.in);
		System.out.println("Give me a float number: ");
		double userFloat = floatIn.nextDouble();
		System.out.println("Your float number is: " + userFloat);
		
		// ------------------------------------------
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter a number:");
		int value = scanner.nextInt();
		
		while(value != 5) {
			System.out.println("Enter a number:");
			value = scanner.nextInt();
		}
		
		System.out.println("Got 5!");
		
		int valueB = 0;
		int expectedNum = 8;
		int tryCounter = 0;
		
		do {
			System.out.println("Enter a number:");
			/* int valueB = scanner.nextInt();		 not gonna work - variable scope, only inside do{} block */
			valueB = scanner.nextInt();
			tryCounter ++;
			
			if(tryCounter == 3) {
				System.out.println("I`m waiting for number " + expectedNum);
			}
		} while(valueB != expectedNum);
		
		System.out.println("Got " + expectedNum + "!");
	}
}
