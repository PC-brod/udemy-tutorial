class Machine {
    public void start() {
        System.out.println("Machine started");
    }
}

class Camera extends Machine {
    public void start() {
        System.out.println("Camera started");
    }
    public void cameraStart() {
        System.out.println("Camera started - cameraStart");
    }
    public void snap() {
        System.out.println("Photo taken");
    }
}

public class App {
    public static void main(String[] args) {
        Machine machine1 = new Machine();
        Camera camera1 = new Camera();

        machine1.start();
        camera1.start();
        camera1.snap();

        // Upcasting
        Machine machine2 = camera1; // this is upcasting because we moved up from Camera to Machine
        machine2.start();           // "Camera started" because Camera and Machine have method with same name
        /*
        machine2.cameraStarted();   // not gonna work
        machine2.snap();            // not gonna work
         */

        // Down casting
        Machine machine3 = new Camera();
        machine3.start();
        /*
        machine3.snap(); // not gonna work
         */
        Camera camera2 = (Camera) machine3; // here JAVA need confirmation by (Camera)
        camera2.start();
        camera2.snap();
        camera2.cameraStart();

        // Not gonna work - runtime error
        Machine machine4 = new Machine();
        // Camera camera3 = (Camera) machine4; // looks fine, but...
        // camera3.start();
        // camera3.cameraStart();
        // camera3.snap();
    }
}
