import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

public class App {
    public static void main(String[] args) {

        // HashSet does not retain order.
        Set<String> stringSet = new HashSet<String>();

        iterateSet(stringSet);

        stringSet.add("asd");
        stringSet.add("ikjhk");
        stringSet.add("p[ok");
        stringSet.add("qewa");
        stringSet.add("';/.");

        // Adding duplicate items makes no effect
        stringSet.add("asd");
        stringSet.add("asd");

        System.out.println(stringSet);

        System.out.println("***********************************");

        // LinkedHashSet remembers the order you added items in
        Set<String> stringSet1 = new LinkedHashSet<String>();
        stringSet1.add("asd");
        stringSet1.add("ikjhk");
        stringSet1.add("p[ok");
        stringSet1.add("qewa");
        stringSet1.add("';/.");

        // Adding duplicate items makes no effect
        stringSet1.add("asd");
        stringSet1.add("asd");

        System.out.println(stringSet1);

        System.out.println("***********************************");

        // TreeSet sorts in natural order
        Set<String> stringSet2 = new TreeSet<String>();
        stringSet2.add("asd");
        stringSet2.add("ikjhk");
        stringSet2.add("p[ok");
        stringSet2.add("qewa");
        stringSet2.add("';/.");

        // Adding duplicate items makes no effect
        stringSet2.add("asd");
        stringSet2.add("asd");

        System.out.println(stringSet2);

        ///////////////// Iteration //////////

        iterateSet(stringSet);
        iterateSet(stringSet1);
        iterateSet(stringSet2);

        /// Does set contains something specific? \\\
        if (stringSet.contains("qewa")) {
            System.out.println("Contains 'qewa'");
        }

        /// Intersection identical items in two sets \\\
        Set<String> stringSet3 = new TreeSet<String>();
        stringSet3.add("asd");
        stringSet3.add("ikjhk");
        stringSet3.add("vbm");
        stringSet3.add("qewa");
        stringSet3.add("rytmvjdsufgjhkbur");

        System.out.println(stringSet3);

        System.out.println("======================================");

        Set<String> intersection = new HashSet<String>(stringSet2);
        System.out.println(stringSet2);
        System.out.println(intersection);

        System.out.println("======================================");

        intersection.retainAll(stringSet3);
        System.out.println("Intersection of them is: " + intersection);

        System.out.println("======================================");

        /// Difference \\\
        Set<String> difference = new HashSet<String>(stringSet2);
        difference.removeAll(stringSet3);
        System.out.println("Difference of them is: " + difference);

        System.out.println("======================================");
    }

    public static void iterateSet(Set<String> set) {
        System.out.println("#############################");

        if (set.isEmpty()) {
            System.out.println("Empty set.");
        } else {
            for (String element : set) {
                System.out.println(element);
            }
        }
    }
}
