public class App {
    // There is two basic types of exceptions

    public static void main(String[] args) {
        // Checked exceptions - developer must handle them
        try {
            Thread.sleep(150);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
/*
        // Runtime (unchecked) exception that we are not forced to handle
        int value = 7;
        value = value/0; // returns: Exception in thread "main" java.lang.ArithmeticException: / by zero
        System.out.println(value);

 */
/*
        // No pointer (unchecked) exception
        String text = null;
        System.out.println(text.length()); // returns: Exception in thread "main" java.lang.NullPointerException

 */

        String[] texts = {"one", "two", "three"};
        try {
            System.out.println(texts[3]); // returns: Exception in thread "main" java.lang.ArrayIndexOutOfBoundsException: Index 3 out of bounds for length 3
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage()+" "+e.toString());
        }
    }
}
