class Frog {
	String name; // private String name; - private will force encapsulation
	int age; // private int age;

	public void setName(String newName) {
		name = newName; // setting instance variable in method
	}

	public void setAge(int age) {
		this.age = age;
	}

	public void setInfo(String name, int age) {
		setName(name); // it is possible to type: this.setName(name);
		setAge(age);
	}

	public String getName() {
		return name;
	}

	public int getAge() {
		return age;
	}

	/*
	 * public void setName(String name) { // this.name - instance name from top, is
	 * equal to name from method parameter in () // this. refers to an object that
	 * you are currently in this.name = name; }
	 */
}

public class App {

	public static void main(String[] args) {

		Frog frog1 = new Frog();

		frog1.name = "Paul"; // setting instance variable with equals sign
		frog1.age = 10; // if name and age would be private as in comment above, it would be impossible
						// to access them directly here
		System.out.println(frog1.getName());
		System.out.println(frog1.getAge());

		frog1.setName("Peter"); // calling a method
		frog1.setAge(2);
		System.out.println(frog1.getName());
		System.out.println(frog1.getAge());

		Frog frog2 = new Frog();

		frog2.setInfo("Ron", 6);
		System.out.println(frog2.getName());
		System.out.println(frog2.getAge());
	}

}
