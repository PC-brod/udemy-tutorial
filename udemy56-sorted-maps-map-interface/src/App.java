import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public class App {
    public static void main(String[] args) {
        HashMap<Integer, String> hashMap = new HashMap<Integer, String>(); // usually orders them by key, but no guarantee
        LinkedHashMap<Integer, String> linkedHashMap = new LinkedHashMap<Integer, String>(); // same order like data was putted
        TreeMap<Integer, String> treeMap = new TreeMap<Integer, String>(); // natural order

        testMap(hashMap);
        testMap(linkedHashMap);
        testMap(treeMap);
    }

    public static void testMap(Map<Integer, String> map) {
        map.put(10, "ten");
        map.put(-1, "ii");
        map.put(13, "zebra");
        map.put(0, "the Shadow");
        map.put(2, "tuuut");

        System.out.println();

        for (Integer key: map.keySet()){
            System.out.println("Key: " + key + "\nValue: " + map.get(key) + "\n");
        }

        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");
    }
}
