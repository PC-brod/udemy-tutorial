package test;

public class Machine implements Info {

    private int id = 7;

    public void start() {
        System.out.println("System started.");
    }

    public void showInfo() {
        System.out.println("test.Machine id is: " + id);
    }
}
