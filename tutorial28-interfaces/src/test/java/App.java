import test.Info;
import test.Machine;
import test.Person;

public class App {

    public static void main(String[] args) {

        Machine mach1 = new Machine();
        mach1.start();

        Person person1 = new Person("Luke");
        person1.greet();

        Info info1 = new Machine();
        info1.showInfo();

        Info info2 = person1;
        info2.showInfo();

        System.out.println();

        doInfo(mach1);
        doInfo(person1);

    }

    private static void doInfo(Info info) {
        info.showInfo();

    }
}
