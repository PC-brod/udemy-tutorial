public class App {
    public static void main(String[] args) {
        App app = new App();

        int value = 7;
        System.out.println("1. Value: " + value);
        app.show(value);
        System.out.println("4. Value: " + value);

        System.out.println("==========================================");

        Person person = new Person("Bob");
        System.out.println("1. Person: " + person);
        app.show(person);
        System.out.println("4. Person: " + person);
    }

    public void show(int value) {
        System.out.println("2. Value: " + value);
        value = value + 5;
        System.out.println("3. Value: " + value);
    }

    public void show(Person person) {
        System.out.println("2. Person: " + person);
        person.setName("Ronald");
        person = new Person("Mike");
        System.out.println("3. Person: " + person);

    }
}
