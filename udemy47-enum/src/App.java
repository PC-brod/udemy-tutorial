public class App {
/*
    public static final int DOG = 0;
    public static final int CAT = 1;
    public static final int MOUSE = 2;

 */

    public static void main(String[] args) {
        Animal animal = Animal.CAT;

        switch (animal) {
            case DOG -> {
                System.out.println("Dog");
                break;
            }
            case CAT -> {
                System.out.println("Cat");
                break;
            }
            case MOUSE -> {
                System.out.println("Mouse");
                break;
            }
            default -> {break;}
        }

        System.out.println(Animal.MOUSE);
        System.out.println(Animal.MOUSE.getClass());
        System.out.println(Animal.MOUSE.getDeclaringClass());
        System.out.println(Animal.MOUSE instanceof Enum);
        System.out.println(Animal.MOUSE.getName());
        System.out.println("Enum name as a string: " + Animal.MOUSE.name());
    }
}
