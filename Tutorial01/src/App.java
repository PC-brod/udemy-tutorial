
public class App {

	public static void main(String[] args) {
		System.out.println("Hello World!"); // sysout	Ctr+Space
		int myNumber; // declare integer variable
		
		myNumber = 44; // initialize variable
		
		System.out.println(myNumber);
		
		int otherNumber = 55; // both in same time
		
		System.out.println(otherNumber);
		
		short myShort = 123; // 16 bit value, up to 32000
		long myLong = 594325; // 64 bit value
		double myDouble = 7.92255;
		float myFloat = 6985.6f;
		
		char myChar = 'y';
		boolean myBoolean = false;
		
		byte myByte = 127; // up to 127
		
		System.out.println(myShort);
		System.out.println(myLong);
		System.out.println(myDouble);
		System.out.println(myFloat);
		System.out.println(myChar);
		System.out.println(myBoolean);
		System.out.println(myByte);
	}

}
