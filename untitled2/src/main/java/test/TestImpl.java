package test;

public class TestImpl implements TestInterface {
    private String name;

    public TestImpl(String name) {
        this.name = name;
    }

    public void greet() {
        System.out.println("Hello.");
    }

    public void showInfo() {
        System.out.println("test.Person name is: " + name);
    }
}
