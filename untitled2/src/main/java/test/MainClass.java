package test;

public class MainClass {
    public static void main(String[] args) {
        TestImpl testimpl1 = new TestImpl("Luke");
        testimpl1.greet();

        testimpl1.showInfo();

        System.out.println();

        doInfo(testimpl1);
    }

    private static void doInfo(TestInterface info) {
        info.showInfo();

    }
}
