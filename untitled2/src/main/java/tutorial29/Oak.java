package tutorial29;

public class Oak extends Plant {
    public Oak() {
        /*
        type = "tree"; // won`t work type is private
        */

        // This works
        size = "Large";

        // No access specified
        height = 10;
    }
}
