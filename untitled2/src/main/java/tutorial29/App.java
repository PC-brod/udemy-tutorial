package tutorial29;

// tutorial29-public-private-protected

/*
* private       - only within same package
* public        - from anywhere
* protected     - same class, subclass, and same package
* no modifier   - same package only
 */
public class App {
    private Field field1;

    public static void main(String[] args) {
        Plant plant1 = new Plant();

        System.out.println(plant1.name);
        System.out.println(plant1.myConstant);
        /*
        System.out.println(plant1.type); // won`t work type is private
         */

        Oak oak1 = new Oak();
        System.out.println(oak1.size);

        Field field1 = new Field();
        System.out.println(field1);

        System.out.println(plant1.size);

        Plant plant2 = new Plant();
        System.out.println(plant2.height);
    }
}
