package tutorial29;

public class Plant {
    // Bad practice
    public String name; // public can be accessible anywhere
    // Acceptable practice - final
    public final static int myConstant = 10;

    private String type; // private can be accessible only within this class

    protected String size; // protected can be accessible within it`s class,
                            // but also in other classes, but in same package
    int height; // no specification of access

    public Plant() {
        name = "Rose";
        type = "Flower";
        size = "Medium";
        height = 50;
    }
}
