/*
constructor

is a special method which is run every time you create an instance of your class.
*/

class Machine {
	private String name;
	private int code;

	// constructor must have a same name as its class
	public Machine() {
		// name = "Bob";
		this("abc", 0); // setting default values in this Machine, it must be on first line.
						// Of course system will automatically recognize, that there is string and int
						// and
						// proper constructor Machine with parameters string and int will be called
		System.out.println("Constructor 1");
		System.out.println("Constructor 1 - test of variables values: " + name + ", " + code);
	}

	public Machine(String name) {
		// this.name = name;
		this(name, 0);
		System.out.println("Constructor 2 with string: " + this.name);
	}

	public Machine(String name, int code) {
		this.name = name;
		this.code = code;
		System.out.println("Constructor 3 with string: " + this.name + " and code: " + this.code);
	}
}

public class App {
	public static void main(String[] args) {
		Machine machine1 = new Machine();
		new Machine(); // another way how to call the constructor Machine

		Machine machine2 = new Machine("John"); // system will automatically find constructor that accepts string
												// variable as a parameter

		new Machine("Bill", 8);
	}
}
