import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class App {

    public static void main(String[] args) {

        StringCompare stringCompare = new StringCompare();

        AlphabeticalComparator alphabeticalComparator = new AlphabeticalComparator();

        AlphabeticalComparatorR acr = new AlphabeticalComparatorR();


        // Natural order examples:
        List<String> animals = new ArrayList<String>();

        animals.add("dog");
        animals.add("tiger");
        animals.add("snake");
        animals.add("donkey");
        animals.add("cat");
        animals.add("monkey");

        Collections.sort(animals); // will give them alphabetical order

        for (String animal: animals) {
            System.out.println(animal);
        }

        List<Integer> numbers = new ArrayList<Integer>();
        numbers.add(5);
        numbers.add(2);
        numbers.add(1);
        numbers.add(84);
        numbers.add(46);
        numbers.add(7);

        Collections.sort(numbers);

        for (Integer number: numbers){ // wil give them numerical order
            System.out.println(number);

        }

        Collections.sort(animals, stringCompare); // sort by their length

        for (String animal: animals) {
            System.out.println(animal);
        }

        System.out.println("//////////////////////////////////\nAlphabeticalComparator");

        Collections.sort(animals, alphabeticalComparator);

        for (String animal: animals) {
            System.out.println(animal);
        }

        System.out.println("RRRRRRRRRRRRRRRRRRRRRRRRR\nAlphabeticalComparatorR");

       Collections.sort(animals, acr);

        for (String animal: animals) {
            System.out.println(animal);
        }

        System.out.println("IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII");

        Collections.sort(numbers, new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return -o1.compareTo(o2);
            }
        });

        for (Integer num: numbers) {
            System.out.println(num);
        }

        ///// Sorting arbitrary objects /////

        List<Person> people = new ArrayList<Person>();

        people.add(new Person(4, "Bob"));
        people.add(new Person(3, "Barbra"));
        people.add(new Person(1, "Jana"));
        people.add(new Person(2, "John"));

        for (Person person: people){
            System.out.println(person);
        }

        System.out.println("sssssssssssssssssssssssssss");
        System.out.println(people);

        Collections.sort(people, new Comparator<Person>() {
            @Override
            public int compare(Person p1, Person p2) {

                if (p1.getId() > p2.getId()) {
                    System.out.println(p1.getName() + " vs " + p2.getName());
                    System.out.println("1");
                    return 1;
                }
                else if (p1.getId() < p2.getId()){
                    System.out.println(p1.getName() + " vs " + p2.getName());
                    System.out.println("-1");
                    return -1;
                }
                return 0;
            }
        });

        System.out.println(people);

        for (Person person: people){
            System.out.println(person);
        }

        System.out.println("nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn");
        Collections.sort(people, new Comparator<Person>() {
            @Override
            public int compare(Person p1, Person p2) {
                return p1.getName().compareTo(p2.getName());
            }
        });

        System.out.println(people);

        for (Person person: people){
            System.out.println(person);
        }
    }
}
